#!/bin/bash
InstallScriptsPath="`pwd`/../InstallScripts/"
source ../InstallScripts/InstallFunctions.sh

if [ $1 == "-h" ]; then
{
	echo -e "\
	-h --help: Print this.
	--cuda: Setup Your PC with CUDA 10.1.
	"
	exit
}
elif [ $1 == "--help" ]; then
{
	echo -e "\
	-h --help: Print this.
	--cuda: Setup Your PC with CUDA 10.1.
	"
	exit
}
elif [ $1 == "--cuda" ]; then
{
	echo "Setup Your PC with CUDA 10.1."
	read -p "Enter to continue."
}
elif [ -z $1 ]; then
{
	echo "Setup Your PC WITHOUT CUDA 10.1."
	echo "You can use \"--cuda\" option to setup Your PC with CUDA 10.1."
	read -p "Enter to continue setup your PC."
}
else
{
	echo "Wrong parameters. Input \"-l\" or \"-r\" to specify PCs you want to setup"
	exit
}
fi
read -p "Really?"

remove_apt_apt_lock

download_deb_from_filenic

setup_convenient_utilities
install_timeshift
timeshift_backup "New System"
setup_dev_env_packages
install_input_method

install_python37_pip_ubuntu1804

install_librealsense2
install_PCL_base_packages
install_OpenCV_base_packages

if [ $1 == "--cuda" ]; then
{
	install_PCL_CUDA
	install_OpenCV_CUDA

}
else
{
	install_PCL_noCUDA
	install_OpenCV_noCUDA
	install_user_noCUDA_python_packages
}
fi

install_openrtm_all

install_visual_code
install_pycharm

# install_setup_vncserver

timeshift_backup "Env setup success"

setup_development_env

timeshift_backup "All setup success"

