To use this updater you need to add this two sentences to the end of CMakeLists.txt in rtc root path.

# Add by Dong, for updating cpp and h file automatically.
add_custom_target(updateidl ALL COMMAND "UpdateFromIDL" ".." DEPENDS ALL_IDL_TGT)
add_custom_target(updateidl_virtual COMMAND "UpdateFromIDL" ".." "-t" DEPENDS ALL_IDL_TGT)
