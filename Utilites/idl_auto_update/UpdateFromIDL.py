#!/usr/bin/python3

import re
import os
import sys


def find_idx_continue_condition(source_inList, condition_list):
	__index = 0
	for condition in condition_list:
		for __content in source_inList[__index:]:
			if __content.find(condition) != -1:
				break
			__index += 1
	if __index == source_inList.__len__():
		return False
	return __index



def make_new_cpp_classTextList(module_name, interface_name):
	cpp_new_class = []
	cpp_new_class.append("/*" + "\n")
	cpp_new_class.append(" * Example implementational code for IDL interface " + module_name + "::" + interface_name + "\n")
	cpp_new_class.append(" */" + "\n")
	# cpp_new_class.append(module_name + "_" + interface_name + "SVC_impl::" + module_name + "_" + interface_name + "SVC_impl(){};" + "\n")
	cpp_new_class.append(module_name + "_" + interface_name + "SVC_impl::" + module_name + "_" + interface_name + "SVC_impl(){};" + "\n")
	cpp_new_class.append("" + "\n")
	cpp_new_class.append(module_name + "_" + interface_name + "SVC_impl::~" + module_name + "_" + interface_name + "SVC_impl()" + "\n")
	cpp_new_class.append("{" + "\n")
	cpp_new_class.append("  // Please add extra destructor code here." + "\n")
	cpp_new_class.append("}" + "\n")
	cpp_new_class.append("" + "\n")
	cpp_new_class.append("/*" + "\n")
	cpp_new_class.append(" * Methods corresponding to IDL attributes and operations" + "\n")
	cpp_new_class.append(" */" + "\n")
	cpp_new_class.append("" + "\n")
	cpp_new_class.append("// End of example implementational code" + "\n")
	return cpp_new_class


def make_new_hpp_classTextList(module_name, interface_name):
	hpp_new_class_template = []
	hpp_new_class_template.append("/*!" + "\n")
	hpp_new_class_template.append(" * @class " + module_name + "_" + interface_name + "SVC_impl" + "\n")
	hpp_new_class_template.append(" * Example class implementing IDL interface " + module_name + "::" + interface_name + "\n")
	hpp_new_class_template.append(" */" + "\n")
	hpp_new_class_template.append("Class " + module_name + "_" + interface_name + "SVC_impl" + "\n")
	hpp_new_class_template.append(" : public virtual POA_" + module_name + "::" + interface_name + "," + "\n")
	hpp_new_class_template.append("   public virtual PortableServer::RefCountServantBase" + "\n")
	hpp_new_class_template.append("{" + "\n")
	hpp_new_class_template.append(" private:" + "\n")
	hpp_new_class_template.append("   // Make sure all instances are built on the heap by making the" + "\n")
	hpp_new_class_template.append("   // destructor non-public" + "\n")
	hpp_new_class_template.append("   //virtual ~" + module_name + "_" + interface_name + "SVC_impl();" + "\n")
	hpp_new_class_template.append("" + "\n")
	hpp_new_class_template.append(" public:" + "\n")
	hpp_new_class_template.append("  /*!" + "\n")
	hpp_new_class_template.append("   * @brief standard constructor" + "\n")
	hpp_new_class_template.append("   */" + "\n")
	hpp_new_class_template.append("   " + module_name + "_" + interface_name + "SVC_impl();" + "\n")
	hpp_new_class_template.append("  /*!" + "\n")
	hpp_new_class_template.append("   * @brief destructor" + "\n")
	hpp_new_class_template.append("   */" + "\n")
	hpp_new_class_template.append("   virtual ~" + module_name + "_" + interface_name + "SVC_impl();" + "\n")
	hpp_new_class_template.append("" + "\n")
	hpp_new_class_template.append("   // attributes and operations" + "\n")
	hpp_new_class_template.append("" + "\n")
	hpp_new_class_template.append("};" + "\n")
	return hpp_new_class_template




module_dict = {}


def check_dir(path='.'):
	for file in os.listdir(path):
		if file.find('rtc.conf') != -1:
			return True
	return False


def updateServicePortImplFile(rtc_root_path, test_run=False):
	IDL_Files = []
	IDL_File_names = []
	for file in os.listdir(rtc_root_path + '/idl'):
		if os.path.splitext(file)[-1] == '.idl':
			IDL_File_names.append(file.split('.')[0])
			IDL_Files.append(open(rtc_root_path + '/idl/' + file, 'r'))

	module_names = []
	interface_name = []
	cur_module_name = ''
	cur_interface_name = ''
	for file_handle in IDL_Files:
		content_list = file_handle.readlines()
		file_name = file_handle.name.split('/')[-1].split('.')[0]
		module_dict.update({file_name: {}})
		for content in content_list:

			module = re.findall('module ([A-z,0-9,_]*)', content)
			interface = re.findall('interface ([A-z,0-9,_]*)', content)
			function_names = re.findall('[A-z,0-9,_]* ([A-z,0-9,_]*)\([A-z,0-9,_, ]*\);', content)
			if interface.__len__() > 0:
				cur_interface_name = interface[0]
				try:
					module_dict[file_name][cur_module_name][cur_interface_name]
				except KeyError:
					module_dict[file_name][cur_module_name].update({cur_interface_name: []})
				# interface_name += interface
			if module.__len__() > 0:
				cur_module_name = module[0]
				try:
					module_dict[file_name][cur_module_name]
				except KeyError:
					module_dict[file_name].update({cur_module_name: {}})
				# module_names += module
			if function_names.__len__() > 0:
				module_dict[file_name][cur_module_name][cur_interface_name] += function_names

	cpp_name = ''
	h_name = ''
	for file_name in module_dict.keys():
		cpp_name = rtc_root_path + '/src/' + file_name + "SVC_impl.cpp"
		h_name = file_name + "SVC_impl.h"
		for (dirpath, dirnames, filenames) in os.walk(rtc_root_path + '/include'):
			for name in filenames:
				if name.find(h_name) != -1:
					h_name = dirpath + '/' + h_name

		write_lines = []
		skel_hh_handle = open(rtc_root_path + '/build/idl/' + file_name + '.hh')
		skel_hh_str_list = skel_hh_handle.readlines()

		impl_h_handle = open(h_name, 'r')
		impl_h_str_list = impl_h_handle.readlines()
		# impl_h_handle = open(h_name, 'w')
		impl_cpp_handle = open(cpp_name, 'r')
		impl_cpp_str_list = impl_cpp_handle.readlines()
		# impl_cpp_handle = open(cpp_name, 'w')

		impl_h_str_list.reverse()
		__index = 0
		hpp_file_ed_idx = -1
		for hpp_content in impl_h_str_list:
			if hpp_content.find('#endif') != -1:
				hpp_file_ed_idx = -__index - 1
			__index += 1
		impl_h_str_list.reverse()

		for module_name in module_dict[file_name].keys():
			for interface_name in module_dict[file_name][module_name].keys():
				hh_condition1 = "class _objref_" + interface_name
				hh_condition2 = "// IDL operations"
				hh_endcondition = "// Constructors"
				hpp_condition1 = "class " + module_name + "_" + interface_name + "SVC_impl"
				hpp_condition2 = "// attributes and operations"
				hpp_condition3 = "};"
				cpp_condition1 = "Example implementational code for IDL interface " + module_name + "::" + interface_name
				cpp_condition2 = "Methods corresponding to IDL attributes and operations"
				cpp_condition3 = "// End of example implementational code"

				__index = find_idx_continue_condition(skel_hh_str_list, [hh_condition1, hh_condition2])
				__num = module_dict[file_name][module_name][interface_name].__len__()
				copy_candidate = skel_hh_str_list[__index: __index + __num + 1]

				if find_idx_continue_condition(impl_h_str_list, [hpp_condition1]) == False:
					__add_list_hpp = make_new_hpp_classTextList(module_name, interface_name)
					impl_h_str_list[hpp_file_ed_idx: hpp_file_ed_idx] = __add_list_hpp
					__add_list_cpp = make_new_cpp_classTextList(module_name, interface_name)
					impl_cpp_str_list[-1:-1] = __add_list_cpp

				hpp_st_idx = find_idx_continue_condition(impl_h_str_list, [hpp_condition1, hpp_condition2])
				hpp_ed_idx = find_idx_continue_condition(impl_h_str_list, [hpp_condition1, hpp_condition2, hpp_condition3]) - 1

				cpp_st_idx = find_idx_continue_condition(impl_cpp_str_list, [cpp_condition1, cpp_condition2])
				cpp_ed_idx = find_idx_continue_condition(impl_cpp_str_list, [cpp_condition1, cpp_condition2, cpp_condition3]) - 1

				for function_name in module_dict[file_name][module_name][interface_name]:
					__index = hpp_st_idx
					hpp_content_found = False
					for hpp_content in impl_h_str_list[hpp_st_idx: hpp_ed_idx]:
						if hpp_content.find(function_name) != -1:
							for copy_content in copy_candidate:
								if copy_content.find(function_name) != -1:
									impl_h_str_list[__index] = copy_content
									hpp_content_found = True
									break
							if hpp_content_found:
								break
						else:
							pass
						__index += 1
					if hpp_content_found == False:
						for copy_content in copy_candidate:
							if copy_content.find(function_name) != -1:
								impl_h_str_list.insert(hpp_ed_idx, copy_content)
								hpp_ed_idx += 1

					__index = cpp_st_idx
					cpp_content_found = False
					for cpp_content in impl_cpp_str_list[cpp_st_idx: cpp_ed_idx]:
						if cpp_content.find(function_name) != -1:
							for copy_content in copy_candidate:
								if copy_content.find(function_name) != -1:
									tmp = list(re.findall('([A-z,0-9,_,:,&,*]*) ([A-z,0-9,_]*)(\([A-z,0-9,_,:,&,*, ]*\))', copy_content)[0])
									tmp[1] = module_name + "_" + interface_name + "SVC_impl::" + tmp[1]
									impl_cpp_str_list[__index] = tmp[0] + " " + tmp[1] + tmp[2] + "\n"
									cpp_content_found = True
									break
							if cpp_content_found:
								break
						__index += 1
					if cpp_content_found == False:
						for copy_content in copy_candidate:
							if copy_content.find(function_name) != -1:
								line = []
								tmp = list(re.findall('([A-z,0-9,_,:,&,*]*) ([A-z,0-9,_]*)(\([A-z,0-9,_,:,&,*, ]*\))',
								                      copy_content)[0])
								tmp[1] = module_name + "_" + interface_name + "SVC_impl::" + tmp[1]
								line.append(tmp[0] + " " + tmp[1] + tmp[2] + "\n")
								line.append("{" + "\n")
								line.append(
									"\t// Please insert your code here and remove the following warning pragma" + "\n")
								line.append("#ifndef WIN32" + "\n")
								line.append("\t#warning \"Code missing in function <" + line[0] + ">\"" + "\n")
								line.append("#endif" + "\n")
								line.append("\treturn;" + "\n")
								line.append("}" + "\n")
								impl_cpp_str_list[cpp_ed_idx: cpp_ed_idx] = line

								cpp_ed_idx += line.__len__()

		if test_run:
			fh1 = open(rtc_root_path + '/test_' + file_name + ".h", '+w')
			fh2 = open(rtc_root_path + '/test_' + file_name + ".cpp", '+w')
		else:
			fh1 = open(h_name, '+w')
			fh2 = open(cpp_name, '+w')
		fh1.writelines(impl_h_str_list)
		fh2.writelines(impl_cpp_str_list)


if __name__ == '__main__':
	if sys.argv[1] in ['-h', '-help', '--help']:
		print("Normal usage:")
		print("UpdateFromIDL.py [Your RTC root folder path]")
		print("EXAMPLE: UpdateFromIDL.py ~/Workspace/RTC_Vision/")
		print("-t --test: For testing:")
		print("Edited files will be wrote to a new file, with prefix [test_]")
		print("UpdateFromIDL.py [Your RTC root folder path] --test")
		print("EXAMPLE: UpdateFromIDL.py ~/Workspace/RTC_Vision/")
		exit()
	if check_dir(sys.argv[1]) == False:
		print("RTC root folder must contain [rtc.conf] file, but this folder didn't.")
		exit()

	try:
		if sys.argv[2] in ['-t', '--test']:
			updateServicePortImplFile(sys.argv[1], True)
	except IndexError:
		updateServicePortImplFile(sys.argv[1])

else:
		pass
