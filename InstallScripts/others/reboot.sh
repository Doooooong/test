#!/bin/bash

# arg1:The full path and name of shell file when you want to excute at reboot.
# arg3:The message displayed before reboot
function onetime_reboot_process()
{
	local args=("$@")
local identifier="EDDEDDACCF"
	sudo bash -c "echo \"gnome-terminal --display :0 -- /bin/bash -c \\\"bash ${args[0]} && sudo sed -i '/${identifier}/d' /etc/profile; bash\\\"\" >> /etc/profile"
	read -p "ENTER to reboot!"
	sudo reboot
}
 # ${comment}
onetime_reboot_process `pwd`/reboot_cmd.sh
#  >> /etc/crontab
