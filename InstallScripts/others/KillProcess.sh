#!/bin/bash

function last()
{
	status=$?
	echo 'Ctrl-C is pressed!!'
	echo  in trap, status captured
	exit $status
}

trap 'last'  {1,2,3,15}

