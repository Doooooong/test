#!/bin/bash

# Both side of computers can use ssh connection to connect each other without password.
# arg1: ssh connection USERNAME@IP_ADDRESS
# arg2: The name of key (no space).
# arg3: The connection password of USERNAME@IP_ADDRESS or nothing for inputing when excution.

if [[ $1 == "-h" || $1 == "--help"  || $1 == ""]]; then
{
	echo -e "\
	Make a no password ssh connection for two side of PCs.
	You can make a no password ssh connection to the remote computer, and the remote computer can also make a no password ssh connection to your computer.
	Options:
	\t-h --help: Print this help.
	Usage:
	\tsetup_ssh_key_both [REMOTE USERNAME@IP_ADDRESS] [THE KEY NAME] [PASSWORD]
	\t\t[REMOTE USERNAME@IP_ADDRESS]: ssh connection USERNAME@IP_ADDRESS
	\t\t[THE KEY NAME]: The name of private key and public key that you want to save. No password access need to create a pair of a private key and a public key. The private key should always storeged in your PC and the public key should copy to the remote PC.
	\t\t[PASSWORD]: (Optional argument) The password of remote PC for specified user. If you don't provide a password here, you will be ask the password while the command executing.
	"
	exit
}
fi


# Setup ssh for local mechine to the remote machine.
setup_ssh_key $1 $2 $3

# Setup ssh for remote machine to the local machine.
read -sp "[sudo] password for `users`:" password

self_ip=`ip -f inet -o addr | grep -v "127.0.0.1" | cut -d\  -f 7 | cut -d/ -f 1 | awk 'NR == 1'`
timestamp=`date +%Y_%m_%d__%H_%M_%S`
key_name=`users`_$timestamp
host=`users`@${self_ip}

ssh $1 "ssh-keyscan -H ${self_ip} >> ~/.ssh/known_hosts"
ssh $1 "ssh-keygen -P \"\" -t rsa -f ~/.ssh/$key_name"
ssh $1 "sshpass -p ${password} ssh-copy-id -i ~/.ssh/$key_name.pub ${host}"
