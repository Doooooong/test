#!/bin/bash

function install_other_packages()
{
	sudo dpkg -i ../Fundamental/vtk8.1_8.1.0-1_amd64.deb
	sudo dpkg -i ../OpenCV/opencv_4.1.2-nocuda-1_amd64.deb
	sudo dpkg -i ../PCL/pcl-1.9.1-nocuda_1.9.1-1_amd64.deb
}

function install_other_CUDA_packages()
{
	sudo dpkg -i ../Fundamental/vtk8.1_8.1.0-1_amd64.deb
	sudo dpkg -i ../OpenCV/opencv4.1.2-gpu_4.1.2-gpu-1_amd64.deb
	sudo dpkg -i ../PCL/pcl-trunk-cuda_1.9.x-trunk-1_amd64.deb
}

function install_openrtm_all()
{
	sudo dpkg -i ../OpenRTM/omniorb/omniorb_4.2.2-1_amd64.deb
	sudo dpkg -i ../OpenRTM/omniorb/omniorb-python3.7_4.2.2-1_amd64.deb

	sudo bash -c 'echo /usr/local/lib/python3.7/site-packages/ >> /usr/local/lib/python3.7/dist-packages/site-packages-path.pth'
	sudo ln -sf /usr/local/bin/omniidl /usr/bin/omniidl
	sudo ln -sf /usr/local/bin/omniNames /usr/bin/omniNames
	sudo ln -sf /usr/local/bin/omniidlrun.py /usr/bin/omniidlrun.py
	sudo ln -sf /usr/local/bin/omnicpp /usr/bin/omnicpp
	sudo ln -sf /usr/local/bin/omniMapper /usr/bin/omniMapper

	sudo dpkg -i ../OpenRTM/Make Dummy PKG/Dummy-libomniorb4-2.deb
	sudo dpkg -i ../OpenRTM/Make Dummy PKG/Dummy-libomnithread4.deb
	sudo dpkg -i ../OpenRTM/Make Dummy PKG/Dummy-omniorb-nameserver.deb

	# We use our own omniORB packages, so we have to 
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist-idl_1.2.1-0_amd64.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist_1.2.1-0_amd64.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist-dev_1.2.1-0_amd64.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist-python_1.2.1-0_amd64.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist-python3.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtp_1.2.1-0_amd64.deb
	sudo -H python3.7 -m pip install rtshell-aist
}

function setup_ssh_private_key()
{
	timestamp=`date +%Y_%m_%d__%H_%M_%S`
	key_name=SharePC_`users`_$timestamp
	ssh-keygen -t rsa -f ~/.ssh/$key_name
	sshpass -p "zutt0issy0" ssh-copy-id -i ~/.ssh/$key_name.pub cookteam@192.168.3.90
}

function install_visual_code()
{
	curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
	sudo install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/
	sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

	sudo apt-get install apt-transport-https
	sudo apt-get update
	sudo apt-get install code

	code --install-extension xyz.local-history
	code --install-extension ms-vscode.cpptools
	code --install-extension austin.code-gnu-global
	code --install-extension streetsidesoftware.code-spell-checker
	code --install-extension ms-python.python
	code --install-extension tomsaunders.vscode-workspace-explorer
	code --install-extension Kelvin.vscode-sshfs

	timestamp=`date +%Y_%m_%d__%H_%M_%S`
	key_name=SharePC_`users`_$timestamp
	ssh-keygen -t rsa -f ~/.ssh/$key_name
	sshpass -p "zutt0issy0" ssh-copy-id -i ~/.ssh/$key_name.pub cookteam@192.168.3.90
	echo -e "\
	{
	\t	\"sshfs.configs\": [
	\t	{
	\t\t		\"host\": \"192.168.3.90\",
	\t\t		\"name\": \"RTC_Vision-SharePC\",
	\t\t		\"privateKeyPath\": \"~/.ssh/$key_name\",
	\t\t		\"root\": \"/home/cookteam/Workspace/RTCs/RTC_Vision_`users`\"
	\t\t		\"username\": \"test\"
	\t	},
		
	\t	],
	}
	" >> ~/.config/Code/User/settings.json
}	
