#!/bin/bash
set -x
ShellFilePath=`pwd`
#######################################
# Base environment
#######################################
# Remove case sensitive.
grep 'set completion-ignore-case on' /etc/inputrc || sudo bash -c 'echo "set completion-ignore-case on">>/etc/inputrc'
# Add template files to Ubuntu.
cp ${ShellFilePath}/../TemplateFiles/* ~/Templates/



# #####################################
# Python environment
# #####################################
# Make python env
sudo apt install python3.7 python3.7-dev python3-distutils python3-testresources
sudo ln -sf /usr/bin/python3.7 /usr/bin/python3
# Fix errors when update to python3.7
sudo ln -s /usr/lib/python3/dist-packages/apt_pkg.cpython-{36m,37m}-x86_64-linux-gnu.so
sudo ln -s /usr/lib/python3/dist-packages/gi/_gi.cpython-{36m,37m}-x86_64-linux-gnu.so
# Install the pip for python3.7
# sudo apt install python3-pip
wget https://bootstrap.pypa.io/get-pip.py || read -p "Cannot get get-pip.py, installation terminated! Please contact administrator!!! EXIT!";exit -1
sudo python3.7 get-pip.py

read -p "Enter to continue!"

# #####################################
# Development environment
# #####################################
sudo apt -y install build-essential cmake cmake-qt-gui git gitk libgtk2.0-dev pkg-config doxygen
sudo dpkg -i ../Fundamental/vtk8.1_8.1.0-1_amd64.deb
sudo apt -y install qtbase5-dev libqt5core5a libqt5concurrent5 libqt5gui5 libqt5opengl5 libqt5widgets5
sudo apt -y install openssh-server sshpass

# librealsense2
sudo apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key

sudo add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo bionic main" -u
sudo apt -y install librealsense2=2.29.0-0~realsense0.1580
sudo apt -y install librealsense2-gl=2.29.0-0~realsense0.1580
sudo apt -y install librealsense2-utils=2.29.0-0~realsense0.1580
sudo apt -y install librealsense2-dev=2.29.0-0~realsense0.1580
sudo apt -y install librealsense2-dbg=2.29.0-0~realsense0.1580
sudo apt -y install librealsense2-dkms

# PCL
sudo apt -y install libphonon-dev mpi-default-dev openmpi-bin openmpi-common libflann1.* libflann-dev libeigen3-dev libboost-all-dev libqhull* libusb-dev libgtest-dev git-core freeglut3-dev libxmu-dev libxi-dev libusb-1.0-0-dev graphviz mono-complete libqt5core5a libqt5concurrent5 libqt5gui5 libqt5opengl5 libqt5widgets5 openjdk-8-jdk openjdk-8-jre phonon-backend-gstreamer phonon-backend-vlc 

# OpenCV
sudo apt -y install libavcodec-dev libavformat-dev libswscale-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev

# Python package
sudo -H python3.7 -m pip install numpy scipy matplotlib
sudo -H python3.7 -m pip install GitPython multiexit pyrealsense2
# python3.7 -m pip install opencv-python opencv-contrib-python

# omniorb
sudo apt install omniorb omniorb-nameserver omniidl omniidl-python libomniorb4-dev

set +x
