#! /bin/bash

# '$@'

menuitems() {
	local items=("$@")
    echo "Avaliable options:" >> /dev/stderr
    for i in ${!items[@]}; do
        printf "%3d%s) %s\n" $((i+1)) "${choices[i]:- }" "${items[i]}" >> /dev/stderr
    done
    [[ "$msg" ]] && echo "$msg" >> /dev/stderr; :
}

function multiselection_menu()
{
	local items=("$@")
	local prompt="Enter an option (enter again to uncheck, press RETURN when done): "
	while menuitems ${items[@]} && read -rp "$prompt" num && [[ "$num" ]]; do
		[[ "$num" != *[![:digit:]]* ]] && (( num > 0 && num <= ${#items[@]} )) || {
		    msg="Invalid option: $num"; clear >> /dev/stderr; continue
		}
		if [ $num == ${#items[@]} ];then
		  exit
		fi
		((num--)); msg="${items[num]} was ${choices[num]:+un-}selected"
		[[ "${choices[num]}" ]] && choices[num]="" || choices[num]="x"
		clear >> /dev/stderr
	done

	clear >> /dev/stderr
	printf "You selected :\n" >> /dev/stderr
	msg=" nothing"
	for i in ${!items[@]}; do
		[[ "${choices[i]}" ]] && { printf " %d) %s\n" "$((i+1))" "${items[i]}" >> /dev/stderr; msg=""; echo "${i}"; }
	done
	# echo "$msg" >> /dev/stderr
}

function loop_confirm_multisel_menu_mainBody()
{
	while :
	do
		clear >> /dev/stderr
		local items=("$@")
		multiselection_menu ${items[@]}
		
		read -p "Input \"Y\" to confirm your selection, others will exit this shell : " sel
		if [ $sel == "Y" ]
		then
			break
			IFS=${OFS}
			echo "$i"
		else
			IFS=${OFS}
			exit
		fi
	done
	IFS=${OFS}
}

function last()
{
	status=$?
	echo 'Ctrl-C is pressed!!'
	IFS=${OFS}
	exit $status
}

trap 'last'  {1,2,3,15}

# Usage:
# ret=($(loop_confirm_multisel_menu "file1 comment" "file2" "file3" "file4" "file3" "file4" "Quit option"))

function loop_confirm_multisel_menu()
{
	OFS=$IFS
	IFS=';'
	tmp=$(loop_confirm_multisel_menu_mainBody "$*")
	ret=($tmp)
	echo "$ret"
	IFS=${OFS}
}

