#!/bin/bash

# arg1: ssh connection USERNAME@IP_ADDRESS
# arg2: The name of key (no space).
# arg3: The connection password of USERNAME@IP_ADDRESS or nothing for inputing when excution.

# if [ $1 == "-h" ]; then
if [[ $1 == "-h" || $1 == "--help" || $1 == "" ]]; then
{
	echo -e "\
	Make a no password ssh connection to a specify remote PC.
	Options:
	\t-h --help: Print this help.
	Usage:
	\tsetup_ssh_key [REMOTE USERNAME@IP_ADDRESS] [THE KEY NAME] [PASSWORD]
	\t\t[REMOTE USERNAME@IP_ADDRESS]: ssh connection USERNAME@IP_ADDRESS, for example: cookteam@192.168.3.90
	\t\t[THE KEY NAME]: The name of private key and public key that you want to save. No password access need to create a pair of a private key and a public key. The private key should always storeged in your PC and the public key should copy to the remote PC.
	\t\t[PASSWORD]: (Optional argument) The password of remote PC for specified user. If you don't provide a password here, you will be ask the password while the command executing.
	"
	exit
}
fi

timestamp=`date +%Y_%m_%d__%H_%M_%S`
key_name=$2_$timestamp
host_ip=`echo $1 | tr '@' '\n' | sed -n 2p`
ssh-keyscan -H ${host_ip} >> ~/.ssh/known_hosts
ssh-keygen -P "" -t rsa -f ~/.ssh/$key_name
if [[ $3 == "" ]]; then
{
	ssh-copy-id -i ~/.ssh/$key_name.pub $1
}
else
{
	sshpass -p $3 ssh-copy-id -i ~/.ssh/$key_name.pub $1
}
fi