#!/bin/bash

function remove_apt_apt_lock()
{
	res=false
	while [[ $res = false ]]
	do
		echo "===================================="
		apt_pid=($(ps ax | grep apt | awk '{print $1}'))
		ps ax | grep apt | awk '{print $1}'
		echo '------------------------------------'
		sudo kill -9 $apt_pid
		sudo rm /var/lib/dpkg/lock-frontend
		sudo rm /var/lib/dpkg/lock
		ps ax | grep apt | awk '{print $1}'
		res=true
		sudo apt install || (res=true; echo "false")
		echo '------------------------------------'
		echo $res
	done
}

function find_add_segments_to_file()
{
	segment="$1"
	filename="$2"
	grep "$segment" $filename &> /dev/null || sudo bash -c "echo $segment >> $filename"
}

function setup_dev_env_packages()
{
	remove_apt_apt_lock
	sudo apt install build-essential cmake cmake-qt-gui git gitk libgtk2.0-dev pkg-config doxygen p7zip-full curl net-tools iotop cutecom
	sudo apt install smbclient
	read -p "Enter to continue."
	sudo apt install openssh-server sshpass vnc4server
	# sudo apt install inotify-tools
	
	sudo dpkg -i ${InstallScriptsPath}/../Fundamental/vtk8.1_8.1.0-1_amd64.deb
}

function setup_convenient_utilities()
{
	# Remove case sensitive.
	grep 'set completion-ignore-case on' /etc/inputrc || sudo bash -c 'echo "set completion-ignore-case on">>/etc/inputrc'
	# Add template files to Ubuntu.
	cp ${InstallScriptsPath}/../TemplateFiles/* ~/Templates/
	chmod -x ~/Templates/*
	chmod +x ~/Templates/Untitled\ Shell.sh
}

function install_input_method()
{
	sudo apt install ibus-pinyin ibus-mozc
	ibus-daemon 	
	ibus exit
	ibus restart

	sleep 2
	echo "(STEP 1) Please add input method at next apperence GUI window."
	echo "Setup sequence: [Input Method] -> [Add] -> [Japanese] and [Chinese](optional) -> [Mozc] for Japanese and [pinyin] for Chinese."
	sleep 5
	read -p "Enter to set."
	ibus-setup
	read -p "Waiting for editing!"

	echo "(STEP 1) Please add input method to ubuntu system at next apperence GUI window."
	echo "Setup sequence(Japanese): [Input Sources] -> [+] mark -> [Japanese] -> [Japanese (Mozc)] (need scroll to bottom)"
	echo "Setup sequence(Chinese): [Input Sources] -> [+] mark -> [...] -> Input [chinese] to search box -> [Other] -> [Chinese (pinyin)]"
	sleep 5	
	read -p "Enter to set."
	gnome-control-center region

	read -p "Waiting for editing!"
}

function install_timeshift()
{
	(sudo apt-get install timeshift && echo "Timeshift has been installed!") || 
	(
		sudo add-apt-repository -y ppa:teejee2008/timeshift
		sudo apt-get update
		sudo apt-get -y install timeshift
	)
}

# arg1: Comment message for this backup. If the same comment has been found, do nothing.
# example: timeshift_backup "test comment     comment"
function timeshift_backup()
{
	locals="$@"
	(sudo timeshift --list | grep "$locals") ||
	(
		sudo timeshift --create --comments "$locals"
	)
}

function install_python37_pip_ubuntu1804()
{
	# Make python env
	sudo apt install -y python3.7 python3.7-dev python3-distutils python3-testresources
	sudo ln -sf /usr/bin/python3.7 /usr/bin/python3
	# Fix errors when update to python3.7
	sudo ln -s /usr/lib/python3/dist-packages/apt_pkg.cpython-{36m,37m}-x86_64-linux-gnu.so
	sudo ln -s /usr/lib/python3/dist-packages/gi/_gi.cpython-{36m,37m}-x86_64-linux-gnu.so
	# Install the pip for python3.7
	# sudo apt install python3-pip
	[ -e get-pip.py ] || 
	(
		wget https://bootstrap.pypa.io/get-pip.py || 
			(
				read -p "Cannot get get-pip.py, installation terminated! Please contact administrator!!! EXIT!"
				exit -1
			)
	)

	sudo -H python3.7 get-pip.py
	
	# No *.pyc file
	find_add_segments_to_file "PYTHONDONTWRITEBYTECODE=True" /etc/profile
	find_add_segments_to_file "export PYTHONDONTWRITEBYTECODE" /etc/profile
}

function install_base_python_packages()
{
	sudo -H python3.7 -m pip install sklearn numpy matplotlib scipy seaborn
	sudo -H python3.7 -m pip install multiexit GitPython pyserial

	# Fix package pillow cannot import.
	sudo ln -s /usr/lib/python3/dist-packages/PIL/_imaging.cpython-{36,37}m-x86_64-linux-gnu.so 
	sudo ln -s /usr/lib/python3/dist-packages/PIL/_imagingcms.cpython-{36,37}m-x86_64-linux-gnu.so 
	sudo ln -s /usr/lib/python3/dist-packages/PIL/_imagingft.cpython-{36,37}m-x86_64-linux-gnu.so 
	sudo ln -s /usr/lib/python3/dist-packages/PIL/_imagingmath.cpython-{36,37}m-x86_64-linux-gnu.so 
	sudo ln -s /usr/lib/python3/dist-packages/PIL/_imagingmorph.cpython-{36,37}m-x86_64-linux-gnu.so 
	sudo ln -s /usr/lib/python3/dist-packages/PIL/_webp.cpython-{36,37}m-x86_64-linux-gnu.so
}
function install_PA10PC_python_packages()
{
	install_base_python_packages
	sudo -H python3.7 -m pip install sklearn
}
function install_user_noCUDA_python_packages()
{
	install_base_python_packages
	sudo -H python3.7 -m pip install pyrealsense2 opencv-python opencv-contrib-python tensorflow
	sudo -H python3.7 -m pip install torch==1.3.1+cpu torchvision==0.4.2+cpu -f https://download.pytorch.org/whl/torch_stable.html  # PyTorch1.3 cpu version.
}
function install_user_CUDA_python_packages()
{
	install_base_python_packages
	sudo -H python3.7 -m pip install pyrealsense2 tensorflow-gpu torch torchvision
}
function install_SharePC_python_packages()
{
	install_base_python_packages
	sudo -H python3.7 -m pip install pyrealsense2 tensorflow-gpu torch torchvision
}

function install_librealsense2()
{
	sudo apt-key adv --keyserver keys.gnupg.net --recv-key F6E65AC044F831AC80A06380C8B3A55A6F3EFCDE || sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-key

	sudo add-apt-repository "deb http://realsense-hw-public.s3.amazonaws.com/Debian/apt-repo bionic main" -u
	sudo apt -y install librealsense2
	sudo apt -y install librealsense2-gl
	sudo apt -y install librealsense2-utils
	sudo apt -y install librealsense2-dev
	sudo apt -y install librealsense2-dbg
	sudo apt -y install librealsense2-dkms
	sudo apt -y install librealsense2-udev-rules
}

function install_PCL_base_packages()
{
	sudo apt -y install libtbb-dev libtbb2 libphonon-dev mpi-default-dev openmpi-bin openmpi-common libflann1.* libflann-dev libeigen3-dev libboost-all-dev libqhull* libusb-dev libgtest-dev git-core freeglut3-dev libxmu-dev libxi-dev libusb-1.0-0-dev graphviz mono-complete libqt5core5a libqt5concurrent5 libqt5gui5 libqt5opengl5 libqt5widgets5 openjdk-8-jdk openjdk-8-jre phonon-backend-gstreamer phonon-backend-vlc 
}

function install_OpenCV_base_packages()
{
	sudo apt -y install libavcodec-dev libavformat-dev libswscale-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
}

function install_PCL_noCUDA()
{
	install_PCL_base_packages
	sudo dpkg -i ${InstallScriptsPath}/../PCL/pcl-1.9.1-nocuda_1.9.1-1_amd64.deb
	sudo cp ${InstallScriptsPath}/../Utilites/app_desktop_files/pcl_viewer.desktop /usr/share/applications/
	sudo mkdir /usr/share/icon/pcl/
	sudo cp ${InstallScriptsPath}/../Utilites/app_desktop_files/icon/pcl.png /usr/share/icon/pcl/
}

function install_PCL_CUDA()
{
	install_PCL_base_packages
	sudo dpkg -i ${InstallScriptsPath}/../PCL/pcl-trunk_1.9.1-2019.12.02-1_amd64.deb
	sudo cp ${InstallScriptsPath}/../Utilites/app_desktop_files/pcl_viewer.desktop /usr/share/applications/
	sudo mkdir /usr/share/icon/pcl/
	sudo cp ${InstallScriptsPath}/../Utilites/app_desktop_files/icon/pcl.png /usr/share/icon/pcl/
}

function install_OpenCV_noCUDA()
{
	install_OpenCV_base_packages
	sudo dpkg -i ${InstallScriptsPath}/../OpenCV/opencv_4.1.2-nocuda-1_amd64.deb
}

function install_OpenCV_CUDA()
{
	install_OpenCV_base_packages
	sudo dpkg -i ${InstallScriptsPath}/../OpenCV/opencv4.1.2-gpu_4.1.2-gpu-1_amd64.deb
}

function install_other_packages()
{
	sudo dpkg -i ${InstallScriptsPath}/../Fundamental/vtk8.1_8.1.0-1_amd64.deb
	sudo dpkg -i ${InstallScriptsPath}/../OpenCV/opencv_4.1.2-nocuda-1_amd64.deb
	sudo dpkg -i ${InstallScriptsPath}/../PCL/pcl-1.9.1-nocuda_1.9.1-1_amd64.deb
}

function install_other_CUDA_packages()
{
	sudo dpkg -i ${InstallScriptsPath}/../Fundamental/vtk8.1_8.1.0-1_amd64.deb
	sudo dpkg -i ${InstallScriptsPath}/../OpenCV/opencv4.1.2-gpu_4.1.2-gpu-1_amd64.deb
	sudo dpkg -i ${InstallScriptsPath}/../PCL/pcl-trunk_1.9.1-2019.12.02-1_amd64.deb
}

function install_openrtm_all()
{
	sudo apt install openjdk-8-jre
	sudo dpkg -i ${InstallScriptsPath}/../OpenRTM/omniorb/omniorb_4.2.2-1_amd64.deb
	sudo dpkg -i ${InstallScriptsPath}/../OpenRTM/omniorb/omniorb-python3.7_4.2.2-1_amd64.deb

	sudo bash -c 'echo /usr/local/lib/python3.7/site-packages/ >> /usr/local/lib/python3.7/dist-packages/site-packages-path.pth'
	sudo ln -sf /usr/local/bin/omniidl /usr/bin/omniidl
	sudo ln -sf /usr/local/bin/omniNames /usr/bin/omniNames
	sudo ln -sf /usr/local/bin/omniidlrun.py /usr/bin/omniidlrun.py
	sudo ln -sf /usr/local/bin/omnicpp /usr/bin/omnicpp
	sudo ln -sf /usr/local/bin/omniMapper /usr/bin/omniMapper

	# We use our own omniORB packages, so we have to install DUMMY deb packages of omniORB.
	sudo dpkg -i ${InstallScriptsPath}/../OpenRTM/Make_Dummy_PKG/Dummy-libomniorb4-2.deb
	sudo dpkg -i ${InstallScriptsPath}/../OpenRTM/Make_Dummy_PKG/Dummy-libomnithread4.deb
	sudo dpkg -i ${InstallScriptsPath}/../OpenRTM/Make_Dummy_PKG/Dummy-omniorb-nameserver.deb

	# And install OpenRTM with ignoring dependency.
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist-idl_1.2.1-0_amd64.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist_1.2.1-0_amd64.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist-dev_1.2.1-0_amd64.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist-python_1.2.1-0_amd64.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtm-aist-python3.deb
	sudo dpkg --force-all -i ../OpenRTM/openrtp_1.2.1-0_amd64.deb
	sudo -H python3.7 -m pip install rtshell-aist

	# Let openrtp force use openjdk-8. When you installed openjdk-11. (openjdk-11 is the dependency of CUDA10.1)
	sudo sed -i '1 i\-vm\n/usr/lib/jvm/java-1.8.0-openjdk-amd64/bin/java' /usr/lib/x86_64-linux-gnu/openrtm-1.2/openrtp/eclipse.ini 
	sudo cp ${InstallScriptsPath}/../Utilites/app_desktop_files/openrtp.desktop /usr/share/applications/

	# Install idl auto update python program to system
	sudo ln -s ${InstallScriptsPath}/../Utilites/idl_auto_update/UpdateFromIDL.py /usr/bin/UpdateFromIDL
}

# arg1: ssh connection USERNAME@IP_ADDRESS
# arg2: The name of key (no space).
# arg3: The connection password of USERNAME@IP_ADDRESS or nothing for inputing when excution.
function setup_ssh_key()
{
	echo "Using [ssh-copy-id] command and public key, ssh connection can be without input password."
	timestamp=`date +%Y_%m_%d__%H_%M_%S`
	key_name=$2_$timestamp
	host_ip=`echo $1 | tr '@' '\n' | sed -n 2p`
	ssh-keyscan -H ${host_ip} >> ~/.ssh/known_hosts
	ssh-keygen -P "" -t rsa -f ~/.ssh/$key_name
	if [[ $3 == "" ]]; then
	{
		ssh-copy-id -i ~/.ssh/$key_name.pub $1
	}
	else
	{
		sshpass -p $3 ssh-copy-id -i ~/.ssh/$key_name.pub $1
	}
	fi
}

# Both side of computers can use ssh connection to connect each other without password.
# arg1: ssh connection USERNAME@IP_ADDRESS
# arg2: The name of key (no space).
# arg3: The connection password of USERNAME@IP_ADDRESS or nothing for inputing when excution.
function setup_ssh_key_both()
{
	# Setup ssh for local mechine.
	setup_ssh_key $1 $2 $3
	
	read -sp "[sudo] password for `users`:" password

	self_ip=`ip -f inet -o addr | grep -v "127.0.0.1" | cut -d\  -f 7 | cut -d/ -f 1 | awk 'NR == 1'`
	timestamp=`date +%Y_%m_%d__%H_%M_%S`
	key_name=`users`_$timestamp
	host=`users`@${self_ip}

	ssh $1 "ssh-keyscan -H ${self_ip} >> ~/.ssh/known_hosts"
	ssh $1 "ssh-keygen -P \"\" -t rsa -f ~/.ssh/$key_name"
	ssh $1 "sshpass -p ${password} ssh-copy-id -i ~/.ssh/$key_name.pub ${host}"
}

function install_visual_code()
{
	curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
	sudo install -o root -g root -m 644 packages.microsoft.gpg /usr/share/keyrings/
	sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/packages.microsoft.gpg] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'

	sudo apt-get install apt-transport-https
	sudo apt-get update
	sudo apt-get install code

	code --install-extension xyz.local-history  # Auto backup extention, revert file to any time!!
	code --install-extension ms-vscode.cpptools  # c++ support
	code --install-extension visualstudioexptteam.vscodeintellicode  # Auto complition
	code --install-extension austin.code-gnu-global  # 
	code --install-extension streetsidesoftware.code-spell-checker  # English spell checker
	code --install-extension ms-python.python  # Python support
	code --install-extension tomsaunders.vscode-workspace-explorer  # Workspace manager
	code --install-extension Kelvin.vscode-sshfs  # SSH file system manager.
	code --install-extension twxs.cmake
	code --install-extension edonet.vscode-command-runner

	setup_ssh_key cookteam@192.168.3.90 SharePC_`users`_ zutt0issy0
	echo -e "\
	{
	\t	\"sshfs.configs\": [
	\t	{
	\t\t		\"host\": \"192.168.3.90\",
	\t\t		\"name\": \"RTC_Vision-SharePC\",
	\t\t		\"privateKeyPath\": \"~/.ssh/$key_name\",
	\t\t		\"root\": \"/home/cookteam/Workspace/RTCs/RTC_Vision_`users`\"
	\t\t		\"username\": \"test\"
	\t	},
		
	\t	],
	}
	" >> ~/.config/Code/User/settings.json
}

function install_setup_vncserver()
{
	sudo apt install vnc4server tightvncserver
	sudo apt install xfce4 xfce*
	echo "Please set the password to 123456."
	vnc4server :1
	vnc4server -kill :1
	echo -e "\
	unset SESSION_MANAGER
	unset DBUS_SESSION_BUS_ADDRESS
	startxfce4 &
	" >> ~/.vnc/xstartup
	vncserver
	echo '@reboot vncserver :1' | crontab
}

function Net_setup()
{
	if [[ $1 == "-l" ]]; then
	{
		nmcli device | awk '$2=="ethernet"{print $1}'
		ifconfig
		gedit ${InstallScriptsPath}/../PA10_PC/net_config_files/99-PA10-PC_Left.yaml
		read -p "Waiting for editing!"

		sudo cp ${InstallScriptsPath}/../PA10_PC/net_config_files/99-PA10-PC_Left.yaml /etc/netplan/
		sudo netplan apply
	}
	elif [[ $1 == "-r" ]]; then
	{
		nmcli device | awk '$2=="ethernet"{print $1}'
		ifconfig
		gedit ${InstallScriptsPath}/../PA10_PC/net_config_files/99-PA10-PC_Right.yaml
		read -p "Waiting for editing!"

		sudo cp ${InstallScriptsPath}/../PA10_PC/net_config_files/99-PA10-PC_Right.yaml /etc/netplan/
		sudo netplan apply
	}
	elif [[ $1 == "-share" ]]; then
	{
		nmcli device | awk '$2=="ethernet"{print $1}'
		ifconfig
		gedit ${InstallScriptsPath}/../PA10_PC/net_config_files/99-SharePC.yaml
		read -p "Waiting for editing!"

		sudo cp ${InstallScriptsPath}/../PA10_PC/net_config_files/99-SharePC.yaml /etc/netplan/
		sudo netplan apply
	}
	else
	{
		echo "Wrong parameters. Input \"-l\" or \"-r\" to specify PCs you want to setup"
	}
	fi
}

function PA10_PC_ControlBoard_setup()
{
	cd ${InstallScriptsPath}/../PA10_PC/pa
	bash ./Driver_Setup.sh
}

function PA10_PC_gripper_setup()
{
	sudo -H python3 -m pip install pyserial
	sudo adduser pa10 dialout
	if [[ $1 == "-l" ]]; then
	{
		sudo -S mv -i ${InstallScriptsPath}/../PA10_PC/RTC_parralelGripper/bash_aliases_PA10_L /etc/
		sudo -S bash -c "echo -e \"if [ -f /etc/bash_aliases_PA10_L ]; then\n\t . /etc/bash_aliases_PA10_L \nfi\">>/etc/bash.bashrc"

		sudo -S cp ${InstallScriptsPath}/../PA10_PC/RTC_parralelGripper/parraleGripper_l /etc/init.d/
		sudo -S cp ${InstallScriptsPath}/../PA10_PC/RTC_parralelGripper/namingServer /etc/init.d/
		cd /etc/init.d/
		sudo chmod +x parraleGripper_l
		sudo chmod +x namingServer

		sudo systemctl unmask parraleGripper_l
		sudo systemctl unmask namingServer
	}
	elif [[ $1 == "-r" ]]; then
	{
		sudo -S mv -i ${InstallScriptsPath}/../PA10_PC/RTC_parralelGripper/bash_aliases_PA10_R /etc/
		sudo -S bash -c "echo -e \"if [ -f /etc/bash_aliases_PA10_R ]; then\n\t . /etc/bash_aliases_PA10_R \nfi\">>/etc/bash.bashrc"

		sudo -S cp ${InstallScriptsPath}/../PA10_PC/RTC_parralelGripper/parraleGripper_r /etc/init.d/
		sudo -S cp ${InstallScriptsPath}/../PA10_PC/RTC_parralelGripper/namingServer /etc/init.d/
		cd /etc/init.d/
		sudo chmod +x parraleGripper_r
		sudo chmod +x namingServer

		sudo systemctl unmask parraleGripper_r
		sudo systemctl unmask namingServer		
	}
	elif [[ $1 == "-share" ]]; then
	{
		bash ${InstallScriptsPath}/../PA10_PC/RTC_parralelGripper/MakeGripperStartUp.sh
	}
	else
	{
		echo "Wrong parameters. Input \"-l\" or \"-r\" to specify PCs you want to setup"
		
	}
	fi
}

function install_PA10_PC_package()
{
	sudo apt -y install libeigen3-dev

}

# function copy_()
# {}

function setup_development_env()
{
	if [[ $1 == "-h" ]]; then
	{
		echo -e "\
		-h --help: Print this.
		-l: Setup PA 10 Left PC.
		-r: Setup PA 10 Right PC.
		"
		exit
	}
	elif [[ $1 == "--help" ]]; then
	{
		echo -e "\
		-h --help: Print this.
		-l: Setup PA 10 Left PC.
		-r: Setup PA 10 Right PC.
		"
		exit
	}
	elif [[ $1 == "-pa10" ]]; then
	{
		# RTC_PA10; RTC_parllel
		echo "PA10 PC development environment setup."
		read -p "Enter to continue."
	}
	elif [[ $1 == "-share" ]]; then
	{
		echo "Share PC development environment setup."
		read -p "Enter to continue."
	}
	elif [[ $1 == "-user" ]]; then
	{
		echo "User PC development environment setup."
		read -p "Enter to continue."
	}
	else
	{
		echo "Wrong parameters, Exit."
		exit
	}
	fi
}

function ssh_PA10_PC_setup()
{
	if [[ $1 == "-l" ]]; then
	{
		setup_ssh_key pa10@192.168.3.44 PA10_Right_ zutt0issy0
	}
	elif [[ $1 == "-r" ]]; then
	{
		echo "Do nothing"	
	}
	else
	{
		echo -e "\
		-h --help: Print this.
		-l: Setup PA 10 Left PC.
		-r: Setup PA 10 Right PC.
		"
		exit
	}
	fi
}

function ssh_SharePC_setup()
{
	setup_ssh_key_both pa10@192.168.3.28 PA10_Left_ zutt0issy0
}

function ssh_USER_setup()
{
	# ssh for PA10 left PC.
	setup_ssh_key pa10@192.168.3.28 PA10_Left_ zutt0issy0
	# ssh for PA10 right PC.
	setup_ssh_key pa10@192.168.3.44 PA10_Right_ zutt0issy0
	# ssh for share_PC with both side no password connection (Using your own share PC account).
	read -sp "[sudo] password for `users`:" password
	setup_ssh_key_both `user`@192.168.3.90 Share_PC_ $password


	# rsync -anP --include-from=rsync_include --exclude-from=rsync_exclude ./ dong@192.168.3.101:ws
}

function install_turboVNC()
{
	sudo apt install vnc4server
	sudo apt install xfce4 xfce*
	wget https://sourceforge.net/projects/turbovnc/files/2.2.3/turbovnc_2.2.3_amd64.deb
	wget https://sourceforge.net/projects/virtualgl/files/2.6.3/virtualgl_2.6.3_amd64.deb
	sudo dpkg -i turbovnc_2.2.3_amd64.deb
	sudo dpkg -i virtualgl_2.6.3_amd64.deb
	echo "Please set the password to 123456."
	echo "Please set the password to 123456."
	echo "Please set the password to 123456."
	echo "Please set the password to 123456."
	echo "Please set the password to 123456."
	echo "Please set the password to 123456."
	echo "Please set the password to 123456."
	echo "Please set the password to 123456."
	echo "Please set the password to 123456."
	echo "Please set the password to 123456."
	vnc4server :1
	vnc4server -kill :1
	echo '@reboot /opt/TurboVNC/bin/vncserver :1' | crontab
}

function install_pycharm()
{
	cd ~/Downloads
	wget https://download.jetbrains.com/python/pycharm-community-2019.3.1.tar.gz
	tar xzvf pycharm-community-2019.3.1.tar.gz -C ~/
	cd ~/pycharm-community-2019.3.1/bin
	sudo ln -s `pwd`/pycharm.sh /usr/bin/pycharm
	sudo ln -s `pwd`/pycharm.png /usr/bin/pycharm.png
	sudo ln -s `pwd`/pycharm.svg /usr/bin/pycharm.svg
	sudo cp ${InstallScriptsPath}/../Utilites/app_desktop_files/pycharm.desktop /usr/share/applications
}

function download_deb_from_filenic()
{
	sudo apt install smbclient
	mkdir ${InstallScriptsPath}/../OpenCV/
	mkdir ${InstallScriptsPath}/../PCL/
	mkdir ${InstallScriptsPath}/../OpenRTM/
	mkdir ${InstallScriptsPath}/../Fundamental/

	cd ${InstallScriptsPath}/../OpenCV
	smbget -a smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/OpenCV/opencv4.1.2-gpu_4.1.2-gpu-1_amd64.deb
smbget -a smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/OpenCV/opencv_4.1.2-nocuda-1_amd64.deb

	cd ${InstallScriptsPath}/../PCL
	smbget -a smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/PCL/pcl-1.9.1-nocuda_1.9.1-1_amd64.deb
	smbget -a smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/PCL/pcl-trunk_1.9.1-2019.12.02-1_amd64.deb

	cd ${InstallScriptsPath}/../OpenRTM
	smbget -a smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/OpenRTM/openrtp_1.2.1-0_amd64.deb

	cd ${InstallScriptsPath}/../Fundamental/
smbget -a smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/Fundamental/libcudnn7_7.6.5.32-1+cuda10.1_amd64.deb
smbget -a smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/Fundamental/libcudnn7-dev_7.6.5.32-1+cuda10.1_amd64.deb
smbget -a smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/Fundamental/vtk8.1_8.1.0-1_amd64.deb

}

function setup_remmina()
{
	cp -rf ${InstallScriptsPath}/../Utilites/remmina ~/.local/share/
}

