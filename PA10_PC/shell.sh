#!/bin/bash
ShellFilePath=`pwd`

#######################################
# Timeshift install and back up.
#######################################
sudo apt-get install timeshift || 
	(
read -p "111Really?"
		sudo add-apt-repository -y ppa:teejee2008/timeshift
		sudo apt-get update
		sudo apt-get -y install timeshift
	)
read -p "222Really?"
# Create a snapshoot for new system.
(sudo timeshift --list | grep "New\ System.\ Auto\ backup\ by\ setup\ script.") ||
(
	sudo timeshift --create --comments "New System. Auto backup by setup script."
)

#######################################
# Base environment
#######################################
# Remove case sensitive.
grep 'set completion-ignore-case on' /etc/inputrc || sudo bash -c 'echo "set completion-ignore-case on">>/etc/inputrc'
# Add template files to Ubuntu.
cp ${ShellFilePath}/../TemplateFiles/* ~/Templates/

# #####################################
# Python environment
# #####################################
# Make python env
sudo apt install -y python3.7 python3.7-dev python3-distutils python3-testresources
sudo ln -sf /usr/bin/python3.7 /usr/bin/python3
# Fix errors when update to python3.7
sudo ln -s /usr/lib/python3/dist-packages/apt_pkg.cpython-{36m,37m}-x86_64-linux-gnu.so
sudo ln -s /usr/lib/python3/dist-packages/gi/_gi.cpython-{36m,37m}-x86_64-linux-gnu.so
# Install the pip for python3.7
# sudo apt install python3-pip
[ -e get-pip.py ] || 
(
	wget https://bootstrap.pypa.io/get-pip.py || 
		(
			read -p "Cannot get get-pip.py, installation terminated! Please contact administrator!!! EXIT!"
			exit -1
		)
)

sudo -H python3.7 get-pip.py

# #####################################
# Development environment
# #####################################
sudo apt -y install build-essential cmake cmake-qt-gui git gitk libgtk2.0-dev pkg-config doxygen p7zip-full curl
sudo apt -y install openssh-server sshpass vnc4server

# omniorb
sudo apt install openjdk-8-jre

###sudo apt install omniorb omniorb-nameserver omniidl omniidl-python libomniorb4-dev
# Copy a omniidl library in python3.6 for python3,7.
###sudo ln -sf /usr/lib/omniidl/_omniidl.cpython-{36m,37m}-x86_64-linux-gnu.so

# OpenRTM
# Import other script files(This segment cannot execute at first line of this file)
source ../InstallScripts/FundamentalPackage.sh

install_openrtm_all ||
(
	read -p "Cannot install OpenRTM! Please contact administrator!!! EXIT!"
			exit -1
)

# Serial port python library
sudo -H python3.7 -m pip install pyserial numpy 

# Visual Studio Code install.
install_visual_code ||
(
	read -p "Cannot install Visual Studio! Please contact administrator!!! EXIT!"
			exit -1
)

read -p "The installation is NO error? Enter ANY key to continue, or Ctrl-C to terminate."
read -p "Really?"

(sudo timeshift --list | grep "Fundamental\ env\ setup\ finished!\ Auto\ backup\ by\ setup\ script.") ||
(
	sudo timeshift --create --comments "Fundamental env setup finished! Auto backup by setup script."
)

# #####################################
# Some setup
# #####################################
# gripper and naming service restart command
# vnc4server setup

# #####################################
# Startup script setup
# #####################################
# vnc4server
# Install PA10 movement control card driver.
# Start gripper RTC.

