#!/bin/bash
InstallScriptsPath="`pwd`/../InstallScripts/"
source ../InstallScripts/InstallFunctions.sh
if [[ $1 == "-h" ]]; then
{
	echo -e "\
	-h --help: Print this.
	-l: Setup PA 10 Left PC.
	-r: Setup PA 10 Right PC.
	"
	exit
}
elif [[ $1 == "--help" ]]; then
{
	echo -e "\
	-h --help: Print this.
	-l: Setup PA 10 Left PC.
	-r: Setup PA 10 Right PC.
	"
	exit
}
elif [[ $1 == "-l" ]]; then
{
	echo "Setup PA 10 Left PC."
	read -p "Enter to continue."
}
elif [[ $1 == "-r" ]]; then
{
	echo "Setup PA 10 Right PC."
	read -p "Enter to continue."
}
else
{
	echo "Wrong parameters. Input \"-l\" or \"-r\" to specify PCs you want to setup"
	exit
}
fi

download_deb_from_filenic
remove_apt_apt_lock

install_timeshift
timeshift_backup "Empty System"
setup_convenient_utilities
setup_dev_env_packages
Net_setup $1
install_input_method
timeshift_backup "Base setup finished."

install_python37_pip_ubuntu1804

#install_librealsense2
#install_PCL_base_packages
#install_OpenCV_base_packages
#install_PCL_noCUDA
#install_PCL_CUDA
#install_OpenCV_noCUDA
#install_OpenCV_CUDA

install_openrtm_all

install_visual_code

install_turboVNC

timeshift_backup "Env setup success"


PA10_PC_ControlBoard_setup $1
PA10_PC_gripper_setup $1
ssh_PA10_PC_setup $1

timeshift_backup "All setup success"

read -p "Enter to REBOOT!!!!"
# sudo reboot
# Leptrino force sensor, serial port setup: baud rate:460800 others:8-N-1. Use cutecom open /dev/ttyACM0. Send and receive mode is [Hex], and send these data:[10 02 04 FF 2A 00 10 03 D2]. If received some data, that is mean the connecting is succeese.

