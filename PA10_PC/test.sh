#!/bin/bash
res=false
while [[ $res = false ]]
do
	echo "===================================="
	apt_pid=($(ps ax | grep apt | awk '{print $1}'))
	ps ax | grep apt | awk '{print $1}'
	echo '------------------------------------'
	sudo kill -9 $apt_pid
	sudo rm /var/lib/dpkg/lock-frontend
	sudo rm /var/lib/dpkg/lock
	ps ax | grep apt | awk '{print $1}'
	res=true
	sudo apt install || (res=true; echo "false")
	echo '------------------------------------'
	echo $res
done
