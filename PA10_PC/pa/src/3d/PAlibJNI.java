public class PAlibJNI {
	public native long pa_ini_sysJNI();
	public native long pa_ter_sysJNI();
	public native long pa_opn_armJNI(long arm);
	public native long pa_cls_armJNI(long arm);
	public native long pa_sta_simJNI(long arm);
	public native long pa_stp_simJNI(long arm);
	public native long pa_sta_armJNI(long arm);
	public native long pa_stp_armJNI(long arm);
	public native float pa_get_aglJNI(long arm,long axis);
}









