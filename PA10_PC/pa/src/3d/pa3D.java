// Java
import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;

// Java3D
import javax.media.j3d.*;
import javax.vecmath.*;
import com.sun.j3d.utils.behaviors.mouse.*;
import com.sun.j3d.utils.behaviors.picking.*;
import com.sun.j3d.utils.geometry.*;
import com.sun.j3d.utils.universe.SimpleUniverse;

// VRML
import com.sun.j3d.loaders.vrml97.VrmlLoader;
import com.sun.j3d.loaders.Scene;

public class pa3D
  extends Frame {
    private static final String CLIENT_ROBOT = "PA10";

    private static final int CLIENT_NODE_GROUP = 0;
    private static final int CLIENT_NODE_BRANCHGROUP = 1;
    private static final int CLIENT_NODE_TRANSFORMGROUP = 2;

    private static final int CLIENT_OK = 0;

    private static final float CLIENT_EPS =  0.0001f;
    private static final float CLIENT_EPS2 = 0.00001f;

    private final Vector3f V3FS1 = new Vector3f(0.0f, 0.0f, 1.0f);
    private final Vector3f V3FS2 = new Vector3f(0.0f, 1.0f, 0.0f);
    private final Vector3f V3FS3 = new Vector3f(0.0f, 0.0f, 1.0f);
    private final Vector3f V3FE1 = new Vector3f(0.0f, 1.0f, 0.0f);
    private final Vector3f V3FE2 = new Vector3f(0.0f, 0.0f, 1.0f);
    private final Vector3f V3FW1 = new Vector3f(0.0f, 1.0f, 0.0f);
    private final Vector3f V3FW2 = new Vector3f(0.0f, 0.0f, 1.0f);
/*
    private final Vector3f V3FS1 = new Vector3f(1.0f, 0.0f, 0.0f);
    private final Vector3f V3FS2 = new Vector3f(0.0f, 1.0f, 0.0f);
    private final Vector3f V3FS3 = new Vector3f(1.0f, 0.0f, 0.0f);
    private final Vector3f V3FE1 = new Vector3f(0.0f, 1.0f, 0.0f);
    private final Vector3f V3FE2 = new Vector3f(1.0f, 0.0f, 0.0f);
    private final Vector3f V3FW1 = new Vector3f(0.0f, 1.0f, 0.0f);
    private final Vector3f V3FW2 = new Vector3f(1.0f, 0.0f, 0.0f);
*/

    private static float[]	agl_   = new float[7];
    private static pa3D		frame_ = null;

    private static PAthread st_ = null;

    private String vrmlfile_ = null;     // 

    private SimpleUniverse su_     = null;      // 
    private BranchGroup bgroot_    = null;     // 
    private TransformGroup tgroot_ = null;  // 

    private Hashtable tableRob_ = new Hashtable();  // 

    private Vector defRobName_ = new Vector();
    private Vector defRobTran_ = new Vector();

    private TransformGroup tgview_  = null;  // 
    private Transform3DEx t3dHand_ = null;  // 


    static {
	System.loadLibrary("PAlibIMPL");
    }
    //-----------------------------------------------------
    public static void main(String args[]) {

        int width  = 256;
        int height = 256;
        frame_ = new pa3D(args[0], width, height);
        frame_.setVisible(true);

    }	// main }

    //-----------------------------------------------------
    //-----------------------------------------------------
    //-----------------------------------------------------
    //-----------------------------------------------------
    //-----------------------------------------------------
    public pa3D(String objfile, int width, int height) {
        super();
        vrmlfile_ = objfile;     // 

        setSize(width, height);
        setTitle("pa3D");
        _init();
        addWindowListener(
          new WindowAdapter() {
              public void windowClosing(WindowEvent evt) {
                  pa3D.this._destroy();
                  pa3D.this.setVisible(false);
                  pa3D.this.dispose();
                  System.exit(-1);
              }
          }
        );
	st_=new PAthread();
	st_.start();

    }	// Client }

    //-----------------------------------------------------
    //-----------------------------------------------------
    //-----------------------------------------------------
    //-----------------------------------------------------
    //-----------------------------------------------------
    public void _init() {

        setLayout(new java.awt.BorderLayout(0, 10));
        Canvas3D c = new Canvas3D(null);
        add(c, BorderLayout.CENTER);
        su_ = new SimpleUniverse(c);
        bgroot_ = new BranchGroup();
        bgroot_.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        bgroot_.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
        bgroot_.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);

        Transform transform = new Transform();
        String url = null;
        int ret = 0;

        System.out.println("loading vrml file ... " + vrmlfile_);
        Hashtable tableRob = new Hashtable();
        tableRob_.put(CLIENT_ROBOT, tableRob);
        url = _addObject(   //
          tableRob, defRobName_, defRobTran_, tgroot_, vrmlfile_, transform);
        if (url == null) {
            System.out.println("* rob-file may be invalid.");
            System.exit(-1);
        }
        System.out.println("fineshed\n");

        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0,0.0,0.0), 100.0);
        PickMouseBehaviorEx beh = new PickMouseBehaviorEx(c, bgroot_, bounds);
        beh.setSchedulingBounds(bounds);
        bgroot_.addChild(beh);

        _createLights(bgroot_);
        bgroot_.compile();
        su_.getViewingPlatform().setNominalViewingTransform();
        su_.addBranchGraph(bgroot_);

        tgview_ = su_.getViewingPlatform().getViewPlatformTransform();

        System.out.println("ready");
    }
	
    //-----------------------------------------------------
    public void _destroy() {
        System.out.println("destroy");
    }		// _destroy }

    //-----------------------------------------------------
    private String _addObject(
      java.util.Hashtable table,
      Vector vecName,               // 
      Vector vecTran,               // 
      TransformGroup tgroot,        // 
      String filename,              // 
      Transform transform  // 
    ) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException ex) {
            System.out.println("* FileNotFoundException occurred");
            return null;
        }

        VrmlLoader wl = new VrmlLoader();
        Scene scene = null;
		try {
			scene = wl.load(filename);
	    } catch (Exception e) {
            System.out.println("not found VRML-File");
		}

        Hashtable ht = scene.getNamedObjects();
        java.lang.Object key = null;
        for (Enumeration keys = ht.keys(); keys.hasMoreElements();) {
            key = keys.nextElement();
            table.put(key, ht.get(key));
        }

        TransformGroup tglocalroot = new TransformGroup();
        tglocalroot.setCapability(TransformGroup.ALLOW_CHILDREN_READ);
        tglocalroot.setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
        tglocalroot.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        tglocalroot.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        tglocalroot.setTransform(new Transform3DEx(transform));
        bgroot_.addChild(tglocalroot);
        BranchGroup bg = scene.getSceneGroup();
        tglocalroot.addChild(bg);
        _analyse(table, vecName, vecTran, tglocalroot, new Transform3DEx(), 0);

        return filename;
    }
	
    //-----------------------------------------------------
    private void _analyse(
      Hashtable table,
      Vector vecName,
      Vector vecTran,
      Node node,
      Transform3D t3dParent,
      int depth
    ) {

        if (node instanceof Group) {
            int nodetype = CLIENT_NODE_GROUP;
            try {
                BranchGroup bg = (BranchGroup)node;
                nodetype = CLIENT_NODE_BRANCHGROUP;
            } catch (ClassCastException exbg) {
                try {
                    TransformGroup tg = (TransformGroup)node;
                    nodetype = CLIENT_NODE_TRANSFORMGROUP;
                } catch (ClassCastException extg) {
                }
            }
            if (nodetype == CLIENT_NODE_GROUP){
                Group g = (Group)node;
                g.setCapability(Group.ALLOW_CHILDREN_READ);
                g.setCapability(Group.ALLOW_CHILDREN_WRITE);
                for (int i = 0; i < g.numChildren(); i++) {
                    _analyse(table, vecName, vecTran, g.getChild(i), t3dParent, depth + 1);
                }
            }
            else if (nodetype == CLIENT_NODE_BRANCHGROUP) {
                BranchGroup bg = (BranchGroup)node;
                bg.setCapability(BranchGroup.ALLOW_CHILDREN_READ);
                bg.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
                for (int i = 0; i < bg.numChildren(); i++) {
                    _analyse(table, vecName, vecTran, bg.getChild(i), t3dParent, depth + 1);
                }
            }
            else if (nodetype == CLIENT_NODE_TRANSFORMGROUP) {
                TransformGroup tg = (TransformGroup)node;
                tg.setCapability(TransformGroup.ALLOW_CHILDREN_READ);
                tg.setCapability(TransformGroup.ALLOW_CHILDREN_WRITE);
                tg.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
                tg.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
                if (table.containsValue(tg)) {
                    java.lang.Object key = null;
                    for (Enumeration keys = table.keys(); keys.hasMoreElements();) {
                        key = keys.nextElement();
                        if (table.get(key) == tg) {
                            break;
                        }
                    }
                    vecName.add(key);   // 
                    vecTran.add(tg);    // 
                    for (int i = 0; i < depth; i++) { System.out.print("  "); }
                    System.out.println("----------");
                    for (int i = 0; i < depth; i++) { System.out.print("  "); }
                    System.out.println("(TransformGroup) : DEF " + key.toString());
					AxisAngle4f a4f = new AxisAngle4f();
                    Vector3f v3f = new Vector3f();
                    Transform3DEx t3dLocal = new Transform3DEx();
                    tg.getTransform(t3dLocal);
                    Transform3DEx t3dWorld = new Transform3DEx(t3dParent);
                    t3dWorld.mul(t3dLocal);
                    t3dHand_ = new Transform3DEx(t3dWorld);
					for (int i = 0; i < tg.numChildren(); i++) {
                        _analyse(table, vecName, vecTran, tg.getChild(i), t3dWorld, depth + 1);
                    }
                }
                else {
                    Transform3DEx t3dLocal = new Transform3DEx();
                    tg.getTransform(t3dLocal);
                    Transform3DEx t3dWorld = new Transform3DEx(t3dParent);
                    t3dWorld.mul(t3dLocal);
                   for (int i = 0; i < tg.numChildren(); i++) {
                        _analyse(table, vecName, vecTran, tg.getChild(i), t3dWorld, depth + 1);
                    }
                }
            }

        }
        else if (node instanceof Link) {
            Link l = (Link)node;
            SharedGroup sg = l.getSharedGroup();
            for (int i = 0; i < sg.numChildren(); i++) {
                _analyse(table, vecName, vecTran, sg.getChild(i), t3dParent, depth + 1);
            }
        }
        else if (node instanceof Shape3D) {
        }
		else {
            System.out.println("* The node " + node.toString() + " is not supported.");
        }

    }
	
    //-----------------------------------------------------
    private void _createLights(BranchGroup graphRoot) {

        BoundingSphere bounds = new BoundingSphere(
          new Point3d(0.0,0.0,0.0), 100.0
        );

        Color3f alColor = new Color3f(1.0f, 1.0f, 1.0f);    // 
        AmbientLight aLgt = new AmbientLight(alColor);
        aLgt.setInfluencingBounds(bounds);  //
        graphRoot.addChild(aLgt);   //

        Color3f lColor1 = new Color3f(1.0f, 1.0f, 1.0f);    // 
        Vector3f lDir1  = new Vector3f(1.0f, -1.0f, -1.0f);  //
        DirectionalLight lgt1 = new DirectionalLight(lColor1, lDir1);
        lgt1.setInfluencingBounds(bounds);  // 
        graphRoot.addChild(lgt1);   // 

    }

    //-----------------------------------------------------
    private static boolean _vectorEpsilonEquals(
      Vector3f v,
      AxisAngle4f a,
      float eps
    ) {
        return ((Math.abs(v.x - a.x) <= eps)
            &&  (Math.abs(v.y - a.y) <= eps)
            &&  (Math.abs(v.z - a.z) <= eps));
    }

    //-----------------------------------------------------
    private static boolean _invalid(Vector3f v) {

        boolean invalid = false;

        invalid = invalid || Float.isNaN(v.x);
        invalid = invalid || Float.isNaN(v.y);
        invalid = invalid || Float.isNaN(v.z);
        invalid = invalid || Float.isInfinite(v.x);
        invalid = invalid || Float.isInfinite(v.y);
        invalid = invalid || Float.isInfinite(v.z);
        invalid = invalid || (v.x < -5 || 5 < v.x);
        invalid = invalid || (v.y < -5 || 5 < v.y);
        invalid = invalid || (v.z < -5 || 5 < v.z);

        return invalid;
    }

    //-----------------------------------------------------
    private synchronized int _nextTransform() {
        // TransformGroup
        TransformGroup tgbs = new TransformGroup();
        TransformGroup tgs1 = new TransformGroup();
        TransformGroup tgs2 = new TransformGroup();
        TransformGroup tgs3 = new TransformGroup();
        TransformGroup tge1 = new TransformGroup();
        TransformGroup tge2 = new TransformGroup();
        TransformGroup tgw1 = new TransformGroup();
        TransformGroup tgw2 = new TransformGroup();
        // Transform3D
        Transform3DEx t3dw = new Transform3DEx();
        Transform3DEx t3dbs = new Transform3DEx();
        Transform3DEx t3ds1 = new Transform3DEx();
        Transform3DEx t3ds2 = new Transform3DEx();
        Transform3DEx t3ds3 = new Transform3DEx();
        Transform3DEx t3de1 = new Transform3DEx();
        Transform3DEx t3de2 = new Transform3DEx();
        Transform3DEx t3dw1 = new Transform3DEx();
        Transform3DEx t3dw2 = new Transform3DEx();

        AxisAngle4f a4fs1 = new AxisAngle4f();
        AxisAngle4f a4fs2 = new AxisAngle4f();
        AxisAngle4f a4fs3 = new AxisAngle4f();
        AxisAngle4f a4fe1 = new AxisAngle4f();
        AxisAngle4f a4fe2 = new AxisAngle4f();
        AxisAngle4f a4fw1 = new AxisAngle4f();
        AxisAngle4f a4fw2 = new AxisAngle4f();

        Vector3f v3fs1 = new Vector3f();
        Vector3f v3fs2 = new Vector3f();
        Vector3f v3fs3 = new Vector3f();
        Vector3f v3fe1 = new Vector3f();
        Vector3f v3fe2 = new Vector3f();
        Vector3f v3fw1 = new Vector3f();
        Vector3f v3fw2 = new Vector3f();

        tgbs = (TransformGroup)(defRobTran_.get(0));
        tgs1 = (TransformGroup)(defRobTran_.get(1));
        tgs2 = (TransformGroup)(defRobTran_.get(2));
        tgs3 = (TransformGroup)(defRobTran_.get(3));
        tge1 = (TransformGroup)(defRobTran_.get(4));
        tge2 = (TransformGroup)(defRobTran_.get(5));
        tgw1 = (TransformGroup)(defRobTran_.get(6));
        tgw2 = (TransformGroup)(defRobTran_.get(7));

        tgbs.getTransform(t3dbs);
        tgs1.getTransform(t3ds1);
        tgs2.getTransform(t3ds2);
        tgs3.getTransform(t3ds3);
        tge1.getTransform(t3de1);
        tge2.getTransform(t3de2);
        tgw1.getTransform(t3dw1);
        tgw2.getTransform(t3dw2);

        t3ds1.get(a4fs1, v3fs1);
        t3ds2.get(a4fs2, v3fs2);
        t3ds3.get(a4fs3, v3fs3);
        t3de1.get(a4fe1, v3fe1);
        t3de2.get(a4fe2, v3fe2);
        t3dw1.get(a4fw1, v3fw1);
        t3dw2.get(a4fw2, v3fw2);

        a4fs1.x = V3FS1.x; a4fs1.y = V3FS1.y; a4fs1.z = V3FS1.z;
        if (!_vectorEpsilonEquals(V3FS2, a4fs2, CLIENT_EPS)) {
            a4fs2.angle = -a4fs2.angle;
        }
        a4fs2.x = V3FS2.x; a4fs2.y = V3FS2.y; a4fs2.z = V3FS2.z;
        if (!_vectorEpsilonEquals(V3FS3, a4fs3, CLIENT_EPS)) {
            a4fs3.angle = -a4fs3.angle;
        }
        a4fs3.x = V3FS3.x; a4fs3.y = V3FS3.y; a4fs3.z = V3FS3.z;
        if (!_vectorEpsilonEquals(V3FE1, a4fe1, CLIENT_EPS)) {
            a4fe1.angle = -a4fe1.angle;
        }
        a4fe1.x = V3FE1.x; a4fe1.y = V3FE1.y; a4fe1.z = V3FE1.z;
        if (!_vectorEpsilonEquals(V3FE2, a4fe2, CLIENT_EPS)) {
            a4fe2.angle = -a4fe2.angle;
        }
        a4fe2.x = V3FE2.x; a4fe2.y = V3FE2.y; a4fe2.z = V3FE2.z;
        if (!_vectorEpsilonEquals(V3FW1, a4fw1, CLIENT_EPS)) {
            a4fw1.angle = -a4fw1.angle;
        }
        a4fw1.x = V3FW1.x; a4fw1.y = V3FW1.y; a4fw1.z = V3FW1.z;
        if (!_vectorEpsilonEquals(V3FW2, a4fw2, CLIENT_EPS)) {
            a4fw2.angle = -a4fw2.angle;
        }
        a4fw2.x = V3FW2.x; a4fw2.y = V3FW2.y; a4fw2.z = V3FW2.z;

        a4fs1.angle = (float)0.1;
        a4fs2.angle = (float)0.2;
        a4fs3.angle = (float)0.3;
        a4fe1.angle = (float)0.4;
        a4fe2.angle = (float)0.5;
        a4fw1.angle = (float)0.6;
        a4fw2.angle = (float)0.7;

// for DEBUG
		a4fs1.angle = agl_[0];
		a4fs2.angle = agl_[1];
		a4fs3.angle = agl_[2];
		a4fe1.angle = agl_[3];
		a4fe2.angle = agl_[4];
		a4fw1.angle = agl_[5];
		a4fw2.angle = agl_[6];

//		agl_[0]+=(float)0.01;
//		agl_[1]+=(float)0.01;
//		agl_[2]+=(float)0.01;
//		agl_[3]+=(float)0.01;
//		agl_[4]+=(float)0.01;
//		agl_[5]+=(float)0.01;
//		agl_[6]+=(float)0.01;
// end DEBUG

        t3ds1.set(a4fs1, v3fs1);
        t3ds2.set(a4fs2, v3fs2);
        t3ds3.set(a4fs3, v3fs3);
        t3de1.set(a4fe1, v3fe1);
        t3de2.set(a4fe2, v3fe2);
        t3dw1.set(a4fw1, v3fw1);
        t3dw2.set(a4fw2, v3fw2);

        tgs1.setTransform(t3ds1);
        tgs2.setTransform(t3ds2);
        tgs3.setTransform(t3ds3);
        tge1.setTransform(t3de1);
        tge2.setTransform(t3de2);
        tgw1.setTransform(t3dw1);
        tgw2.setTransform(t3dw2);


        return CLIENT_OK;

    }

    //--------------------------------------------------------
    class PickMouseBehaviorEx
      extends PickMouseBehavior {

        int lastX_ = 0;
        int lastY_ = 0;

        Transform3DEx t3dVR_ = null;
        Transform3DEx t3dVT_ = null;

        int prev_ = pa3D.CLIENT_OK;

        WakeupOr mouseCriterion_;

        //--------------------------------------------------------
        public PickMouseBehaviorEx(
          Canvas3D canvas,
          BranchGroup branchGroup,
          javax.media.j3d.Bounds bounds
        ) {
            super(canvas, branchGroup, bounds);
        }

        //--------------------------------------------------------
        public void initialize() {
			t3dVR_ = new Transform3DEx();
			t3dVT_ = new Transform3DEx();
            
            WakeupCriterion[] mouseEvents_ = new WakeupCriterion[3];
            mouseEvents_[0] = new WakeupOnAWTEvent(MouseEvent.MOUSE_PRESSED);
            mouseEvents_[1] = new WakeupOnAWTEvent(MouseEvent.MOUSE_RELEASED);
            mouseEvents_[2] = new WakeupOnAWTEvent(MouseEvent.MOUSE_DRAGGED);
            mouseCriterion_ = new WakeupOr(mouseEvents_);
            wakeupOn(mouseCriterion_);
        }

        //--------------------------------------------------------
        public void processStimulus(java.util.Enumeration criteria) {
            WakeupCriterion wakeup;
            AWTEvent[] event;
            int id;

            while (criteria.hasMoreElements()) {
                wakeup = (WakeupCriterion)criteria.nextElement();
                if (wakeup instanceof WakeupOnAWTEvent) {
                    event = ((WakeupOnAWTEvent)wakeup).getAWTEvent();
                    for (int i = 0; i < event.length; i++) {
                        id = event[i].getID();
                        if (id == MouseEvent.MOUSE_PRESSED) {
                            lastX_ = ((MouseEvent)event[i]).getX();
                            lastY_ = ((MouseEvent)event[i]).getY();

                            Transform3DEx t3dV = new Transform3DEx();
                            AxisAngle4f a4fV = new AxisAngle4f();
                            Vector3f v3fV = new Vector3f();
                            tgview_.getTransform(t3dV);
                            t3dV.get(a4fV, v3fV);
                            t3dVR_.setRotation(a4fV);
                            Transform3DEx t3dVRI = new Transform3DEx(t3dVR_);
                            t3dVRI.transpose();
                            t3dVRI.transform(v3fV);
                            t3dVT_.setTranslation(v3fV);
                        }
                        else if (id == MouseEvent.MOUSE_DRAGGED) {
                            int curX = ((MouseEvent)event[i]).getX();
                            int curY = ((MouseEvent)event[i]).getY();
                            int diffX = curX - lastX_;
                            int diffY = -(curY - lastY_);
                            lastX_ = curX;
                            lastY_ = curY;

                            Transform3DEx t3dV = new Transform3DEx();
                            int m = ((MouseEvent)event[i]).getModifiers();
                            if ((m & InputEvent.BUTTON1_MASK) != 0
                            //        && (m & InputEvent.ALT_MASK) == 0) {     // 
                                    && (m & InputEvent.SHIFT_MASK) == 0) {     // 
                                Vector3f v3fM = new Vector3f((float)diffX, (float)diffY, 0.0f);
                                Vector3f v3fH = new Vector3f(0.0f, 0.0f, 1.0f); // 
                                Vector3f v3fAxis = new Vector3f();
                                v3fAxis.cross(v3fM, v3fH);
                                v3fAxis.normalize();
                                float angle = v3fM.length() / 100.0f;
                                Transform3DEx t3dR = new Transform3DEx();
                                Quat4f q4fR = new Quat4f();
                                q4fR.set(new AxisAngle4f(v3fAxis.x, v3fAxis.y, v3fAxis.z, angle));
                                t3dR.setRotation(q4fR);
                                t3dVR_.mul(t3dR);
                            }
                            if ((m & InputEvent.BUTTON2_MASK) != 0
                                || ((m & InputEvent.BUTTON1_MASK) != 0
                               //    && (m & InputEvent.ALT_MASK) != 0)) {     // 
                                   && (m & InputEvent.SHIFT_MASK) != 0)) {     // 
                                Vector3f v3fM = new Vector3f(0.0f, 0.0f, (float)diffY / 100.0f);
                                Transform3DEx t3dT = new Transform3DEx();
                                t3dT.setTranslation(v3fM);
                                t3dVT_.mul(t3dT);
                            }
                            if ((m & InputEvent.BUTTON3_MASK) != 0) {   // 
                               Vector3f v3fM = new Vector3f(-(float)diffX / 100.0f, -(float)diffY / 100.0f, 0.0f);
                                Transform3DEx t3dT = new Transform3DEx();
                                t3dT.setTranslation(v3fM);
                                t3dVT_.mul(t3dT);
                            }
                            t3dV.mul(t3dVR_, t3dVT_);
                            tgview_.setTransform(t3dV);
                        }
                    }

                }
            }
            wakeupOn(mouseCriterion_);
        }    

        //--------------------------------------------------------
        public void updateScene(int xpos, int ypos) {
        }
    }

//---------------------------------------------------------------------
//		thread for PA-Library 
//			current axis angle(pa_get_agl()) get realtime value 
//---------------------------------------------------------------------
	public class PAthread extends Canvas implements Runnable
	{
		private Thread 		thread;
		private PAlibJNI 	paJNI;

		public void run()
		{
			while( true )
			{
			//	repaint();
				try
				{
					Thread.sleep(100);
					agl_[0] = paJNI.pa_get_aglJNI(0,1);
					agl_[1] = paJNI.pa_get_aglJNI(0,2);
					agl_[2] = paJNI.pa_get_aglJNI(0,3);
					agl_[3] = paJNI.pa_get_aglJNI(0,4);
					agl_[4] = paJNI.pa_get_aglJNI(0,5);
					agl_[5] = paJNI.pa_get_aglJNI(0,6);
					agl_[6] = paJNI.pa_get_aglJNI(0,7);
					_nextTransform();
				}
				catch( Exception e )
				{
					System.out.println( e.toString() );
				}
			//	Thread.yield();
			}
		}
		public void start()
		{
			if( thread == null ){
				thread = new Thread( this );
				paJNI = new PAlibJNI();
				paJNI.pa_ini_sysJNI();
				paJNI.pa_opn_armJNI(0);
			//	paJNI.pa_sta_simJNI(0);
			}
			thread.start();
		}
		public void stop()
		{
		  if( thread != null )
			{
			//	paJNI.pa_stp_simJNI(0);
				paJNI.pa_cls_armJNI(0);
				paJNI.pa_ter_sysJNI();
//				thread.stop();
//				thread = null;
			}
		}
	}


	public class Transform3DEx
	  extends Transform3D {

		//--------------------------------------------------
		//*
		public Transform3DEx() {
			super();
		}

		//--------------------------------------------------
		//*
		public Transform3DEx(Transform3D transform) {
			super(transform);
		}

		//--------------------------------------------------
		//*
		public Transform3DEx(
		  float posX, float posY, float posZ,
		  float axisX, float axisY, float axisZ, float angle
		) {
			super();

			Quat4f q4f = new Quat4f();
			q4f.set(new AxisAngle4f(axisX, axisY, axisZ, angle));

			Vector3f v3f = new Vector3f(posX, posY, posZ);

			float scale = 1.0f;

			set(q4f, v3f, scale);
		}

		//--------------------------------------------------
		public Transform3DEx(Transform transform) {
		//*
			this(
			  transform.posX, transform.posY, transform.posZ,
			  transform.axisX, transform.axisY, transform.axisZ, transform.angle
			);
		}

		//--------------------------------------------------
		//*
		public void get(AxisAngle4f a4f, Vector3f v3f) {
			Matrix4f m = new Matrix4f();
			Quat4f q = new Quat4f();

			get(m);

			v3f.x = m.m03;
			v3f.y = m.m13;
			v3f.z = m.m23;

			m.m03 = 0.0f;
			m.m13 = 0.0f;
			m.m23 = 0.0f;

			q.set(m);
			a4f.set(q);
		}

		//--------------------------------------------------
		public void set(AxisAngle4f a4f, Vector3f v3f) {
			Quat4f q = new Quat4f();
			q.set(a4f);
			Vector3f v = new Vector3f(v3f);
			set(q, v, 1.0f);
		}

		//--------------------------------------------------
		public void get(Transform transform) {
			AxisAngle4f aa4f = new AxisAngle4f();
			Vector3f v3f = new Vector3f();
			this.get(aa4f, v3f);
			transform.posX = v3f.x;
			transform.posY = v3f.y;
			transform.posZ = v3f.z;
			transform.axisX = aa4f.x;
			transform.axisY = aa4f.y;
			transform.axisZ = aa4f.z;
			transform.angle = aa4f.angle;
		}

		//--------------------------------------------------
		public void transform(float[/*3*/] p) {
			Point3f p3f = new Point3f(p);
			this.transform(p3f);
			p3f.get(p);
		}

	}

	public final class Transform {
		//	instance variables
		public float posX;
		public float posY;
		public float posZ;
		public float axisX;
		public float axisY;
		public float axisZ;
		public float angle;
		//	constructors
		public Transform() { }

		public Transform(float __posX, float __posY, float __posZ, float __axisX, float __axisY, float __axisZ, float __angle) {
			posX = __posX;
			posY = __posY;
			posZ = __posZ;
			axisX = __axisX;
			axisY = __axisY;
			axisZ = __axisZ;
			angle = __angle;
		}
	}
}


