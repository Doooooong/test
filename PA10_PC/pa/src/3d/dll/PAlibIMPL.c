#include	<stdio.h>
#include	"../PAlibJNI.h"
#include	<pa.h>

/*
 * Class:     PAlibJNI
 * Method:    pa_ini_sysJNI
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL
Java_PAlibJNI_pa_1ini_1sysJNI(JNIEnv *env,jobject thisObj){
	pa_ini_sys();
	return(0);
}
/*
 * Class:     PAlibJNI
 * Method:    pa_ter_sysJNI
 * Signature: ()J
 */
JNIEXPORT jlong JNICALL
Java_PAlibJNI_pa_1ter_1sysJNI(JNIEnv *env,jobject thisObj){
	pa_ter_sys();
	return(0);
}
/*
 * Class:     PAlibJNI
 * Method:    pa_opn_armJNI
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL
Java_PAlibJNI_pa_1opn_1armJNI(JNIEnv *env,jobject thisObj,jlong arm){
	pa_opn_arm(arm);
	return(0);
}
/*
 * Class:     PAlibJNI
 * Method:    pa_opn_clsJNI
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL
Java_PAlibJNI_pa_1cls_1armJNI(JNIEnv *env,jobject thisObj,jlong arm){
	pa_cls_arm(arm);
	return(0);
}
/*
 * Class:     PAlibJNI
 * Method:    pa_sta_armJNI
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL
Java_PAlibJNI_pa_1sta_1armJNI(JNIEnv *env,jobject thisObj,jlong arm){
	pa_sta_arm(arm);
	return(0);
}
/*
 * Class:     PAlibJNI
 * Method:    pa_stp_armJNI
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL
Java_PAlibJNI_pa_1stp_1armJNI(JNIEnv *env,jobject thisObj,jlong arm){
	pa_ext_arm(arm);
	return(0);
}
/*
 * Class:     PAlibJNI
 * Method:    pa_sta_simJNI
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL
Java_PAlibJNI_pa_1sta_1simJNI(JNIEnv *env,jobject thisObj,jlong arm){
	pa_sta_sim(arm);
	return(0);
}
/*
 * Class:     PAlibJNI
 * Method:    pa_stp_simJNI
 * Signature: (J)J
 */
JNIEXPORT jlong JNICALL
Java_PAlibJNI_pa_1stp_1simJNI(JNIEnv *env,jobject thisObj,jlong arm){
	pa_ext_sim(arm);
	return(0);
}

JNIEXPORT jfloat JNICALL
Java_PAlibJNI_pa_1get_1aglJNI(
				   JNIEnv *env,
				   jobject thisObj,
				   jlong arm,
				   jlong axis){
	
	ANGLE	agl;
	jfloat	tmp;
	pa_get_agl(arm,&agl);
	switch(axis){
	case	1:
		tmp=agl.s1;
		break;
	case	2:
		tmp=agl.s2;
		break;
	case	3:
		tmp=agl.s3;
		break;
	case	4:
		tmp=agl.e1;
		break;
	case	5:
		tmp=agl.e2;
		break;
	case	6:
		tmp=agl.w1;
		break;
	case	7:
		tmp=agl.w2;
		break;
	}
	return(tmp);
}

