#define	SAMPLE_MAIN

#define	SAMPLE_VERSION	"0.03"

//ANSI C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

//X Toolkit
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>

//Athena Widget
#include <X11/Xaw3d/Command.h>
#include <X11/Xaw3d/Form.h>
#include <X11/Xaw3d/Toggle.h>
#include <X11/Xaw3d/AsciiText.h>
#include <X11/Xaw3d/Dialog.h>
#include <X11/Xaw3d/Viewport.h>
#include <X11/Xaw3d/List.h>
#include <X11/Xaw3d/Scrollbar.h>

//PA
#include <pa.h>
#include "Resource.h"
#include "arm.h"

//External Functions(Callbacks)
extern void	QuitProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	HomProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	EscProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	SafProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	BrkProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	RstProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	AxsPntProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	MovPntProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	PlyPntProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	RepStpProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	AllAxsProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	NonAxsProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	ReadFileProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	WriteFileProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	RefFileProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	SvoProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	UpProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	DownProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	TimerProc(Widget w,XtIntervalId *id);
extern void	DialogProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	RestartProc(Widget w,caddr_t clntd,caddr_t calld);

extern void	MovXyzProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	MovYprProc(Widget w,caddr_t clntd,caddr_t calld);

extern void	ChgPntProc(Widget w,caddr_t clntd,caddr_t calld);
extern void	AddPntProc1(Widget w,caddr_t clntd,caddr_t calld);
extern void	AddPntProc2(Widget w,caddr_t clntd,caddr_t calld);
extern void	DelPntProc1(Widget w,caddr_t clntd,caddr_t calld);
extern void	DelPntProc2(Widget w,caddr_t clntd,caddr_t calld);

static void	Quit(Widget,XEvent*,String*,Cardinal*);
static void	ExitProc(void);
static void	SetupMem(void);
static void	SetupGUI(void);
static void	SetupDialog(void);
static void	SetupFiler(void);
static void	ListProc(Widget w,caddr_t clntd,XawListReturnStruct* list);

static Widget	tim_lbl;		//タイマ用ラベル
static char	FullPathName[255];	//教示データフルパス名
static XtActionsRec actions[]={
	{	"QuitKey",Quit	},
};

//メイン
int main(int argc,char **argv)
{
	long	err;

	//国際化対応ロケール
	XtSetLanguageProc(NULL,NULL,NULL);

	//ツールキットの初期化
	AppObj.TopLevel=XtVaAppInitialize(
		&AppObj.AppContext,
		"sample.res",		//リソースファイル名
		options,XtNumber(options),
		&argc,argv,
		NULL,0);

	//アーム情報リソースの読み込み
	XtVaGetApplicationResources(AppObj.TopLevel,&Res,resources,
		XtNumber(resources),NULL,0);

	//引数処理
	if(Res.version){
		printf("sample for linux ver %s\n",SAMPLE_VERSION);
		exit(0);
	}
	if(Res.usage){
		printf("sample for linux ver %s\n",SAMPLE_VERSION);
		printf("./sample [-?] [-v] [-S] [-armNo NO] [-interval MSEC] [-valueFormat %F] [-file FILE]\n\n");
		printf("\t-?                    this message.\n");
		printf("\t-v                    display version info.\n");
		printf("\t-S , -s               Simulation Mode.\n");
		printf("\t-armNo NO             Use ARM Number.\n");
		printf("\t-interval MSEC        Interval msec.\n");
		printf("\t-valueFormat %F       value format type.\n");
		printf("\t-file FILE            load teach file.\n");
		printf("\t-debug                Debug Mode.\n");
		exit(0);
	}
	if(Res.debug){
		printf("Warning: this is Debug Mode\n");
	}

	//メモリ初期化
	SetupMem();

	//GUIウイジェットの初期化
	SetupGUI();

	//Popupダイアログの初期化
	SetupDialog();

	//Popupファイラの初期化
	SetupFiler();

	//終了時ハンドラの設定
	atexit(ExitProc);
	XtAppAddActions(AppObj.AppContext,actions,XtNumber(actions));

	//アームの初期化
	err=pa_ini_sys();
	if(err<0){printf("pa_ini_sys() error %x\n",err);exit(err);}


printf("arm=%d\n",Res.armNo);

	err=pa_opn_arm(Res.armNo);
	if(err<0){printf("pa_opn_arm() error %x\n",err);exit(err);}

	if(Res.sim==False){
		err=pa_sta_arm(Res.armNo);	//通常時
	}else{
		err=pa_sta_sim(Res.armNo);	//シミュレーション時
	}
	if(err<0){printf("pa_sta_???() error %x\n",err);exit(err);}

/*
	pa_del_pnt(Res.armNo,PD_ALL);
	//引数処理(教示ファイル読み込み)
	if(Res.file!=NULL){
		err=pa_lod_pnt(Res.armNo,0,Res.file);
		SetFileName(Res.file);
		if(err<0){printf("pa_lod_pnt() error %x\n",err);exit(err);}
	}
*/
	//タイマハンドラ設定
	TimerProc(tim_lbl,(XtIntervalId)0);

	XtRealizeWidget(AppObj.TopLevel);

	XtAppMainLoop(AppObj.AppContext);

	return(0);
}	

//終了関数
static void Quit(Widget w,XEvent *event,String *params,Cardinal *num_params)
{
	QuitProc(w,NULL,NULL);
}
static void ExitProc(void)
{
	int	i;

	printf("Program END!\n");

	//ファイラ(ファイル一覧)で使用したメモリの開放
	for(i=0;i<sizeof(FileNames)/sizeof(FileNames[0]);i++){
		if(FileNames[i]!=NULL){
			free(FileNames[i]);
			FileNames[i]=NULL;
		}
	}

	//アーム終了処理
	if(Res.sim==False){
		pa_ext_arm(Res.armNo);
	}else{
		pa_ext_sim(Res.armNo);
	}
	pa_cls_arm(Res.armNo);
	pa_ter_sys();
}

//メモリ初期化
static void SetupMem(void)
{
	int	i;

	OldArmStatus.error=-1;			//エラー番号
	OldArmStatus.angle.s1=999.9;		//S1軸値
	OldArmStatus.angle.s2=999.9;		//S2軸値
	OldArmStatus.angle.s3=999.9;		//S3軸値
	OldArmStatus.angle.e1=999.9;		//E1軸値
	OldArmStatus.angle.e2=999.9;		//E2軸値
	OldArmStatus.angle.w1=999.9;		//W1軸値
	OldArmStatus.angle.w2=999.9;		//W2軸値
	OldArmStatus.noap[0][3]=999.9;		//RX
	OldArmStatus.noap[1][3]=999.9;		//RY
	OldArmStatus.noap[2][3]=999.9;		//RZ
	OldArmStatus.ypr[0]=999.9;		//Yaw
	OldArmStatus.ypr[1]=999.9;		//Pitch
	OldArmStatus.ypr[2]=999.9;		//Roll
	OldArmMode=OldCurPnt=OldPntNum=-1;	//モード、カレント、総数


	for(i=0;i<sizeof(FileNames)/sizeof(FileNames[0]);i++)
		FileNames[i]=NULL;		//ファイラ用
}

//GUIウイジェットの初期化
static void SetupGUI(void)
{
	//表示がすべてウイジェットなのでメモリを消費します
	Widget	form;

	Widget	title,label1,label2,label3,label4,label5;
	Widget	label6,label7,label8;
	Widget	hom_axs,esc_axs,saf_axs,brk_on;
	Widget	svo_on;
	Widget	up,down;

	Widget	all_axs,non_axs;
	Widget	deg[7];
	int	i;
	char	name[80];
	Widget	lod_pnt,sav_pnt,ref_fil;
	Arg	args[10];
	Widget	cur_lbl,num_lbl;
	Widget	arm_lbl,err_clr,err_lbl;
        Widget	mov_pnt1,mov_pnt2,rep_ply,rep_stp;

	Widget	quit;
	Widget	restart_btn;

	Widget	dX_lbl,dY_lbl,dZ_lbl,dYaw_lbl,dPitch_lbl,dRoll_lbl;
	Widget	mov_xyz,mov_ypr;
	Widget	mm[3];
	Widget	deg2[3];

	Widget	chg_pnt;
	Widget	add_pnt1,add_pnt2,del_pnt1,del_pnt2;

	//フォームウイジェット
	form=XtVaCreateManagedWidget(
		"form",formWidgetClass,AppObj.TopLevel,NULL);

	//タイトル
	title=XtVaCreateManagedWidget(
		"title",labelWidgetClass,form,NULL);

	//姿勢移動セクション
	label1=XtVaCreateManagedWidget(
		"label1",labelWidgetClass,form,NULL);
	hom_axs=XtVaCreateManagedWidget(
		"hom_axs",commandWidgetClass,form,NULL);
	XtAddCallback(hom_axs,XtNcallback,(XtCallbackProc)HomProc,NULL);
	esc_axs=XtVaCreateManagedWidget(
		"esc_axs",commandWidgetClass,form,NULL);
	XtAddCallback(esc_axs,XtNcallback,(XtCallbackProc)EscProc,NULL);
	saf_axs=XtVaCreateManagedWidget(
		"saf_axs",commandWidgetClass,form,NULL);
	XtAddCallback(saf_axs,XtNcallback,(XtCallbackProc)SafProc,NULL);
	brk_on=XtVaCreateManagedWidget(
		"brk_on",commandWidgetClass,form,NULL);
	XtAddCallback(brk_on,XtNcallback,(XtCallbackProc)BrkProc,NULL);

	//各軸の動作セクション
	label2=XtVaCreateManagedWidget(
		"label2",labelWidgetClass,form,NULL);
	svo_on=XtVaCreateManagedWidget(
		"svo_on",commandWidgetClass,form,NULL);
	up=XtVaCreateManagedWidget(
		"up",commandWidgetClass,form,NULL);
	down=XtVaCreateManagedWidget(
		"down",commandWidgetClass,form,NULL);
	XtAddCallback(svo_on,XtNcallback,(XtCallbackProc)SvoProc,NULL);
	XtAddCallback(up,XtNcallback,(XtCallbackProc)UpProc,NULL);
	XtAddCallback(down,XtNcallback,(XtCallbackProc)DownProc,NULL);

	AppObj.turbo=XtVaCreateManagedWidget(
		"turbo",toggleWidgetClass,form,NULL);

	//操作軸の選択セクション
	label3=XtVaCreateManagedWidget(
		"label3",labelWidgetClass,form,NULL);
	AppObj.s1_axs=XtVaCreateManagedWidget(
		"s1_axs",toggleWidgetClass,form,NULL);
	AppObj.s2_axs=XtVaCreateManagedWidget(
		"s2_axs",toggleWidgetClass,form,NULL);
	AppObj.s3_axs=XtVaCreateManagedWidget(
		"s3_axs",toggleWidgetClass,form,NULL);
	AppObj.e1_axs=XtVaCreateManagedWidget(
		"e1_axs",toggleWidgetClass,form,NULL);
	AppObj.e2_axs=XtVaCreateManagedWidget(
		"e2_axs",toggleWidgetClass,form,NULL);
	AppObj.w1_axs=XtVaCreateManagedWidget(
		"w1_axs",toggleWidgetClass,form,NULL);
	AppObj.w2_axs=XtVaCreateManagedWidget(
		"w2_axs",toggleWidgetClass,form,NULL);
	all_axs=XtVaCreateManagedWidget(
		"all_axs",commandWidgetClass,form,NULL);
	XtAddCallback(all_axs,XtNcallback,(XtCallbackProc)AllAxsProc,NULL);
	non_axs=XtVaCreateManagedWidget(
		"non_axs",commandWidgetClass,form,NULL);
	XtAddCallback(non_axs,XtNcallback,(XtCallbackProc)NonAxsProc,NULL);

	//現在角度セクション
	label4=XtVaCreateManagedWidget(
		"label4",labelWidgetClass,form,NULL);
	AppObj.s1_val=XtVaCreateManagedWidget(
		"s1_val",labelWidgetClass,form,NULL);
	AppObj.s2_val=XtVaCreateManagedWidget(
		"s2_val",labelWidgetClass,form,NULL);
	AppObj.s3_val=XtVaCreateManagedWidget(
		"s3_val",labelWidgetClass,form,NULL);
	AppObj.e1_val=XtVaCreateManagedWidget(
		"e1_val",labelWidgetClass,form,NULL);
	AppObj.e2_val=XtVaCreateManagedWidget(
		"e2_val",labelWidgetClass,form,NULL);
	AppObj.w1_val=XtVaCreateManagedWidget(
		"w1_val",labelWidgetClass,form,NULL);
	AppObj.w2_val=XtVaCreateManagedWidget(
		"w2_val",labelWidgetClass,form,NULL);
	for(i=0;i<7;i++){
		sprintf(name,"deg%d",i+1);
		deg[i]=XtVaCreateManagedWidget(
			name,labelWidgetClass,form,NULL);
	}

	//連続運転セクション
	label5=XtVaCreateManagedWidget(
		"label5",labelWidgetClass,form,NULL);
	lod_pnt=XtVaCreateManagedWidget(
		"lod_pnt",commandWidgetClass,form,NULL);
	XtAddCallback(lod_pnt,XtNcallback,(XtCallbackProc)ReadFileProc,NULL);
	sav_pnt=XtVaCreateManagedWidget(
		"sav_pnt",commandWidgetClass,form,NULL);
	XtAddCallback(sav_pnt,XtNcallback,(XtCallbackProc)WriteFileProc,NULL);

	i=0;	//エディット可能に
	XtSetArg(args[i],XtNeditType,XawtextEdit);	i++;
	AppObj.fil_txt=XtCreateManagedWidget(
		"fil_txt",asciiTextWidgetClass,form,args,i);

	ref_fil=XtVaCreateManagedWidget(
		"ref_fil",commandWidgetClass,form,NULL);
	XtAddCallback(ref_fil,XtNcallback,(XtCallbackProc)RefFileProc,NULL);
	
	cur_lbl=XtVaCreateManagedWidget(
		"cur_lbl",labelWidgetClass,form,NULL);
	AppObj.cur_pnt=XtVaCreateManagedWidget(
		"cur_pnt",labelWidgetClass,form,NULL);
	num_lbl=XtVaCreateManagedWidget(
		"num_lbl",labelWidgetClass,form,NULL);
	AppObj.num_pnt=XtVaCreateManagedWidget(
		"num_pnt",labelWidgetClass,form,NULL);
	mov_pnt1=XtVaCreateManagedWidget(
		"mov_pnt1",commandWidgetClass,form,NULL);
	XtAddCallback(mov_pnt1,XtNcallback,(XtCallbackProc)AxsPntProc,NULL);
	mov_pnt2=XtVaCreateManagedWidget(
		"mov_pnt2",commandWidgetClass,form,NULL);
	XtAddCallback(mov_pnt2,XtNcallback,(XtCallbackProc)MovPntProc,NULL);
	rep_ply=XtVaCreateManagedWidget(
		"rep_ply",commandWidgetClass,form,NULL);
	XtAddCallback(rep_ply,XtNcallback,(XtCallbackProc)PlyPntProc,NULL);
	rep_stp=XtVaCreateManagedWidget(
		"rep_stp",commandWidgetClass,form,NULL);
	XtAddCallback(rep_stp,XtNcallback,(XtCallbackProc)RepStpProc,NULL);

	//アームの状態セクション
	arm_lbl=XtVaCreateManagedWidget(
		"arm_lbl",labelWidgetClass,form,NULL);
	AppObj.arm_sts=XtVaCreateManagedWidget(
		"arm_sts",labelWidgetClass,form,NULL);
	err_clr=XtVaCreateManagedWidget(
		"err_clr",commandWidgetClass,form,NULL);
	XtAddCallback(err_clr,XtNcallback,(XtCallbackProc)RstProc,NULL);
	err_lbl=XtVaCreateManagedWidget(
		"err_lbl",labelWidgetClass,form,NULL);
	AppObj.err_num=XtVaCreateManagedWidget(
		"err_num",labelWidgetClass,form,NULL);
	restart_btn=XtVaCreateManagedWidget(
		"restart_btn",commandWidgetClass,form,NULL);
	XtAddCallback(restart_btn,XtNcallback,(XtCallbackProc)RestartProc,NULL);

	//終了
	quit=XtVaCreateManagedWidget(
		"quit",commandWidgetClass,form,NULL);
	XtAddCallback(quit,XtNcallback,(XtCallbackProc)QuitProc,NULL);

	//ベース座標系セクション
	label6=XtVaCreateManagedWidget(
		"label6",labelWidgetClass,form,NULL);
	dX_lbl=XtVaCreateManagedWidget(
		"dX_lbl",labelWidgetClass,form,NULL);
	dY_lbl=XtVaCreateManagedWidget(
		"dY_lbl",labelWidgetClass,form,NULL);
	dZ_lbl=XtVaCreateManagedWidget(
		"dZ_lbl",labelWidgetClass,form,NULL);
	dYaw_lbl=XtVaCreateManagedWidget(
		"dYaw_lbl",labelWidgetClass,form,NULL);
	dPitch_lbl=XtVaCreateManagedWidget(
		"dPitch_lbl",labelWidgetClass,form,NULL);
	dRoll_lbl=XtVaCreateManagedWidget(
		"dRoll_lbl",labelWidgetClass,form,NULL);

	i=0;
	XtSetArg(args[i],XtNeditType,XawtextEdit);	i++;
	AppObj.dX=XtCreateManagedWidget(
		"dX",asciiTextWidgetClass,form,args,i);
	AppObj.dY=XtCreateManagedWidget(
		"dY",asciiTextWidgetClass,form,args,i);
	AppObj.dZ=XtCreateManagedWidget(
		"dZ",asciiTextWidgetClass,form,args,i);
	AppObj.dYaw=XtCreateManagedWidget(
		"dYaw",asciiTextWidgetClass,form,args,i);
	AppObj.dPitch=XtCreateManagedWidget(
		"dPitch",asciiTextWidgetClass,form,args,i);
	AppObj.dRoll=XtCreateManagedWidget(
		"dRoll",asciiTextWidgetClass,form,args,i);

	mov_xyz=XtVaCreateManagedWidget(
		"mov_xyz",commandWidgetClass,form,NULL);
	mov_ypr=XtVaCreateManagedWidget(
		"mov_ypr",commandWidgetClass,form,NULL);
	XtAddCallback(mov_xyz,XtNcallback,(XtCallbackProc)MovXyzProc,NULL);
	XtAddCallback(mov_ypr,XtNcallback,(XtCallbackProc)MovYprProc,NULL);

	//現在位置／姿勢セクション
	label7=XtVaCreateManagedWidget(
		"label7",labelWidgetClass,form,NULL);
	AppObj.rx_val=XtVaCreateManagedWidget(
		"rx_val",labelWidgetClass,form,NULL);
	AppObj.ry_val=XtVaCreateManagedWidget(
		"ry_val",labelWidgetClass,form,NULL);
	AppObj.rz_val=XtVaCreateManagedWidget(
		"rz_val",labelWidgetClass,form,NULL);
	label8=XtVaCreateManagedWidget(
		"label8",labelWidgetClass,form,NULL);
	AppObj.yaw_val=XtVaCreateManagedWidget(
		"yaw_val",labelWidgetClass,form,NULL);
	AppObj.pitch_val=XtVaCreateManagedWidget(
		"pitch_val",labelWidgetClass,form,NULL);
	AppObj.roll_val=XtVaCreateManagedWidget(
		"roll_val",labelWidgetClass,form,NULL);
	for(i=0;i<3;i++){
		sprintf(name,"mm%d",i+1);
		mm[i]=XtVaCreateManagedWidget(
			name,labelWidgetClass,form,NULL);
	}
	for(i=0;i<3;i++){
		sprintf(name,"deg2_%d",i+1);
		deg2[i]=XtVaCreateManagedWidget(
			name,labelWidgetClass,form,NULL);
	}

	//ポイント関係セクション
	AppObj.cur_top=XtVaCreateManagedWidget(
		"cur_top",toggleWidgetClass,form,NULL);
	AppObj.cur_next=XtVaCreateManagedWidget(
		"cur_next",toggleWidgetClass,form,NULL);
	AppObj.cur_bottom=XtVaCreateManagedWidget(
		"cur_bottom",toggleWidgetClass,form,NULL);
		//ラジオグループに
	XawToggleChangeRadioGroup(AppObj.cur_top,AppObj.cur_top);
	XawToggleChangeRadioGroup(AppObj.cur_next,AppObj.cur_top);
	XawToggleChangeRadioGroup(AppObj.cur_bottom,AppObj.cur_top);

	i=0;	//'先頭'をデフォルトでONに
	XtSetArg(args[i],XtNstate,True);	i++;
	XtSetValues(AppObj.cur_top,args,i);

	chg_pnt=XtVaCreateManagedWidget(
		"chg_pnt",commandWidgetClass,form,NULL);
	XtAddCallback(chg_pnt,XtNcallback,(XtCallbackProc)ChgPntProc,NULL);

	add_pnt1=XtVaCreateManagedWidget(
		"add_pnt1",commandWidgetClass,form,NULL);
	add_pnt2=XtVaCreateManagedWidget(
		"add_pnt2",commandWidgetClass,form,NULL);
	XtAddCallback(add_pnt1,XtNcallback,(XtCallbackProc)AddPntProc1,NULL);
	XtAddCallback(add_pnt2,XtNcallback,(XtCallbackProc)AddPntProc2,NULL);
	del_pnt1=XtVaCreateManagedWidget(
		"del_pnt1",commandWidgetClass,form,NULL);
	del_pnt2=XtVaCreateManagedWidget(
		"del_pnt2",commandWidgetClass,form,NULL);
	XtAddCallback(del_pnt1,XtNcallback,(XtCallbackProc)DelPntProc1,NULL);
	XtAddCallback(del_pnt2,XtNcallback,(XtCallbackProc)DelPntProc2,NULL);

	//タイマ確認用
	tim_lbl=XtVaCreateManagedWidget(
		"tim_lbl",labelWidgetClass,form,NULL);
}
//----------------------------------------
//ダイアログルーチンの実装部
//----------------------------------------
//ダイアログコールバック
void PopdownProc1(Widget w,Widget target,XButtonEvent *event)
{
	DlgStatus=DIALOG_YES;
	XtPopdown(target);
}
void PopdownProc2(Widget w,Widget target,XButtonEvent *event)
{
	DlgStatus=DIALOG_NO;
	XtPopdown(target);
}

//ダイアログウイジェットの初期化
static void SetupDialog(void)
{
	Widget	yes_btn,no_btn;

	//TransientShellウイジェット
	AppObj.PopupShell=XtVaCreatePopupShell(
		"popupshell",transientShellWidgetClass,AppObj.TopLevel,NULL);

	//ダイアログ
	AppObj.Dialog=XtVaCreateWidget(
		"dialog",dialogWidgetClass,AppObj.PopupShell,NULL);
	XtManageChild(AppObj.Dialog);

	//はい
	yes_btn=XtVaCreateManagedWidget(
		"yes_btn",commandWidgetClass,AppObj.Dialog,NULL);
	XtAddEventHandler(yes_btn,ButtonReleaseMask,False
		,(XtEventHandler)PopdownProc1,AppObj.PopupShell);

	//いいえ
	no_btn=XtVaCreateManagedWidget(
		"no_btn",commandWidgetClass,AppObj.Dialog,NULL);
	XtAddEventHandler(no_btn,ButtonReleaseMask,False
		,(XtEventHandler)PopdownProc2,AppObj.PopupShell);
}

//ダイアログ
int Dialog(String title,String label)
{
	Arg	args[10];
	int	i;
	int	status=0;
	XEvent	event;
	Position	x,y;
	Dimension	width,height;

	//TopLevelシェルの位置と大きさを得る
	i=0;
	XtSetArg(args[i],XtNx,&x);	i++;
	XtSetArg(args[i],XtNy,&y);	i++;
	XtSetArg(args[i],XtNwidth,&width);	i++;
	XtSetArg(args[i],XtNheight,&height);	i++;
	XtGetValues(AppObj.TopLevel,args,i);
	
	//タイトル文字と位置の設定
	i=0;
	XtSetArg(args[i],XtNtitle,title);	i++;
	XtSetArg(args[i],XtNx,x+width/3);	i++;
	XtSetArg(args[i],XtNy,y+height/3);	i++;
	XtSetValues(AppObj.PopupShell,args,i);

	//ダイアログウイジェットのラベル設定
	i=0;
	XtSetArg(args[i],XtNlabel,label);	i++;
	XtSetValues(AppObj.Dialog,args,i);

//	XtPopup(AppObj.PopupShell,XtGrabNone);		//Modeless?
//	XtPopup(AppObj.PopupShell,XtGrabExclusive);	//System Modal?
	XtPopup(AppObj.PopupShell,XtGrabNonexclusive);	//Application Modal?

	//モードレスイベント待ち
	DlgStatus=DIALOG_RESET;
	while(1){
		if(XtAppPending(AppObj.AppContext)){
			XtAppNextEvent(AppObj.AppContext,&event);
			XtDispatchEvent(&event);
		}
		if(DlgStatus!=DIALOG_RESET)	break;
	}	
	return DlgStatus;
}

//--------------------------------------
//ファイラルーチンの実装部
//--------------------------------------
//ファイラコールバック
void OkProc(Widget w,Widget target,XButtonEvent *event)
{
	XawListReturnStruct *list;

	list=XawListShowCurrent(AppObj.filelist);

	if(list->list_index==XAW_LIST_NONE){
		SetFileName("");
	}else{
		strcpy(FullPathName,PathDir);
		if(FullPathName[strlen(FullPathName)]!='/')
			strcat(FullPathName,"/");
		strcat(FullPathName,list->string);
		SetFileName(FullPathName);	//メインウインドウに文字列セット
	}
	XtPopdown(target);
}
void CancelProc(Widget w,Widget target,XButtonEvent *event)
{
	XtPopdown(target);
}
//ファイラウイジェットの初期化
static void SetupFiler(void)
{
	Widget	form,view;
	Widget	title,ok_btn,cancel_btn;

	//TransientShellウイジェット
	AppObj.FilePopupShell=XtVaCreatePopupShell(
		"filepopupshell",transientShellWidgetClass,
		AppObj.TopLevel,NULL);
	//Formウイジェット
	form=XtVaCreateManagedWidget(
		"form",formWidgetClass,AppObj.FilePopupShell,NULL);
	//タイトル
	title=XtVaCreateManagedWidget(
		"title",labelWidgetClass,form,NULL);
	//パス名
	AppObj.pathname=XtVaCreateManagedWidget(
		"pathname",labelWidgetClass,form,NULL);
	//Viewportウイジェット
	view=XtVaCreateManagedWidget(
		"view",viewportWidgetClass,form,NULL);
	//リスト
	AppObj.filelist=XtVaCreateManagedWidget(
		"filelist",listWidgetClass,view,NULL);
	XtAddCallback(AppObj.filelist,XtNcallback,
			(XtCallbackProc)ListProc,NULL);
	//OK
	ok_btn=XtVaCreateManagedWidget(
		"ok_btn",commandWidgetClass,form,NULL);
	XtAddEventHandler(ok_btn,ButtonReleaseMask,False
		,(XtEventHandler)OkProc,AppObj.FilePopupShell);
	//CANCEL
	cancel_btn=XtVaCreateManagedWidget(
		"cancel_btn",commandWidgetClass,form,NULL);
	XtAddEventHandler(cancel_btn,ButtonReleaseMask,False
		,(XtEventHandler)CancelProc,AppObj.FilePopupShell);

	//起動時のディレクトリ
	if(chdir(Res.defaultDir)<0){
		printf("Directory Error\n");
	}
	getcwd(PathDir,sizeof(PathDir));
}
//パス名フィールドのセット
static void SetFilerPathName(char *path)
{
        Arg     args[10];
        int     i;
        i=0;
        XtSetArg(args[i],XtNlabel,path);	i++;
        XtSetValues(AppObj.pathname,args,i);
}
//リストにファイル名アイテムをセットする
static void FilerFiles(char *path)
{
	FILE	*fp;
	int	i;
	long	lines=0;
	char	tmp[255];
	DIR	*dp;
	struct dirent *dep;
	int	err;
	struct stat buf;

	for(i=0;i<sizeof(FileNames)/sizeof(FileNames[0]);i++){
		if(FileNames[i]!=NULL){
			free(FileNames[i]);
			FileNames[i]=NULL;
		}
	}

	dp=opendir(".");
	if(dp==NULL){
		printf("directry file open error\n");
		return;
	}

	//ディレクトリ内のファイル数と文字合計の計算
	while((dep=readdir(dp))!=NULL){
		FileNames[lines]=malloc(strlen(dep->d_name)+1);

		//アクセス可能なファイルのみ表示する
		if(access(dep->d_name,R_OK)<0){
//			printf("Read Permission Failed : %s\n",dep->d_name);
			continue;
		}

		//ディレクトリの時文字の最後に"/"を付ける
		err=stat(dep->d_name,&buf);
		if(buf.st_mode&S_IFDIR){
			strcpy(FileNames[lines],dep->d_name);
			strcat(FileNames[lines],"/");
		}else{
			strcpy(FileNames[lines],dep->d_name);
		}
		if(++lines>=FILES_MAX){
			printf("File Num %d over! abort\n",FILES_MAX);
			break;
		}
	};
	closedir(dp);

	XawListChange(AppObj.filelist,FileNames,0,0,False);
}

//ファイラ
int Filer()
{
	int	i;
	Arg	args[10];

	//現在のパス名
	SetFilerPathName(PathDir);

	strcpy(OldSelectName,"");

	//ファイル名リストセット
	FilerFiles(PathDir);

	XtPopup(AppObj.FilePopupShell,XtGrabNonexclusive);//Application Modal?
}

//リストコールバック
static void ListProc(Widget w,caddr_t clntd,XawListReturnStruct* list)
{
	struct stat buf;
	int	err;

	if(strcmp(list->string,OldSelectName)==0){
		err=stat(list->string,&buf);
		if(buf.st_mode&S_IFDIR){
			//ディレクトリの書換え
			chdir(list->string);
			getcwd(PathDir,sizeof(PathDir));
			
			SetFilerPathName(PathDir);
			FilerFiles(PathDir);

			strcpy(OldSelectName,"");
		}else{
			strcpy(OldSelectName,list->string);

			//疑似ダブルクリック!!!
			strcpy(FullPathName,PathDir);
			if(FullPathName[strlen(FullPathName)]!='/')
				strcat(FullPathName,"/");
			strcat(FullPathName,list->string);
			SetFileName(FullPathName);
			XtPopdown(AppObj.FilePopupShell);
		}
	}else{
		strcpy(OldSelectName,list->string);
	}
}
