//
//ArmData.h for Arm Sample Program for Linux
//

#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>

//アーム用リソース定義
typedef struct {
	int	interval;
	Pixel	intervalFg1,intervalFg2;
	String	valueFormat;
	String	valueDFormat;
	Boolean	sim;
	int	armNo;
	float	armDest;
	float	armDestTurbo;
	String	defaultDir;
	String	file;

	String	usage;
	String	version;
	Boolean	debug;
} RES,*RESPTR;

//グローバルWidget定義
typedef	struct {
	XtAppContext	AppContext;
	Widget		TopLevel;

	Widget		s1_val,s2_val,s3_val,e1_val,e2_val,w1_val,w2_val;
	Widget		arm_sts,err_num;
	Widget		s1_axs,s2_axs,s3_axs,e1_axs,e2_axs,w1_axs,w2_axs;
	Widget		turbo;
	Widget		cur_pnt,num_pnt;
	Widget		fil_txt;

	Widget		dX,dY,dZ,dYaw,dPitch,dRoll;

	Widget		rx_val,ry_val,rz_val;
	Widget		yaw_val,pitch_val,roll_val;

	Widget		cur_top,cur_next,cur_bottom;

	Widget		PopupShell,Dialog;	//ダイアログ用
	Widget		FilePopupShell,filelist;//ファイラ用
	Widget		pathname;		//ファイラ用
} APPOBJ;

#define	DIALOG_RESET	0	//
#define	DIALOG_YES	1	//
#define	DIALOG_NO	2	//ダイアログ用
#define	DIALOG_OK	1	//
#define	DIALOG_CANCEL	2	//

#define	FILES_MAX	1024	//ファイル一覧で表示するファイル最大数

#ifdef	SAMPLE_MAIN
	//アプリケーションリソース
	static XtResource resources[]={
		{"interval","Interval",XtRInt,sizeof(int),
			XtOffset(RESPTR,interval),XtRString,"1000"	},
		{"intervalFg1","IntervalFG1",XtRPixel,sizeof(Pixel),
			XtOffset(RESPTR,intervalFg1),XtRString,"Black" },
		{"intervalFg2","IntervalFG2",XtRPixel,sizeof(Pixel),
			XtOffset(RESPTR,intervalFg2),XtRString,"White" },
		{"valueFormat","ValueFormat",XtRString,sizeof(String),
			XtOffset(RESPTR,valueFormat),XtRString,"%f" },
		{"valueDFormat","ValueDFormat",XtRString,sizeof(String),
			XtOffset(RESPTR,valueDFormat),XtRString,"%f" },
		{"sim","Sim",XtRBoolean,sizeof(Boolean),
			XtOffset(RESPTR,sim),XtRString,"false"	},
		{"armNo","ArmNo",XtRInt,sizeof(int),
			XtOffset(RESPTR,armNo),XtRString,"1"	},
		{"armDest","ArmDest",XtRFloat,sizeof(float),
			XtOffset(RESPTR,armDest),XtRString,"0.01"	},
		{"armDestTurbo","ArmDestTurbo",XtRFloat,sizeof(float),
			XtOffset(RESPTR,armDestTurbo),XtRString,"0.1"	},
		{"defaultDir","DefaultDir",XtRString,sizeof(String),
			XtOffset(RESPTR,defaultDir),XtRString,"/" },
		{"file","File",XtRString,sizeof(String),
			XtOffset(RESPTR,file),XtRString,NULL },

		{"usage","Usage",XtRString,sizeof(String),
			XtOffset(RESPTR,usage),XtRString,NULL },
		{"version","Version",XtRString,sizeof(String),
			XtOffset(RESPTR,version),XtRString,NULL },
		{"debug","Debug",XtRBoolean,sizeof(Boolean),
			XtOffset(RESPTR,debug),XtRString,"false"	},
	};
	//コマンドラインリソース(主要なものに限定)
	static XrmOptionDescRec options[]={
		{"-interval","*interval",XrmoptionSepArg,NULL},
		{"-valueFormat","*valueFormat",XrmoptionSepArg,NULL},
		{"-S","*sim",XrmoptionNoArg,"true"},
		{"-s","*sim",XrmoptionNoArg,"true"},
		{"-armNo","*armNo",XrmoptionSepArg,NULL},
		{"-file","*file",XrmoptionSepArg,NULL},
		{"-?","*usage",XrmoptionIsArg,NULL},
		{"-v","*version",XrmoptionIsArg,NULL},
		{"-debug","*debug",XrmoptionNoArg,"true"},
	};

	RES		Res;			//アーム情報リソース
	int		DlgStatus;		//ダイアログの返事
	char		PathDir[255];		//ファイラのデフォルトDir
	static char	*FileNames[FILES_MAX];	//ファイラ用ファイル名集合
	static char	OldSelectName[255];	//ファイラ制御用

	APPOBJ		AppObj;
#else
	extern int	Dialog(String title,String label);
	extern int	SvoDialog(AXIS axs);
	extern int	Filer(void);
	extern RES	Res;
	extern APPOBJ	AppObj;
#endif
