//
//arm.h for Arm Sample Program for Linux
//(global value defines)

#ifdef	SAMPLE_MAIN
	ARMSTATUS		ArmStatus;
	ARMSTATUS		OldArmStatus;
	long			ArmMode;
	long			OldArmMode;
	AXIS			SelectAxis;
	long			CurPnt;
	long			OldCurPnt;
	long			PntNum;
	long			OldPntNum;
#else
	extern	ARMSTATUS		ArmStatus;
	extern	ARMSTATUS		OldArmStatus;
	extern	long			ArmMode;
	extern	long			OldArmMode;
	extern	AXIS			SelectAxis;
	extern	long			CurPnt;
	extern	long			OldCurPnt;
	extern	long			PntNum;
	extern	long			OldPntNum;
#endif
