! 汎用アームサンプルリソースファイル

!ESCキーで終了
sample*form.translations: #override <Key>Escape : QuitKey()

!--------------
!アームリソース
!--------------
*interval: 300
*intervalFg1: Red
*intervalFg2: Blue
*valueFormat: %8.2f
*valueDFormat: %4.2f
*sim: true
*armNo: 0
*armDest: 0.01
*armDestTurbo: 0.1
*defaultDir: ./
*debug: false

!------------------
!ダイアログリソース
!------------------
.sample.popupshell.dialog.label.leftBitmap: Right
.sample.popupshell.dialog.label.resizable: true
*popupshell*dialog.background: lightblue
*popupshell*dialog.label.width: 370
*popupshell*dialog.label.vertDistance: 20
*popupshell*dialog.label.horizDistance: 20
*popupshell*dialog.label.background: lightblue
*popupshell*yes_btn.vertDistance: 40
*popupshell*yes_btn.label: はい
*popupshell*yes_btn.foreground: black
*popupshell*yes_btn.background: lightgrey
*popupshell*no_btn.vertDistance: 40
*popupshell*no_btn.foreground: black
*popupshell*no_btn.background: lightgrey
*popupshell*no_btn.label: いいえ

!----------------
!ファイラリソース
!----------------
*filepopupshell.title: ファイル参照
*filepopupshell.width: 500
*filepopupshell.height: 300
*filepopupshell.form.title.label: ファイル名を選択してください
*filepopupshell.form.pathname.width: 485
*filepopupshell.form.pathname.fromVert: title
*filepopupshell.form.pathname.vertDistance: 10
!*filepopupshell.form.pathname.background: white
*filepopupshell.form.pathname.resizable: true

*filepopupshell.form.view.fromVert: pathname
*filepopupshell.form.view.vertDistance: 10
*filepopupshell.form.view.width: 485
*filepopupshell.form.view.height: 200
*filepopupshell.form.view.allowHoriz: true
*filepopupshell.form.view.allowVert: true
*filepopupshell.form.view.useBottom: true
*filepopupshell.form.view.useRight: true

*filepopupshell.form.view.filelist.background: white

*filepopupshell.form.ok_btn.fromVert: view
*filepopupshell.form.ok_btn.vertDistance: 10
*filepopupshell.form.ok_btn.label: OK

*filepopupshell.form.cancel_btn.fromHoriz: ok_btn
*filepopupshell.form.cancel_btn.horizDistance: 80
*filepopupshell.form.cancel_btn.fromVert: view
*filepopupshell.form.cancel_btn.vertDistance: 10
*filepopupshell.form.cancel_btn.label: キャンセル

!-------------------
!その他のGUIリソース
!-------------------
*Toggle*ShapeStyle: oval
*Command*ShapeStyle: rectangle

.sample*international: True
.sample*fontSet: -*-*-*-*-*--14-*

.sample.title: 可搬式知能汎用アーム for Linux サンプルプログラム
.sample.width: 780
.sample.height: 500

!フォーム
*form*top: ChainTop
*form*bottom: ChainTop
*form*left: ChainLeft
*form*right: ChainLeft
*form*background: lightgray

!タイトルラベル
*form.title.horizDistance: 10
*form.title.label: PA-10を各軸動作させます。下のボタンをクリックしてください。
*form.title.borderWidth: 0

!ボタンのデフォルトの幅
*Command.width: 180

!姿勢移動
*form.label1.fromVert: title
*form.label1.horizDistance: 10
*form.label1.vertDistance: 10
*form.label1.label: 姿勢の移動
*form.label1.borderWidth: 0
*form.label1.background: lightgreen
*form.label1.width: 0

*form.hom_axs.fromVert: label1
*form.hom_axs.horizDistance: 10
*form.hom_axs.vertDistance: 0
*form.hom_axs.label: 基本姿勢に移動します

*form.esc_axs.fromVert: hom_axs
*form.esc_axs.horizDistance: 10
*form.esc_axs.vertDistance: 10
*form.esc_axs.label: 退避姿勢に移動します

*form.saf_axs.fromVert: esc_axs
*form.saf_axs.horizDistance: 10
*form.saf_axs.vertDistance: 10
*form.saf_axs.label: 安全姿勢に移動します

*form.brk_on.fromVert: saf_axs
*form.brk_on.horizDistance: 10
*form.brk_on.vertDistance: 10
*form.brk_on.foreground: red
*form.brk_on.label: 全軸のブレーキをONします

!各軸の動作
*form.label2.fromVert: brk_on
*form.label2.horizDistance: 10
*form.label2.vertDistance: 10
*form.label2.label: 各軸の操作
*form.label2.borderWidth: 0
*form.label2.background: lightgreen

*form.svo_on.horizDistance: 10
*form.svo_on.fromVert: label2
*form.svo_on.vertDistance: 0
*form.svo_on.label: 各軸毎にサーボONします
*form.svo_on.foreground: darkgreen

*form.up.width: 60
*form.up.height: 40
*form.up.horizDistance: 10
*form.up.fromVert: svo_on
*form.up.vertDistance: 10
*form.up.label: UP

*form.down.width: 60
*form.down.height: 40
*form.down.fromVert: svo_on
*form.down.vertDistance: 10
*form.down.fromHoriz: up
*form.down.horizDistance: 3
*form.down.label: DOWN

*form.turbo.width: 50
*form.turbo.fromVert: svo_on
*form.turbo.vertDistance: 20
*form.turbo.fromHoriz: down
*form.turbo.horizDistance: 5
*form.turbo.label: Turbo
*form.turbo.foreground: red

!操作軸の選択
*form.label3.fromHoriz: hom_axs
*form.label3.horizDistance: 10
*form.label3.fromVert: title
*form.label3.vertDistance: 10
*form.label3.label: 操作軸の選択
*form.label3.borderWidth: 0
*form.label3.background: lightgreen

*form.s1_axs.fromHoriz: hom_axs
*form.s1_axs.horizDistance: 20
*form.s1_axs.fromVert: label3
*form.s1_axs.vertDistance: 0
*form.s1_axs.label: S1軸選択
*form.s1_axs.foreground: brown

*form.s2_axs.fromHoriz: hom_axs
*form.s2_axs.horizDistance: 20
*form.s2_axs.fromVert: s1_axs
*form.s2_axs.vertDistance: 1
*form.s2_axs.label: S2軸選択
*form.s2_axs.foreground: brown

*form.s3_axs.fromHoriz: hom_axs
*form.s3_axs.horizDistance: 20
*form.s3_axs.fromVert: s2_axs
*form.s3_axs.vertDistance: 1
*form.s3_axs.label: S3軸選択
*form.s3_axs.foreground: brown

*form.e1_axs.fromHoriz: hom_axs
*form.e1_axs.horizDistance: 20
*form.e1_axs.fromVert: s3_axs
*form.e1_axs.vertDistance: 1
*form.e1_axs.label: E1軸選択
*form.e1_axs.foreground: brown

*form.e2_axs.fromHoriz: hom_axs
*form.e2_axs.horizDistance: 20
*form.e2_axs.fromVert: e1_axs
*form.e2_axs.vertDistance: 1
*form.e2_axs.label: E2軸選択
*form.e2_axs.foreground: brown

*form.w1_axs.fromHoriz: hom_axs
*form.w1_axs.horizDistance: 20
*form.w1_axs.fromVert: e2_axs
*form.w1_axs.vertDistance: 1
*form.w1_axs.label: W1軸選択
*form.w1_axs.foreground: brown

*form.w2_axs.fromHoriz: hom_axs
*form.w2_axs.horizDistance: 20
*form.w2_axs.fromVert: w1_axs
*form.w2_axs.vertDistance: 1
*form.w2_axs.label: W2軸選択
*form.w2_axs.foreground: brown

*form.all_axs.fromHoriz: hom_axs
*form.all_axs.horizDistance: 18
*form.all_axs.fromVert: w2_axs
*form.all_axs.vertDistance: 5
*form.all_axs.label: 全軸選択
*form.all_axs.width: 70

*form.non_axs.fromHoriz: hom_axs
*form.non_axs.horizDistance: 18
*form.non_axs.fromVert: all_axs
*form.non_axs.vertDistance: 2
*form.non_axs.label: 全軸解除
*form.non_axs.width: 70

!現在角度
*form.label4.fromHoriz: label3
*form.label4.horizDistance: 20
*form.label4.fromVert: title
*form.label4.vertDistance: 10
*form.label4.label: 現在角度
*form.label4.borderWidth: 0
*form.label4.background: lightgreen

*form.s1_val.background: grey
*form.s1_val.borderWidth: 0
*form.s1_val.justify: right
*form.s1_val.width: 60
*form.s1_val.fromHoriz: s1_axs
*form.s1_val.horizDistance: 20
*form.s1_val.fromVert: label4
*form.s1_val.vertDistance: 5
*form.s1_val.label: 0.0

*form.s2_val.background: grey
*form.s2_val.borderWidth: 0
*form.s2_val.justify: right
*form.s2_val.width: 60
*form.s2_val.fromHoriz: s1_axs
*form.s2_val.horizDistance: 20
*form.s2_val.fromVert: s1_val
*form.s2_val.vertDistance: 14
*form.s2_val.label: 0.0

*form.s3_val.background: grey
*form.s3_val.borderWidth: 0
*form.s3_val.justify: right
*form.s3_val.width: 60
*form.s3_val.fromHoriz: s1_axs
*form.s3_val.horizDistance: 20
*form.s3_val.fromVert: s2_val
*form.s3_val.vertDistance: 14
*form.s3_val.label: 0.0

*form.e1_val.background: grey
*form.e1_val.borderWidth: 0
*form.e1_val.justify: right
*form.e1_val.width: 60
*form.e1_val.fromHoriz: s1_axs
*form.e1_val.horizDistance: 20
*form.e1_val.fromVert: s3_val
*form.e1_val.vertDistance: 14
*form.e1_val.label: 0.0

*form.e2_val.background: grey
*form.e2_val.borderWidth: 0
*form.e2_val.justify: right
*form.e2_val.width: 60
*form.e2_val.fromHoriz: s1_axs
*form.e2_val.horizDistance: 20
*form.e2_val.fromVert: e1_val
*form.e2_val.vertDistance: 14
*form.e2_val.label: 0.0

*form.w1_val.background: grey
*form.w1_val.borderWidth: 0
*form.w1_val.justify: right
*form.w1_val.width: 60
*form.w1_val.fromHoriz: s1_axs
*form.w1_val.horizDistance: 20
*form.w1_val.fromVert: e2_val
*form.w1_val.vertDistance: 14
*form.w1_val.label: 0.0

*form.w2_val.background: grey
*form.w2_val.borderWidth: 0
*form.w2_val.justify: right
*form.w2_val.width: 60
*form.w2_val.fromHoriz: s1_axs
*form.w2_val.horizDistance: 20
*form.w2_val.fromVert: w1_val
*form.w2_val.vertDistance: 14
*form.w2_val.label: 0.0

*form.deg1.fromHoriz: s1_val
*form.deg1.horizDistance: 3
*form.deg1.fromVert: label4
*form.deg1.vertDistance: 5
*form.deg1.borderWidth: 0
*form.deg1.label: deg

*form.deg2.fromHoriz: s1_val
*form.deg2.horizDistance: 3
*form.deg2.fromVert: deg1
*form.deg2.vertDistance: 14
*form.deg2.borderWidth: 0
*form.deg2.label: deg

*form.deg3.fromHoriz: s1_val
*form.deg3.horizDistance: 3
*form.deg3.fromVert: deg2
*form.deg3.vertDistance: 14
*form.deg3.borderWidth: 0
*form.deg3.label: deg

*form.deg4.fromHoriz: s1_val
*form.deg4.horizDistance: 3
*form.deg4.fromVert: deg3
*form.deg4.vertDistance: 14
*form.deg4.borderWidth: 0
*form.deg4.label: deg

*form.deg5.fromHoriz: s1_val
*form.deg5.horizDistance: 3
*form.deg5.fromVert: deg4
*form.deg5.vertDistance: 14
*form.deg5.borderWidth: 0
*form.deg5.label: deg

*form.deg6.fromHoriz: s1_val
*form.deg6.horizDistance: 3
*form.deg6.fromVert: deg5
*form.deg6.vertDistance: 14
*form.deg6.borderWidth: 0
*form.deg6.label: deg

*form.deg7.fromHoriz: s1_val
*form.deg7.horizDistance: 3
*form.deg7.fromVert: deg6
*form.deg7.vertDistance: 14
*form.deg7.borderWidth: 0
*form.deg7.label: deg

!連続運転
*form.label5.horizDistance: 10
*form.label5.fromVert: up
*form.label5.vertDistance: 10
*form.label5.label: 連続運転
*form.label5.borderWidth: 0
*form.label5.background: lightgreen

*form.lod_pnt.horizDistance: 10
*form.lod_pnt.fromVert: label5
*form.lod_pnt.vertDistance: 0
*form.lod_pnt.width: 110
*form.lod_pnt.label: 教示データ読込

*form.sav_pnt.horizDistance: 10
*form.sav_pnt.fromVert: lod_pnt
*form.sav_pnt.vertDistance: 1
*form.sav_pnt.width: 110
*form.sav_pnt.label: 教示データ格納
*form.sav_pnt.foreground: magenta

*form.fil_txt.fromHoriz: lod_pnt
*form.fil_txt.horizDistance: 5
*form.fil_txt.fromVert: label5
*form.fil_txt.vertDistance: 5
*form.fil_txt.width: 310
*form.fil_txt.height: 30
*form.fil_txt.scrollHorizontal: whenneeded

*form.ref_fil.fromHoriz: fil_txt
*form.ref_fil.horizDistance: 5
*form.ref_fil.fromVert: label5
*form.ref_fil.vertDistance: 5
*form.ref_fil.width: 50
*form.ref_fil.background: yellow
*form.ref_fil.label: 参照...

*form.cur_lbl.horizDistance: 10
*form.cur_lbl.fromVert: sav_pnt
*form.cur_lbl.vertDistance: 10
*form.cur_lbl.borderWidth: 0
*form.cur_lbl.label: カレントポイント

*form.cur_pnt.fromHoriz: cur_lbl
*form.cur_pnt.horizDistance: 5
*form.cur_pnt.fromVert: sav_pnt
*form.cur_pnt.vertDistance: 10
*form.cur_pnt.justify: right
*form.cur_pnt.background: grey
*form.cur_pnt.borderWidth: 0
*form.cur_pnt.width: 50
*form.cur_pnt.label: 0

*form.num_lbl.horizDistance: 10
*form.num_lbl.fromVert: cur_lbl
*form.num_lbl.vertDistance: 10
*form.num_lbl.borderWidth: 0
*form.num_lbl.label: 総数

*form.num_pnt.fromHoriz: cur_lbl
*form.num_pnt.horizDistance: 5
*form.num_pnt.fromVert: cur_pnt
*form.num_pnt.vertDistance: 10
*form.num_pnt.justify: right
*form.num_pnt.background: grey
*form.num_pnt.borderWidth: 0
*form.num_pnt.width: 50
*form.num_pnt.label: 0

*form.mov_pnt1.fromHoriz: cur_pnt
*form.mov_pnt1.horizDistance: 20
*form.mov_pnt1.fromVert: sav_pnt
*form.mov_pnt1.vertDistance: 10
*form.mov_pnt1.width: 180
*form.mov_pnt1.label: カレントポイント各軸移動

*form.mov_pnt2.fromHoriz: cur_pnt
*form.mov_pnt2.horizDistance: 20
*form.mov_pnt2.fromVert: mov_pnt1
*form.mov_pnt2.vertDistance: 10
*form.mov_pnt2.width: 180
*form.mov_pnt2.label: カレントポイント直線移動

*form.rep_ply.fromHoriz: mov_pnt1
*form.rep_ply.horizDistance: 10
*form.rep_ply.fromVert: sav_pnt
*form.rep_ply.vertDistance: 10
*form.rep_ply.width: 100
*form.rep_ply.label: 連続運転開始

*form.rep_stp.fromHoriz: mov_pnt2
*form.rep_stp.horizDistance: 10
*form.rep_stp.fromVert: rep_ply
*form.rep_stp.vertDistance: 10
*form.rep_stp.width: 100
*form.rep_stp.label: 連続運転停止

!アームの状態
*form.arm_lbl.borderWidth: 0
*form.arm_lbl.horizDistance: 0
*form.arm_lbl.fromVert: num_lbl
*form.arm_lbl.vertDistance: 40
*form.arm_lbl.width: 100
*form.arm_lbl.label: アームの状態

*form.arm_sts.fromHoriz: arm_lbl
*form.arm_sts.horizDistance: 0
*form.arm_sts.fromVert: num_lbl
*form.arm_sts.vertDistance: 40
*form.arm_sts.width: 215
*form.arm_sts.foreground: blue
*form.arm_sts.label: 

*form.err_clr.fromHoriz: arm_sts
*form.err_clr.horizDistance: 5
*form.err_clr.fromVert: num_lbl
*form.err_clr.vertDistance: 40
*form.err_clr.width: 95
*form.err_clr.background: pink
*form.err_clr.label: エラークリア

*form.err_lbl.borderWidth: 0
*form.err_lbl.horizDistance: 0
*form.err_lbl.fromVert: arm_lbl
*form.err_lbl.vertDistance: 10
*form.err_lbl.width: 100
*form.err_lbl.label: エラー番号

*form.err_num.fromHoriz: err_lbl
*form.err_num.horizDistance: 0
*form.err_num.fromVert: arm_sts
*form.err_num.vertDistance: 10
*form.err_num.width: 390
*form.err_num.foreground: red
*form.err_num.label: 

*form.restart_btn.fromHoriz: err_clr
*form.restart_btn.horizDistance: 5
*form.restart_btn.fromVert: num_lbl
*form.restart_btn.vertDistance: 40
*form.restart_btn.label: 通信再開
*form.restart_btn.borderWidth: 0
*form.restart_btn.background: purple
*form.restart_btn.width: 70

!終了
*form.quit.fromHoriz: restart_btn
*form.quit.horizDistance: 50
*form.quit.fromVert: rep_stp
*form.quit.vertDistance: 40
*form.quit.label: ESC:終了
*form.quit.background: lightcyan
*form.quit.width: 200
*form.quit.height: 40

!ベース座標系
*form.label6.fromHoriz: label4
*form.label6.horizDistance: 60
*form.label6.fromVert: title
*form.label6.vertDistance: 10
*form.label6.label: ベース座標系
*form.label6.borderWidth: 0
*form.label6.background: lightgreen

*form.dX_lbl.fromHoriz: deg1
*form.dX_lbl.horizDistance: 20
*form.dX_lbl.fromVert: label6
*form.dX_lbl.vertDistance: 5
*form.dX_lbl.borderWidth: 0
*form.dX_lbl.label:  ΔX

*form.dY_lbl.fromHoriz: deg1
*form.dY_lbl.horizDistance: 20
*form.dY_lbl.fromVert: dX_lbl
*form.dY_lbl.vertDistance: 15
*form.dY_lbl.borderWidth: 0
*form.dY_lbl.label: ΔY

*form.dZ_lbl.fromHoriz: deg1
*form.dZ_lbl.horizDistance: 20
*form.dZ_lbl.fromVert: dY_lbl
*form.dZ_lbl.vertDistance: 15
*form.dZ_lbl.borderWidth: 0
*form.dZ_lbl.label: ΔZ

*form.dYaw_lbl.fromHoriz: deg1
*form.dYaw_lbl.horizDistance: 20
*form.dYaw_lbl.fromVert: dZ_lbl
*form.dYaw_lbl.vertDistance: 40
*form.dYaw_lbl.borderWidth: 0
*form.dYaw_lbl.label: ΔYaw

*form.dPitch_lbl.fromHoriz: deg1
*form.dPitch_lbl.horizDistance: 20
*form.dPitch_lbl.fromVert: dYaw_lbl
*form.dPitch_lbl.vertDistance: 15
*form.dPitch_lbl.borderWidth: 0
*form.dPitch_lbl.label: ΔPitch

*form.dRoll_lbl.fromHoriz: deg1
*form.dRoll_lbl.horizDistance: 20
*form.dRoll_lbl.fromVert: dPitch_lbl
*form.dRoll_lbl.vertDistance: 15
*form.dRoll_lbl.borderWidth: 0
*form.dRoll_lbl.label: ΔRoll

*form.dX.fromHoriz: dX_lbl
*form.dX.horizDistance: 30
*form.dX.fromVert: label6
*form.dX.vertDistance: 5
*form.dX.width: 60
*form.dX.scrollHorizontal: never

*form.dY.fromHoriz: dX_lbl
*form.dY.horizDistance: 30
*form.dY.fromVert: dX
*form.dY.vertDistance: 12
*form.dY.width: 60
*form.dY.scrollHorizontal: never

*form.dZ.fromHoriz: dX_lbl
*form.dZ.horizDistance: 30
*form.dZ.fromVert: dY
*form.dZ.vertDistance: 12
*form.dZ.width: 60
*form.dZ.scrollHorizontal: never

*form.dYaw.fromHoriz: dX_lbl
*form.dYaw.horizDistance: 30
*form.dYaw.fromVert: dZ
*form.dYaw.vertDistance: 40
*form.dYaw.width: 60
*form.dYaw.scrollHorizontal: never

*form.dPitch.fromHoriz: dX_lbl
*form.dPitch.horizDistance: 30
*form.dPitch.fromVert: dYaw
*form.dPitch.vertDistance: 12
*form.dPitch.width: 60
*form.dPitch.scrollHorizontal: never

*form.dRoll.fromHoriz: dX_lbl
*form.dRoll.horizDistance: 30
*form.dRoll.fromVert: dPitch
*form.dRoll.vertDistance: 12
*form.dRoll.width: 60
*form.dRoll.scrollHorizontal: never

*form.mov_xyz.fromHoriz: dX
*form.mov_xyz.horizDistance: 10
*form.mov_xyz.fromVert: label6
*form.mov_xyz.vertDistance: 5
*form.mov_xyz.label: 位置偏差\n移動します
*form.mov_xyz.width: 80
*form.mov_xyz.height: 80

*form.mov_ypr.fromHoriz: dX
*form.mov_ypr.horizDistance: 10
*form.mov_ypr.fromVert: mov_xyz
*form.mov_ypr.vertDistance: 45
*form.mov_ypr.label: 姿勢偏差\n移動します
*form.mov_ypr.width: 80
*form.mov_ypr.height: 80

!現在位置／現在姿勢
*form.label7.fromHoriz: mov_xyz
*form.label7.horizDistance: 30
*form.label7.fromVert: title
*form.label7.vertDistance: 10
*form.label7.label: 現在位置
*form.label7.borderWidth: 0
*form.label7.background: lightgreen

*form.rx_val.background: grey
*form.rx_val.borderWidth: 0
*form.rx_val.justify: right
*form.rx_val.width: 60
*form.rx_val.fromHoriz: mov_xyz
*form.rx_val.horizDistance: 30
*form.rx_val.fromVert: label7
*form.rx_val.vertDistance: 5
*form.rx_val.label: 0.0

*form.ry_val.background: grey
*form.ry_val.borderWidth: 0
*form.ry_val.justify: right
*form.ry_val.width: 60
*form.ry_val.fromHoriz: mov_xyz
*form.ry_val.horizDistance: 30
*form.ry_val.fromVert: rx_val
*form.ry_val.vertDistance: 14
*form.ry_val.label: 0.0

*form.rz_val.background: grey
*form.rz_val.borderWidth: 0
*form.rz_val.justify: right
*form.rz_val.width: 60
*form.rz_val.fromHoriz: mov_xyz
*form.rz_val.horizDistance: 30
*form.rz_val.fromVert: ry_val
*form.rz_val.vertDistance: 14
*form.rz_val.label: 0.0

*form.label8.fromHoriz: mov_xyz
*form.label8.horizDistance: 30
*form.label8.fromVert: rz_val
*form.label8.vertDistance: 20
*form.label8.label: 現在姿勢
*form.label8.borderWidth: 0
*form.label8.background: lightgreen

*form.yaw_val.background: grey
*form.yaw_val.borderWidth: 0
*form.yaw_val.justify: right
*form.yaw_val.width: 60
*form.yaw_val.fromHoriz: mov_xyz
*form.yaw_val.horizDistance: 30
*form.yaw_val.fromVert: label8
*form.yaw_val.vertDistance: 5
*form.yaw_val.label: 0.0

*form.pitch_val.background: grey
*form.pitch_val.borderWidth: 0
*form.pitch_val.justify: right
*form.pitch_val.width: 60
*form.pitch_val.fromHoriz: mov_xyz
*form.pitch_val.horizDistance: 30
*form.pitch_val.fromVert: yaw_val
*form.pitch_val.vertDistance: 14
*form.pitch_val.label: 0.0

*form.roll_val.background: grey
*form.roll_val.borderWidth: 0
*form.roll_val.justify: right
*form.roll_val.width: 60
*form.roll_val.fromHoriz: mov_xyz
*form.roll_val.horizDistance: 30
*form.roll_val.fromVert: pitch_val
*form.roll_val.vertDistance: 14
*form.roll_val.label: 0.0

*form.mm1.fromHoriz: rx_val
*form.mm1.horizDistance: 3
*form.mm1.fromVert: label7
*form.mm1.vertDistance: 5
*form.mm1.borderWidth: 0
*form.mm1.label: mm

*form.mm2.fromHoriz: ry_val
*form.mm2.horizDistance: 3
*form.mm2.fromVert: mm1
*form.mm2.vertDistance: 14
*form.mm2.borderWidth: 0
*form.mm2.label: mm

*form.mm3.fromHoriz: rz_val
*form.mm3.horizDistance: 3
*form.mm3.fromVert: mm2
*form.mm3.vertDistance: 14
*form.mm3.borderWidth: 0
*form.mm3.label: mm

*form.deg2_1.fromHoriz: yaw_val
*form.deg2_1.horizDistance: 3
*form.deg2_1.fromVert: label8
*form.deg2_1.vertDistance: 5
*form.deg2_1.borderWidth: 0
*form.deg2_1.label: deg

*form.deg2_2.fromHoriz: pitch_val
*form.deg2_2.horizDistance: 3
*form.deg2_2.fromVert: deg2_1
*form.deg2_2.vertDistance: 14
*form.deg2_2.borderWidth: 0
*form.deg2_2.label: deg

*form.deg2_3.fromHoriz: roll_val
*form.deg2_3.horizDistance: 3
*form.deg2_3.fromVert: deg2_2
*form.deg2_3.vertDistance: 14
*form.deg2_3.borderWidth: 0
*form.deg2_3.label: deg

!カレントポイント関係
*form.cur_top.fromHoriz: ref_fil
*form.cur_top.horizDistance: 80
*form.cur_top.fromVert: dRoll_lbl
*form.cur_top.vertDistance: 20
*form.cur_top.label: 先頭
*form.cur_top.foreground: blue

*form.cur_next.fromHoriz: cur_top
*form.cur_next.horizDistance: 0
*form.cur_next.fromVert: dRoll_lbl
*form.cur_next.vertDistance: 20
*form.cur_next.label: 次の点
*form.cur_next.foreground: blue

*form.cur_bottom.fromHoriz: cur_next
*form.cur_bottom.horizDistance: 0
*form.cur_bottom.fromVert: dRoll_lbl
*form.cur_bottom.vertDistance: 20
*form.cur_bottom.label: 最後
*form.cur_bottom.foreground: blue

*form.chg_pnt.fromHoriz: ref_fil
*form.chg_pnt.horizDistance: 60
*form.chg_pnt.fromVert: cur_top
*form.chg_pnt.vertDistance: 5
*form.chg_pnt.label: カレントポイント変更

*form.add_pnt1.fromHoriz: ref_fil
*form.add_pnt1.horizDistance: 30
*form.add_pnt1.fromVert: chg_pnt
*form.add_pnt1.vertDistance: 20
*form.add_pnt1.label: PTP(各軸)読込
*form.add_pnt1.width: 105

*form.add_pnt2.fromHoriz: ref_fil
*form.add_pnt2.horizDistance: 30
*form.add_pnt2.fromVert: add_pnt1
*form.add_pnt2.vertDistance: 10
*form.add_pnt2.label: PTP(直線)読込
*form.add_pnt2.width: 105

*form.del_pnt1.fromHoriz: add_pnt1
*form.del_pnt1.horizDistance: 5
*form.del_pnt1.fromVert: chg_pnt
*form.del_pnt1.vertDistance: 20
*form.del_pnt1.label: カレントデータ削除
*form.del_pnt1.width: 140

*form.del_pnt2.fromHoriz: add_pnt1
*form.del_pnt2.horizDistance: 5
*form.del_pnt2.fromVert: del_pnt1
*form.del_pnt2.vertDistance: 10
*form.del_pnt2.label: 全データ削除
*form.del_pnt2.width: 140

!タイマ確認用
*form.tim_lbl.horizDistance: 0
*form.tim_lbl.borderWidth: 0
*form.tim_lbl.label: *
