//ANSI C
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//X Toolkit
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>

//Athena Widget
#include <X11/Xaw3d/Toggle.h>
#include <X11/Xaw3d/Text.h>

//PA
#include <pa.h>
#include <paerr.h>
#include "Resource.h"
#include "arm.h"

static Boolean	RepeatMode=False;
static long	PA_count;
static void	SetRepeat(void);
static void	ResetRepeat(void);

//終了
void QuitProc(Widget w,caddr_t clntd,caddr_t calld)
{
	ResetRepeat();
	if(Dialog(
		"プログラムの終了",
		"プログラムを終了しますか？"
		)==DIALOG_YES){
		exit(0);
	}
}

//基本姿勢移動
void HomProc(Widget w,caddr_t clntd,caddr_t calld)
{
	ResetRepeat();
	if(Dialog(
		"基本姿勢に移動",
		"基本姿勢に移動しますか？"
		)==DIALOG_YES){
		pa_exe_hom(Res.armNo,WM_NOWAIT);
	}
}
//退避姿勢移動
void EscProc(Widget w,caddr_t clntd,caddr_t calld)
{
	ResetRepeat();
	if(Dialog(
		"退避姿勢に移動",
		"退避姿勢に移動しますか？"
		)==DIALOG_YES){
		pa_exe_esc(Res.armNo,WM_NOWAIT);
	}
}
//安全姿勢移動
void SafProc(Widget w,caddr_t clntd,caddr_t calld)
{
	ResetRepeat();
	if(Dialog(
		"安全姿勢に移動",
		"安全姿勢に移動しますか？"
		)==DIALOG_YES){
		pa_exe_saf(Res.armNo,WM_NOWAIT);
	}
}
//全軸ブレーキ
void BrkProc(Widget w,caddr_t clntd,caddr_t calld)
{
	ResetRepeat();
	pa_stp_arm(Res.armNo,WM_NOWAIT);
}
//エラークリア
void RstProc(Widget w,caddr_t clntd,caddr_t calld)
{
	pa_rst_ctl(Res.armNo);
}

//全軸選択
void AllAxsProc(Widget w,caddr_t clntd,caddr_t calld)
{
	Arg	args[10];
	int	i;

	//トグルボタンをすべてONに
	i=0;
	XtSetArg(args[i],XtNstate,True);	i++;
	XtSetValues(AppObj.s1_axs,args,i);
	XtSetValues(AppObj.s2_axs,args,i);
	XtSetValues(AppObj.s3_axs,args,i);
	XtSetValues(AppObj.e1_axs,args,i);
	XtSetValues(AppObj.e2_axs,args,i);
	XtSetValues(AppObj.w1_axs,args,i);
	XtSetValues(AppObj.w2_axs,args,i);
}
//全軸解除
void NonAxsProc(Widget w,caddr_t clntd,caddr_t calld)
{
	Arg	args[10];
	int	i;

	//トグルボタンをすべてOFFに
	i=0;
	XtSetArg(args[i],XtNstate,False);	i++;
	XtSetValues(AppObj.s1_axs,args,i);
	XtSetValues(AppObj.s2_axs,args,i);
	XtSetValues(AppObj.s3_axs,args,i);
	XtSetValues(AppObj.e1_axs,args,i);
	XtSetValues(AppObj.e2_axs,args,i);
	XtSetValues(AppObj.w1_axs,args,i);
	XtSetValues(AppObj.w2_axs,args,i);
}

//選択されている軸を得る
static AXIS GetSelectAxis()
{
	Arg	args[10];
	int	i;
	Boolean	state;
	AXIS	axis=0;
	i=0;
	XtSetArg(args[i],XtNstate,&state);	i++;
	XtGetValues(AppObj.s1_axs,args,i);
	if(state)	axis|=S1;
	XtGetValues(AppObj.s2_axs,args,i);
	if(state)	axis|=S2;
	XtGetValues(AppObj.s3_axs,args,i);
	if(state)	axis|=S3;
	XtGetValues(AppObj.e1_axs,args,i);
	if(state)	axis|=E1;
	XtGetValues(AppObj.e2_axs,args,i);
	if(state)	axis|=E2;
	XtGetValues(AppObj.w1_axs,args,i);
	if(state)	axis|=W1;
	XtGetValues(AppObj.w2_axs,args,i);
	if(state)	axis|=W2;

	return axis;
}

//各軸サーボON
void SvoProc(Widget w,caddr_t clntd,caddr_t calld)
{
	AXIS	axis;
	ANGLE	angle;

	if(Dialog(
		"サーボロック",
		"サーボロックしますか？"
		)==DIALOG_YES){
		axis=GetSelectAxis();
		if(axis==0)
			printf("Warning : No axis selected!\n");
	
		pa_get_agl(Res.armNo,&angle);	//現在角度
		pa_exe_axs(Res.armNo,axis,&angle,WM_NOWAIT);
	}
}

//TURBOかどうかを得る
static Boolean IsTurboMode()
{
	Arg	args[10];
	int	i;
	Boolean	state;
	i=0;
	XtSetArg(args[i],XtNstate,&state);	i++;
	XtGetValues(AppObj.turbo,args,i);
	return state;
}

//UP/DOWN
void UpProc(Widget w,caddr_t clntd,caddr_t calld)
{
	AXIS	axis;
	ANGLE	angle;
	float	dest;

	ResetRepeat();

	if((axis=GetSelectAxis())==0){
		printf("Warning : No axis selected!\n");
		return;
	}

	if(IsTurboMode())
		dest=(float)Res.armDestTurbo;
	else
		dest=(float)Res.armDest;

	pa_get_agl(Res.armNo,&angle);	//現在角度

	angle.s1+=dest;
	angle.s2+=dest;
	angle.s3+=dest;
	angle.e1+=dest;
	angle.e2+=dest;
	angle.w1+=dest;
	angle.w2+=dest;

	pa_exe_axs(Res.armNo,axis,&angle,WM_NOWAIT);
}
void DownProc(Widget w,caddr_t clntd,caddr_t calld)
{
	AXIS	axis;
	ANGLE	angle;
	float	dest;

	ResetRepeat();

	if((axis=GetSelectAxis())==0){
		printf("Warning : No axis selected!\n");
		return;
	}

	if(IsTurboMode())
		dest=(float)Res.armDestTurbo;
	else
		dest=(float)Res.armDest;

	pa_get_agl(Res.armNo,&angle);	//現在角度

	angle.s1-=dest;
	angle.s2-=dest;
	angle.s3-=dest;
	angle.e1-=dest;
	angle.e2-=dest;
	angle.w1-=dest;
	angle.w2-=dest;

	pa_exe_axs(Res.armNo,axis,&angle,WM_NOWAIT);
}

//各軸値の表示
static void SetAxis(Widget w,float value)
{
	char	buffer[80];
	Arg	args[10];
	int	i=0;
	sprintf(buffer,Res.valueFormat,value*180.0/M_PI);
	XtSetArg(args[i],XtNlabel,buffer);	i++;
	XtSetValues(w,args,i);
}

//現在位置の表示
static void SetXyzYpr(Widget w,float value)
{
	char	buffer[80];
	Arg	args[10];
	int	i=0;
	sprintf(buffer,Res.valueFormat,value);
	XtSetArg(args[i],XtNlabel,buffer);	i++;
	XtSetValues(w,args,i);
}

//アーム状態の表示
static void SetStatus(String str)
{
	Arg	args[10];
	int	i;
	Dimension	width;

	//ラベルウイジェットのサイズの補償
	i=0;
	XtSetArg(args[i],XtNwidth,&width);	i++;
	XtGetValues(AppObj.arm_sts,args,i);

	i=0;
	XtSetArg(args[i],XtNlabel,str);	i++;
	XtSetArg(args[i],XtNwidth,width);	i++;
	XtSetValues(AppObj.arm_sts,args,i);
}
//エラー番号
static void SetError(String str)
{
	Arg	args[10];
	int	i;
	Dimension	width;

	//ラベルウイジェットのサイズの補償
	i=0;
	XtSetArg(args[i],XtNwidth,&width);	i++;
	XtGetValues(AppObj.err_num,args,i);

	i=0;
	XtSetArg(args[i],XtNlabel,str);	i++;
	XtSetArg(args[i],XtNwidth,width);	i++;
	XtSetValues(AppObj.err_num,args,i);
}

//カレントポイント表示
static void SetCur(long cur)
{
	char	buffer[80];
	Arg	args[10];
	int	i;
	Dimension	width;

	sprintf(buffer,"%d",cur);

	//ラベルウイジェットのサイズの補償
	i=0;
	XtSetArg(args[i],XtNwidth,&width);	i++;
	XtGetValues(AppObj.cur_pnt,args,i);

	i=0;
	XtSetArg(args[i],XtNlabel,buffer);	i++;
	XtSetArg(args[i],XtNwidth,width);	i++;
	XtSetValues(AppObj.cur_pnt,args,i);
}
//総数表示
static void SetNum(long num)
{
	char	buffer[80];
	Arg	args[10];
	int	i;
	Dimension	width;

	sprintf(buffer,"%d",num);

	//ラベルウイジェットのサイズの補償
	i=0;
	XtSetArg(args[i],XtNwidth,&width);	i++;
	XtGetValues(AppObj.num_pnt,args,i);

	i=0;
	XtSetArg(args[i],XtNlabel,buffer);	i++;
	XtSetArg(args[i],XtNwidth,width);	i++;
	XtSetValues(AppObj.num_pnt,args,i);
}

//カレントポイント移動(各軸)
void AxsPntProc(Widget w,caddr_t clntd,caddr_t calld)
{
	if(PntNum<1)	return;
	if(Dialog(
		"カレントポイントに移動",
		"カレントポイントに各軸移動しますか？"
		)==DIALOG_YES){
		pa_axs_pnt(Res.armNo,WM_NOWAIT);
	}
}
//カレントポイント移動(直線)
void MovPntProc(Widget w,caddr_t clntd,caddr_t calld)
{
	if(PntNum<1)	return;
	if(Dialog(
		"カレントポイントに移動",
		"カレントポイントに直線移動しますか？"
		)==DIALOG_YES){
		pa_mov_pnt(Res.armNo,WM_NOWAIT);
	}
}
//連続運転開始
static void SetRepeat()
{
	RepeatMode=True;
}
static void ResetRepeat()
{
	if(RepeatMode==True){
		printf("RepeatMode STOPPED\n");
		RepeatMode=False;
		pa_sus_arm(Res.armNo,WM_NOWAIT);
	}
}

void PlyPntProc(Widget w,caddr_t clntd,caddr_t calld)
{
	if(PntNum<1)	return;
	if(Dialog(
		"連続運転開始",
		"連続運転を開始します よろしいですか？"
		)==DIALOG_YES){
		pa_get_cnt(Res.armNo,&PA_count);
		if(CurPnt<2){
		//	pa_ena_jmp(Res.armNo,JMP_OFF);
			pa_ply_pnt(Res.armNo,PB_FORE,1,WM_NOWAIT);
		}else{
		//	pa_ena_jmp(Res.armNo,JMP_OFF);
			pa_ply_pnt(Res.armNo,PB_BACK,1,WM_NOWAIT);
		}
		SetRepeat();
	}
}
//連続運転停止
void RepStpProc(Widget w,caddr_t clntd,caddr_t calld)
{
	ResetRepeat();
}

//教示データ読み込み
void ReadFileProc(Widget w,caddr_t clntd,caddr_t calld)
{
	Arg	args[10];
	int	i;
	String	filename;

	i=0;
	XtSetArg(args[i],XtNstring,&filename);	i++;
	XtGetValues(AppObj.fil_txt,args,i);

	pa_lod_pnt(Res.armNo,1,(char*)filename);
}
//教示データ書き込み
void WriteFileProc(Widget w,caddr_t clntd,caddr_t calld)
{
	Arg	args[10];
	int	i;
	String	filename;

	if(PntNum<1)	return;
	i=0;
	XtSetArg(args[i],XtNstring,&filename);	i++;
	XtGetValues(AppObj.fil_txt,args,i);
printf("save:%s\n",filename);
	pa_sav_pnt(Res.armNo,(char*)filename);
}
//ファイル名テキストに文字列をセット(参照用)
void SetFileName(String file)
{
	Arg	args[10];
	int	i=0;
	String	filename;
	XawTextBlock	blk;

	blk.firstPos=0;
	blk.length=strlen(file);
	blk.ptr=file;
	blk.format=0;

	//変更前の現在のファイル名を得る
	XtSetArg(args[i],XtNstring,&filename);	i++;
	XtGetValues(AppObj.fil_txt,args,i);

	//テキスト置換
	XawTextReplace(AppObj.fil_txt,0,strlen(filename),&blk);
}

//ファイル参照
void RefFileProc(Widget w,caddr_t clntd,caddr_t calld)
{
	Filer();
}

//アップデート
static void UpdateArm(void)
{
	pa_get_sts(Res.armNo,&ArmStatus);
	pa_get_mod(Res.armNo,&ArmMode);
	pa_get_cur(Res.armNo,&CurPnt);
	pa_get_num(Res.armNo,&PntNum);
}
static void UpdateScreen(void)
{
	char	message[255];

//MATRIX	noap;
//long	cnt;
	//軸値の更新
	if(OldArmStatus.angle.s1!=ArmStatus.angle.s1)
		SetAxis(AppObj.s1_val,ArmStatus.angle.s1);
	if(OldArmStatus.angle.s2!=ArmStatus.angle.s2)
		SetAxis(AppObj.s2_val,ArmStatus.angle.s2);
	if(OldArmStatus.angle.s3!=ArmStatus.angle.s3)
		SetAxis(AppObj.s3_val,ArmStatus.angle.s3);
	if(OldArmStatus.angle.e1!=ArmStatus.angle.e1)
		SetAxis(AppObj.e1_val,ArmStatus.angle.e1);
	if(OldArmStatus.angle.e2!=ArmStatus.angle.e2)
		SetAxis(AppObj.e2_val,ArmStatus.angle.e2);
	if(OldArmStatus.angle.w1!=ArmStatus.angle.w1)
		SetAxis(AppObj.w1_val,ArmStatus.angle.w1);
	if(OldArmStatus.angle.w2!=ArmStatus.angle.w2)
		SetAxis(AppObj.w2_val,ArmStatus.angle.w2);

	//現在位置の更新
	if(OldArmStatus.noap[0][3]!=ArmStatus.noap[0][3])
		SetXyzYpr(AppObj.rx_val,ArmStatus.noap[0][3]);
	if(OldArmStatus.noap[1][3]!=ArmStatus.noap[1][3])
		SetXyzYpr(AppObj.ry_val,ArmStatus.noap[1][3]);
	if(OldArmStatus.noap[2][3]!=ArmStatus.noap[2][3])
		SetXyzYpr(AppObj.rz_val,ArmStatus.noap[2][3]);

	//現在姿勢の更新
	if(OldArmStatus.ypr[0]!=ArmStatus.ypr[0])
		SetXyzYpr(AppObj.yaw_val,ArmStatus.ypr[0]);
	if(OldArmStatus.ypr[1]!=ArmStatus.ypr[1])
		SetXyzYpr(AppObj.pitch_val,ArmStatus.ypr[1]);
	if(OldArmStatus.ypr[2]!=ArmStatus.ypr[2])
		SetXyzYpr(AppObj.roll_val,ArmStatus.ypr[2]);

	//アーム状態の表示
	if(OldArmMode!=ArmMode){
	/*	pa_get_mes(ArmMode,message);	*/
		switch(ArmMode){
		case  1:SetStatus("--------------------------(1)");	break;
		case  2:SetStatus("--------------------------(2)");	break;
		case  3:SetStatus("ブレーキ停止状態          (3)");	break;
		case  4:SetStatus("--------------------------(4)");	break;
		case  5:SetStatus("--------------------------(5)");	break;
		case  6:SetStatus("--------------------------(6)");	break;
		case  7:SetStatus("--------------------------(7)");	break;
		case  8:SetStatus("各軸角度制御状態          (8)");	break;
		case  9:SetStatus("各軸速度制御状態          (9)");	break;
		case 10:SetStatus("サーボロック状態         (10)");	break;
		case 11:SetStatus("簡易自重補償状態         (11)");	break;
		case 12:SetStatus("自重補償状態             (12)");	break;
		case 13:SetStatus("RMRC制御状態             (13)");	break;
		case 14:SetStatus("RMRC冗長軸補正状態       (14)");	break;
		case 15:SetStatus("各軸制御サーボロック状態 (15)");	break;
		case 16:SetStatus("CPデータプレイバック状態 (16)");	break;
		case 17:SetStatus("各軸軸位置補正状態       (17)");	break;
		case 18:SetStatus("プレイバック円補間状態   (18)");	break;
		case 19:SetStatus("プレイバック直線補間状態 (19)");	break;
		case 20:SetStatus("プレイバック円弧補間状態 (20)");	break;
		case 21:SetStatus("RMRC制御サーボロック状態 (21)");	break;
		case 22:SetStatus("プレイバック開始待状態   (22)");	break;
		case 23:SetStatus("各軸制御サーボロック状態 (23)");	break;
		case 24:SetStatus("RMRC制御サーボロック状態 (24)");	break;
		case 25:SetStatus("プレイバック開始待状態   (25)");	break;
		case 26:SetStatus("プレイバック開始待状態   (26)");	break;
		case 27:SetStatus("冗長軸制御状態           (27)");	break;
		case 28:SetStatus("RMRCリアル制御状態       (28)");	break;
		case 29:SetStatus("プレイバック各軸補間状態 (29)");	break;
		case 30:SetStatus("座標変換位置補正状態     (30)");	break;
		case 31:SetStatus("冗長軸S３補間制御状態    (31)");	break;
		case 32:SetStatus("各軸リアル制御状態       (32)");	break;
		default:SetStatus("不明な状態               (**)");	break;
		}
	}
	//エラー番号の表示
	if(OldArmStatus.error!=ArmStatus.error){
		pa_err_mes(ArmStatus.error,message);
		SetError(message);
	}
	//カレントポイント
	if(OldCurPnt!=CurPnt){
		SetCur(CurPnt);
	}
	//総数
	if(OldPntNum!=PntNum){
		SetNum(PntNum);
	}


//pa_get_cnp(Res.armNo,&cnt,noap);
//printf("%d \n",cnt);

}

//タイマルーチン
void TimerProc(Widget w,XtIntervalId *id)
{
	static Boolean flag=False;
	Arg	args[1];
	int	i;
	i=0;
	if(flag==False){
		XtSetArg(args[i],XtNforeground,Res.intervalFg1);	i++;
		flag=True;
	}else{
		XtSetArg(args[i],XtNforeground,Res.intervalFg2);	i++;
		flag=False;
	}
	XtSetValues(w,args,i);

	UpdateArm();				//アーム情報更新

	UpdateScreen();				//表示更新

	if(RepeatMode==True){			//連続運転中?
		if(ArmStatus.error==ERR_OK){	//エラーが無ければ
			long	cnt;
			pa_get_cnt(Res.armNo,&cnt);
			if(PA_count!=cnt){	//動作終了
				pa_get_cnt(Res.armNo,&PA_count);
				if(CurPnt<2){
					pa_ena_jmp(Res.armNo,JMP_OFF);
					pa_ply_pnt(Res.armNo,PB_FORE,1,WM_NOWAIT);
				}else{
					pa_ena_jmp(Res.armNo,JMP_OFF);
					pa_ply_pnt(Res.armNo,PB_BACK,1,WM_NOWAIT);
				}
			}
		}else{
			ResetRepeat();
		}
	}else{
		ResetRepeat();
	}

	//値の更新
	memcpy(&OldArmStatus,&ArmStatus,sizeof(ARMSTATUS));
	OldArmMode=ArmMode;
	OldCurPnt=CurPnt;
	OldPntNum=PntNum;

	XtAppAddTimeOut(
		AppObj.AppContext,
		Res.interval,
		(XtTimerCallbackProc)TimerProc,w);
}

//通信再開
void RestartProc(Widget w,caddr_t clntd,caddr_t calld)
{
	long	com;

	ResetRepeat();

	pa_get_com(Res.armNo,&com);
	switch(com){
	case	1:
		pa_sta_arm(Res.armNo);
		break;
	case	2:
		pa_sta_sim(Res.armNo);
		break;
	}
}

//位置偏差
void MovXyzProc(Widget w,caddr_t clntd,caddr_t calld)
{
	Arg	args[10];
	String	values;
	char	buffer[80];
	XawTextBlock	blk;
	float	dx,dy,dz;

	ResetRepeat();

	blk.firstPos=0;
	blk.ptr=buffer;
	blk.format=0;

	//入力値をフォーマット(文字列数値→変換)
	XtSetArg(args[0],XtNstring,&values);
	XtGetValues(AppObj.dX,args,1);
	dx=atof(values);
	sprintf(buffer,Res.valueDFormat,dx);
	blk.length=strlen(buffer);
	XawTextReplace(AppObj.dX,0,strlen(values),&blk);
	//カレットを最終位置に
	XawTextSetInsertionPoint(AppObj.dX,strlen(values));

	XtGetValues(AppObj.dY,args,1);
	dy=atof(values);
	sprintf(buffer,Res.valueDFormat,dy);
	blk.length=strlen(buffer);
	XawTextReplace(AppObj.dY,0,strlen(values),&blk);
	XawTextSetInsertionPoint(AppObj.dY,strlen(values));

	XtGetValues(AppObj.dZ,args,1);
	dz=atof(values);
	sprintf(buffer,Res.valueDFormat,dz);
	blk.length=strlen(buffer);
	XawTextReplace(AppObj.dZ,0,strlen(values),&blk);
	XawTextSetInsertionPoint(AppObj.dZ,strlen(values));

	if(Dialog(
		"位置偏差移動",
		"入力値に移動しますか？"
		)==DIALOG_YES){
		pa_mov_XYZ(Res.armNo,dx,dy,dz,WM_NOWAIT);
	}
}
//姿勢偏差
void MovYprProc(Widget w,caddr_t clntd,caddr_t calld)
{
	Arg	args[10];
	String	values;
	char	buffer[80];
	XawTextBlock	blk;
	float	dyaw,dpitch,droll;

	ResetRepeat();

	blk.firstPos=0;
	blk.ptr=buffer;
	blk.format=0;

	//入力値をフォーマット(文字列数値→変換)
	XtSetArg(args[0],XtNstring,&values);
	XtGetValues(AppObj.dYaw,args,1);
	dyaw=atof(values);
	sprintf(buffer,Res.valueDFormat,dyaw);
	blk.length=strlen(buffer);
	XawTextReplace(AppObj.dYaw,0,strlen(values),&blk);
	XawTextSetInsertionPoint(AppObj.dYaw,strlen(values));

	XtGetValues(AppObj.dPitch,args,1);
	dpitch=atof(values);
	sprintf(buffer,Res.valueDFormat,dpitch);
	blk.length=strlen(buffer);
	XawTextReplace(AppObj.dPitch,0,strlen(values),&blk);
	XawTextSetInsertionPoint(AppObj.dPitch,strlen(values));

	XtGetValues(AppObj.dRoll,args,1);
	droll=atof(values);
	sprintf(buffer,Res.valueDFormat,droll);
	blk.length=strlen(buffer);
	XawTextReplace(AppObj.dRoll,0,strlen(values),&blk);
	XawTextSetInsertionPoint(AppObj.dRoll,strlen(values));

	if(Dialog(
		"姿勢偏差移動",
		"入力値に移動しますか？"
		)==DIALOG_YES){
	/*
		pa_mov_YPR(Res.armNo,dyaw,dpitch,droll,WM_NOWAIT);
	*/
		pa_mov_YPR(Res.armNo,(dyaw*3.141592)/180.0,
				     (dpitch*3.141592)/180.0,
				     (droll*3.141592)/180.0,WM_NOWAIT);
	}
}

//カレントポイント変更
void ChgPntProc(Widget w,caddr_t clntd,caddr_t calld)
{
//	Widget current=XawToggleGetCurrent(AppObj.cur_top);
	Boolean	state;
	Arg	args[10];
	XtSetArg(args[0],XtNstate,&state);
	XtGetValues(AppObj.cur_top,args,1);
	if(state==True)
		pa_chg_pnt(Res.armNo,PM_TOP,0);
	XtGetValues(AppObj.cur_next,args,1);
	if(state==True)
		pa_chg_pnt(Res.armNo,PM_NEXT,0);
	XtGetValues(AppObj.cur_bottom,args,1);
	if(state==True)
		pa_chg_pnt(Res.armNo,PM_BTM,0);
}
//PTP読み込み(ポイント追加)
void AddPntProc1(Widget w,caddr_t clntd,caddr_t calld)
{
	if(Dialog(
		"PTP(各軸)読込",
		"教示点追加します よろしいですか？"
		)==DIALOG_YES){
		pa_add_pnt(Res.armNo,PT_AXS);
	}
}
void AddPntProc2(Widget w,caddr_t clntd,caddr_t calld)
{
	if(Dialog(
		"PTP(直線)読込",
		"教示点追加します よろしいですか？"
		)==DIALOG_YES){
		pa_add_pnt(Res.armNo,PT_PTP);
	}
}
//ポイントデータ削除
void DelPntProc1(Widget w,caddr_t clntd,caddr_t calld)
{
	if(PntNum<1)	return;
	if(Dialog(
		"カレントデータ削除",
		"教示点削除します よろしいですか？"
		)==DIALOG_YES){
		pa_del_pnt(Res.armNo,PD_CUR);
	}
}
void DelPntProc2(Widget w,caddr_t clntd,caddr_t calld)
{
	if(PntNum<1)	return;
	if(Dialog(
		"全データ削除",
		"教示点削除します よろしいですか？"
		)==DIALOG_YES){
		pa_del_pnt(Res.armNo,PD_ALL);
	}
}

