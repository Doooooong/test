#define	SAMPLE_MAIN

#define	SAMPLE_VERSION	"0.03"

//ANSI C
#include <iostream>
#include <iomanip>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>

//PA
#include <pa10/pa.h>
#include <pa10/paerr.h>

#include <stdint.h>

union uni_test
{
	struct
	{
		long aa;
		long bb;
	};
	unsigned char buff[20];

}test;
/*

std::cout << "size uint32_t:" << sizeof(uint32_t) << std::endl;
std::cout << "size SHORT:" << sizeof(short) << std::endl;
std::cout << "size INT:" << sizeof(int) << std::endl;
std::cout << "size LONG:" << sizeof(long) << std::endl;
std::cout << "size LONG LONG:" << sizeof(uni_test) << std::endl;

test.aa=0x0123456789101112;
test.bb=0x1314151617181920;
for(int i = 0; i < 20; i++)
	std::cout << std::to_string(i) << ": 0x" << std::hex << int(test.buff[i]) << std::endl; //std::setw(4) << std::hex << static_cast<int>

*/

//�ᥤ��
int main(int argc,char **argv)
{
	PARAM para;
int dummy;
	int err=pa_ini_sys();


	if(err<0){printf("pa_ini_sys() error %x\n",err);exit(err);}

	int armNo = 0;
	printf("arm=%d\n",armNo);

	err=pa_opn_arm(armNo);
	err=pa_sta_arm(armNo);
	
	if(err<0){printf("pa_sta_???() error %x\n",err);exit(err);}

	err=pa_rst_ctl(armNo);



std::cout << "1: Stop:" << std::endl;
std::cin >> dummy;



	ANGLE angle;
	std::cout << "+++++ move_here_pa10:start +++++" << std::endl;
	//Read axis
	err = pa_get_agl(ARM0,&angle);
	std::cout << "pa_get_agl" << " " << err << std::endl;
	//Control axis angle 
	err = pa_exe_axs(ARM0,AXISALL,&angle,WM_WAIT);




std::cout << "2: Stop:" << std::endl;
std::cin >> dummy;




err = pa_get_prm(ARM0, &para);
  std::cout << "pa_get_prm" << " " << err << std::endl;

std::cout<< "-- papci_params ---------------------------------------------------------------------------------------"<< std::endl;
  char ca_tmp[512];
  sprintf( ca_tmp, "vel(joint velocity limit)[rad/sec]   : [% 7.3f, % 7.3f, % 7.3f, % 7.3f, % 7.3f, % 7.3f, % 7.3f]",
		  para.vel[0],
		  para.vel[1],
		  para.vel[2],
		  para.vel[3],
		  para.vel[4],
		  para.vel[5],
		  para.vel[6]);
  std::cout<< ca_tmp<< std::endl;

  sprintf( ca_tmp, "pul(joint position upper limit)[rad] : [% 7.3f, % 7.3f, % 7.3f, % 7.3f, % 7.3f, % 7.3f, % 7.3f]",
		  para.pul[0],
		  para.pul[1],
		  para.pul[2],
		  para.pul[3],
		  para.pul[4],
		  para.pul[5],
		  para.pul[6]);
  std::cout<< ca_tmp<< std::endl;

  sprintf( ca_tmp, "pdl(joint position lower limit)[rad] : [% 7.3f, % 7.3f, % 7.3f, % 7.3f, % 7.3f, % 7.3f, % 7.3f]",
		  para.pdl[0],
		  para.pdl[1],
		  para.pdl[2],
		  para.pdl[3],
		  para.pdl[4],
		  para.pdl[5],
		  para.pdl[6]);
  std::cout<< ca_tmp<< std::endl;

  sprintf( ca_tmp, "arl(arm length)[mm]                  : [% 7.1f, % 7.1f, % 7.1f, % 7.1f, % 7.1f, % 7.1f, % 7.1f]",
		  para.arl[0],
		  para.arl[1],
		  para.arl[2],
		  para.arl[3],
		  para.arl[4],
		  para.arl[5],
		  para.arl[6]);
  std::cout<< ca_tmp<< std::endl;
  sprintf( ca_tmp, "tol(tool transform)[mm, rad?]        : [% 7.1f, % 7.1f, % 7.1f, % 7.4f, % 7.4f, % 7.4f]",
		  para.tol[0],
		  para.tol[1],
		  para.tol[2],
		  para.tol[3],
		  para.tol[4],
		  para.tol[5],
		  para.tol[6]);
  std::cout<< ca_tmp<< std::endl;





std::cout << "3: Stop:" << std::endl;
std::cin >> dummy;





int i;
  float j_vel[7];

  std::cout << "+++++ on_pa10:start        +++++" << std::endl;

  for(i=0;i<7;i++) j_vel[i] = 0.0;

  // Setting timeout([10ms])
  err=pa_set_tim(ARM0,100); // 100 means 100[10ms] = 1000[ms]
  std::cout << "pa_set_tim" << " " << err << std::endl;
  // Setting velocity control mode
  err=pa_mod_vel(ARM0,VM_ONE,S1|S2|S3|E1|E2|W1|W2);
  std::cout << "pa_mod_vel" << " " << err << std::endl;
  // Setting velocity control
  err=pa_odr_vel(ARM0,j_vel);
  std::cout << "pa_odr_vel" << " " << err << std::endl;
  std::cout << "+++++ on_pa10:end          +++++" << std::endl;


err = pa_mod_vel(ARM0 , VM_XYZYPR, 0);

		if(err != ERR_OK)
		{
			std::cout<< "error, cannot change mode to RMRC :"<< err<<std::endl;
			while(1);
		}

float vel_driver[6];
for(int i=0; i<7 ; i++)
	{
		 vel_driver[i]=0;
		 //std::cout << " " << vel_driver[i];
	}
	
	vel_driver[0] = 1;




std::cout << "!!!!!! ARM moving !!!!!!!" << std::endl;
std::cin >> dummy;





	// send order to pa10
	err = pa_odr_vel(ARM0, vel_driver);

	return(0);
}	

