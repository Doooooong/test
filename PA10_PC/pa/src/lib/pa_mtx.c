/*


	PAlib convert matrix function


*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	��ɸ�Ѵ����������
		����	armno		������No.
			mtx0		����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_set_mtx(ARM armno, MATRIX mtx0)
{
	short	j,k;
	u32	ldmy;
	u32	st;
	MATRIX	Imtx = {{ (float)1.0, (float)0.0, (float)0.0, (float)0.0 },
			{ (float)0.0, (float)1.0, (float)0.0, (float)0.0 },
			{ (float)0.0, (float)0.0, (float)1.0, (float)0.0 }};

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	SendMatrixEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	6;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	for(j=0;j<3;j++){
		st=pa_cpy_flt((u32)armno,&(_comuni->hd.wrk[0].xyz[j]),&(mtx0[j][3]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt((u32)armno,&(_comuni->hd.wrk[1].xyz[j]),&(Imtx[j][3]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		for(k=0;k<3;k++){
			st=pa_cpy_flt((u32)armno,&(_comuni->hd.wrk[0].noa[j][k]),&(mtx0[j][k]),2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt((u32)armno,&(_comuni->hd.wrk[1].noa[j][k]),&(Imtx[j][k]),2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
		}
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
