/*****************************************************************************/
/*                                                                           */
/*  FILE        :papci.c                                                     */
/*  DESCRIPTION :PA10-(6C/7C) motion control board Linux Driver source file  */
/*  history:                                                                 */
/*     v1.0.0:  2002.11.30 by m.togawa                               	     */
/*        original issue                                                     */
/*****************************************************************************/
#define MODULE
#define __KERNEL__

/*
#include <linux/autoconf.h>
#if defined(CONFIG_MODVERSIONS) && !defined(MODVERSIONS)
#define MODVERSIONS
#endif

#ifdef MODVERSIONS
#include <linux/modversions.h>
#endif
*/
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/sched.h>
#include <linux/init.h>
#include <asm/uaccess.h>
#include <linux/ioport.h>
#include <linux/errno.h>
#include <linux/pci.h>
#include <linux/delay.h>
#include <asm/io.h>
#include <linux/version.h>  // Added
#include <linux/moduleparam.h>  // Added
#include <linux/uaccess.h>  // Added
#include <linux/vmalloc.h>
#include <linux/io-mapping.h>

/*
#include <linux/mm.h>
#include <linux/string.h>
#include <linux/malloc.h>
#include <linux/fs.h>
#include <linux/stddef.h>
#include <linux/version.h>
#include <asm/segment.h>
*/

#if LINUX_VERSION_CODE < 0x20155
#include <linux/bios32.h>
#endif


#include <asm/uaccess.h>
#define memcpy_fromfs 	copy_from_user
#define memcpy_tofs 	copy_to_user

#include	"../../../../include/papci.h"  // Modified code


#define VENDOR_ID 0x8086	
#define DEVICE_ID 0xb555


#define	MAX_BOARD	4	/* support max board */

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dong");

static int devmajor=0;
static char *devname="papci";
struct pci_dev *dev = NULL;  // Added.

PCI_CONFIG_DATA _papci_[MAX_BOARD];


#if LINUX_VERSION_CODE > 0x20115
// MODULE_PARM(devmajor, "i");
module_param_unsafe(devmajor, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
// MODULE_PARM(devname, "s");
module_param_unsafe(devname, charp, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);  // Not char but charp.
#endif


/**
 **
 **/
static int papci_open(struct inode * inode, struct file * file)
{
	// MOD_INC_USE_COUNT;
	__module_get(THIS_MODULE);  // Not certained.
	
	return 0;
}
/**
 **
 **/
static int papci_close(struct inode * inode, struct file * file)
{
	// MOD_DEC_USE_COUNT;
	// Not concert.pci_dev_put();    Reference: https://www.kernel.org/doc/html/latest/PCI/pci.html#how-to-find-pci-devices-manually
	module_put(THIS_MODULE);  // Not concert.
	module_put_and_exit(0);  // Not concert.
	return 0;
}
/**
 **
 **/
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,36)  // ioctl is removed from kernel version v2.6.36-rc1
static int papci_ioctl(struct inode * inode, struct file * file, unsigned int function, unsigned long arg)
#else
static long papci_ioctl(struct file * file, unsigned int function, unsigned long arg)
#endif
{
	int 	stat=0;
	PA_ARG	parg;
printk("PAPCI: Entering papci_ioctl()\n");
#if LINUX_VERSION_CODE > KERNEL_VERSION(4,12,0)
	if(access_ok((void*)arg,sizeof(PA_ARG)) ? 0 : -EFAULT)  // verify_area is completly deprecated from kernel version 4.12.
#else
	if(access_ok(VERIFY_WRITE,(void*)arg,sizeof(PA_ARG)))  // verify_area is completly deprecated from kernel version 4.12.
#endif
	{
		printk("<0>verify error\n");
		return(-1);
	}
printk("PAPCI: == Before copy_from_user()\n");
	copy_from_user(&parg,(void*)arg,sizeof(PA_ARG));  // You should include the linux/uaccess.h
/*
printk("<0>arm=%ld addr=%ld size=%ld\n",parg.arm,parg.addr,parg.size);
*/
printk("PAPCI: == switch(function)\n");
	switch(function){
	case	PA_WRITE_PCI:
printk("PAPCI: == Before PA_WRITE_PCI\n");
		memcpy((void*)(_papci_[parg.arm].mem+parg.addr),
						&parg.data[0],parg.size);
		break;
	case	PA_READ_PCI:
printk("PAPCI: == Before PA_READ_PCI\n");
		memcpy((void*)&parg.data[0],
			(void*)(_papci_[parg.arm].mem+parg.addr),parg.size);
printk("PAPCI: == Before PA_READ_PCI copy_from_user()\n");
		copy_to_user((void*)arg,(void*)&parg,sizeof(PA_ARG));
		break;
	case	PA_INTR_PCI:
printk("PAPCI: == Before PA_INTR_PCI\n");
		memcpy(_papci_[parg.arm].intr,&parg.data[0],1);
		break;
	}
printk("PAPCI: == After all papci_ioctl\n");
printk("======= PAPCI: papci_ioctl END =======\n");
	return stat;
}


#if LINUX_VERSION_CODE >= 0x020400
static	struct file_operations papci_fops = {
	open:		papci_open,
	release:	papci_close,

#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,36)  // ioctl is removed from kernel version v2.6.36-rc1
	ioctl:		papci_ioctl,
#else
	unlocked_ioctl: papci_ioctl,  // unlocked_ioctl: called by the ioctl(2) system call.  Reference:https://qiita.com/akachochin/items/94ba679b2941f55c1d2d
	compat_ioctl: papci_ioctl,  // compat_ioctl: called by the ioctl(2) system call when 32 bit system calls
#endif
};
#else
static struct file_operations papci_fops = {    // Linux 2.2.10�
	NULL,		// loff_t  llseek(struct file *, loff_t, int)
	NULL,		// ssize_t read(struct file *, char *, size_t, loff_t *)
	NULL,		// ssize_t write(struct file *, const char *, 
			//			size_t, loff_t *)
	NULL,		// int     readdir(struct file *, void *, filldir_t)
	NULL,		// u int   poll(struct file *, 
			//			struct poll_table_struct *)
	papci_ioctl,	// int     ioctl(struct inode *,struct file *, 
			//			unsigned int, unsigned long)
	NULL,		// int     mmap(struct file *, struct vm_area_struct *)
	papci_open,	// int     open(struct inode *, struct file *)
	NULL,		// int     flush(struct file *)
	papci_close,	// int     release(struct inode *, struct file *)
	NULL,		// int     fsync(struct file *, struct dentry *)
	NULL,		// int     fasync(int, struct file *, int)
	NULL,		// int     check_media_change(kdev_t dev)
	NULL,		// int     revalidate(kdev_t dev)
	NULL,		// int     lock(struct file *, int,
			//			 struct file_lock *)
};
#endif


int init_module(void)
{
	
	int r,stat;
	int	baseaddr;
	unsigned char i,bus,dev_fn;

printk("<0>====== version=%x ========\n",LINUX_VERSION_CODE);

	for(i=0;i<MAX_BOARD;i++)
	{
		_papci_[i].VendorID=NODEVICE;
		_papci_[i].PortAdr=0;
	}

	for(i=0;i<MAX_BOARD;i++)
	{
		// r=pcibios_find_device(VENDOR_ID, DEVICE_ID, i, &bus, &dev_fn);
		// if(r!=PCIBIOS_SUCCESSFUL)
		// {
		// 	if(r!=PCIBIOS_DEVICE_NOT_FOUND)
		// 	{
		// 		printk("<0>find error: %X:see linux/pci.h\n",r);
		// 	}
		// 	break;
		// }
		
		printk("Probe 1\n");
		dev = pci_get_device(VENDOR_ID, DEVICE_ID, dev);
		printk("Probe 2\n");
		if(!dev)
			continue;
		printk("Probe 3\n");
		int ret = pci_enable_device(dev);
		if(ret != 0)
		{
			printk("Enable faild! The error code: %d\n", ret);
			continue;
		}
		  // If pci_get_device function returns 0, it loops forever.
				// configure_device(dev);  // I can't find any reference to describe this function.

		pci_request_region(dev, 0, "486IRQ");
struct io_mapping test = io_mapping_create_wc((u32)(baseaddr & PCI_BASE_ADDRESS_MEM_MASK), 0x1000);
		printk("Probe 4\n");
		/* read pci config area (intrrupt mem area) */
		// pcibios_read_config_dword (bus,dev_fn, PCI_BASE_ADDRESS_0+0,&baseaddr);
		pci_read_config_dword(dev, PCI_BASE_ADDRESS_0 + 0, &baseaddr);  // Changed
		printk("Probe 5\n");
		io_map
		_papci_[i].intr_top = ioremap((u32)(baseaddr & PCI_BASE_ADDRESS_MEM_MASK), 0x1000);
		_papci_[i].intr = _papci_[i].intr_top + 0x9e;
		printk("<0>       intr addr=0x%lx\n",baseaddr&PCI_BASE_ADDRESS_MEM_MASK);

		/* read pci config area (mem area) */
		// pcibios_read_config_dword (bus,dev_fn, PCI_BASE_ADDRESS_0+16,&baseaddr);
		pci_read_config_dword(dev, (u32)(PCI_BASE_ADDRESS_0 + 16), &baseaddr);  // Changed
	/*
		_papci_[i].mem_top=
		ioremap((baseaddr&PCI_BASE_ADDRESS_MEM_MASK)+0x006049a0,0x1000);
	*/
		_papci_[i].mem_top = ioremap((u32)(baseaddr & PCI_BASE_ADDRESS_MEM_MASK) + 0x006001a0, 0x1000);
		_papci_[i].mem=_papci_[i].mem_top;
printk("<0>       mem  addr=0x%lx\n",baseaddr&PCI_BASE_ADDRESS_MEM_MASK);

printk("<0>PApci:found device(%04X,%04X,%d) on Bus:%d  Device:%d Function:%d\n" ,VENDOR_ID, DEVICE_ID, i, bus, dev_fn>>3, dev_fn&0x7);

		/* read pci config area (I/O port area) */
		// pcibios_read_config_dword (bus,dev_fn, PCI_BASE_ADDRESS_0+4,&baseaddr);
		pci_read_config_dword(dev, (u32)(PCI_BASE_ADDRESS_0+4), &baseaddr);  // Changed
		_papci_[i].PortAdr = (u32)(baseaddr & PCI_BASE_ADDRESS_IO_MASK);

		_papci_[i].VendorID=VENDOR_ID;
		_papci_[i].DeviceID=DEVICE_ID;
		_papci_[i].BoardNo=i+1;
		_papci_[i].BusNumber=bus;
		_papci_[i].SlotNumber=dev_fn>>3;
		_papci_[i].FuncNumber=dev_fn&0x7;
	}
	printk("Probe 6\n");
	stat=register_chrdev(devmajor,devname,&papci_fops);	
	if(0>stat)
	{
		printk("<0>PApci: device registration error\n");
		return -EBUSY;
	}
	else if(devmajor==0)
	  	devmajor=stat;
	
	printk("<0>install '%s' into major %d\n",devname,devmajor);

	for(i=0;i<MAX_BOARD;i++){
		if(_papci_[i].PortAdr!=0){
			request_region(_papci_[i].PortAdr,0x20,devname);
		}
	}
	printk("====== Succeed !!!! ======\n");
	return 0;
}
/**
 **
 **/
void cleanup_module(void)
{
	int	i;
	printk("<0>remove '%s' from major %d\n",devname,devmajor);
	for(i=0;i<MAX_BOARD;i++){
		if(_papci_[i].PortAdr!=0){
			vfree(_papci_[i].mem_top);
			vfree(_papci_[i].intr_top);
		 	release_region(_papci_[i].PortAdr,0x20);
		}
	}
	pci_dev_put(dev);
	pci_disable_device(dev);
	// if (unregister_chrdev(devmajor,devname)){
	// 	printk ("PApci: unregister_chrdev failed\n");
	// }
	unregister_chrdev(devmajor,devname);
}
