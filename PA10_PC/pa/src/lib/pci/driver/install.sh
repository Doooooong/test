#!/bin/bash

# Install pa10 control board driver to system.
echo "Enter!!" >> /home/pa10/log.txt
echo 'zutt0issy0' | sudo -S bash -c 'insmod /home/pa10/.startup_scripts/papci.ko devmajor=238'
echo 'zutt0issy0' | sudo -S bash -c 'mknod -m 666 /dev/papci c 238 0'
sleep 2

# Set property of serial port to writable and readable.
sudo chmod o+wr /dev/serial/by-id/*

# Naming server setup.
rtm-naming | tee /home/pa10/log.txt

