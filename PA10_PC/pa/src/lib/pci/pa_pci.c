/*

	PAlib memory copy (float,u32)function

*/
#include	<stdio.h>
#include	<fcntl.h>
#include	<error.h>

#include	<pa.h>
#include	<pactl.h>
#include	<paerr.h>

#include	<papci.h>

#define	PAPCI_DEV	"/dev/papci"


#define	MAX_ARM	16				/*	486�������դ����*/

#define	ARM0_FLG		0x0001
#define	ARM1_FLG		0x0002
#define	ARM2_FLG		0x0004
#define	ARM3_FLG		0x0008
#define	ARM4_FLG		0x0010
#define	ARM5_FLG		0x0020
#define	ARM6_FLG		0x0040
#define	ARM7_FLG		0x0080
#define	ARM8_FLG		0x0100
#define	ARM9_FLG		0x0200
#define	ARM10_FLG		0x0400
#define	ARM11_FLG		0x0800
#define	ARM12_FLG		0x1000
#define	ARM13_FLG		0x2000
#define	ARM14_FLG		0x4000
#define	ARM15_FLG		0x8000

static	unsigned int	_arm_flg_=0x00;

static	char	MUTEX_NAME[MAX_ARM][12]={	
		{"ArmMutex-00"},{"ArmMutex-00"},{"ArmMutex-02"},{"ArmMutex-00"},
		{"ArmMutex-04"},{"ArmMutex-04"},{"ArmMutex-06"},{"ArmMutex-06"},
		{"ArmMutex-08"},{"ArmMutex-08"},{"ArmMutex-10"},{"ArmMutex-10"},
		{"ArmMutex-12"},{"ArmMutex-12"},{"ArmMutex-14"},{"ArmMutex-14"}};
//static	HANDLE		__Mutex[MAX_ARM];

#define	INT_OFFSET	0x9e
#define	TAG_SIZE	0x2400

/*��ư����CPU  2PORT RAM ADD	*/
COMUNIptr _comuni = 0x00;

static int*	_486req;
extern int  	CtlPCI( int );

u32 _d14[MAX_ARM];
static u32 _d14_[MAX_ARM];

u32 _d3[MAX_ARM];
static u32 _d3_[MAX_ARM];

static	void	*__winRT2[MAX_ARM];

typedef unsigned char	DataType;

static	int	_dev_;

/******************************************************************/
/******************************************************************/
/******************************************************************/
DllExport u32  MakeMutex(u32 arm)
{
/*
	__Mutex[arm]=CreateMutex(NULL,FALSE,MUTEX_NAME[arm]);
*/
	return(0);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
DllExport u32  DestroyMutex(u32 arm)
{
/*
	CloseHandle(__Mutex[arm]);
*/
	return(0);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
DllExport u32  LockMutex(u32 arm)
{
/*
	WaitForSingleObject(__Mutex[arm],INFINITE);
*/
	return(0);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
DllExport u32  UnlockMutex(u32 arm)
{
/*
	ReleaseMutex(__Mutex[arm]);
*/
	return(0);
}

/******************************************************************
	WinRT Initialize
******************************************************************/
DllExport u32  WinRTinit(u32 arm)
{
	_dev_=open(PAPCI_DEV,O_RDWR);
	if(_dev_<0)	return(-1);
	return(0);
}
/******************************************************************
	WinRT Close
******************************************************************/
DllExport u32  WinRTabort(u32 arm)
{
	close(_dev_);
	return(0);
}
/******************************************************************

******************************************************************/
int	CtlPCI( int arm )
{
 	return(INT_OFFSET);
}
/******************************************************************

******************************************************************/
DllExport u32  pa_mem_blk(u32 arm,void *dest,void *src,size_t n, int sw)
{
	DataType	*s=(DataType *)src;
	DataType	*d=(DataType *)dest;
	DataType	*ss=(DataType *)src;
	DataType	*dd=(DataType *)dest;
	unsigned int	ptr;
	PA_ARG		arg;

	arg.arm=arm/2;
	arg.size=n;

	arm=arm%2;
	switch(sw){
	case	1:	/* read */
		ptr=(unsigned int)s;
		arg.addr=(u32)(arm*TAG_SIZE + ptr);
		ioctl(_dev_,PA_READ_PCI,&arg);
		memcpy((void*)dd,(void*)&arg.data[0],n);
		break;
	case	2:	/* write */
		ptr=(unsigned int)dd;
		arg.addr=(u32)(arm*TAG_SIZE + ptr);
		memcpy((void*)&arg.data[0],(void*)s,n);
		ioctl(_dev_,PA_WRITE_PCI,&arg);
		break;
	case	3:	/* copy */
		ptr=(unsigned int)s;
		arg.addr=(u32)(arm*TAG_SIZE + ptr);
		ioctl(_dev_,PA_READ_PCI,&arg);

		ptr=(unsigned int)d;
		arg.addr=(u32)(arm*TAG_SIZE + ptr);
		ioctl(_dev_,PA_WRITE_PCI,&arg);
		break;
	}
	return(0);
}

/******************************************************************

******************************************************************/
static	 u32  pa_cpy_4(u32 arm,void *dest,void *src, int sw)
{
	int		i;
	DataType	*s=(DataType *)src;
	DataType	*d=(DataType *)dest;
	DataType	*ss=(DataType *)src;
	DataType	*dd=(DataType *)dest;
	unsigned int	ptr;
	PA_ARG		arg;

	arg.arm=arm/2;
	arg.size=sizeof(float);

	arm=arm%2;
	switch(sw){
	case	1:	/* read */
		ptr=(unsigned int)s;
		arg.addr=(u32)(arm*TAG_SIZE + ptr);
		ioctl(_dev_,PA_READ_PCI,&arg);
		/* swap */
		for(i=0;i<sizeof(float);i++){
			*dd=arg.data[sizeof(float)-(i+1)];
			dd++;
		}
		break;
	case	2:	/* write */
		ptr=(unsigned int)dd;
		arg.addr=(u32)(arm*TAG_SIZE + ptr);
		/* swap */
		for(i=0;i<sizeof(float);i++){
			arg.data[sizeof(float)-(i+1)]=*s;
			s++;
		}
		ioctl(_dev_,PA_WRITE_PCI,&arg);
		break;
	case	3:	/* copy */
		ptr=(unsigned int)s;
		arg.addr=(u32)(arm*TAG_SIZE + ptr);
		ioctl(_dev_,PA_READ_PCI,&arg);

		ptr=(unsigned int)d;
		arg.addr=(u32)(arm*TAG_SIZE + ptr);
		ioctl(_dev_,PA_WRITE_PCI,&arg);
		break;
	}
	return(0);
}
/******************************************************************

******************************************************************/
DllExport u32  pa_mem_cpy(u32 arm,void *dest,void *src,size_t n, int sw)
{
	int	i,st;
	for(i=0;i<n;i+=4){
		st=pa_cpy_4(arm,dest,src,sw);
		dest+=i;
		src+=i;
	}
	return(st);
}
/******************************************************************

******************************************************************/
DllExport u32  pa_cpy_flt(u32 arm,void *dest,void *src, int sw)
{
	int	st;
	st=pa_cpy_4(arm,dest,src,sw);
	return(st);
}
/******************************************************************

******************************************************************/
DllExport u32  pa_cpy_lng(u32 arm,void *dest,void *src, int sw)
{
	int	st;
	// printf("before pa_cpy_4");
	st=pa_cpy_4(arm,dest,src,sw);
	return(st);
}
/******************************************************************

******************************************************************/
DllExport u32  pa_cpy_uby(u32 arm,void *dest,void *src, int sw)
{
	DataType	*s=(DataType *)src;
	PA_ARG		arg;

	arg.arm=arm/2;
	arg.size=1;
	arg.data[0]=*s;
	ioctl(_dev_,PA_INTR_PCI,&arg);
	return(0);
}
/******************************************************************

******************************************************************/
DllExport u32  pa_ret_lng(u32 arm,void *src)
{
	int	st;
	u32	tmp;

	st=pa_cpy_4(arm,(void*)&tmp,src,1);
	return(tmp);
}
/***************************************************************

***************************************************************/
static u32 chk_stat(ARM armno, short* arm)
{
	switch(armno){
	case ARM0:		*arm=0;		break;
	case ARM1:		*arm=1;		break;
	case ARM2:		*arm=2;		break;
	case ARM3:		*arm=3;		break;
	case ARM4:		*arm=4;		break;
	case ARM5:		*arm=5;		break;
	case ARM6:		*arm=6;		break;
	case ARM7:		*arm=7;		break;
	case ARM8:		*arm=8;		break;
	case ARM9:		*arm=9;		break;
	case ARM10:		*arm=10;	break;
	case ARM11:		*arm=11;	break;
	case ARM12:		*arm=12;	break;
	case ARM13:		*arm=13;	break;
	case ARM14:		*arm=14;	break;
	case ARM15:		*arm=15;	break;
	default:			return ERR_ARM;
	}
	return ERR_OK;
}
/***************************************************************

***************************************************************/
DllExport u32  pa_map_ctl(ARM armno)
{
	u32	err;
	short	arm;
	if( ERR_OK==(err=chk_stat(armno,&arm)) ){
		_486req = (int *)CtlPCI( arm );
		_d14[arm]=_d14_[arm];
	}
	return err;
}
/***************************************************************

*******************************************************************/
DllExport u32  pa_req_ctl(ARM armno, u32 num)
{
	u32			err;
	static	u32		_d9,d14;
	short 			arm;
	static	u32		ldmy;
    	static	unsigned char	ubdmy;
	u32			tmp,ocr=0;
	u32			intr_no;

	if( ERR_OK!=(err=chk_stat(armno,&arm)) )return err;
	if(armno%2)	intr_no=2;
	else		intr_no=1;

	pa_cpy_lng((u32)armno,&_d9,&(_comuni->hu.flg.flag[3]),1);
	d14=_d14_[arm];							
	pa_cpy_lng((u32)armno,&(_d14_[arm]),&(_comuni->hu.err.dmy[14]),1);
	pa_cpy_lng((u32)armno,&(_d14[arm]),&(_comuni->hu.err.dmy[14]),1);
	
	for(;;){
		pa_cpy_lng((u32)armno,&ldmy,&(_comuni->hu.flg.flag[4]),1);
		ldmy +=(u32)(1);
		pa_cpy_lng((u32)armno,&(_comuni->hu.flg.flag[4]),&ldmy,2);
//		ubdmy = (unsigned char)((int)armno+1+8*ocr);
		ubdmy = (unsigned char)(intr_no);
	 	pa_cpy_uby((u32)armno,_486req,&ubdmy,2 );
		if( !num-- ){
			_d14_[arm]=_d14[arm]=d14;
			return ERR_INT;
			// break;
		}
		for(tmp=0;tmp<1000;tmp++){
			pa_cpy_lng(	(u32)armno,&ldmy,&(_comuni->hu.flg.flag[3]),1);
			if( _d9 != ldmy )break;
		}
		if( _d9 != ldmy )	break;
		
		pa_cpy_lng((u32)armno,	&ldmy,&(_comuni->hu.flg.flag[5]),1);
		ldmy +=(u32)1;
		pa_cpy_lng((u32)armno,	&(_comuni->hu.flg.flag[5]),&ldmy,2);
	}
	ldmy =(u32)0;
	pa_cpy_lng((u32)armno,	&(_comuni->hu.flg.flag[0]),&ldmy,2);
	pa_cpy_lng((u32)armno,	&(_comuni->hu.flg.flag[1]),&ldmy,2);
	return ERR_OK;
}
/***************************************************************

*******************************************************************/
DllExport u32  pa_req_sub(ARM armno, u32 num)
{
	u32 			err;
	u32		 	_d9,d3;
	short 			arm;
	u32			ldmy;
    	unsigned char		ubdmy;
	u32			tmp;
	u32			intr_no;

	if( ERR_OK!=(err=chk_stat(armno,&arm)) )return err;

	if(arm%2)	intr_no=2;
	else		intr_no=1;

	pa_cpy_lng((u32)armno,	&(_d9),&(_comuni->hu.flg.flag[3]),1);
	d3=_d3_[arm];
	pa_cpy_lng((u32)armno,	&(_d3_[arm]),&(_comuni->hu.err.dmy[3]),1);
	pa_cpy_lng((u32)armno,	&(_d3[arm]),&(_comuni->hu.err.dmy[3]),1);
	
	for(;;){
		pa_cpy_lng((u32)armno,	&ldmy,&(_comuni->hu.flg.flag[4]),1);
		ldmy +=(u32)1;
		pa_cpy_lng((u32)armno,	&(_comuni->hu.flg.flag[4]),&ldmy,2);
//		ubdmy = (unsigned char)((int)armno+1);
		ubdmy = (unsigned char)(intr_no);
	    	pa_cpy_uby((u32)armno,	_486req,&ubdmy,2 );
		if( !num-- ){
			_d3_[arm]=_d3[arm]=d3;
			return ERR_INT;
		}
		for(tmp=0;tmp<100;tmp++){
			pa_cpy_lng((u32)armno,	&ldmy,&(_comuni->hu.flg.flag[3]),1);
			if( _d9 != ldmy )break;
		}
		if( _d9 != ldmy )	break;
		pa_cpy_lng((u32)armno,	&ldmy,&(_comuni->hu.flg.flag[5]),1);
		ldmy +=(u32)1;
		pa_cpy_lng((u32)armno,	&(_comuni->hu.flg.flag[5]),&ldmy,2);
	}

	ldmy = (u32)	0;
	pa_cpy_lng((u32)armno,	&(_comuni->hu.flg.flag[0]),&ldmy,2);
	pa_cpy_lng((u32)armno,	&(_comuni->hu.flg.flag[1]),&ldmy,2);
	return ERR_OK;
}
/*********************************************************************

*********************************************************************/
DllExport short  pa_fsh_chk(ARM armno)
{
	static	u32	ldmy;

	if( ERR_OK!=pa_map_ctl(armno) )return ERR_OK;
	pa_cpy_lng((u32)armno,	&ldmy,&(_comuni->hu.err.dmy[14]),1);

	return _d14[armno]!=ldmy?0:1;
}
/*********************************************************************

**********************************************************************/
DllExport short  pa_fsh_sub(ARM armno)
{
	u32	ldmy;

	if( ERR_OK!=pa_map_ctl(armno) )return ERR_OK;
	pa_cpy_lng(	(u32)armno,&ldmy,&(_comuni->hu.err.dmy[3]),1);

	return _d3[armno]!=ldmy?0:1;
}
/***************************************************************

***************************************************************/
DllExport u32  pa_ini_sys(void)
{
	return ERR_OK;
}
/***************************************************************

***************************************************************/
DllExport u32  pa_ter_sys(void)
{
	return ERR_OK;
}
/***************************************************************

***************************************************************/
DllExport u32  pa_opn_arm(ARM armno)
{
	MakeMutex((u32)armno);
	return WinRTinit((u32)armno);
}
/***************************************************************

***************************************************************/
DllExport u32  pa_cls_arm(ARM armno)
{
	u32	err=ERR_OK;
	DestroyMutex((u32)armno);
	WinRTabort((u32)armno);
	return err;
}
