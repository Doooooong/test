/*

	PAlib memory copy (float,long)function

*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<winsock2.h>
#include	<windows.h>

#include	<pa.h>
#include	<pactl.h>
#include	<paerr.h>

#define	ERR_SOCK_CREATE		-5001	/* ソケット作成エラー		*/
#define	ERR_SOCK_CONNECT	-5002	/* ソケット接続エラー		*/
#define	ERR_SOCK_HOST		-5003	/* gethostbyname()エラー	*/
#define	ERR_SOCK_SEND		-5004	/* ソケット送信エラー		*/
#define	ERR_SOCK_FAILED		-5005	/* ソケットWait Failed		*/
#define	ERR_SOCK_TIMEOUT	-5006	/* ソケットWait timeout		*/
#define	ERR_SOCK_EVENT		-5007	/* ソケットerror event		*/
#define	ERR_SOCK_CLOSE		-5008	/* ソケット断線				*/
#define	ERR_SOCK_READ		-5009	/* ソケット受信エラー		*/
#define	ERR_SOCK_DATA		-5010	/* ソケットデータ異常		*/
#define	ERR_SOCK_SEQ		-5011	/* ソケットデータ連番エラー	*/
#define	ERR_SOCK_NOTHING	-5012	/* ソケット未接続		*/

#define	SOCK_PORT		3000	/* ソケット番号			*/
#define	SOCK_TIMEOUT	1000		/* ソケット受信タイムアウト	*/

#define	MAX_ARM		16		/* 486最大割り付け枚数	*/
#define	MAX_SOCKET	32		/* ソケット最大接続数		*/

static	char			_MutexName_[MAX_ARM][12]={
	{"ArmMutex-01"},{"ArmMutex-02"},{"ArmMutex-03"},{"ArmMutex-04"},
	{"ArmMutex-05"},{"ArmMutex-06"},{"ArmMutex-07"},{"ArmMutex-08"},
	{"ArmMutex-09"},{"ArmMutex-10"},{"ArmMutex-11"},{"ArmMutex-12"},
	{"ArmMutex-13"},{"ArmMutex-14"},{"ArmMutex-15"},{"ArmMutex-16"}};
static	unsigned long	_MutexFLG_=0;
static	HANDLE			_Mutex_[MAX_ARM];



#define	MAX_BUFF_SIZE	2048	/* ソケット送受信用パケット最大サイズ	*/

static	unsigned long	_SOCKflg_=0;
static	SOCKET			_sock_[MAX_ARM];
static	unsigned char	_SENDbuff_[MAX_ARM][MAX_BUFF_SIZE];
static	long			_SENDsize_[MAX_ARM];
static	unsigned char	_RECVbuff_[MAX_ARM][MAX_BUFF_SIZE];
static	long			_RECVsize_[MAX_ARM];
static	unsigned char	_SEQno_[MAX_ARM];
static	HANDLE			_SOCKevent_[MAX_ARM];

long			_SOCKerr_[MAX_ARM]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};


#define	MAX_BLOCK	16

/* 送信用の構造体ID */
#define	HED_AREA	0
#define	DWN_AREA	1
#define	PLY_AREA	2
#define	NOA_AREA	3
#define	JMP_AREA	4
#define	BSE_AREA	5
#define	PRM_AREA	6
#define	WRK1_AREA	7
#define	WRK2_AREA	8
#define	MAT_AREA	9
#define	TRN_AREA	10
#define	DIO_AREA	11
#define	GTE_AREA	12

/* 受信用の構造体ID */
#define	hed_RECV_FLG	(unsigned long)0x00000001
#define	rio_RECV_FLG	(unsigned long)0x00000002
#define	pos_RECV_FLG	(unsigned long)0x00000004
#define	mat_RECV_FLG	(unsigned long)0x00000008
#define	trn_RECV_FLG	(unsigned long)0x00000010
#define	ply_RECV_FLG	(unsigned long)0x00000020
#define	noa_RECV_FLG	(unsigned long)0x00000040
#define	jmp_RECV_FLG	(unsigned long)0x00000080
#define	bse_RECV_FLG	(unsigned long)0x00000100
#define	prm_RECV_FLG	(unsigned long)0x00000200
#define	err_RECV_FLG	(unsigned long)0x00000400
#define	dio_RECV_FLG	(unsigned long)0x00000800
#define	flg_RECV_FLG	(unsigned long)0x00001000
#define	ypr_RECV_FLG	(unsigned long)0x00002000
#define	cub_RECV_FLG	(unsigned long)0x00004000
#define	ard_RECV_FLG	(unsigned long)0x00008000
#define	get_RECV_FLG	(unsigned long)0x00010000
#define	sts_RECV_FLG	(unsigned long)0x00020000
#define	dbg_RECV_FLG	(unsigned long)0x00040000

#define	fprm_RECV_FLG	(unsigned long)0x10000000
#define	force_RECV_FLG	(unsigned long)0x20000000

/* 受信した構造体のreadフラグ(LockMutex()でリセットされる)	*/
static	unsigned long	_recv_flg_[MAX_ARM]=
			{0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L,0L};


static	int		_SENDblock_[MAX_ARM][MAX_BLOCK];


/* 運動制御CPUへの指令値を動作開始コマンドを発令するまで蓄えておくエリア	*/
/* 及び受信データを蓄えておくエリア										*/
static	COMUNI	_Scomuni_[MAX_ARM];

/*	運動制御CPU  2PORT RAM ADD	*/
COMUNIptr _comuni = 0x00;

/*	個々のアームの_comuni->hu.ErrorNote.dummy[14].dummy[3]を保存。				*/
static long _d14_[MAX_ARM];
static long _d3_[MAX_ARM];

typedef unsigned char	DataType;


#define	LAN_SOURCE
#include	<pammc.h>

/******************************************************************/
/******************************************************************/
/******************************************************************/
static	unsigned long	GetArmFlg(unsigned long arm)
{
	unsigned long	i,tmp;

	if(!arm)	return(1);
	tmp=1;
	for(i=0;i<arm;i++){
		tmp*=2;
	}
	return(tmp);

}
/******************************************************************/
/******************************************************************/
/******************************************************************/
DllExport long __stdcall MakeMutex(long arm)
{
//	unsigned long	flg;
//	flg=GetArmFlg(arm);
//if(!(_MutexFLG_&flg))_Mutex_[arm]=CreateMutex(NULL,FALSE,_MutexName_[arm]);
//	_MutexFLG_|=flg;
	return(0);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
DllExport long __stdcall DestroyMutex(long arm)
{
//	unsigned long	flg;
//	flg=GetArmFlg(arm);
//	if(_MutexFLG_&flg)	CloseHandle(_Mutex_[arm]);
//	_MutexFLG_&=~flg;
	return(0);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
DllExport long __stdcall LockMutex(long arm)
{
//	unsigned long	flg;
//	flg=GetArmFlg(arm);
//	if(_MutexFLG_&flg)	WaitForSingleObject(_Mutex_[arm],INFINITE);
	_recv_flg_[arm]=0;
	return(0);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
DllExport long __stdcall UnlockMutex(long arm)
{
//	unsigned long	flg;
//	flg=GetArmFlg(arm);
//	if(_MutexFLG_&flg)	ReleaseMutex(_Mutex_);
	_recv_flg_[arm]=0;
	return(0);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
static	long	SendSOCK(long arm,int size)
{
	if(send(_sock_[arm],(const char*)&_SENDbuff_[arm][0],size,0)!=size){
		return(ERR_SOCK_SEND);
	}
	return(0);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
static	long	ReceiveSOCK(long arm)
{
	WSANETWORKEVENTS	events;
	unsigned char	tmp[32];
	long	i,st,ret;
	long	num=0;
	long	len=0;
	long	req=26;	/* まずは[要求サイズ]までの26バイトを受信する	*/
			/* Rコマンドをとりあえずの対象としている	*/
	long	sw=1;	/* １回目の受信(すなわち[要求サイズ]まで)	*/
	unsigned char	seq;
	unsigned char	buff[256];

	memset(&_RECVbuff_[arm][0],0,sizeof(MAX_BUFF_SIZE));

	while(1){

		seq=_SEQno_[arm]-1;

		st=WaitForMultipleObjects(1, &_SOCKevent_[arm], FALSE, 
								SOCK_TIMEOUT);
		switch(st){
		case	WAIT_FAILED:
			return(ERR_SOCK_FAILED);
			break;
		case	WAIT_TIMEOUT:
			return(ERR_SOCK_TIMEOUT);
			break;
		default:
			//どのイベントか
			ret=WSAEnumNetworkEvents(_sock_[arm], _SOCKevent_[arm], &events);
			if(ret==SOCKET_ERROR){
				return(ERR_SOCK_EVENT);
				break;
			}else if(events.lNetworkEvents&FD_READ){	/* 受信割り込み */
				ret=recv(_sock_[arm], &_RECVbuff_[arm][len], req, 0);
				if(ret!=SOCKET_ERROR){
					len+=ret;
				//	if(ret==req){
						switch(sw){
						case	1:		/* 要求サイズまでの受信	*/
							if(_RECVbuff_[arm][0]=='O'){
							//	if(_SEQno_[arm]-1!=_RECVbuff_[arm][1]){	/* 連番チェック */
								if(seq!=_RECVbuff_[arm][1]){	/* 連番チェック */
//sprintf(buff,"SEQ[O] error:%x %x",seq,_RECVbuff_[arm][1]);
//MessageBox(NULL,buff,buff,MB_OK);
									return(ERR_SOCK_SEQ);
								}
								return(len);
							}else if(_RECVbuff_[arm][0]=='C'){
							//	if(_SEQno_[arm]-1!=_RECVbuff_[arm][1]){	/* 連番チェック */
								if(seq!=_RECVbuff_[arm][1]){	/* 連番チェック */
//sprintf(buff,"SEQ[C] error:%x %x",seq,_RECVbuff_[arm][1]);
//MessageBox(NULL,buff,buff,MB_OK);
									return(ERR_SOCK_SEQ);
								}
								return(len);
							}else if(_RECVbuff_[arm][0]=='S'){
							//	if(_SEQno_[arm]-1!=_RECVbuff_[arm][1]){	/* 連番チェック */
								if(seq!=_RECVbuff_[arm][1]){	/* 連番チェック */
//sprintf(buff,"SEQ[S] error:%x %x",seq,_RECVbuff_[arm][1]);
//MessageBox(NULL,buff,buff,MB_OK);
									return(ERR_SOCK_SEQ);
								}
								return(len);
							}else if(_RECVbuff_[arm][0]=='s'){
							//	if(_SEQno_[arm]-1!=_RECVbuff_[arm][1]){	/* 連番チェック */
								if(seq!=_RECVbuff_[arm][1]){	/* 連番チェック */
//sprintf(buff,"SEQ[s] error:%x %x",seq,_RECVbuff_[arm][1]);
//MessageBox(NULL,buff,buff,MB_OK);
									return(ERR_SOCK_SEQ);
								}
								return(len);
							}else if(_RECVbuff_[arm][0]=='R'){
								tmp[4]=(unsigned char)NULL;
								for(i=22;i<26;i++){
									tmp[i-22]=_RECVbuff_[arm][i];
								}
								sscanf(tmp,"%x",&req);
								sw=3;
							}else{
								return(ERR_SOCK_DATA);
							}
							break;
				//		case	2:		/* 要求サイズまでの受信	*/
				//			tmp[4]=(unsigned char)NULL;
				//			for(i=22;i<26;i++){
				//				tmp[i-22]=_RECVbuff_[arm][i];
				//			}
				//			sscanf(tmp,"%x",&req);
				//			sw=3;
				//			break;
						case	3:		/* お尻までの受信		*/
						//	if(_SEQno_[arm]-1!=_RECVbuff_[arm][1]){
							if(seq!=_RECVbuff_[arm][1]){
//sprintf(buff,"SEQ[R] error:%x %x",seq,_RECVbuff_[arm][1]);
//MessageBox(NULL,buff,buff,MB_OK);
								return(ERR_SOCK_SEQ);
							}
							return(len);
							break;
						}
					//}
				}else{
					return(ERR_SOCK_READ);
				}
			}else if(events.lNetworkEvents&FD_CLOSE){	/* 断線			*/
//MessageBox(NULL,"FD_CLOSE",NULL,MB_OK);
				return(ERR_SOCK_CLOSE);
			}else{
				return(-1);
//MessageBox(NULL,"not found event code",NULL,MB_OK);
			}
			break;
		}
	}
}


/******************************************************************/
/******************************************************************/
/******************************************************************/
static	void	SwapI2M(DataType *data,unsigned long *swap,int flg)
{
	unsigned char	tmp[4];
	if(flg){
		tmp[0]=*(data+3);
		tmp[1]=*(data+2);
		tmp[2]=*(data+1);
		tmp[3]=*(data+0);
	}else{
		tmp[3]=*(data+3);
		tmp[2]=*(data+2);
		tmp[1]=*(data+1);
		tmp[0]=*(data+0);
	}
	memcpy(swap,&tmp[0],4);
	return;
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
static	void	TokenRecvData(long arm,void *dest,void *src,size_t n,int flg)
{
	int				pos=26;
	size_t			i;
	unsigned char	val[4];
	unsigned long	*p;
	// 受信データ(バイナリィ)は必ず受信バッファの配列26から始まるので、その値を抜き出す
	// この時にモトローラ→インテルへのスワップも行っておく
	p=dest;
	if(flg){
		for(i=0;i<n;i+=4){
			val[3]=_RECVbuff_[arm][pos];
			pos++;
			val[2]=_RECVbuff_[arm][pos];
			pos++;
			val[1]=_RECVbuff_[arm][pos];
			pos++;
			val[0]=_RECVbuff_[arm][pos];
			pos++;
			memcpy(p,&val[0],4);
			p++;
		}
	}else{
		for(i=0;i<n;i+=4){
			val[0]=_RECVbuff_[arm][pos];
			pos++;
			val[1]=_RECVbuff_[arm][pos];
			pos++;
			val[2]=_RECVbuff_[arm][pos];
			pos++;
			val[3]=_RECVbuff_[arm][pos];
			pos++;
			memcpy(p,&val[0],4);
			p++;
		}
	}
	return;
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
static	long	SendData(long arm,void *dest,void *src,size_t n,int flg)
{
	int				i;
	DataType		*s=(DataType *)src;
	DataType		*d=(DataType *)dest;
	unsigned long	swap;
	unsigned char	*p;

	/* sendした構造体領域を記憶させる */
	if((void*)&_comuni->hd.hed<=dest && dest<(void*)&_comuni->hd.dwn){
		_SENDblock_[arm][HED_AREA]=1;
	}else if((void*)&_comuni->hd.dwn<=dest && dest<(void*)&_comuni->hd.ply){
		_SENDblock_[arm][DWN_AREA]=1;
	}else if((void*)&_comuni->hd.ply<=dest && dest<(void*)&_comuni->hd.noa){
		_SENDblock_[arm][PLY_AREA]=1;
	}else if((void*)&_comuni->hd.noa<=dest && dest<(void*)&_comuni->hd.jmp){
		_SENDblock_[arm][NOA_AREA]=1;
	}else if((void*)&_comuni->hd.jmp<=dest && dest<(void*)&_comuni->hd.bse){
		_SENDblock_[arm][JMP_AREA]=1;
	}else if((void*)&_comuni->hd.bse<=dest && dest<(void*)&_comuni->hd.prm){
		_SENDblock_[arm][BSE_AREA]=1;
	}else if((void*)&_comuni->hd.prm<=dest && dest<(void*)&_comuni->hd.wrk[0]){
		_SENDblock_[arm][PRM_AREA]=1;
	}else if((void*)&_comuni->hd.wrk[0]<=dest && dest<(void*)&_comuni->hd.wrk[1]){
		_SENDblock_[arm][WRK1_AREA]=1;
	}else if((void*)&_comuni->hd.wrk[1]<=dest && dest<(void*)&_comuni->hd.mat){
		_SENDblock_[arm][WRK2_AREA]=1;
	}else if((void*)&_comuni->hd.mat<=dest && dest<(void*)&_comuni->hd.trn){
		_SENDblock_[arm][MAT_AREA]=1;
	}else if((void*)&_comuni->hd.trn<=dest && dest<(void*)&_comuni->hd.dio){
		_SENDblock_[arm][TRN_AREA]=1;
	}else if((void*)&_comuni->hd.dio<=dest && dest<(void*)&_comuni->hd.gte){
		_SENDblock_[arm][DIO_AREA]=1;
	}else if((void*)&_comuni->hd.gte<=dest){
		_SENDblock_[arm][GTE_AREA]=1;
	}

	/* 運動制御に送信するデータを動作開始するまでの間、蓄えておく	*/
	/* 注) バイトスワップした状態で蓄えておく						*/
	p=(unsigned char*)&_Scomuni_[arm];
//	for(i=0;i<(int)d;i++)	p++;
	p+=(int)d;
	for(i=0;i<(int)n;i+=4){
		SwapI2M(s,&swap,flg);		/* インテル系→モトローラ系へのスワップ	*/
		memcpy(p,&swap,4);
	}
	return(0);
}
/******************************************************************/
/******************************************************************/
/******************************************************************/
static	long	RecvData(long arm,void *dest,void *src,size_t n,int flg)
{
	int				st;
	int				cpy_size;
	int				size=0;
	unsigned long	recv_area=0L;
	unsigned char	tmp[128];
	void			*top_dst,*top_src;
	unsigned char	*p;

	/* srcで指し示す先頭アドレスからどの構造体領域からデータを受信しようと	*/
	/* しているのかを検索する											*/
	if((void*)&_comuni->hu.hed<=src && src<(void*)&_comuni->hu.rio){
		recv_area = hed_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.hed;
		top_src=(void *)&_comuni->hu.hed;
		cpy_size=sizeof(HED);
	}else if((void*)&_comuni->hu.rio<=src && src<(void*)&_comuni->hu.pos){
		recv_area = rio_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.rio;
		top_src=(void *)&_comuni->hu.rio;
		cpy_size=sizeof(RIO);
	}else if((void*)&_comuni->hu.pos<=src && src<(void*)&_comuni->hu.mat[0]){
		recv_area = pos_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.pos;
		top_src=(void *)&_comuni->hu.pos;
		cpy_size=sizeof(POS);
	}else if((void*)&_comuni->hu.mat[0]<=src && src<(void*)&_comuni->hu.trn){
		recv_area = mat_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.mat[0];
		top_src=(void *)&_comuni->hu.mat[0];
		cpy_size=sizeof(MRK)*2;
	}else if((void*)&_comuni->hu.trn<=src && src<(void*)&_comuni->hu.ply){
		recv_area = trn_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.trn;
		top_src=(void *)&_comuni->hu.trn;
		cpy_size=sizeof(TRN);
	}else if((void*)&_comuni->hu.ply<=src && src<(void*)&_comuni->hu.noa){
		recv_area = ply_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.ply;
		top_src=(void *)&_comuni->hu.ply;
		cpy_size=sizeof(PLY);
	}else if((void*)&_comuni->hu.noa<=src && src<(void*)&_comuni->hu.jmp){
		recv_area = noa_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.noa;
		top_src=(void *)&_comuni->hu.noa;
		cpy_size=sizeof(NOA);
	}else if((void*)&_comuni->hu.jmp<=src && src<(void*)&_comuni->hu.bse){
		recv_area = jmp_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.jmp;
		top_src=(void *)&_comuni->hu.jmp;
		cpy_size=sizeof(JMP);
	}else if((void*)&_comuni->hu.bse<=src && src<(void*)&_comuni->hu.prm){
		recv_area = bse_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.bse;
		top_src=(void *)&_comuni->hu.bse;
		cpy_size=sizeof(BSE);
	}else if((void*)&_comuni->hu.prm<=src && src<(void*)&_comuni->hu.err){
		recv_area = prm_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.prm;
		top_src=(void *)&_comuni->hu.prm;
		cpy_size=sizeof(PRM);
	}else if((void*)&_comuni->hu.err<=src && src<(void*)&_comuni->hu.dio){
		recv_area = err_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.err;
		top_src=(void *)&_comuni->hu.err;
		cpy_size=sizeof(ERR);
	}else if((void*)&_comuni->hu.dio<=src && src<(void*)&_comuni->hu.flg){
		recv_area = dio_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.dio;
		top_src=(void *)&_comuni->hu.dio;
		cpy_size=sizeof(DIO);
	}else if((void*)&_comuni->hu.flg<=src && src<(void*)&_comuni->hu.ypr){
		recv_area = flg_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.flg;
		top_src=(void *)&_comuni->hu.flg;
		cpy_size=sizeof(FLG);
	}else if((void*)&_comuni->hu.ypr<=src && src<(void*)&_comuni->hu.cub){
		recv_area = ypr_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.ypr;
		top_src=(void *)&_comuni->hu.ypr;
		cpy_size=sizeof(YPR);
	}else if((void*)&_comuni->hu.cub<=src && src<(void*)&_comuni->hu.ard){
		recv_area = cub_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.cub;
		top_src=(void *)&_comuni->hu.cub;
		cpy_size=sizeof(CUB);
	}else if((void*)&_comuni->hu.ard<=src && src<(void*)&_comuni->hu.get){
		recv_area = ard_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.ard;
		top_src=(void *)&_comuni->hu.ard;
		cpy_size=sizeof(ARD);
	}else if((void*)&_comuni->hu.get<=src && src<(void*)&_comuni->hu.sts){
		recv_area = get_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.get;
		top_src=(void *)&_comuni->hu.get;
		cpy_size=sizeof(GET);
	}else if((void*)&_comuni->hu.sts<=src && src<(void*)&_comuni->hu.dbg){
		recv_area = sts_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.sts;
		top_src=(void *)&_comuni->hu.sts;
		cpy_size=sizeof(STS);
	}else if((void*)&_comuni->hu.dbg<=src && src<(void*)&_comuni->fu.fprm){
		recv_area = dbg_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].hu.dbg;
		top_src=(void *)&_comuni->hu.dbg;
		cpy_size=sizeof(DBG);
	}else if((void*)&_comuni->fu.fprm<=src && src<(void*)&_comuni->fu.force){
		recv_area = fprm_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].fu.fprm;
		top_src=(void *)&_comuni->fu.fprm;
		cpy_size=sizeof(D_FPARAMETER);
	}else if((void*)&_comuni->fu.force<=src){
		recv_area = force_RECV_FLG;
		top_dst=(void *)&_Scomuni_[arm].fu.force;
		top_src=(void *)&_comuni->fu.force;
		cpy_size=sizeof(D_FORCE);
	}else{
		return(-1);
	}

	if(!(_recv_flg_[arm] & recv_area)){	/* LockMutex()が呼ばれて以降、初めてreadする構造体エリアである */
		/* コマンド	*/
		_SENDbuff_[arm][size]='R';
		size++;
		/* 連番  */
		_SENDbuff_[arm][size]=_SEQno_[arm];
		_SEQno_[arm]++;
		size++;
		/* アーム番号	*/
		sprintf(tmp,"%4.4x",arm);
		memcpy(&_SENDbuff_[arm][size],&tmp[0],4);
		size+=4;
		/* 先頭アドレス	*/
	//	sprintf(tmp,"%4.4x",src);
		sprintf(tmp,"%4.4x",top_src);
		memcpy(&_SENDbuff_[arm][size],&tmp[0],4);
		size+=4;
		/* サイズ	*/
	//	sprintf(tmp,"%4.4x",n);
		sprintf(tmp,"%4.4x",cpy_size);
		memcpy(&_SENDbuff_[arm][size],&tmp[0],4);
		size+=4;
		_SENDbuff_[arm][size]=(char)NULL;
		st=SendSOCK(arm,size);		// 送信
		if(st)	return(st);
		st=ReceiveSOCK(arm);	// 応答待ち
		/* 受信したデータを解析する	*/
		if(st>=0){
			TokenRecvData(arm,top_dst,top_src,cpy_size,flg);
			p=(unsigned char *)&_Scomuni_[arm];
			p+=(int)src;
			memcpy(dest,(void *)p,n);
			_recv_flg_[arm] |= recv_area;			/* 受信したことを記憶してローカルメモリに一時保管 */
		}
		return(st);
	}else{				/* 既に受信している構造体エリアなので、一時保管しておいた値をコピーする */
		p=(unsigned char *)&_Scomuni_[arm];
		p+=(int)src;
		memcpy(dest,(void *)p,n);
		return(0);
	}

}
/******************************************************************

******************************************************************/
static	int	MakeOpenCmd(long arm)
{
	int		size=0;
	char	buff[8];
	/* コマンド */
	_SENDbuff_[arm][size]='O';
	size++;
	/* 連番  */
	_SENDbuff_[arm][size]=_SEQno_[arm];
	_SEQno_[arm]++;
	size++;
	/* アーム番号	*/
	sprintf(buff,"%4.4x",arm);
	memcpy(&_SENDbuff_[arm][size],&buff[0],4);
	size+=4;
	/* 受信タイムアウト	*/
	strcpy(buff,"FFFF");
	memcpy(&_SENDbuff_[arm][size],&buff[0],4);
	size+=4;
	/* 予備	*/
	strcpy(buff,"FFFF");
	memcpy(&_SENDbuff_[arm][size],&buff[0],4);
	size+=4;
	return(size);
}
/******************************************************************

******************************************************************/
static	int	MakeCloseCmd(long arm)
{
	int		size=0;
	char	buff[8];
	/* コマンド */
	_SENDbuff_[arm][size]='O';
	size++;
	/* 連番  */
	_SENDbuff_[arm][size]=_SEQno_[arm];
	_SEQno_[arm]++;
	size++;
	/* アーム番号	*/
	sprintf(buff,"%4.4x",arm);
	memcpy(&_SENDbuff_[arm][size],&buff[0],4);
	size+=4;
	/* 予備	*/
	strcpy(buff,"FFFF");
	memcpy(&_SENDbuff_[arm][size],&buff[0],4);
	size+=4;
	/* 予備	*/
	strcpy(buff,"FFFF");
	memcpy(&_SENDbuff_[arm][size],&buff[0],4);
	size+=4;
	return(size);
}

/******************************************************************

******************************************************************/
static	int	MakeSendCmd(long arm)
{
	int		len=0,size=0;
	int		size_pos;
	char	buff[8];
	/* コマンド */
	_SENDbuff_[arm][size]='S';
	size++;
	/* 連番  */
	_SENDbuff_[arm][size]=_SEQno_[arm];
	_SEQno_[arm]++;
	size++;
	/* アーム番号	*/
	sprintf(buff,"%4.4x",arm);
	memcpy(&_SENDbuff_[arm][size],&buff[0],4);
	size+=4;

	/* サイズを格納する先頭位置を記憶しておく */
	size_pos=size;

	size+=8;	/* サイズ＋予備の８バイト分移動する */

	/* 構造体領域を記憶させたsend要求構造体を連結し、サイズを求める */
	if(_SENDblock_[arm][HED_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.hed,sizeof(HED));
		len+=sizeof(HED);
		size+=sizeof(HED);
	}
	if(_SENDblock_[arm][DWN_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.dwn,sizeof(DWN));
		len+=sizeof(DWN);
		size+=sizeof(DWN);
	}
	if(_SENDblock_[arm][PLY_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.ply,sizeof(PLY));
		len+=sizeof(PLY);
		size+=sizeof(PLY);
	}
	if(_SENDblock_[arm][NOA_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.noa,sizeof(NOA));
		len+=sizeof(NOA);
		size+=sizeof(NOA);
	}
	if(_SENDblock_[arm][JMP_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.jmp,sizeof(JMP));
		len+=sizeof(JMP);
		size+=sizeof(JMP);
	}
	if(_SENDblock_[arm][BSE_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.bse,sizeof(BSE));
		len+=sizeof(BSE);
		size+=sizeof(BSE);
	}
	if(_SENDblock_[arm][PRM_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.prm,sizeof(PRM));
		len+=sizeof(PRM);
		size+=sizeof(PRM);
	}
	if(_SENDblock_[arm][WRK1_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.wrk[0],sizeof(MRK));
		len+=sizeof(MRK);
		size+=sizeof(MRK);
	}
	if(_SENDblock_[arm][WRK2_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.wrk[1],sizeof(MRK));
		len+=sizeof(MRK);
		size+=sizeof(MRK);
	}
	if(_SENDblock_[arm][MAT_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.mat,sizeof(MRK));
		len+=sizeof(MRK);
		size+=sizeof(MRK);
	}
	if(_SENDblock_[arm][TRN_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.trn,sizeof(TRN));
		len+=sizeof(TRN);
		size+=sizeof(TRN);
	}
	if(_SENDblock_[arm][DIO_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.dio,sizeof(DIO));
		len+=sizeof(DIO);
		size+=sizeof(DIO);
	}
	if(_SENDblock_[arm][GTE_AREA]){
		memcpy(&_SENDbuff_[arm][size],&_Scomuni_[arm].hd.gte,sizeof(GTE));
		len+=sizeof(GTE);
		size+=sizeof(GTE);
	}

	/* サイズ	*/
	sprintf(buff,"%4.4x",len);
	memcpy(&_SENDbuff_[arm][size_pos],&buff[0],4);
	/* 予備	*/
	strcpy(buff,"FFFF");
	memcpy(&_SENDbuff_[arm][size_pos+4],&buff[0],4);
	return(size);
}

/******************************************************************

******************************************************************/
DllExport long __stdcall WinLANinit(long arm,char *tgt)
{
	WSADATA			WSAData;
	SOCKADDR_IN		remote_sin;
	HOSTENT			*he;
	int				st=0;
	u_short			n;
	unsigned long	flg;

	flg=GetArmFlg(arm);
	if(!(_SOCKflg_&flg)){

		memset(&_SENDblock_[arm],0,sizeof(int)*MAX_BLOCK);

		if(WSAStartup(MAKEWORD(1,1),&WSAData)!=0){
			return(ERR_SOCK_CREATE);
		}
		_sock_[arm]=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
		if(_sock_[arm]==INVALID_SOCKET){
			return(ERR_SOCK_CREATE);
		}
		
		he=gethostbyname(tgt);
		if(he==NULL){
			closesocket(_sock_[arm]);
			return(ERR_SOCK_HOST);
		}

		_SOCKevent_[arm]=WSACreateEvent();
		if(_SOCKevent_[arm]==WSA_INVALID_EVENT){
			closesocket(_sock_[arm]);
			return(ERR_SOCK_CREATE);
		}

		remote_sin.sin_family = AF_INET;
//		n=(unsigned short)(SOCK_PORT+arm);
		n=(unsigned short)(SOCK_PORT);
		remote_sin.sin_port = htons(n);
		remote_sin.sin_addr = *((LPIN_ADDR)*he->h_addr_list);

		if(connect(_sock_[arm],(struct sockaddr FAR *)&remote_sin,sizeof(remote_sin))==SOCKET_ERROR){
			closesocket(_sock_[arm]);
			return(ERR_SOCK_CONNECT);
		}

		WSAEventSelect(_sock_[arm], _SOCKevent_[arm], FD_READ|FD_CLOSE);

		Sleep(100);

		_SENDsize_[arm]=MakeOpenCmd(arm);
		st=SendSOCK(arm,_SENDsize_[arm]);
		if(st){
			closesocket(_sock_[arm]);
			return(st);
		}

		st=ReceiveSOCK(arm);
		if(st<0){
			closesocket(_sock_[arm]);
			return(st);
		}

		_SOCKflg_ |= flg;

	}
	return(0);
}
/******************************************************************
	WinRT Close
******************************************************************/
DllExport long __stdcall WinLANabort(long arm)
{
	int				st;
	unsigned long	flg;
	flg=GetArmFlg(arm);
	if(_SOCKflg_ & flg){
		_SENDsize_[arm]=MakeCloseCmd(arm);
		st=SendSOCK(arm,_SENDsize_[arm]);
		if(st!=0)	return(st);
		st=ReceiveSOCK(arm);
		closesocket(_sock_[arm]);
		_SOCKflg_ &= ~flg;
	}
	return(st);
}
/******************************************************************
	サイズnのsrcをdestにメモリコピ−(1バイト単位コピ-)をする
		引き数	dest	コピ-先のアドレス
			src	コピ-元のアドレス
			n	コピ-元のサイズ
			sw	コピ-方向
					=1:  VME  -> ロ-カル
					=2:  ロ-カル -> VME
					=3:  VME  -> VME
		戻値	ERR_OK	正常終了
******************************************************************/
DllExport long __stdcall pa_mem_blk(long arm,void *dest,void *src,size_t n, int sw)
{
	int				st;
	unsigned char	*p;
	unsigned long	flg;

	flg=GetArmFlg(arm);
	if(!(_SOCKflg_&flg))		return(ERR_SOCK_NOTHING);
	switch(sw){
	case	1:		// 運動制御ボード → ME
		st=RecvData(arm,dest,src,n,FALSE);	// destはローカルバッファ、srcは仮想comuni
		break;
	case	2:		// ME → 運動制御ボード
		st=SendData(arm,dest,src,n,FALSE);	// destは仮想comuni、srcはローカルバッファ
		break;
	case	3:		// 運動制御ボード → 運動制御ボード
		p=(unsigned char *)malloc(n);
		st=RecvData(arm,p,src,n,FALSE);		// destはローカルバッファ、srcは仮想comuni
		st=SendData(arm,dest,p,n,FALSE);		// destは仮想comuni、srcはローカルバッファ
		free(p);
		break;
	}
	_SOCKerr_[arm]=st;
	if(st<0)		return(st);
	_SOCKerr_[arm]=0;
	return(0);
}

/******************************************************************
	サイズnのsrcをdestにメモリコピ−(1バイト単位コピ-)をする
		引き数	dest	コピ-先のアドレス
			src	コピ-元のアドレス
			n	コピ-元のサイズ
			sw	コピ-方向
					=1:  VME  -> ロ-カル
					=2:  ロ-カル -> VME
					=3:  VME  -> VME
		戻値	ERR_OK	正常終了
******************************************************************/
DllExport long __stdcall pa_mem_cpy(long arm,void *dest,void *src,size_t n,int sw)
{
	int				st;
	unsigned char	*p;
	unsigned long	flg;

	flg=GetArmFlg(arm);
	if(!(_SOCKflg_&flg))		return(ERR_SOCK_NOTHING);
	switch(sw){
	case	1:		// 運動制御ボード → ME
		st=RecvData(arm,dest,src,n,TRUE);	// destはローカルバッファ、srcは仮想comuni
		break;
	case	2:		// ME → 運動制御ボード
		st=SendData(arm,dest,src,n,TRUE);	// destは仮想comuni、srcはローカルバッファ
		break;
	case	3:		// 運動制御ボード → 運動制御ボード
		p=(unsigned char *)malloc(n);
		st=RecvData(arm,p,src,n,TRUE);		// destはローカルバッファ、srcは仮想comuni
		st=SendData(arm,dest,p,n,TRUE);		// destは仮想comuni、srcはローカルバッファ
		free(p);
		break;
	}
	_SOCKerr_[arm]=st;
	if(st<0)		return(st);
	_SOCKerr_[arm]=0;
	return(0);
}

/******************************************************************
	long(INT32)型のsrcをdestにメモリコピ−(1バイト単位コピ-)をする
		引き数	dest	コピ-先のアドレス
			src	コピ-元のアドレス
			sw	コピ-方向
					=1:  VME  -> ロ-カル
					=2:  ロ-カル -> VME
					=3:  VME  -> VME

		戻値	ERR_OK	正常終了
******************************************************************/
DllExport long __stdcall pa_cpy_lng(long arm,void *dest,void *src, int sw)
{
	int	st;
	st=pa_mem_cpy(arm,dest,src,sizeof(long),sw);
	return(st);
}

/******************************************************************
	float(REAL32)型のsrcをdestにメモリコピ−(1バイト単位コピ-)をする
		引き数	dest	コピ-先のアドレス
			src	コピ-元のアドレス
			sw	コピ-方向
					=1:  VME  -> ロ-カル
					=2:  ロ-カル -> VME
					=3:  VME  -> VME

		戻値	ERR_OK	正常終了
******************************************************************/
DllExport long __stdcall pa_cpy_flt(long arm,void *dest,void *src,int sw)
{
	int	st;
	st=pa_mem_cpy(arm,dest,src,sizeof(float),sw);
	return(st);
}

/******************************************************************
	long(INT32)型のsrcをコピ−(1バイト単位コピ-)し戻り値にする
		コピ-方向 :   VME  -> return値
		引き数	src	コピ-元のアドレス

		戻値		コピ-情報
******************************************************************/
DllExport long __stdcall pa_ret_lng(long arm,void *src)
{
	static	long	tmp;
	pa_mem_cpy(arm,(void*)&tmp,src,sizeof(long),1);
//	pa_cpy_lng(arm,&tmp,src,1);
	return(tmp);
}

/******************************************************************
	unsigned char(UBYTE)型のsrcをdestにメモリコピ−(1バイト単位コピ-)をする
		引き数	dest		コピ-先のアドレス
			src		コピ-元のアドレス
			sw		コピ-方向
					=1:  VME  -> ロ-カル
					=2:  ロ-カル -> VME
					=3:  VME  -> VME
		戻値	ERR_OK		正常終了


		制御基盤へのＡ１６での割り込み専用

******************************************************************/
DllExport long __stdcall pa_cpy_uby(long arm,void *dest,void *src, int sw)
{
	int		st=0;
	_SENDsize_[arm]=MakeSendCmd(arm);
	st=SendSOCK(arm,_SENDsize_[arm]);
	if(st)		return(st);
	st=ReceiveSOCK(arm);
	if(st<0)	return(st);
	/* sendした構造体領域を記憶するバッファをクリアする */
	memset(&_SENDblock_[arm],0,sizeof(int)*MAX_BLOCK);
	return(0);
}


/***************************************************************
	アームの動作が許可されているかどうかの確認
		引数	armno		アームNo.
				arm			ボードアドレス格納ポインタ　ex. 8 ハ 0x800000

		戻値	ERR_OK		正常終了
				ERR_ARM		アームNo.が間違っている
***************************************************************/
static long chk_stat(ARM armno, short* arm)
{
	switch(armno){
	case ARM0:		*arm=0;		break;
	case ARM1:		*arm=1;		break;
	case ARM2:		*arm=2;		break;
	case ARM3:		*arm=3;		break;
	case ARM4:		*arm=4;		break;
	case ARM5:		*arm=5;		break;
	case ARM6:		*arm=6;		break;
	case ARM7:		*arm=7;		break;
	case ARM8:		*arm=8;		break;
	case ARM9:		*arm=9;		break;
	case ARM10:		*arm=10;	break;
	case ARM11:		*arm=11;	break;
	case ARM12:		*arm=12;	break;
	case ARM13:		*arm=13;	break;
	case ARM14:		*arm=14;	break;
	case ARM15:		*arm=15;	break;
	default:			return ERR_ARM;
	}
	return ERR_OK;
}

/***************************************************************
	4ポートRAMをローカルアドレスにマッピングする。
		引数	armno		アームNo.

		戻値	ERR_OK		正常終了.
				ERR_ARM		アームNo.が間違っている.
***************************************************************/
DllExport long __stdcall pa_map_ctl(ARM armno)
{
	long	err=0;
	return err;
}

/***************************************************************
 	486に4ポートRAMに書き込みが完了したことを知らせる。
		引数	num		リトライ回数

		戻値	ERR_OK		正常終了.
				ERR_INT		割り込みが受信されなかった.
*******************************************************************/
DllExport long __stdcall pa_req_ctl(ARM armno, long num)
{
	unsigned char	ubdmy;
	int				st;
	/* 割り込みをかける前の動作完了カウンタを取得しておく	*/
	st=pa_cpy_lng(	(long)armno,&(_d14_[armno]),&(_comuni->hu.err.dmy[14]),1);
	if(st<0)	return(0);
	/* 割り込みをかける */
    st=pa_cpy_uby(	(long)armno,(void *)NULL,&ubdmy,2 );
	return(0);
}

/***************************************************************
 	486に4ポートRAMに書き込みが完了したことを知らせる。
		引数	num		リトライ回数

		戻値	ERR_OK		正常終了.
				ERR_INT		割り込みが受信されなかった.
*******************************************************************/
DllExport long __stdcall pa_req_sub(ARM armno, long num)
{
	unsigned char	ubdmy;
	int				st;
	/* 割り込みをかける前の動作完了カウンタを取得しておく	*/
	st=pa_cpy_lng(	(long)armno,&(_d3_[armno]),&(_comuni->hu.err.dmy[14]),1);
	if(st<0)	return(0);
	/* 割り込みをかける */
    st=pa_cpy_uby(	(long)armno,(void *)NULL,&ubdmy,2 );
	return(0);
}
/*********************************************************************
	エベント完了カウンタのカウントアップを待つ。
		  _d14には、割り込みをかける前の（_comuni->hu.ErrorNote.dummy[14]）
		が保存されている。

		引数	armno		アームNo.

		戻値	0	カウントアップされた（CTLでエベント処理が終了した）
				1	カウントアップされていない（CTLでエベント処理が終了していない）
**********************************************************************/
DllExport short __stdcall pa_fsh_chk(ARM armno)
{
	static	long	ldmy;
	LockMutex((long)armno);
	if( ERR_OK!=pa_map_ctl(armno) )return ERR_OK;
	pa_cpy_lng((long)armno,	&ldmy,&(_comuni->hu.err.dmy[14]),1);
	UnlockMutex((long)armno);
	return _d14_[armno]!=ldmy?0:1;
}


/*********************************************************************
	エベント完了カウンタのカウントアップを待つ。
	_d3には、割り込みをかける前の（_comuni->hu.ErrorNote.dummy[3]）
	が保存されている。

	引数	armno		アームNo.

	戻値	0	カウントアップされた（CTLでエベント処理が終了した）
			1	カウントアップされていない（CTLでエベント処理が終了していない）
**********************************************************************/
DllExport short __stdcall pa_fsh_sub(ARM armno)
{
	long	ldmy;
	LockMutex((long)armno);
	if( ERR_OK!=pa_map_ctl(armno) )return ERR_OK;
	pa_cpy_lng(	(long)armno,&ldmy,&(_comuni->hu.err.dmy[3]),1);
	UnlockMutex((long)armno);
	return _d3_[armno]!=ldmy?0:1;
}

/***************************************************************
	initialize

		戻値	ERR_OK		正常終了
***************************************************************/
DllExport long __stdcall pa_ini_sys(void)
{
	return ERR_OK;
}
/***************************************************************
	terminate

		戻値	ERR_OK		正常終了
***************************************************************/
DllExport long __stdcall pa_ter_sys(void)
{
	return ERR_OK;
}
/***************************************************************
	オープンアーム（LAN版）
		引数	armno		アームNo.

		戻値	ERR_OK		正常終了
***************************************************************/
DllExport long __stdcall pa_con_arm(ARM armno,char* tgt)
{
	long	st;
	MakeMutex((long)armno);
	LockMutex((long)armno);
	st=WinLANinit((long)armno,tgt);
	_SOCKerr_[armno]=st;
	UnlockMutex((long)armno);
	return(st);
}
/***************************************************************
	クローズアーム
		引数	armno		アームNo.

		戻値	ERR_OK		正常終了
***************************************************************/
DllExport long __stdcall pa_cls_arm(ARM armno)
{
	long	st;
	LockMutex((long)armno);
	st=WinLANabort((long)armno);
	_SOCKerr_[armno]=st;
	UnlockMutex((long)armno);
	DestroyMutex((long)armno);
	return st;
}

