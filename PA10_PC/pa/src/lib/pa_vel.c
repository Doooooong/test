/*


	PAlib velocity control function


*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	®�������-�ɤ����ꤹ�롣
		����	armno		������
			vm		®�٥⡼��
			axis		������

		����	ERR_OK		���ｪλ
			ERR_ARM		�����ब�ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_VM		VELMPDE���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_mod_vel(ARM armno, VELMODE vm, AXIS axis)
{
	u32	ldmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	switch(vm){
	case VM_XYZ:
		ldmy = (u32)	RMRCStartEvent3;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case VM_YPR:
		ldmy = (u32)	RMRCStartEvent10;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case VM_xyz:
		ldmy = (u32)	RMRCStartEvent4;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case VM_ypr:
		ldmy = (u32)	RMRCStartEvent5;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case VM_ONE:
		if(axsnum==6 && axis&S3)	axis=axis-S3;							//	6���λ���S3̵��
		ldmy = (u32)	EachVelocityEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case VM_XYZYPR:
		ldmy = (u32)	RMRCStartEvent16;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case VM_xyzypr:
		ldmy = (u32)	RMRCStartEvent17;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_VM;
	}
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	axis;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
