#include	<stdio.h>
#include	<stdlib.h>
#include	<fcntl.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

#ifndef M_PI
#define M_PI        3.1415926535897932384626433832795
#endif
#define	RadToDeg(x) (float)((x)*180.0/M_PI)
#define	DegToRad(x) (float)((x)*M_PI/180.0)

/******************************************************************
	ʸ����򥳥�޶��ڤ�Ǽ���
	��strtok�Ǥ�Ϣ³����ޤδ֤�����Ǥ��ʤ��ä������
******************************************************************/
char *strtokex(char *szSrc, const char cDelimit)
{
	static char		*szStart;
	char			*szRet;
	int				i;

	if(szSrc)		szStart=szSrc;

	for(i=0; ; i++){
	 	if(cDelimit==szStart[i] || szStart[i]==0x0a || szStart[i]==0x0d){
			szStart[i]=(char)NULL;

			szRet=szStart;
			szStart+=(i+1);

			return(szRet);
		}else if(!szStart[i]){
			if(0==i)	return(NULL);

			szRet=szStart;
			szStart+=i;

			return(szRet);
		}
	}
	return(NULL);
}

/******************************************************************
	���ߤζ��������ư���롣
		����	armno		������No.
			pm			��ư�趵�����λ���
			no			jump����λ��ΰ�ư��No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_PM		PNTMOVE���ְ㤤
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_chg_pnt(ARM armno, PNTMOVE pm, u32 no)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	switch(pm){
	case PM_TOP:
		ldmy = (u32)	TopPointerEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PM_NEXT:
		ldmy = (u32)	SForeStepEvent;;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PM_PRIV:
		ldmy = (u32)	SBackStepEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PM_BTM:
		ldmy = (u32)	EndPointerEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PM_JMP:
		ldmy = (u32)	JumpPointerEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&no,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PM_CIR:
		ldmy = (u32)	CircleSerchEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PM_ARC:
		ldmy = (u32)	ArcSerchEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_PM;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	���ߤζ��������ư���롣
		����	armno	������No.
			key		�����ǡ���Key�ֹ����
			cmt		�����Ȼ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_jmp_cmt(ARM armno, u32 key, char* cmt)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_JMP_CMT;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	key;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	cmt[32]='\0';
	if(strlen(cmt)<32){
		st=pa_mem_blk(	(u32)armno,&(_comuni->hd.ply.cmt[0])	,cmt,(size_t)(strlen(cmt)+1),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}
	else{
		st=pa_mem_blk(	(u32)armno,&(_comuni->hd.ply.cmt[0])	,cmt,(size_t)32,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);

}
/******************************************************************
	�����ͤ򶵼������ɲä��롣
		����	armno			������No.
			pt			�������ɲð��֤ȥ�-������

		����	ERR_OK			���ｪλ
			ERR_ARM			������No.���ְ�äƤ���.
			ERR_INT			�����ߤ���������ʤ��ä�.
			ERR_PT			PNTTYPE���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_add_pnt(ARM armno, PNTTYPE pt)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	switch(pt){
	case PT_CP:
		ldmy = (u32)	ReadCPEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_PTP:
		ldmy = (u32)	ReadPTPEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_BCP:
		ldmy = (u32)	ReadCPEvent2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_BPTP:
		ldmy = (u32)	ReadPTPEvent2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_ARC1:
		ldmy = (u32)	ReadArc1Event;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_ARC2:
		ldmy = (u32)	ReadArc2Event;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_ARC3:
		ldmy = (u32)	ReadArc3Event;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_CIR1:
		ldmy = (u32)	ReadCircle1Event;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_CIR2:
		ldmy = (u32)	ReadCircle2Event;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_CIR3:
		ldmy = (u32)	ReadCircle3Event;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_AXS:
		ldmy = (u32)	ReadPTPEvent3;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_BAXS:
		ldmy = (u32)	ReadPTPEvent4;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_POS:
		ldmy = (u32)	ReadPTPEvent5;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_BPOS:
		ldmy = (u32)	ReadPTPEvent6;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_ARC4:
		ldmy = (u32)	ReadArc1Event2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_ARC5:
		ldmy = (u32)	ReadArc2Event2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_ARC6:
		ldmy = (u32)	ReadArc3Event2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_CIR4:
		ldmy = (u32)	ReadCircle1Event2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_CIR5:
		ldmy = (u32)	ReadCircle2Event2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;
	case PT_CIR6:
		ldmy = (u32)	ReadCircle3Event2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		break;

	default:
		UnlockMutex((u32)armno);
		return ERR_PT;
	}
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�������������롣
		����	armno		������No.
			pd		������붵���������

		����	ERR_OK		���ｪλ
			ERR_ARM		ARM���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_PD		PNTDEL���ְ㤤��
			ERR_PRJ		�ץ��������Ȥ�¸�ߤ��ʤ���
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_del_pnt(ARM armno, PNTDEL pd)
{
	extern	u32	pa_act_pnt_sub(ARM,u32);
	extern	u32	pa_ply_set_sub(ARM,u32,u32*);
	u32	ldmy;
	u32	key;	
	u32	num,i;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	switch(pd){
	case PD_CUR:
		ldmy = (u32)	DeleteCPointerEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	0;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	1;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}

		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			UnlockMutex((u32)armno);
			return st;
		}
		while( pa_fsh_chk(armno) );
		UnlockMutex((u32)armno);
		break;
	case PD_FORE:
		ldmy = (u32)	DeletePointerEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	0;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	1;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}

		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			UnlockMutex((u32)armno);
			return st;
		}
		while( pa_fsh_chk(armno) );
		UnlockMutex((u32)armno);
		break;
	case PD_ALL:
		ldmy = (u32)	DeleteAllPointerEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	0;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	1;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}

		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			UnlockMutex((u32)armno);
			return st;
		}
		while( pa_fsh_chk(armno) );
		UnlockMutex((u32)armno);
		break;
	case PD_ALLDATA:
		st=pa_cpy_lng(	(u32)armno,&(num),	&(_comuni->hu.sts.lvl[2]),1);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		if(num==0){
			UnlockMutex((u32)armno);
			return ERR_PRJ;
		}
		for(i=0;i<num;i++){
			if(ERR_OK!=(st=pa_ply_set_sub(armno,0,&key))){
				UnlockMutex((u32)armno);
				return st;
			}
			if(ERR_OK!=(st=pa_act_pnt_sub(armno,key))){
				UnlockMutex((u32)armno);
				return st;
			}

			if( ERR_OK!=(st=pa_map_ctl(armno)) ){
				UnlockMutex((u32)armno);
				return st;
			}
			ldmy = (u32)	DeleteAllPointerEvent;
			st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			ldmy = (u32)	0;
			st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			ldmy = (u32)	1;
			st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}

			if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
				UnlockMutex((u32)armno);
				return st;
			}
			while( pa_fsh_chk(armno) );
		}
		UnlockMutex((u32)armno);
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_PD;
	}
	return pam_get_err(armno);
}

/******************************************************************
	��������°�������ꤹ�롣
		����	armno		������No.
			pa		�ѹ�°�������
			ldat		�ѹ�°����
			fdat		��pa�ˤ����ꤹ��°���ͤ��㤦��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_PA		PntSetID���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_set_pnt(ARM armno, PNTATTR pa, u32* ldat, float fdat)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	switch(pa){
	case PA_CHGVEL:
		ldmy = (u32)	ChVelocityEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.fvl),&fdat,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PA_CHGWAIT:
		ldmy = (u32)	ChWaitTimeEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldat[0],2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PA_VELPTN:
		ldmy = (u32)	ChVptnEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		if(0<=ldat[0]||ldat[0]<=4){
			st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldat[0],2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldat[1],2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldat[2],2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			break;
		}
		UnlockMutex((u32)armno);
		return ERR_PTN;
	case PA_ROTVEL:
		ldmy = (u32)	ChRotateEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.fvl),&fdat,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PA_AXSACC:
		ldmy = (u32)	EV_AXS_ACC;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.fvl),&fdat,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PA_RMRCACC:
		ldmy = (u32)	EV_RMC_ACC;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.fvl),&fdat,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PA_JUMPID:
		ldmy = (u32)	EV_PNT_JMP;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldat[0],2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_PA;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�����ͤǥ����ȥݥ�����ͤ��֤������롣
		����	armno		���������

		����	ERR_OK		���ｪλ
			ERR_ARM		��������꤬�ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_rpl_pnt(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	ReadReplaceEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	���ߤζ�������ư��롣(�ץ쥤�Хå�����ư��)
		����	armno		������No.
			wfp		ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mov_pnt(ARM armno, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	PlayBackReadyEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	��˶�������ư��롣(�ץ쥤�Хå�ư��)
		����	armno		������No.
			pb		ư������
			number		��Ϣ³�ץ쥤�Хå����β������
						��-1�����̵�¡�
			wfp		ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_PB		PLAYBACK���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_ply_pnt(ARM armno, PLAYBACK pb, u32 number, u32 wfp)
{
	u32	st;
	u32	ldmy;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	switch(pb){
	case PB_FORES:
		ldmy = (u32)	ForeStepEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	1;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PB_BACKS:
		ldmy = (u32)	BackStepEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	1;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case PB_FORE:
		ldmy = (u32)	PBTOPStartEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	1;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	number;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_PB;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	��������ե����뤫���-�ɤ��롣
		����	armno	��-��No.
			key	key�ֹ�
			file	��������-���ե���������

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_FILE	�ե����뤬�����ץ�Ǥ��ʤ�
			ERR_INT		�����ߤ���������ʤ��ä�.
******************************************************************/
DllExport u32  pa_lod_pnt(ARM armno, u32 key, char *file)
{
	extern	u32 atox(char*);
	short	i=0;
	PNTDAT	pntdat;
	u32	err;
	static	u32	ldmy;
	u32	cnt=1;
	char	buff[512];
	FILE*	stream;
	u32	num;
	u32	aldmy,rldmy;
	char	cmt[33];
	char	*szTok;
	char	cDelimit=',';
	u32	st;
	PARAM	prm;
	u32	j;

	num=0;

	st=pa_get_prm(armno,&prm);
	if( (stream  = fopen( file, "r" )) != NULL ){
		while(fgets(buff, sizeof(buff), stream)){
			szTok=strtokex(buff, cDelimit);
			if(strcmp(szTok,"JUMP")==0 || strcmp(szTok,"JUMP\n")==0){
				break;
			}
			num++;
		}
		fclose(stream);
	}
	else		return ERR_FILE;

	if(num!=1){
		LockMutex((u32)armno);
		if( ERR_OK!=(err=pa_map_ctl(armno)) ){
			UnlockMutex((u32)armno);
			return err;
		}
		ldmy = (u32)	'S';
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	key;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	1;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	DataLoad;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	3;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
//		UnlockMutex((u32)armno);
	}

	if( (stream  = fopen( file, "r" )) != NULL ){
		while(fgets(buff, sizeof(buff), stream)){
			pntdat.ply.pnt.atr[4]	=(szTok=strtokex(buff, cDelimit))?atol(szTok):0;			/*�̤��ֹ� */
			pntdat.ply.pnt.atr[5]	=(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;			/*������ID */
			memset(pntdat.ply.cmt,0,sizeof(pntdat.ply.cmt));
			if((szTok=strtokex(NULL, cDelimit))){
				int		iLen=strlen(szTok);
				int		iSize=sizeof(pntdat.ply.cmt);
				int		iCopySize=(iLen<=iSize)?iLen:iSize;
				memcpy(pntdat.ply.cmt, szTok, iCopySize);									/*������ */
			}

			pntdat.ply.pnt.agl[0]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/*S1���� */
			pntdat.ply.pnt.agl[1]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/*S2���� */
			pntdat.ply.pnt.agl[2]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/*S3���� */
			pntdat.ply.pnt.agl[3]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/*E1���� */
			pntdat.ply.pnt.agl[4]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/*E2���� */
			pntdat.ply.pnt.agl[5]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/*W1���� */
			pntdat.ply.pnt.agl[6]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/*W2���� */
			pntdat.ply.pnt.vel[0]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/*����®��[mm/sec] */
			pntdat.ply.pnt.vel[1]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/*����®��[deg/sec] */

			szTok=strtokex(NULL, cDelimit);
			if(szTok!=NULL){
				if(strcmp(szTok,"PTP")==0)	pntdat.ply.pnt.atr[0]	=1;					/*������������ PTP/PTP(NOAP) */
				else if(strcmp(szTok,"NOAP")==0)pntdat.ply.pnt.atr[0]	=2;
			}else{
				fclose(stream);
				return ERR_FILE;
			}
			szTok=strtokex(NULL, cDelimit);
			if(szTok!=NULL){
				if(strcmp(szTok,"EACH")==0)		pntdat.ply.pnt.atr[1]	=21;				/*�����ˡ ľ��/��/�߸� */
				else if(strcmp(szTok,"LINE")==0)	pntdat.ply.pnt.atr[1]	=18;
				else if(strcmp(szTok,"ARC")==0)		pntdat.ply.pnt.atr[1]	=17;
				else if(strcmp(szTok,"CIRCLE")==0)	pntdat.ply.pnt.atr[1]	=16;
			}else{
				fclose(stream);
				return ERR_FILE;
			}
			aldmy	=(u32)(((szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f)*10)&0x0000ffff;			/*���١ʳƼ��� */
			rldmy	=((u32)(((szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f)*10)&0x0000ffff)<<16;	/*���١�RMRC�� */
			pntdat.ply.pnt.atr[7]	=aldmy|rldmy;
			szTok=strtokex(NULL, cDelimit);
			if(szTok!=NULL){
				if(strcmp(szTok,"Line")==0)	pntdat.ply.pnt.atr[2]	=0;/*®�٥����� */
				else if(strcmp(szTok,"StartUp")==0)	pntdat.ply.pnt.atr[2]	=1;
				else if(strcmp(szTok,"ShutDown")==0)	pntdat.ply.pnt.atr[2]	=2;
				else if(strcmp(szTok,"Trap")==0)	pntdat.ply.pnt.atr[2]	=3;
			}else{
				fclose(stream);
				return ERR_FILE;
			}
			pntdat.ply.pnt.atr[9]	=(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;				/*��®����[*0.01msec] */
			pntdat.ply.pnt.atr[10]	=(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;				/*��®����[*0.01msec] */
			pntdat.ply.pnt.atr[8]	=(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;				/*JUMP����ֹ� */
			pntdat.ply.pnt.atr[6]	=(szTok=strtokex(NULL, cDelimit))?atox(szTok):0;				/*DO�����16�ʿ��� */
			pntdat.ply.pnt.atr[3]	=(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;				/*�Ԥ�����[msec] */
			if(pntdat.ply.pnt.atr[0]==2){
				pntdat.noa.xyz[0]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.xyz[1]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.xyz[2]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.noa[0][0]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.noa[0][1]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.noa[0][2]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.noa[1][0]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.noa[1][1]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.noa[1][2]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.noa[2][0]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.noa[2][1]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
				pntdat.noa.noa[2][2]	=(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;	/* */
			}
			pntdat.ply.pnt.agl[0]=DegToRad(pntdat.ply.pnt.agl[0]);	/*S1����*/
			pntdat.ply.pnt.agl[1]=DegToRad(pntdat.ply.pnt.agl[1]);	/*S2����*/
			pntdat.ply.pnt.agl[2]=DegToRad(pntdat.ply.pnt.agl[2]);	/*S3����*/
			pntdat.ply.pnt.agl[3]=DegToRad(pntdat.ply.pnt.agl[3]);	/*E1����*/
			pntdat.ply.pnt.agl[4]=DegToRad(pntdat.ply.pnt.agl[4]);	/*E2����*/
			pntdat.ply.pnt.agl[5]=DegToRad(pntdat.ply.pnt.agl[5]);	/*W1����*/
			pntdat.ply.pnt.agl[6]=DegToRad(pntdat.ply.pnt.agl[6]);	/*W2����*/
			for(j=0;j<6;j++){
				if(pntdat.ply.pnt.agl[j]>prm.pul[j]-0.001f || pntdat.ply.pnt.agl[j]<prm.pdl[j]+0.001f)	return ERR_PNT;
			}
			pntdat.ply.pnt.vel[1]=DegToRad(pntdat.ply.pnt.vel[1]);	/*����®��[deg/sec]*/

			pa_map_ctl(armno);

			st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[0]),&(pntdat.ply.pnt.agl[0]),2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[1]),&(pntdat.ply.pnt.agl[1]),2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[2]),&(pntdat.ply.pnt.agl[2]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[3]),&(pntdat.ply.pnt.agl[3]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[4]),&(pntdat.ply.pnt.agl[4]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[5]),&(pntdat.ply.pnt.agl[5]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[6]),&(pntdat.ply.pnt.agl[6]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.vel[0]),&(pntdat.ply.pnt.vel[0]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.vel[1]),&(pntdat.ply.pnt.vel[1]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[0]),&(pntdat.ply.pnt.atr[0]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[1]),&(pntdat.ply.pnt.atr[1]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[2]),&(pntdat.ply.pnt.atr[2]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[3]),&(pntdat.ply.pnt.atr[3]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[4]),&(pntdat.ply.pnt.atr[4]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[5]),&(pntdat.ply.pnt.atr[5]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[6]),&(pntdat.ply.pnt.atr[6]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[7]),&(pntdat.ply.pnt.atr[7]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[8]),&(pntdat.ply.pnt.atr[8]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[9]),&(pntdat.ply.pnt.atr[9]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[10]),&(pntdat.ply.pnt.atr[10]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[11]),&(pntdat.ply.pnt.atr[11]),2	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}

			if(strlen(pntdat.ply.cmt)<32){
				st=pa_mem_blk(	(u32)armno,&(_comuni->hd.ply.cmt[0])	,&(pntdat.ply.cmt[0]),(size_t)(strlen(pntdat.ply.cmt)+1),2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
			}
			else{
				memcpy(cmt,pntdat.ply.cmt,33);
				st=pa_mem_blk(	(u32)armno,&(_comuni->hd.ply.cmt[0])	,cmt,(size_t)32	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
			}
			if(pntdat.ply.pnt.atr[0]==2){
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.xyz[0]),	&(pntdat.noa.xyz[0])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.xyz[1]),	&(pntdat.noa.xyz[1])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.xyz[2]),	&(pntdat.noa.xyz[2])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.noa[0][0]),&(pntdat.noa.noa[0][0])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.noa[0][1]),&(pntdat.noa.noa[0][1])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.noa[0][2]),&(pntdat.noa.noa[0][2])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.noa[1][0]),&(pntdat.noa.noa[1][0])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.noa[1][1]),&(pntdat.noa.noa[1][1])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.noa[1][2]),&(pntdat.noa.noa[1][2])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.noa[2][0]),&(pntdat.noa.noa[2][0])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.noa[2][1]),&(pntdat.noa.noa[2][1])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	armno,&(_comuni->hd.noa.noa[2][2]),&(pntdat.noa.noa[2][2])	,2	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
			}

			i++;
			if(num==i){
				ldmy = (u32)	'E';
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				ldmy = (u32)	key;
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				ldmy = (u32)	1;
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				ldmy = (u32)	DataLoad;
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				ldmy = (u32)	3;
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}

				if( ERR_OK!=(err=pa_req_ctl(armno,RETRY_COUNT)) ){
					UnlockMutex((u32)armno);
					return err;
				}

				while( pa_fsh_chk(armno) );
				UnlockMutex((u32)armno);
				if( pam_get_err(armno) )break;
				break;
			}
			if(num!=i && num>2 && i!=1){
//				LockMutex((u32)armno);
				pa_map_ctl(armno);
				ldmy = (u32)	'C';
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				ldmy = (u32)	key;
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				ldmy = (u32)	1;
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				ldmy = (u32)	DataLoad;
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				ldmy = (u32)	3;
				st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
//				UnlockMutex((u32)armno);
			}

			if( ERR_OK!=(err=pa_req_ctl(armno,RETRY_COUNT)) ){
				UnlockMutex((u32)armno);
				return err;
			}
//			UnlockMutex((u32)armno);
			while( pa_fsh_chk(armno) );
			if( pam_get_err(armno) )break;

		}
		fclose(stream);
	}
	else		return ERR_FILE;
	
	return pam_get_err(armno);
}

/******************************************************************
	��������ե�����˥�-�֤��롣
		����	armno	��-��No.
			file	��������-���ե���������

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_OPEN	pa_opn_arm()�¹Ԥ���Ƥ��ʤ�.
			ERR_FILE	�ե����뤬�����ץ�Ǥ��ʤ�
			ERR_INT		�����ߤ���������ʤ��ä�.
******************************************************************/
DllExport u32  pa_sav_pnt(ARM armno, char *file)
{
	u32	err=0;
	static	u32	datatype,Tnum;
	short	flg=1;
	PNTDAT	pntdat;
	u32	ldmy;
	FILE*	stream;
	float	afdmy;
	float	rfdmy;
	char	cmt[33];
	u32	st;


	if( (stream  = fopen( file, "w" )) != NULL ){
		datatype = 1;
		Tnum = 0;
		LockMutex((u32)armno);
		while(flg){
//			LockMutex((u32)armno);
			if( ERR_OK!=(err=pa_map_ctl(armno)) ){
				UnlockMutex((u32)armno);
				return err;
			}

			ldmy = (u32)	DataSave;
			st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			ldmy = (u32)	3;
			st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}

			if( ERR_OK!=(err=pa_req_ctl(armno,RETRY_COUNT)) ){
				UnlockMutex((u32)armno);
				return err;
			}
//			UnlockMutex((u32)armno);

			while( pa_fsh_chk(armno) );
			if( pam_get_err(armno) )break;

//			LockMutex((u32)armno);
			pa_map_ctl(armno);

			if(datatype)Tnum++;
			ldmy = (u32)	Tnum;
			st=pa_cpy_lng(	(u32)armno,&ldmy,&(_comuni->hu.ply.pnt.atr[0]),1);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			datatype = (short)	ldmy;
			st=pa_cpy_flt(	(u32)armno,&(pntdat.ply.pnt.agl[0])	,&(_comuni->hu.ply.pnt.agl[0])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(	(u32)armno,&(pntdat.ply.pnt.agl[1])	,&(_comuni->hu.ply.pnt.agl[1])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(	(u32)armno,&(pntdat.ply.pnt.agl[2])	,&(_comuni->hu.ply.pnt.agl[2])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(	(u32)armno,&(pntdat.ply.pnt.agl[3])	,&(_comuni->hu.ply.pnt.agl[3])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(	(u32)armno,&(pntdat.ply.pnt.agl[4])	,&(_comuni->hu.ply.pnt.agl[4])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(	(u32)armno,&(pntdat.ply.pnt.agl[5])	,&(_comuni->hu.ply.pnt.agl[5])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(	(u32)armno,&(pntdat.ply.pnt.agl[6])	,&(_comuni->hu.ply.pnt.agl[6])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(	(u32)armno,&(pntdat.ply.pnt.vel[0])	,&(_comuni->hu.ply.pnt.vel[0])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt(	(u32)armno,&(pntdat.ply.pnt.vel[1])	,&(_comuni->hu.ply.pnt.vel[1])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[0])	,&(_comuni->hu.ply.pnt.atr[0])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[1])	,&(_comuni->hu.ply.pnt.atr[1])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[2])	,&(_comuni->hu.ply.pnt.atr[2])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[3])	,&(_comuni->hu.ply.pnt.atr[3])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[4])	,&(_comuni->hu.ply.pnt.atr[4])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[5])	,&(_comuni->hu.ply.pnt.atr[5])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[6])	,&(_comuni->hu.ply.pnt.atr[6])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[7])	,&(_comuni->hu.ply.pnt.atr[7])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[8])	,&(_comuni->hu.ply.pnt.atr[8])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[9])	,&(_comuni->hu.ply.pnt.atr[9])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[10])	,&(_comuni->hu.ply.pnt.atr[10])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_lng(	(u32)armno,&(pntdat.ply.pnt.atr[11])	,&(_comuni->hu.ply.pnt.atr[11])	,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}

			st=pa_mem_blk(	(u32)armno,cmt	,&(_comuni->hu.ply.cmt[0])	,(size_t)32,1	);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			cmt[32]='\0';

			afdmy=(float)(pntdat.ply.pnt.atr[7]&0x0000ffff)/10;
			rfdmy=(float)(pntdat.ply.pnt.atr[7]>>16&0x0000ffff)/10;
			
			fprintf(stream,"%d,%d,%s,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,",
				pntdat.ply.pnt.atr[4],										/*�̤��ֹ�*/
				pntdat.ply.pnt.atr[5],										/*������ID*/
				cmt,														/*������*/
				RadToDeg(pntdat.ply.pnt.agl[0]),							/*S1����*/
				RadToDeg(pntdat.ply.pnt.agl[1]),							/*S2����*/
				RadToDeg(pntdat.ply.pnt.agl[2]),							/*S3����*/
				RadToDeg(pntdat.ply.pnt.agl[3]),							/*E1����*/
				RadToDeg(pntdat.ply.pnt.agl[4]),							/*E2����*/
				RadToDeg(pntdat.ply.pnt.agl[5]),							/*W1����*/
				RadToDeg(pntdat.ply.pnt.agl[6]),							/*W2����*/
				pntdat.ply.pnt.vel[0],										/*����®��[mm/sec]*/
				RadToDeg(pntdat.ply.pnt.vel[1])								/*����®��[deg/sec]*/
			);
			if(pntdat.ply.pnt.atr[0]==1)		fprintf(stream,"PTP,");		/*������������ PTP/PTP(NOAP)*/
			else if(pntdat.ply.pnt.atr[0]==2)	fprintf(stream,"NOAP,");
			if(pntdat.ply.pnt.atr[1]==21)		fprintf(stream,"EACH,");	/*�����ˡ ľ��/��/�߸�*/
			else if(pntdat.ply.pnt.atr[1]==18)	fprintf(stream,"LINE,");
			else if(pntdat.ply.pnt.atr[1]==17)	fprintf(stream,"ARC,");
			else if(pntdat.ply.pnt.atr[1]==16)	fprintf(stream,"CIRCLE,");
			fprintf(stream,"%.1f,%.1f,",afdmy,rfdmy);						/*���١ʳƼ���*//*���١�RMRC��*/
			if(pntdat.ply.pnt.atr[2]==0)		fprintf(stream,"Line,");	/*®�٥�����*/
			else if(pntdat.ply.pnt.atr[2]==1)	fprintf(stream,"StartUp,");
			else if(pntdat.ply.pnt.atr[2]==2)	fprintf(stream,"ShutDown,");
			else if(pntdat.ply.pnt.atr[2]==3)	fprintf(stream,"Trap,");
			fprintf(stream,"%d,%d,%d,%x,%d",
				pntdat.ply.pnt.atr[9],										/*��®����[*0.01msec]*/
				pntdat.ply.pnt.atr[10],										/*��®����[*0.01msec]*/
				pntdat.ply.pnt.atr[8],										/*JUMP����ֹ�*/
				pntdat.ply.pnt.atr[6],										/*DO�����16�ʿ���*/
				pntdat.ply.pnt.atr[3]										/*�Ԥ�����[msec]*/
			);

			if(pntdat.ply.pnt.atr[0]==2){
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.xyz[0])	,&(_comuni->hu.noa.xyz[0])		,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.xyz[1])	,&(_comuni->hu.noa.xyz[1])		,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.xyz[2])	,&(_comuni->hu.noa.xyz[2])		,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.noa[0][0]),&(_comuni->hu.noa.noa[0][0])	,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.noa[0][1]),&(_comuni->hu.noa.noa[0][1])	,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.noa[0][2]),&(_comuni->hu.noa.noa[0][2])	,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.noa[1][0]),&(_comuni->hu.noa.noa[1][0])	,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.noa[1][1]),&(_comuni->hu.noa.noa[1][1])	,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.noa[1][2]),&(_comuni->hu.noa.noa[1][2])	,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.noa[2][0]),&(_comuni->hu.noa.noa[2][0])	,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.noa[2][1]),&(_comuni->hu.noa.noa[2][1])	,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				st=pa_cpy_flt(	(u32)armno,&(pntdat.noa.noa[2][2]),&(_comuni->hu.noa.noa[2][2])	,1	);
				if(st<0){
					UnlockMutex((u32)armno);
					return st;
				}
				fprintf(stream,",%.3f,%.3f,%.3f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f\n",
					pntdat.noa.xyz[0],pntdat.noa.xyz[1],pntdat.noa.xyz[2],
					pntdat.noa.noa[0][0],pntdat.noa.noa[0][1],pntdat.noa.noa[0][2],
					pntdat.noa.noa[1][0],pntdat.noa.noa[1][1],pntdat.noa.noa[1][2],
					pntdat.noa.noa[2][0],pntdat.noa.noa[2][1],pntdat.noa.noa[2][2]);
			}
			else{
				fprintf(stream,",,,,,,,,,,,,\n");
			}

			st=pa_cpy_lng(	(u32)armno,&ldmy,&(_comuni->hu.hed.dmy[0]),1);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			if( ldmy == (u32)'E' )flg=0;
//			UnlockMutex((u32)armno);
		}
		UnlockMutex((u32)armno);
		fclose(stream);
	}
	else		return ERR_FILE;

	return pam_get_err(armno);
}

/******************************************************************
	�ץ쥤�Хå�®�ٷ���
		����	armno	��-��No.
			cof	�ץ쥤�Хå�®�ٷ��������( 0.09 < cof < 3.1 )

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
******************************************************************/
DllExport u32  pa_vel_pnt(ARM armno, float cof)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	PBVelocityEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.fvl),&cof,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	���ߤζ������˳Ƽ������ư��롣(�ץ쥤�Хå��Ƽ�����ư��)
		����	armno		������No.
			wfp		ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_axs_pnt(ARM armno, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	PlayBackReadyEvent3;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�����ƥ��֤ʶ����ǡ������ڴ���
		����	armno		������No.
			key		�����ǡ�����Key�ֹ�

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_act_pnt(ARM armno, u32 key)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_ACT_PNT;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	key;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	���ߥ����ƥ��֤ʶ����ǡ�����Key���ѹ�����
		����	armno		������No.
			key		�����ǡ�����Key�ֹ�

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_chg_key(ARM armno, u32 key)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_CHG_KEY;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	key;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�ֹ����Ƕ����ǡ���Key����
		����	armno		������No.
			number		�����ǡ������ֹ�
			*key		�����ǡ�����Key�ֹ�

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_ply_set(ARM armno, u32 number, u32* key)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_PLY_SET;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	number;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);

	if( ERR_OK==(st=pam_get_err(armno)) ){
		pa_map_ctl(armno);
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,key,&(_comuni->hu.sts.lvl[1]),1)))	return st;
	}
	
	return pam_get_err(armno);
		
}

/******************************************************************
	�����ƥ��ֶ����ǡ���Key����
		����	armno		������No.
			*key		Key�ֹ�

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_get_key(ARM armno, u32* key)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,key,&(_comuni->hu.sts.lvl[0]),1)))	return st;

	return pam_get_err(armno);
}

/******************************************************************
	�����ǡ�������������
		����	armno		������No.
			cmt			������

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_set_cmt(ARM armno, char* cmt)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_SET_CMT;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	cmt[32]='\0';
	if(strlen(cmt)<32){
		st=pa_mem_blk((u32)armno,&(_comuni->hd.ply.cmt[0]),cmt,(size_t)(strlen(cmt)+1),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}
	else{
		st=pa_mem_blk((u32)armno,&(_comuni->hd.ply.cmt[0]),cmt,(size_t)32,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}
	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}


/******************************************************************
	�����ȥ�����ʥץ쥤�Хå����֡ˤμ���
		����	armno		������No.
			tim		�����ȥ�����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_tct_tim(ARM armno, u32* tim)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,tim,&(_comuni->hu.sts.lvl[10]),1)))	return st;

	return pam_get_err(armno);

}
