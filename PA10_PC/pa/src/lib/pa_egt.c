/*

	PAlib arm status get function

*/
#include	<stdio.h>
#include	<stdlib.h>
#include	<fcntl.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	��ư����ãУդΥ��ơݥ������������
		����	armno		������No.
			div			��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_mod(ARM armno, u32* stat)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,stat,&(_comuni->hu.hed.sts),1)))	return st;
	return pam_get_err(armno);
}

/******************************************************************
	��������°�������롣
		����	armno		������No.
			idn			°����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_PA		PntSetID���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_idn(ARM armno, u32* idn)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,idn,&(_comuni->hu.sts.ply.pnt.atr[5]),1)))	return st;
	return pam_get_err(armno);
}


/******************************************************************
	�ץ쥤�Хå���®�ٷ���������������
		����	armno		������No.
			div			��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_pvl(ARM armno, float* div)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,div,&(_comuni->hu.get.fvl[2]),1)))	return st;
	return pam_get_err(armno);
}

/******************************************************************
	�ץ쥤�Хå����ģɣϽ��Ϥ򤹤롿���ʤ����������
		����	armno		������No.
			stat		��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_pdo(ARM armno ,u32* stat)
{
	u32	ldmy;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&ldmy,&(_comuni->hu.get.lvl[3]),1)))	return st;
	if(ldmy==1)	*stat=(u32)1;
	else		*stat=(u32)0;
	return pam_get_err(armno);
}

/******************************************************************
	�ңͣң�����ˤ������Ĺ�������ݥɤ��������
		����	armno		������No.
			stat		��Ǽ�ݥ���
		
		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_jou(ARM armno, u32* stat)
{
	u32	ldmy;
	u32	axsnum;
	u32	st;
	
	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1)))	return st;		//	�����༴������
	if(axsnum==6)		return ERR_OK;

	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&ldmy,&(_comuni->hu.get.lvl[4]),1)))	return st;
	if(ldmy==8)		*stat=(u32)JM_S3HOLD;
	else if(ldmy==4)	*stat=(u32)JM_S3DIV;
	else if(ldmy==2)	*stat=(u32)JM_S3ON;
	else if(ldmy==1)	*stat=(u32)JM_ON;
	else 			*stat=(u32)JM_OFF;

	return pam_get_err(armno);
}

/******************************************************************
	��ư����ץ������ΥСݥ������������
		����	armno		������No.
			div			��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_ver(ARM armno, float* ver)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,ver,&(_comuni->hu.ard.ver),1)))	return st;
	return pam_get_err(armno);
}

/******************************************************************
	�ץ쥤�Хå����κ�ɸ�Ѵ�������������
		����	armno		������No.
			mat0		��Ⱥ�ɸ�����Ǽ�ݥ���
			mat1		�����ǡݥ���ɸ�����Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_mat(ARM armno, MATRIX mat0, MATRIX mat1)
{
	u32 	i,j;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	for(i=0;i<3;i++){
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&mat0[i][3],&(_comuni->hu.mat[0].xyz[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&mat1[i][3],&(_comuni->hu.mat[1].xyz[i]),1)))	return st;
		for(j=0;j<3;j++){
			if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&mat0[i][j],&(_comuni->hu.mat[0].noa[i][j]),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&mat1[i][j],&(_comuni->hu.mat[1].noa[i][j]),1)))		return st;
		}
	}
	return pam_get_err(armno);
}
/******************************************************************
	�ץ쥤�Хå����ε�ƻ���ե��å��ͤ��������
		����	armno	������No.
			sns	��ƻ���ե��åȤȤ���Ϳ�����Ƥ��륻�󥵾���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_sns(ARM armno, TRANSMATP sns)
{
	int		i;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(sns->Enable),&(_comuni->hu.trn.Enable),1)))		return st;
	for(i=0;i<3;i++){
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(sns->_xyz[i]),&(_comuni->hu.trn._xyz[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(sns->Ixyz[i]),&(_comuni->hu.trn.Ixyz[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(sns->_XYZ[i]),&(_comuni->hu.trn._XYZ[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(sns->IXYZ[i]),&(_comuni->hu.trn.IXYZ[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(sns->_wave[i]),&(_comuni->hu.trn._wave[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(sns->Iwave[i]),&(_comuni->hu.trn.Iwave[i]),1)))	return st;
	}
	return pam_get_err(armno);
}

/******************************************************************
	�����ȥݥ���Ȥ��ߡ��߸̤λ��Σ��������ܶ�����������������
		����	armno		������No.
			pno		�ߡ��߸̤μ���No��
			pntdat		��������-����¤�ΤؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_cpt(ARM armno, PNTNO pno, PNTDATP pntdat)
{
	u32	ldmy;
	u32	i,j;
	u32	axsnum;
	char	cmt[33];
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	pno;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	GetPntAttrEvent2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);

	if( ERR_OK==(st=pam_get_err(armno)) ){
		pa_map_ctl(armno);

		for(i=0;i<7;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->ply.pnt.agl[i]),&(_comuni->hu.ply.pnt.agl[i]),1)))		return st;
			if(axsnum==6)	pntdat->ply.pnt.agl[2]=0.0f;	
		}
		for(i=0;i<2;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->ply.pnt.vel[i]),&(_comuni->hu.ply.pnt.vel[i]),1)))		return st;
		}
		for(i=0;i<12;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->ply.pnt.atr[i]),&(_comuni->hu.ply.pnt.atr[i]),1)))		return st;
		}
		for(i=0;i<3;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->noa.xyz[i]),&(_comuni->hu.noa.xyz[i]),1)))			return st;
			for(j=0;j<3;j++){
				if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->noa.noa[i][j]),&(_comuni->hu.noa.noa[i][j]),1)))	return st;
			}
		}
		if(ERR_OK!=(st=pa_mem_blk((u32)armno,cmt,&(_comuni->hu.sts.ply.cmt[0]),(size_t)32,1)))					return st;
		memcpy(pntdat->ply.cmt,cmt,32);
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.cid),&(_comuni->hu.jmp.cid),1)))					return st;
		for(i=0;i<8;i++){
			for(j=0;j<2;j++){
				if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].cnd[j]),&(_comuni->hu.jmp.jdg[i].cnd[j]),1)))	return st;
			}
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].xdi),&(_comuni->hu.jmp.jdg[i].xdi),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].tim),&(_comuni->hu.jmp.jdg[i].tim),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].key),&(_comuni->hu.jmp.jdg[i].key),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].pid),&(_comuni->hu.jmp.jdg[i].pid),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].cnt),&(_comuni->hu.jmp.jdg[i].cnt),1)))		return st;
		}
	}

	return pam_get_err(armno);
}

/******************************************************************
	��ɸ�;�����������
		����	armno		������No.
			tar		ARMTARGETE�� �ؤ� �ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_tar(ARM armno, ARMTARGETP tar)
{
	short 	i,j;
	float	fdmy;
	u32	axsnum;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1)))	return st;			//	�����༴������
	
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.tgt.agl[0]),1)))	return st;
	tar->angle.s1=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.tgt.agl[1]),1)))	return st;
	tar->angle.s2=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.tgt.agl[2]),1)))	return st;
	if(axsnum==6)		tar->angle.s3=0.0f;	
	else if(axsnum==7)	tar->angle.s3=(float)(fdmy);

	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.tgt.agl[3]),1)))	return st;
	tar->angle.e1=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.tgt.agl[4]),1)))	return st;
	tar->angle.e2=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.tgt.agl[5]),1)))	return st;
	tar->angle.w1=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.tgt.agl[6]),1)))	return st;
	tar->angle.w2=(float)(fdmy);

	for(i=0;i<3;i++){
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(tar->noap[i][3]),&(_comuni->hu.pos.tgt.xyz[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(tar->ypr[i]),&(_comuni->hu.ypr.tgt[3+i]),1)))		return st;
		for(j=0;j<3;j++){
			if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(tar->noap[i][j]),&(_comuni->hu.pos.tgt.noa[i][j]),1)))	return st;
		}
	}

	return pam_get_err(armno);
}
/******************************************************************
	�����쥯������ˤ�����ޥ˾���(ŷ�ߤꡤ���֤�)���������
		����	armno		������No.
			stat		��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_drt(ARM armno, u32* stat)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,stat,&(_comuni->hu.get.lvl[0]),1)))	return st;	//��-�����
	return pam_get_err(armno);
}

/******************************************************************
	�̿����֡��̿�̵��/����ߥ�-�����/�µ��˼�������
		����	armno		������No.
			stat		��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_com(ARM armno, u32* stat)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,stat,&(_comuni->hu.get.lvl[7]),1)))	return st;	//��-�����
	return pam_get_err(armno);
}

/******************************************************************
	�����ॢ���Ȼ��֤��������
		����	armno		������No.
			tim		��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_tim(ARM armno, u32* tim)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,tim,&(_comuni->hu.get.lvl[1]),1)))	return st;
	return pam_get_err(armno);
}

/******************************************************************
�ץ쥤�Хå���DO����ܥåȤ�ư���Ʊ�����Ƽ�ưŪ����ߤ��뤫�ݤ����������
		����	armno		������No.
			stat		��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_dlc(ARM armno, u32* stat)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,stat,&(_comuni->hu.get.lvl[2]),1)))	return st;
	return pam_get_err(armno);
}

/******************************************************************
	��ü���ե��å��ͤΥ�ߥå��ͤ��������
		����	armno		������No.
			dat			��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_lmt(ARM armno, float* dat)
{
	float	fdmy;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&fdmy,&(_comuni->hu.get.fvl[0]),1)))	return st;
	*dat=(float)fdmy;	
	return pam_get_err(armno);
}

/******************************************************************
	�ӣ�������ף����ޤǤε�Υ(���ݥ�Ĺ)���������
		����	armno		������No.
			len			��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_lgh(ARM armno, float* len)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )		return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,len,&(_comuni->hu.ypr.len[0]),1)))	return st;
	return pam_get_err(armno);
}
