/*


	PAlib joucyou control fuction


*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	®������
		����	armno		������No.
			spd[]		����,����®�٤�[0]..[2]�����
					�Ƽ�®�٤ϡ���[0]..[6]�����
					���ֻ���®�٤�[0]..[5]�����
			
		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_odr_vel(ARM armno, float spd[])
{
	short	i;
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	7;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	for(i=0;i<7;i++){
		st=pa_cpy_flt((u32)armno,&(_comuni->hd.mat.agl[i]),&(spd[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}

	ldmy = (u32)	SendJSEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	��Ĺ������mode
		����	armno		������No.
			jm		mode

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_JM		JOUMODE���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mod_jou(ARM armno, JOUMODE jm)
{
	u32	ldmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if(axsnum==6){
		UnlockMutex((u32)armno);
		return ERR_OK;
	}

	switch(jm){
	case JM_OFF:
		ldmy = (u32)	JouCtrlEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	0;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case JM_ON:
		ldmy = (u32)	JouCtrlEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	1;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case JM_S3ON:
		ldmy = (u32)	JouCtrlEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case JM_S3DIV:
		ldmy = (u32)	JouCtrlEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	4;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case JM_S3HOLD:
		ldmy = (u32)	JouCtrlEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	8;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case JM_VSET:
		ldmy = (u32)	RMRCStartEvent15;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	0xfL;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	0x7f;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case JM_SET:
		ldmy = (u32)	RMRCStartEvent7;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	0xfL;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	0x7f;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case JM_RESET:
		ldmy = (u32)	JouParamResetEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_JM;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	��Ĺ������on/off
		����	armno		������No.
			jt			�ѥ�᡼����ư����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_JT		JOUTYPE���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_odr_jou(ARM armno, JOUTYPE jt)
{
	u32	ldmy;
	float	fdmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if(axsnum==6){
		UnlockMutex((u32)armno);
		return ERR_OK;
	}


	ldmy = (u32)	SendJSEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	7;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	switch(jt){
	case JT_RIGHT:
		fdmy = 1.0;
		break;
	case JT_HOLD:
		fdmy = 0.0;
		break;
	case JT_LEFT:
		fdmy = -1.0;
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_JT;
	}

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[0]),&fdmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

