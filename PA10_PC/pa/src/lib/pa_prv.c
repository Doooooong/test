/*

	PAlib private function


*/

#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

#define	TextBufSize	80+1
#define	CRX	0x0a

/**************************************************************************
	�ѵ׻
		����	armno		������No.
				axis		ư�����
				angle		ư����ٹ�¤�� [rad]
				wfp			ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
				ERR_INT		�����ߤ���������ʤ��ä�.
				ERR_ARM		������No.���ְ�äƤ���.
				�ʾ��¾�������फ��Υ��顼������ޤ���
*************************************************************************/
DllExport u32 __stdcall pa_exe_edr(ARM armno, AXIS axis, ANGLEP angle, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	RunningTestEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	ldmy = (u32)	axis;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[0]),&(angle->s1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[1]),&(angle->s2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[2]),&(angle->s3),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[3]),&(angle->e1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[4]),&(angle->e2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[5]),&(angle->w1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[6]),&(angle->w2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	

	ldmy = (u32)	349;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	4port ROM �� ��ư����ѥ��-����ư���楳��ȥ�-�� �� ROM �� ��-��.
		����	armno		���������

		����	ERR_OK		���ｪλ
				ERR_ARM		��������꤬�ְ�äƤ���.
				ERR_INT		�����ߤ���������ʤ��ä�.
				ERR_PRM		������ϥѥ�᡼�����ѹ��ϤǤ��ʤ�
				�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_sav_rom(ARM armno)
{
	u32	ldmy;
	u32	stat;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&stat,&(_comuni->hu.get.lvl[7]),1);	//��-�����
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	

	if(stat==0){
		ldmy = (u32)	SaveParamEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}	
		ldmy = (u32)	0;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}	

		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			UnlockMutex((u32)armno);
			return st;
		}
	}else{
		UnlockMutex((u32)armno);
		return	ERR_PRM;
	}

	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	��ư���楳��ȥ�-��ROM �� ��ư����ѥ��-���� 4port ROM �� load.
		����	armno		���������

		����	ERR_OK		���ｪλ
				ERR_ARM		��������꤬�ְ�äƤ���.
				ERR_INT		�����ߤ���������ʤ��ä�.
				ERR_PRM		������ϥѥ�᡼�����ѹ��ϤǤ��ʤ�
				�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_lod_rom(ARM armno)
{
	u32	ldmy;
	u32	stat;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&stat,&(_comuni->hu.get.lvl[7]),1);	//��-�����
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}	

	if(stat==0){
		ldmy = (u32)	LoadParamEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}	
		ldmy = (u32)	0;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}	

		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			UnlockMutex((u32)armno);
			return st;
		}
		while(pa_fsh_chk(armno));
	}else{
		UnlockMutex((u32)armno);
		return	ERR_PRM;
	}
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
