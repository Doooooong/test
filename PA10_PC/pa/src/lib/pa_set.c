/*


	PAlib set function function


*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	�����ͤ���ܻ����Ȥ��Ƶ���
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_def_hom(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno))){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_GET_HOM;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�����ͤ�ESC�����Ȥ��Ƶ���
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_def_esc(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_GET_ESC;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�����ͤ�saftiy�����Ȥ��Ƶ���
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_def_saf(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_GET_SAF;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�����ͤ���ܻ����Ȥ�������
		����	armno		������No.
			an			ANGLE�� �ؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_set_hom(ARM armno, ANGLEP an)
{
	u32 ldmy;
	u32 axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_DEF_HOM;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.hom[0]),&(an->s1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.hom[1]),&(an->s2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if(axsnum==6)	an->s3=0.0f;
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.hom[2]),&(an->s3),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.hom[3]),&(an->e1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.hom[4]),&(an->e2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.hom[5]),&(an->w1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.hom[6]),&(an->w2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�����ͤ�ESC�����Ȥ�������
		����	armno		������No.
			an			ANGLE�� �ؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_set_saf(ARM armno, ANGLEP an)
{
	u32	ldmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
        if(st<0){
                UnlockMutex((u32)armno);
                return st;
        }


	ldmy = (u32)	EV_DEF_SAF;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.saf[0]),&(an->s1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.saf[1]),&(an->s2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if(axsnum==6)	an->s3=0.0f;
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.saf[2]),&(an->s3),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.saf[3]),&(an->e1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.saf[4]),&(an->e2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.saf[5]),&(an->w1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.saf[6]),&(an->w2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�����ͤ�saftiy�����Ȥ�������
		����	armno		������No.
			an			ANGLE�� �ؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall  pa_set_esc(ARM armno, ANGLEP an)
{
	u32	ldmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_DEF_ESC;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.esc[0]),&(an->s1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.esc[1]),&(an->s2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if(axsnum==6)	an->s3=0.0f;
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.esc[2]),&(an->s3),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.esc[3]),&(an->e1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.esc[4]),&(an->e2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.esc[5]),&(an->w1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.esc[6]),&(an->w2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

