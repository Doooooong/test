//2002/04/25
#include	<stdio.h>
#include	<stdlib.h>
#include	<time.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	16�ʿ�ʸ����10�ʿ�
******************************************************************/
u32  atox(char* szNumStr)
{
	char*		szNum1=("0123456789abcdef");
	char*		szNum2=("0123456789ABCDEF");

	int			i, j;
	int			iWeight=1, iLen=strlen(szNumStr);
	u32		dwRet=0;

	for(i=iLen-1; i>=0; i--){
		//
		// 0x0��0xf�ޤǤ򸡺�
		for(j=0; j<16; j++){
			if(szNumStr[i]==szNum1[j])	break;
		}
		//
		// 0x0��0xf�ǤϤʤ��Τǡ�0xA��0xF�ޤǤ򸡺�
		if(16==j){
			for(j=0; j<16; j++){
				if(szNumStr[i]==szNum2[j])	break;
			}
		}
		//
		// ��������ʸ����̵���Τ�̵��
		if(16==j)	continue;

		dwRet+=j*iWeight;
		iWeight*=16;
	}

	return(dwRet);
}

/******************************************************************
	�ץ���������̾����
		����	armno		������No.
			*name		�ץ���������̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_get_prj(ARM armno, char* name)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_GET_PRJ;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	3;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}


	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);

	if( ERR_OK==(st=pam_get_err(armno)) ){
		pa_map_ctl(armno);

		name[128]='\0';
		if(ERR_OK!=(st=pa_mem_blk((u32)armno,name,&(_comuni->hu.bse.pkt[128]),sizeof(_comuni->hu.bse.pkt)/2,1)))	return st;
	}

	return pam_get_err(armno);
}

/******************************************************************
	�ץ���������̾������
		����	armno		������No.
			*name		�ץ���������̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_set_prj(ARM armno, char* name)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_SET_PRJ;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	3;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	
	name[128]='\0';
	if(strlen(name)<128){
		st=pa_mem_blk(	(u32)armno,&(_comuni->hd.bse.pkt[128])	,name,(size_t)(strlen(name)+1),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}
	else{
		st=pa_mem_blk(	(u32)armno,&(_comuni->hd.bse.pkt[128])	,name,(size_t)128,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	JUMP���󥻡���
		����	armno		������No.
			*name		�ե�����̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_FILE	�ե����뤬�����ץ�Ǥ��ʤ�
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
static u32 pa_sav_jmp(ARM armno, char* name)
{
	extern	u32	pa_jmp_set_sub(ARM,u32,u32,JUMP*);
	u32	jmpnum;
	FILE	*stream;
	u32	i,j;
	JUMP	jmp;
	u32	key;
	u32	st;

	memset(&jmp,0,sizeof(jmp));
	if( ERR_OK!=(st=pa_map_ctl(armno)) )		return st;
	if(ERR_OK!=(st=pa_get_key((u32)armno,&key)))	return st;

	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmpnum),&(_comuni->hu.sts.lvl[5]),1)))	return st;
	
	if(jmpnum!=0){
		if( (stream  = fopen( name, "a" )) != NULL ){
			fprintf(stream,"JUMP\n");	/*�����ǡ�����JUMP�����ڤ�ʬ��*/
			for(j=0;j<jmpnum;j++){
				if(ERR_OK!=(st=pa_jmp_set_sub(armno,key,j,&jmp))){
					return st;
				}

				for(i=0;i<8;i++){
					fprintf(stream,"%d,%d,",jmp.cid,key);	/*����ֹ�,key�ֹ�*/
					if(jmp.jdg[i].cnd[0] & JMPENABLE)						fprintf(stream,"Enable,");		/*���ͭ��*/
					else													fprintf(stream,"Disable,");		/*���̵��*/
					if((jmp.jdg[i].cnd[0]&DI_WAIT)==DI_WAIT)				fprintf(stream,"DI_WAIT,");		/*DI���WAIT*/
					else if((jmp.jdg[i].cnd[0]&DI_WAITJUMP)==DI_WAITJUMP)	fprintf(stream,"DI_WAITJUMP,");	/*DI���WAIT��JUMP*/
					else if((jmp.jdg[i].cnd[0]&DI_JUMP)==DI_JUMP)			fprintf(stream,"DI_JUMP,");		/*DI���JUMP*/
					else if((jmp.jdg[i].cnd[0]&NO_JUMP)==NO_JUMP)			fprintf(stream,"JUMP,");		/*̵���JUMP*/
					else													fprintf(stream,",");			/*�������*/
					if((jmp.jdg[i].cnd[0]&LEVEL_ON)==LEVEL_ON)				fprintf(stream,"LEVEL_ON,");	/*ON����*/
					else if((jmp.jdg[i].cnd[0]&LEVEL_OFF)==LEVEL_OFF)		fprintf(stream,"LEVEL_OFF,");	/*OFF����*/
					else if((jmp.jdg[i].cnd[0]&EDGE_ON)==EDGE_ON)			fprintf(stream,"EDGE_ON,");		/*ON�ˤʤä���*/
					else if((jmp.jdg[i].cnd[0]&EDGE_OFF)==EDGE_OFF)			fprintf(stream,"EDGE_OFF,");	/*OFF�ˤʤä���*/
					else													fprintf(stream,",");			/*�������*/	
//					if(jmp.jdg[i].cnd[0] & DI_OFF)							fprintf(stream,"B,");			/*������*/
//					else													fprintf(stream,"A,");			/*������*/
					if(jmp.jdg[i].cnd[0] & DIO_EXTERNAL)					fprintf(stream,"EXTERNAL,");	/*��ĥDI*/
					else													fprintf(stream,"INTERNAL,");	/*��¢DI*/
					fprintf(stream,"%x,",jmp.jdg[i].xdi);										/*DI���*/
					fprintf(stream,"%d,",jmp.jdg[i].tim);										/*�����ॢ����*/
					fprintf(stream,"%d,",jmp.jdg[i].key);										/*JUMP�趵���ǡ���Key*/
					fprintf(stream,"%d\n",jmp.jdg[i].pid);										/*�����ǡ���Key��ζ�����ID*/
				}
			}
			fclose(stream);
		}
		else		return ERR_FILE;
	}

	return pam_get_err(armno);
}

/******************************************************************
	JUMP���������
		����	armno		������No.
			key			Key�ֹ�
			*name		�ե�����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_FILE	�ե����뤬�����ץ�Ǥ��ʤ�
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
static u32 pa_lod_jmp(ARM armno, u32 key, char* name)
{
	extern	char *strtokex(char*,const char);
	extern	u32 pa_set_jmp_sub(ARM,u32,u32,JUMP*);
	FILE	*stream;
	char	buff[256],jmpbuff[256];
	u32	readflg;
	JUMP	jmp;
	char	*szTok;
	char	cDelimit=',';
	u32	st;
	u32	key_t;

	memset(&jmp,0,sizeof(jmp));
	readflg=0;
	if( (stream  = fopen( name, "r" )) != NULL ){
		while(fgets(buff, sizeof(buff), stream)){
			strcpy(jmpbuff,buff);
			szTok=strtokex(buff, cDelimit);
			if(strcmp(szTok,"JUMP")==0 || strcmp(szTok,"JUMP\n")==0){
				readflg=1;
			}
			else if(readflg>=1){
				jmp.cid	=(szTok=strtokex(jmpbuff, cDelimit))?atol(szTok):0;		/*JUMP����ֹ� */
				key_t		=(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;	/*Key�ֹ� */
				szTok=strtokex(NULL, cDelimit);
				if(strcmp(szTok,"Enable")==0)		jmp.jdg[readflg-1].cnd[0]=JMPENABLE;		/*JUMP���ͭ�� */
				else if(strcmp(szTok,"Disable")==0)	jmp.jdg[readflg-1].cnd[0]=JMPDISABLE;		/*JUMP���̵�� */
				szTok=strtokex(NULL, cDelimit);
				if(strcmp(szTok,"JUMP")==0)		jmp.jdg[readflg-1].cnd[0]|=NO_JUMP;		/*̵���JUMP */
				else if(strcmp(szTok,"DI_JUMP")==0)	jmp.jdg[readflg-1].cnd[0]|=DI_JUMP;		/*DI���JUMP */
				else if(strcmp(szTok,"DI_WAITJUMP")==0)	jmp.jdg[readflg-1].cnd[0]|=DI_WAITJUMP;		/*DI���JUMP */
				else if(strcmp(szTok,"DI_WAIT")==0)	jmp.jdg[readflg-1].cnd[0]|=DI_WAIT;		/*DI���JUMP */
				else					jmp.jdg[readflg-1].cnd[0]|=0;			/*������� */
				szTok=strtokex(NULL, cDelimit);
				if(strcmp(szTok,"LEVEL_ON")==0)		jmp.jdg[readflg-1].cnd[0]|=LEVEL_ON;		/*ON���� */
				else if(strcmp(szTok,"LEVEL_OFF")==0)	jmp.jdg[readflg-1].cnd[0]|=LEVEL_OFF;		/*OFF���� */
				else if(strcmp(szTok,"EDGE_ON")==0)	jmp.jdg[readflg-1].cnd[0]|=EDGE_ON;		/*ON�ˤ����� */
				else if(strcmp(szTok,"EDGE_OFF")==0)	jmp.jdg[readflg-1].cnd[0]|=EDGE_OFF;		/*OFF�ˤ����� */
				else					jmp.jdg[readflg-1].cnd[0]|=0;			/*������� */
//				if(strcmp(szTok,"A")==0)		jmp.jdg[readflg-1].cnd[0]|=DI_ON;		/*DION�ˤ����� */
//				else if(strcmp(szTok,"B")==0)		jmp.jdg[readflg-1].cnd[0]|=DI_OFF;		/*DIOFF�ˤ����� */
				szTok=strtokex(NULL, cDelimit);
				if(strcmp(szTok,"INTERNAL")==0)		jmp.jdg[readflg-1].cnd[0]|=DIO_INTERNAL;	/*��¢ */
				else if(strcmp(szTok,"EXTERNAL")==0)	jmp.jdg[readflg-1].cnd[0]|=DIO_EXTERNAL;	/*��ĥ */
				jmp.jdg[readflg-1].xdi		=(szTok=strtokex(NULL, cDelimit))?atox(szTok):0;	/*DI��� */
				jmp.jdg[readflg-1].tim		=(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;	/*�����ॢ���� */
				jmp.jdg[readflg-1].key		=(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;	/*JUMP�趵���ǡ���Key */
				jmp.jdg[readflg-1].pid		=(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;	/*�����ǡ���Key��ζ�����ID */
				if(readflg==8){
					if(ERR_OK!=(st=pa_set_jmp_sub(armno,key,jmp.cid,&jmp))){
						fclose(stream);
						return st;
					}
					readflg=0;
					memset(&jmp,0,sizeof(jmp));
				}
				readflg++;
			}
		}
		fclose(stream);
	}
	else		return ERR_FILE;

	return pam_get_err(armno);
}

/******************************************************************
	�ץ��������ȥ�����
		����	armno		������No.
			*name		��Ǽ�ե����̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_FILE	�ե����뤬�����ץ�Ǥ��ʤ�
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_sav_prj(ARM armno, char* fdname)
{
	extern	u32	pa_get_prj_sub(ARM,char*);
	extern	u32	pa_ply_set_sub(ARM,u32,u32*);
	extern	u32	pa_act_pnt_sub(ARM,u32);
	extern	u32	pa_sav_pnt_sub(ARM,char*);
	u32		num;
	u32		i;
	u32		key;
	char		buff[128];
	char		dataname[256];
	char		prjname[256];
	char		name[128];
	JUMP		jmp;
	FILE		*prj_stream;
	struct	tm *today;
	time_t		ltime;
	ARMSTATUS	sts;
	float		armVer=0.0f,prjVer=0.0f;
	u32		st;

	memset(&jmp,0,sizeof(jmp));
	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	if(ERR_OK!=(pa_cpy_lng((u32)armno,&(num),&(_comuni->hu.sts.lvl[2]),1))){
		UnlockMutex((u32)armno);
		return st;
	}

	strcpy(prjname,fdname);
	strcat(prjname,"\\project.csv");

	if( (prj_stream  = fopen( prjname, "w" )) != NULL ){
		if (ERR_OK!=(st=pa_get_ver(armno,&armVer))){
			fclose(prj_stream);
			return st;
		}
		fprintf(prj_stream,"%.1f\n",prjVer);
		fprintf(prj_stream,"%.1f\n",armVer);
	    time( &ltime );
		today = localtime( &ltime );
	    strftime( buff, 128, "%Y/%m/%d\n", today );
		fprintf( prj_stream, buff );
		if( ERR_OK!=(st=pa_get_sts(armno,&sts)) ){
			fclose(prj_stream);
			return st;
		}
		if(sts.axis==6)			fprintf( prj_stream, "6\n" );
		else if(sts.axis==7)	fprintf( prj_stream, "7\n" );
		if(ERR_OK!=(st=pa_get_prj_sub(armno,name))){
			fclose(prj_stream);
			return st;
		}
		fprintf(prj_stream,"%s\n",name);

		for(i=0;i<num;i++){
			if( ERR_OK!=(st=pa_ply_set_sub(armno,i,&key)) ){		//	Key�ֹ����
				fclose(prj_stream);
				return st;
			}

			fprintf(prj_stream,"%d\n",key);

			if(ERR_OK!=(st=pa_act_pnt_sub(armno,key))){
				fclose(prj_stream);
				return st;
			}
			strcpy(dataname,fdname);
			strcat(dataname,"\\");
			sprintf(buff,"%d",key);
			strcat(dataname,buff);
			strcat(dataname,".csv");
			if(ERR_OK!=(st=pa_sav_pnt_sub(armno,dataname))){
				fclose(prj_stream);
				return st;
			}				
			if(ERR_OK!=(st=pa_sav_jmp(armno,dataname))){
				fclose(prj_stream);
				return st;
			}
		}
		UnlockMutex((u32)armno);
		fclose(prj_stream);
	}
	else		return ERR_FILE;

	return pam_get_err(armno);
}

/******************************************************************
	�ץ��������ȥ�����
		����	armno		������No.
			*name		��Ǽ�ե����̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_FILE	�ե����뤬�����ץ�Ǥ��ʤ�
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_lod_prj(ARM armno, char* fdname)
{
	extern	u32	pa_set_prj_sub(ARM,char*);
	extern	u32	pa_lod_pnt_sub(ARM,u32,char*);
	u32				err;
	char				dataname[256];
	char				filename[256];
	char				prjname[256];
	u32				*key;
	u32				keynum;
	FILE				*prj_stream;
	char				buff[256];
	float				armVer,prjVer;
	u32				i;

	i=0;
	keynum=0;
	strcpy(prjname,fdname);
	strcat(prjname,"\\project.csv");
	if( (prj_stream  = fopen( prjname, "r" )) != NULL ){
		while(fgets(buff, sizeof(buff), prj_stream)){
			if(i==0)	prjVer=(float)atof(buff);
			if(i==1)	armVer=(float)atof(buff);
			if(i==4){
				buff[strlen(buff)-1]='\0';
				if(ERR_OK!=(err=pa_set_prj_sub(armno,buff))){
					fclose(prj_stream);
					return err;
				}
			}
			if(i>4)		keynum++;
			i++;
		}
		key=malloc(sizeof(u32)*(keynum+1));
		if(key==NULL){
			fclose(prj_stream);
			return ERR_MALLOC;
		}
		fclose(prj_stream);
	}
	else{
		return ERR_FILE;
	}

	i=0;
	keynum=0;
	if( (prj_stream  = fopen( prjname, "r" )) != NULL ){
		while(fgets(buff, sizeof(buff), prj_stream)){
			if(i>4){
				key[keynum]=atol(buff);
				keynum++;
			}
			i++;
		}
		fclose(prj_stream);
	}
	else{
		free(key);
		return ERR_FILE;
	}

	for(i=0;i<keynum;i++){
		strcpy(dataname,fdname);
		sprintf(filename,"\\%d.csv",key[i]);
		strcat(dataname,filename);
		if(ERR_OK!=(err=pa_lod_pnt_sub(armno,key[i],dataname))){
			free(key);
			return err;
		}
		if(ERR_OK!=(err=pa_lod_jmp(armno,key[i],dataname))){
			free(key);
			return err;
		}
	}
	free(key);

	return pam_get_err(armno);
}

/******************************************************************
	�����ǡ�����JUMP���󥻡���
		����	armno		������No.
			*name		�ե�����̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_sav_ptj(ARM armno, char* name)
{
	extern	u32	pa_lod_pnt_sub(ARM,u32,char*);	
	extern	u32	pa_sav_pnt_sub(ARM,char*);	
	u32	st;

	if(ERR_OK!=(st=pa_sav_pnt_sub(armno,name))){
		return st;
	}				

	if(ERR_OK!=(st=pa_sav_jmp(armno,name))){
		return st;
	}

	return pam_get_err(armno);
}

/******************************************************************
	�����ǡ�����JUMP���������
		����	armno		������No.
			*name		�ե�����̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_lod_ptj(ARM armno, char* name)
{
	extern	u32	pa_lod_pnt_sub(ARM,u32,char*);
	u32		st;

	if(ERR_OK!=(st=pa_lod_pnt_sub(armno,0,name))){
		return st;
	}
	if(ERR_OK!=(st=pa_lod_jmp(armno,0,name))){
		return st;
	}

	return pam_get_err(armno);
}

