#include <stdio.h>
#include <string.h>

#include <pa.h>
#include <pacmd.h>
#include <pactl.h>
#include <pammc.h>
#include <paerr.h>

#ifndef 	M_PI
#define	M_PI	3.141593652589793238
#endif

#define RadToDeg(x) (float)((x)*180.0/M_PI)
#define DegToRad(x) (float)((x)*M_PI/180.0) 	
/******************************************************************
        �ֹ����Ƕ����ǡ���Key����
                ����    armno           ������No.
                        number          �����ǡ������ֹ�
                        *key            �����ǡ�����Key�ֹ�

                ����    ERR_OK          ���ｪλ
                        ERR_ARM         ������No.���ְ�äƤ���.
                        ERR_INT         �����ߤ���������ʤ��ä�.
                        �ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_ply_set_sub(ARM armno, u32 number, u32* key)
{
        u32    ldmy;
        u32    st;

        if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;

        ldmy = (u32)   EV_PLY_SET;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))		return st;
        ldmy = (u32)   5;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2)))		return st;
        ldmy = (u32)   number;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2)))	return st;

        if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) )	return st;

        while( pa_fsh_chk(armno) );

        if( ERR_OK==(st=pam_get_err(armno)) ){
                pa_map_ctl(armno);
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,key,&(_comuni->hu.sts.lvl[1]),1)))        return st;
        }

        return pam_get_err(armno);

}

/******************************************************************
        �����ƥ��֤ʶ����ǡ������ڴ���
                ����    armno           ������No.
                        key             �����ǡ�����Key�ֹ�

                ����    ERR_OK          ���ｪλ
                        ERR_ARM         ������No.���ְ�äƤ���.
                        ERR_INT         �����ߤ���������ʤ��ä�.
                        �ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_act_pnt_sub(ARM armno, u32 key)
{
        u32    ldmy;
        u32    st;

        if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;

        ldmy = (u32)   EV_ACT_PNT;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))		return st;
        ldmy = (u32)   1;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2)))		return st;
        ldmy = (u32)   key;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2)))		return st;

        if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) )				return st;
        while( pa_fsh_chk(armno) );
        return pam_get_err(armno);
}

/******************************************************************
        �ץ���������̾����
                ����    armno           ������No.
                        *name           �ץ���������̾

                ����    ERR_OK          ���ｪλ
                        ERR_ARM         ������No.���ְ�äƤ���.
                        ERR_INT         �����ߤ���������ʤ��ä�.
                        �ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_get_prj_sub(ARM armno, char* name)
{
        u32    ldmy;
        u32    st;

        if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
        ldmy = (u32)   EV_GET_PRJ;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))	return st;
        ldmy = (u32)   3;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2)))	return st;

        if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) )	return st;
        while( pa_fsh_chk(armno) );

        if( ERR_OK==(st=pam_get_err(armno)) ){
                pa_map_ctl(armno);

                name[128]='\0';
                if(ERR_OK!=(st=pa_mem_blk((u32)armno,name,&(_comuni->hu.bse.pkt[128]),sizeof(_comuni->hu.bse.pkt)/2,1)))       return st;
        }

        return pam_get_err(armno);
}

/******************************************************************
        ��������ե�����˥�-�֤��롣
                ����    armno   ��-��No.
                        file    ��������-���ե���������

                ����    ERR_OK          ���ｪλ
                        ERR_ARM         ������No.���ְ�äƤ���.
                        ERR_OPEN        pa_opn_arm()�¹Ԥ���Ƥ��ʤ�.
                        ERR_FILE        �ե����뤬�����ץ�Ǥ��ʤ�
                        ERR_INT         �����ߤ���������ʤ��ä�.
******************************************************************/
DllExport u32  pa_sav_pnt_sub(ARM armno, char *file)
{
        u32    err=0;
        static  u32    datatype,Tnum;
        short   flg=1;
        PNTDAT  pntdat;
        u32    ldmy;
        FILE*   stream;
        float   afdmy;
        float   rfdmy;
        char    cmt[33];
        u32    st;


        if( (stream  = fopen( file, "w" )) != NULL ){
                datatype = 1;
                Tnum = 0;
                while(flg){
                        if( ERR_OK!=(err=pa_map_ctl(armno)) )	return err;

                        ldmy = (u32)   DataSave;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))	return st;
                        ldmy = (u32)   3;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2))) return st;
                        
			if( ERR_OK!=(err=pa_req_ctl(armno,RETRY_COUNT)) )	return err;

                        while( pa_fsh_chk(armno) );
                        if( pam_get_err(armno) )break;

                        pa_map_ctl(armno);

                        if(datatype)Tnum++;
                        ldmy = (u32)   Tnum;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&ldmy,&(_comuni->hu.ply.pnt.atr[0]),1)))	return st;
                        datatype = (short)      ldmy;
                        if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.ply.pnt.agl[0]),&(_comuni->hu.ply.pnt.agl[0]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.ply.pnt.agl[1]),&(_comuni->hu.ply.pnt.agl[1]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.ply.pnt.agl[2]),&(_comuni->hu.ply.pnt.agl[2]),1)))       return st;
                        if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.ply.pnt.agl[3]),&(_comuni->hu.ply.pnt.agl[3]),1)))       return st;
                        if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.ply.pnt.agl[4]),&(_comuni->hu.ply.pnt.agl[4]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.ply.pnt.agl[5]),&(_comuni->hu.ply.pnt.agl[5]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.ply.pnt.agl[6]),&(_comuni->hu.ply.pnt.agl[6]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.ply.pnt.vel[0]),&(_comuni->hu.ply.pnt.vel[0]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.ply.pnt.vel[1]),&(_comuni->hu.ply.pnt.vel[1]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[0]),&(_comuni->hu.ply.pnt.atr[0]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[1]),&(_comuni->hu.ply.pnt.atr[1]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[2]),&(_comuni->hu.ply.pnt.atr[2]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[3]),&(_comuni->hu.ply.pnt.atr[3]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[4]),&(_comuni->hu.ply.pnt.atr[4]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[5]),&(_comuni->hu.ply.pnt.atr[5]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[6]),&(_comuni->hu.ply.pnt.atr[6]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[7]),&(_comuni->hu.ply.pnt.atr[7]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[8]),&(_comuni->hu.ply.pnt.atr[8]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[9]),&(_comuni->hu.ply.pnt.atr[9]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[10]),&(_comuni->hu.ply.pnt.atr[10]),1)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat.ply.pnt.atr[11]),&(_comuni->hu.ply.pnt.atr[11]),1)))	return st;

                        if(ERR_OK!=(st=pa_mem_blk((u32)armno,cmt,&(_comuni->hu.ply.cmt[0]),(size_t)32,1)))			return st;
                        cmt[32]='\0';

                        afdmy=(float)(pntdat.ply.pnt.atr[7]&0x0000ffff)/10;
                        rfdmy=(float)(pntdat.ply.pnt.atr[7]>>16&0x0000ffff)/10;

                        fprintf(stream,"%d,%d,%s,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,%.3f,",
                                pntdat.ply.pnt.atr[4],						/*�̤��ֹ�*/
                                pntdat.ply.pnt.atr[5],						/*������ID*/
                                cmt,                                                            /*������*/
                                RadToDeg(pntdat.ply.pnt.agl[0]),                                /*S1����*/
                                RadToDeg(pntdat.ply.pnt.agl[1]),                                /*S2����*/
                                RadToDeg(pntdat.ply.pnt.agl[2]),                                /*S3����*/
                                RadToDeg(pntdat.ply.pnt.agl[3]),                                /*E1����*/
                                RadToDeg(pntdat.ply.pnt.agl[4]),                                /*E2����*/
                                RadToDeg(pntdat.ply.pnt.agl[5]),                                /*W1����*/
                                RadToDeg(pntdat.ply.pnt.agl[6]),                                /*W2����*/
                                pntdat.ply.pnt.vel[0],                                          /*����®��[mm/sec]*/
                                RadToDeg(pntdat.ply.pnt.vel[1])                                 /*����®��[deg/sec]*/
                        );
                        if(pntdat.ply.pnt.atr[0]==1)            fprintf(stream,"PTP,");         /*������������ PTP/PTP(NOAP)*/
                        else if(pntdat.ply.pnt.atr[0]==2)       fprintf(stream,"NOAP,");
                        if(pntdat.ply.pnt.atr[1]==21)           fprintf(stream,"EACH,");        /*�����ˡ ľ��/��/�߸�*/
                        else if(pntdat.ply.pnt.atr[1]==18)      fprintf(stream,"LINE,");
                        else if(pntdat.ply.pnt.atr[1]==17)      fprintf(stream,"ARC,");
                        else if(pntdat.ply.pnt.atr[1]==16)      fprintf(stream,"CIRCLE,");
                        fprintf(stream,"%.1f,%.1f,",afdmy,rfdmy);                               /*���١ʳƼ���*//*���١�RMRC��*/
                        if(pntdat.ply.pnt.atr[2]==0)            fprintf(stream,"Line,");        /*®�٥�����*/
                        else if(pntdat.ply.pnt.atr[2]==1)       fprintf(stream,"StartUp,");
                        else if(pntdat.ply.pnt.atr[2]==2)       fprintf(stream,"ShutDown,");
                        else if(pntdat.ply.pnt.atr[2]==3)       fprintf(stream,"Trap,");
                        fprintf(stream,"%d,%d,%d,%x,%d",
                                pntdat.ply.pnt.atr[9],                                          /*��®����[*0.01msec]*/
                                pntdat.ply.pnt.atr[10],                                         /*��®����[*0.01msec]*/
                                pntdat.ply.pnt.atr[8],                                          /*JUMP����ֹ�*/
                                pntdat.ply.pnt.atr[6],                                          /*DO�����16�ʿ���*/
                                pntdat.ply.pnt.atr[3]                                           /*�Ԥ�����[msec]*/
                        );

                        if(pntdat.ply.pnt.atr[0]==2){
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.xyz[0]),&(_comuni->hu.noa.xyz[0]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.xyz[1]),&(_comuni->hu.noa.xyz[1]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.xyz[2]),&(_comuni->hu.noa.xyz[2]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.noa[0][0]),&(_comuni->hu.noa.noa[0][0]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.noa[0][1]),&(_comuni->hu.noa.noa[0][1]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.noa[0][2]),&(_comuni->hu.noa.noa[0][2]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.noa[1][0]),&(_comuni->hu.noa.noa[1][0]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.noa[1][1]),&(_comuni->hu.noa.noa[1][1]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.noa[1][2]),&(_comuni->hu.noa.noa[1][2]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.noa[2][0]),&(_comuni->hu.noa.noa[2][0]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.noa[2][1]),&(_comuni->hu.noa.noa[2][1]),1)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(pntdat.noa.noa[2][2]),&(_comuni->hu.noa.noa[2][2]),1)))	return st;
                                fprintf(stream,",%.3f,%.3f,%.3f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f,%.6f\n",
                                        pntdat.noa.xyz[0],pntdat.noa.xyz[1],pntdat.noa.xyz[2],                                        
					pntdat.noa.noa[0][0],pntdat.noa.noa[0][1],pntdat.noa.noa[0][2],
                                        pntdat.noa.noa[1][0],pntdat.noa.noa[1][1],pntdat.noa.noa[1][2],
                                        pntdat.noa.noa[2][0],pntdat.noa.noa[2][1],pntdat.noa.noa[2][2]);
                        }
                        else{
                                fprintf(stream,",,,,,,,,,,,,\n");
                        }

                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&ldmy,&(_comuni->hu.hed.dmy[0]),1)))	return st;
                        if( ldmy == (u32)'E' )flg=0;
                }
                fclose(stream);
        }
        else            return ERR_FILE;

        return pam_get_err(armno);
}

/******************************************************************
        �ֹ�����JUMP�������
                ����    armno           ������No.
                        key                     �����ǡ�����Key�ֹ�
                        num                     �ǡ����ֹ�
                        jmp                     JUMP����

                ����    ERR_OK          ���ｪλ
                        ERR_ARM         ������No.���ְ�äƤ���.
                        ERR_INT         �����ߤ���������ʤ��ä�.
                        �ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_jmp_set_sub(ARM armno, u32 key, u32 num, JUMPP jmp)
{
        u32    ldmy;
        u32    i;
        u32    st;

        if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;

        ldmy = (u32)   EV_JMP_SET;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))		return st;
        ldmy = (u32)   5;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2)))		return st;
        ldmy = (u32)   key;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2)))	return st;
        ldmy = (u32)   num;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2)))	return st;

        if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) )				return st;
        while( pa_fsh_chk(armno) );

        if( ERR_OK==(st=pam_get_err(armno)) ){
                pa_map_ctl(armno);
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->cid),&(_comuni->hu.jmp.cid),1)))   return st;
                for(i=0;i<8;i++){
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].cnd[0]),&(_comuni->hu.jmp.jdg[i].cnd[0]),1)))       return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].cnd[1]),&(_comuni->hu.jmp.jdg[i].cnd[1]),1)))       return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].xdi),&(_comuni->hu.jmp.jdg[i].xdi),1)))             return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].tim),&(_comuni->hu.jmp.jdg[i].tim),1)))             return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].key),&(_comuni->hu.jmp.jdg[i].key),1)))             return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].pid),&(_comuni->hu.jmp.jdg[i].pid),1)))             return st;
                        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].cnt),&(_comuni->hu.jmp.jdg[i].cnt),1)))             return st;
                }
        }

        return pam_get_err(armno);

}

/******************************************************************
        �ץ���������̾������
                ����    armno           ������No.
                        *name           �ץ���������̾

                ����    ERR_OK          ���ｪλ
                        ERR_ARM         ������No.���ְ�äƤ���.
                        ERR_INT         �����ߤ���������ʤ��ä�.
                        �ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_set_prj_sub(ARM armno, char* name)
{
        u32    ldmy;
        u32    st;

        if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;

        ldmy = (u32)   EV_SET_PRJ;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))		return st;
        ldmy = (u32)   3;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2)))		return st;

        name[128]='\0';
        if(strlen(name)<128){
                if(ERR_OK!=(st=pa_mem_blk((u32)armno,&(_comuni->hd.bse.pkt[128]),name,(size_t)(strlen(name)+1),2)))	return st;
        }
        else{
                if(ERR_OK!=(st=pa_mem_blk((u32)armno,&(_comuni->hd.bse.pkt[128]) ,name,(size_t)128,2)))		return st;
        }

        if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) )	return st;
        while( pa_fsh_chk(armno) );

        return pam_get_err(armno);
}

/******************************************************************
        ��������ե����뤫���-�ɤ��롣
                ����    armno   ��-��No.
                        key     key�ֹ�
                        file    ��������-���ե���������

                ����    ERR_OK          ���ｪλ
                        ERR_ARM         ������No.���ְ�äƤ���.
                        ERR_FILE        �ե����뤬�����ץ�Ǥ��ʤ�
                        ERR_INT         �����ߤ���������ʤ��ä�.
******************************************************************/
DllExport u32  pa_lod_pnt_sub(ARM armno, u32 key, char *file)
{
        extern  u32 atox(char*);
	extern	char	*strtokex(char*,const char);
        short   i=0;
        PNTDAT  pntdat;
        u32    err;
        static  u32    ldmy;
        u32    cnt=1;
        char    buff[512];
        FILE*   stream;
        u32    num;
        u32    aldmy,rldmy;
        char    cmt[33];
        char    *szTok;
        char    cDelimit=',';
        u32    st;
        PARAM   prm;
        u32    j;

        num=0;

        st=pa_get_prm(armno,&prm);
        if( (stream  = fopen( file, "r" )) != NULL ){
                while(fgets(buff, sizeof(buff), stream)){
                        szTok=strtokex(buff, cDelimit);
                        if(strcmp(szTok,"JUMP")==0 || strcmp(szTok,"JUMP\n")==0){
                                break;
                        }
                        num++;
                }
                fclose(stream);
        }
        else            return ERR_FILE;

        if(num!=1){
                if( ERR_OK!=(err=pa_map_ctl(armno)) )	return err;
                ldmy = (u32)   'S';
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2)))	return st;
                ldmy = (u32)   key;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2)))	return st;
                ldmy = (u32)   1;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2)))	return st;
                ldmy = (u32)   DataLoad;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))		return st;
                ldmy = (u32)   3;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2)))		return st;
        }

        if( (stream  = fopen( file, "r" )) != NULL ){
                while(fgets(buff, sizeof(buff), stream)){
                        pntdat.ply.pnt.atr[4]   =(szTok=strtokex(buff, cDelimit))?atol(szTok):0;                        /*�̤��ֹ� */
                        pntdat.ply.pnt.atr[5]   =(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;                        /*������ID */

                        memset(pntdat.ply.cmt,0,sizeof(pntdat.ply.cmt));
                        if((szTok=strtokex(NULL, cDelimit))){
                                int             iLen=strlen(szTok);
                                int             iSize=sizeof(pntdat.ply.cmt);
                                int             iCopySize=(iLen<=iSize)?iLen:iSize;
                                memcpy(pntdat.ply.cmt, szTok, iCopySize);						/*������ */
                        }

                        pntdat.ply.pnt.agl[0]   =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /*S1���� */
                        pntdat.ply.pnt.agl[1]   =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /*S2���� */
                        pntdat.ply.pnt.agl[2]   =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /*S3���� */
                        pntdat.ply.pnt.agl[3]   =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /*E1���� */
                        pntdat.ply.pnt.agl[4]   =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /*E2���� */
                        pntdat.ply.pnt.agl[5]   =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /*W1���� */
                        pntdat.ply.pnt.agl[6]   =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /*W2���� */
                        pntdat.ply.pnt.vel[0]   =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /*����®��[mm/sec] */
                        pntdat.ply.pnt.vel[1]   =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /*����®��[deg/sec] */

                        szTok=strtokex(NULL, cDelimit);
                        if(szTok!=NULL){
                                if(strcmp(szTok,"PTP")==0)      pntdat.ply.pnt.atr[0]   =1;		/*������������ PTP/PTP(NOAP) */
                                else if(strcmp(szTok,"NOAP")==0)pntdat.ply.pnt.atr[0]   =2;
                        }else{
                                fclose(stream);
                                return ERR_FILE;
                        }
                        szTok=strtokex(NULL, cDelimit);
                        if(szTok!=NULL){
                                if(strcmp(szTok,"EACH")==0)             pntdat.ply.pnt.atr[1]   =21;	/*�����ˡ ľ��/��/�߸� */
                                else if(strcmp(szTok,"LINE")==0)        pntdat.ply.pnt.atr[1]   =18;
                                else if(strcmp(szTok,"ARC")==0)         pntdat.ply.pnt.atr[1]   =17;
                                else if(strcmp(szTok,"CIRCLE")==0)      pntdat.ply.pnt.atr[1]   =16;
                        }else{
                                fclose(stream);
                                return ERR_FILE;
                        }
                        aldmy   =(u32)(((szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f)*10)&0x0000ffff;	/*���١ʳƼ��� */
                        rldmy   =((u32)(((szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f)*10)&0x0000ffff)<<16;/*���١�RMRC�� */
                        pntdat.ply.pnt.atr[7]   =aldmy|rldmy;
                        szTok=strtokex(NULL, cDelimit);
                        if(szTok!=NULL){
                                if(strcmp(szTok,"Line")==0)     pntdat.ply.pnt.atr[2]   =0;/*®�٥����� */
                                else if(strcmp(szTok,"StartUp")==0)     pntdat.ply.pnt.atr[2]   =1;
                                else if(strcmp(szTok,"ShutDown")==0)    pntdat.ply.pnt.atr[2]   =2;
                                else if(strcmp(szTok,"Trap")==0)        pntdat.ply.pnt.atr[2]   =3;
                        }else{
                                fclose(stream); 
                                return ERR_FILE;
                        }
                        pntdat.ply.pnt.atr[9]   =(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;	/*��®����[*0.01msec] */
                        pntdat.ply.pnt.atr[10]  =(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;        /*��®����[*0.01msec] */
                        pntdat.ply.pnt.atr[8]   =(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;        /*JUMP����ֹ� */
                        pntdat.ply.pnt.atr[6]   =(szTok=strtokex(NULL, cDelimit))?atox(szTok):0;        /*DO�����16�ʿ��� */
                        pntdat.ply.pnt.atr[3]   =(szTok=strtokex(NULL, cDelimit))?atol(szTok):0;        /*�Ԥ�����[msec] */
                        if(pntdat.ply.pnt.atr[0]==2){
                                pntdat.noa.xyz[0]       =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.xyz[1]       =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.xyz[2]       =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.noa[0][0]    =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.noa[0][1]    =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.noa[0][2]    =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.noa[1][0]    =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.noa[1][1]    =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.noa[1][2]    =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.noa[2][0]    =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.noa[2][1]    =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                                pntdat.noa.noa[2][2]    =(szTok=strtokex(NULL, cDelimit))?(float)atof(szTok):0.0f;      /* */
                        }
                        pntdat.ply.pnt.agl[0]=DegToRad(pntdat.ply.pnt.agl[0]);  /*S1����*/
                        pntdat.ply.pnt.agl[1]=DegToRad(pntdat.ply.pnt.agl[1]);  /*S2����*/
                        pntdat.ply.pnt.agl[2]=DegToRad(pntdat.ply.pnt.agl[2]);  /*S3����*/
                        pntdat.ply.pnt.agl[3]=DegToRad(pntdat.ply.pnt.agl[3]);  /*E1����*/
                        pntdat.ply.pnt.agl[4]=DegToRad(pntdat.ply.pnt.agl[4]);  /*E2����*/
                        pntdat.ply.pnt.agl[5]=DegToRad(pntdat.ply.pnt.agl[5]);  /*W1����*/
                        pntdat.ply.pnt.agl[6]=DegToRad(pntdat.ply.pnt.agl[6]);  /*W2����*/
                        pntdat.ply.pnt.vel[1]=DegToRad(pntdat.ply.pnt.vel[1]);  /*����®��[deg/sec]*/

                        pa_map_ctl(armno);

                        if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[0]),&(pntdat.ply.pnt.agl[0]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[1]),&(pntdat.ply.pnt.agl[1]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[2]),&(pntdat.ply.pnt.agl[2]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[3]),&(pntdat.ply.pnt.agl[3]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[4]),&(pntdat.ply.pnt.agl[4]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[5]),&(pntdat.ply.pnt.agl[5]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.agl[6]),&(pntdat.ply.pnt.agl[6]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.vel[0]),&(pntdat.ply.pnt.vel[0]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.ply.pnt.vel[1]),&(pntdat.ply.pnt.vel[1]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[0]),&(pntdat.ply.pnt.atr[0]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[1]),&(pntdat.ply.pnt.atr[1]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[2]),&(pntdat.ply.pnt.atr[2]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[3]),&(pntdat.ply.pnt.atr[3]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[4]),&(pntdat.ply.pnt.atr[4]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[5]),&(pntdat.ply.pnt.atr[5]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[6]),&(pntdat.ply.pnt.atr[6]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[7]),&(pntdat.ply.pnt.atr[7]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[8]),&(pntdat.ply.pnt.atr[8]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[9]),&(pntdat.ply.pnt.atr[9]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[10]),&(pntdat.ply.pnt.atr[10]),2)))	return st;
                        if(ERR_OK!=(st=pa_cpy_lng(armno,&(_comuni->hd.ply.pnt.atr[11]),&(pntdat.ply.pnt.atr[11]),2)))	return st;
                        if(strlen(pntdat.ply.cmt)<32){
                                if(ERR_OK!=(st=pa_mem_blk((u32)armno,&(_comuni->hd.ply.cmt[0]),&(pntdat.ply.cmt[0]),(size_t)(strlen(pntdat.ply.cmt)+1),2)))	return st;
                        }
                        else{
                                memcpy(cmt,pntdat.ply.cmt,33);
                                if(ERR_OK!=(st=pa_mem_blk((u32)armno,&(_comuni->hd.ply.cmt[0]),cmt,(size_t)32,2)))	return st;
                        }
                        if(pntdat.ply.pnt.atr[0]==2){
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.xyz[0]),&(pntdat.noa.xyz[0]),2)))		return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.xyz[1]),&(pntdat.noa.xyz[1]),2)))		return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.xyz[2]),&(pntdat.noa.xyz[2]),2)))		return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.noa[0][0]),&(pntdat.noa.noa[0][0]),2)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.noa[0][1]),&(pntdat.noa.noa[0][1]),2)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.noa[0][2]),&(pntdat.noa.noa[0][2]),2)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.noa[1][0]),&(pntdat.noa.noa[1][0]),2)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.noa[1][1]),&(pntdat.noa.noa[1][1]),2)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.noa[1][2]),&(pntdat.noa.noa[1][2]),2)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.noa[2][0]),&(pntdat.noa.noa[2][0]),2)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.noa[2][1]),&(pntdat.noa.noa[2][1]),2)))	return st;
                                if(ERR_OK!=(st=pa_cpy_flt(armno,&(_comuni->hd.noa.noa[2][2]),&(pntdat.noa.noa[2][2]),2)))	return st;
                        }
                        i++;
                        if(num==i){
                                ldmy = (u32)   'E';
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2)))			return st;
                                ldmy = (u32)   key;
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2)))			return st;
                                ldmy = (u32)   1;
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2)))			return st;
                                ldmy = (u32)   DataLoad;
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))				return st;
                                ldmy = (u32)   3;
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2)))				return st;

                                if( ERR_OK!=(err=pa_req_ctl(armno,RETRY_COUNT)) )	return err;

                                while( pa_fsh_chk(armno) );
                                if( pam_get_err(armno) )break;
                                break;
                        }
                        if(num!=i && num>2 && i!=1){
                                pa_map_ctl(armno);
                                ldmy = (u32)   'C';
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2)))			return st;
                                ldmy = (u32)   key;
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2)))			return st;
                                ldmy = (u32)   1;
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2)))			return st;
                                ldmy = (u32)   DataLoad;
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))				return st;
                                ldmy = (u32)   3;
                                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2)))				return st;
                        }

                        if( ERR_OK!=(err=pa_req_ctl(armno,RETRY_COUNT)) )		return err;
                        while( pa_fsh_chk(armno) );
                        if( pam_get_err(armno) )break;
                }
                fclose(stream);
        }
        else            return ERR_FILE;

        return pam_get_err(armno);
}

/******************************************************************
        JUMP��������
                ����    armno           ������No.
                        key                     �����ǡ�����Key�ֹ�
                        id                      ������ID�ֹ�
                        jmp                     JUMP����

                ����    ERR_OK          ���ｪλ
                        ERR_ARM         ������No.���ְ�äƤ���.
                        ERR_INT         �����ߤ���������ʤ��ä�.
                        �ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_set_jmp_sub(ARM armno, u32 key, u32 id, JUMPP jmp)
{
        u32    ldmy;
        u32    i;
        u32    st;

        if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;

        ldmy = (u32)   EV_SET_JMP;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.evt),&ldmy,2)))		return st;
        ldmy = (u32)   5;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.com),&ldmy,2)))		return st;
        ldmy = (u32)   key;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2)))	return st;
        ldmy = (u32)   id;
        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2)))	return st;

        if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.jmp.cid),&(jmp->cid),2)))	return st;
        for(i=0;i<8;i++){
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.jmp.jdg[i].cnd[0]),&(jmp->jdg[i].cnd[0]),2)))	return st;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.jmp.jdg[i].cnd[1]),&(jmp->jdg[i].cnd[1]),2)))	return st;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.jmp.jdg[i].xdi),&(jmp->jdg[i].xdi),2)))		return st;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.jmp.jdg[i].tim),&(jmp->jdg[i].tim),2)))		return st;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.jmp.jdg[i].key),&(jmp->jdg[i].key),2)))		return st;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.jmp.jdg[i].pid),&(jmp->jdg[i].pid),2)))		return st;
                if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(_comuni->hd.jmp.jdg[i].cnt),&(jmp->jdg[i].cnt),2)))		return st;
        }
        if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) )	return st;

        while( pa_fsh_chk(armno) );
        return pam_get_err(armno);

}



