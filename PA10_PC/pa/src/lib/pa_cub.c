//2002/04/23
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	CUBE�λ���
		����	armno	������No.
			num		CUBE�ֹ�
			xyz		������
			ypr		�Ǿ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_set_cub(ARM armno, u32 num, float xyz[], float ypr[])
{
	u32	ldmy;
	u32	i;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}


	ldmy = (u32)	EV_SET_CUB;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	num;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	for(i=0;i<3;i++){
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[i]),&(xyz[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[i]),&(ypr[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT))){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	CUBE�ζ�������
		����	armno	������No.
			num		CUBE�ֹ�
			mod		1:�����Ͷ���
					2:�Ǿ��Ͷ���
					3:�濴����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_get_cub(ARM armno, u32 num, u32 mod)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_GET_CUB;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	num;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	mod;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT))){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	CUBE����Ĺ������
		����	armno	������No.
			num	CUBE�ֹ�
			xyz	���줾����դ�Ĺ��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_cub_len(ARM armno, u32 num, float xyz[])
{
	u32	ldmy;
	u32	i;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_CUB_LEN;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	num;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	for(i=0;i<3;i++){
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[i]),&(xyz[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT))){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	CUBE��̾����Ĥ���
		����	armno		������No.
			num		CUBE�ֹ�
			name		CUBE��̾��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_cub_cmt(ARM armno, u32 num, char*	name)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_CUB_CMT;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	num;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	name[32]='\0';
	if(strlen(name)<32){
		st=pa_mem_blk((u32)armno,&(_comuni->hd.bse.pkt[0]),name,(size_t)(strlen(name)+1),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
	}
	else{
		st=pa_mem_blk((u32)armno,&(_comuni->hd.bse.pkt[0]),name,(size_t)32,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	CUBE�κ��
		����	armno		������No.
			num		CUBE�ֹ�

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_del_cub(ARM armno, u32 num)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_DEL_CUB;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	num;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	CUBE��ͭ��/̵��
		����	armno		������No.
			num		CUBE�ֹ�
			mod		1:ͭ��
					0:̵��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_ena_cub(ARM armno, u32 num, u32 mod)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_ENA_CUB;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	num;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	mod;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	CUBE���󻲾�
		����	armno		������No.
			num		CUBE�ֹ�
			cub		CUBE����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_inf_cub(ARM armno, u32 num, CUBEP cub)
{
	u32	ldmy;
	char	cmt[33];
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_INF_CUB;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	num;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);

	if( ERR_OK==(st=pam_get_err(armno)) ){
		pa_map_ctl(armno);
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(cub->ena),&(_comuni->hu.cub.ena),1)))		return(st);
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(cub->mod),&(_comuni->hu.cub.mod),1)))		return(st);
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(cub->max[0]),&(_comuni->hu.cub.max[0]),1)))	return(st);
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(cub->max[1]),&(_comuni->hu.cub.max[1]),1)))	return(st);
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(cub->max[2]),&(_comuni->hu.cub.max[2]),1)))	return(st);
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(cub->min[0]),&(_comuni->hu.cub.min[0]),1)))	return(st);
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(cub->min[1]),&(_comuni->hu.cub.min[1]),1)))	return(st);
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(cub->min[2]),&(_comuni->hu.cub.min[2]),1)))	return(st);
		
		if(ERR_OK!=(st=pa_mem_blk((u32)armno,cmt,&(_comuni->hu.cub.cmt[0]),sizeof(_comuni->hu.cub.cmt),1)))	return(st);

		memcpy(cub->cmt,cmt,32);		
	}

	return pam_get_err(armno);
}
