#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	<fcntl.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

#define	TextBufSize	80+1
#define	CRX	0x0a


/******************************************************************
	tool parm ���ѹ����롣
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_set_tol(ARM armno, float d0, float d1, float d2, float d3)
{
	float	fdmy;
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	SetToolEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	fdmy = d0;						/* x ���֥��ե��å� */
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.tol[0]),&fdmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	fdmy = d1;						/* y ���֥��ե��å� */
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.tol[1]),&fdmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	fdmy = d2;						/* z ���֥��ե��å� */
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.tol[2]),&fdmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	fdmy = 0.0;						/**/
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.tol[3]),&fdmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	fdmy = 0.0;						/**/
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.tol[4]),&fdmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	fdmy = 0.0;						/**/
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.tol[5]),&fdmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	fdmy = d3;						/* ����̥��ե��å� */
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.tol[6]),&fdmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�ǥե����®�٤��ѹ����롣
		����	armno		������No.
			vt			�ѹ�°�������
			dat			�ѹ�°����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_VT		VELTYPE���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_set_vel(ARM armno, VELTYPE vt, float* dat)
{
	u32	ldmy;
	int		i;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	switch(vt){
	case VT_ONEVEL:
		ldmy = (u32)	EACHVelocityEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		if(axsnum==6)	dat[2]=0.0f;
		for(i=0;i<7;i++){
			st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.dev[i]),&(dat[i]),2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
		}
		break;
	case VT_XYZVEL:
		ldmy = (u32)	RMRCVelocityEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.dev[7]),&(dat[0]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case VT_YPRVEL:
		ldmy = (u32)	RMRCRotateEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.prm.dev[8]),&(dat[0]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_VT;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}







u32	TramParRW(ARM armno,PRM* parameter,char *filename, char mode)
{
	short	fd,num=0;
	File	h;
	PRM		p;
	int		i;
	u32	st;

	switch(mode){
	case 'r':
	case 'R':
		if( (fd=open(filename,O_RDONLY,0666))>0 ){
			num=read( fd, (char*)&h, sizeof(File));
			if( h.id==_CTL_PRM && num==sizeof(File) ){
				
				num=read( fd, (char *)&p, sizeof(PRM));
				for(i=0;i<AXISNUMBER;i++){
					st=pa_cpy_flt(	(u32)armno,&(parameter->pul[i]),&(p.pul[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->pdl[i]),&(p.pdl[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
				}
				for(i=0;i<(AXISNUMBER+2);i++){
					st=pa_cpy_flt(	(u32)armno,&(parameter->vel[i]),&(p.vel[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->dev[i]),&(p.dev[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->lim[i]),&(p.lim[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->ceh[i]),&(p.ceh[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->cem[i]),&(p.cem[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->cel[i]),&(p.cel[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
				}
				for(i=0;i<AXISNUMBER;i++){
					st=pa_cpy_flt(	(u32)armno,&(parameter->pg1[i]),&(p.pg1[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->pg2[i]),&(p.pg2[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->vg1[i]),&(p.vg1[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->tg1[i]),&(p.tg1[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->pcm[i]),&(p.pcm[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->fcm[i]),&(p.fcm[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->arl[i]),&(p.arl[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->arg[i]),&(p.arg[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->arw[i]),&(p.arw[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->hom[i]),&(p.hom[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->saf[i]),&(p.saf[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->esc[i]),&(p.esc[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->tol[i]),&(p.tol[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_flt(	(u32)armno,&(parameter->fvl[i]),&(p.fvl[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_lng(	(u32)armno,&(parameter->dmy[i]),&(p.dmy[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
					st=pa_cpy_lng(	(u32)armno,&(parameter->spa[i]),&(p.spa[i]),2);
					if(st<0){
						close(fd);
						return(st);
					}
				}
				if( num==sizeof(PRM) )break;
			}
		}
		fprintf(stderr,"Can't read \"%s\". %d\n",filename,num);
		close(fd);
		return ERR_FILE;

	case 'w':
	case 'W':
		if( (fd=open(filename,O_RDWR|O_CREAT,0666))>0 ){
			h.id=_CTL_PRM;
			strcpy(h.path,filename);
			h.comment[0]=0;
			num=write( fd, (char*)&h, sizeof(File));

			if( num==sizeof(File) ){
				num=write( fd, (char*)parameter, sizeof(PRM));
				if( num==sizeof(PRM) ){
					break;
				}
			}
		}
		fprintf(stderr,"Can't write \"%s\". %d\n",filename,num);
		close(fd);
		return ERR_FILE;
	}
	close(fd);
	return ERR_OK;
}






/**************************************************************
	���楳��ȥ�����Υѥ��-�� ������
		����	armno		������No.
			file		���楳��ȥ�����ѥ�᡼���ե�����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_FILE	���楳��ȥ�����ѥ�᡼���ե����뤬�����ץ�Ǥ��ʤ�
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_PRM		������ϥѥ�᡼�����ѹ��ϤǤ��ʤ�
****************************************************************/
DllExport u32 __stdcall pa_sav_ctl(ARM armno, char *file)
{
	int		i;
	PRM		p;
	u32	stat;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;

	if( ERR_OK!=(st=pa_cpy_lng((u32)armno,&stat,&(_comuni->hu.get.lvl[7]),1)))	return st;	//��-�����

	if(stat==0){

		for(i=0;i<AXISNUMBER;i++){
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.pul[i]),&(_comuni->hu.prm.pul[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.pdl[i]),&(_comuni->hu.prm.pdl[i]),1)))	return st;
		}
		for(i=0;i<(AXISNUMBER+2);i++){
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.vel[i]),&(_comuni->hu.prm.vel[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.dev[i]),&(_comuni->hu.prm.dev[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.lim[i]),&(_comuni->hu.prm.lim[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.ceh[i]),&(_comuni->hu.prm.ceh[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.cem[i]),&(_comuni->hu.prm.cem[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.cel[i]),&(_comuni->hu.prm.cel[i]),1)))	return st;
		}
		for(i=0;i<AXISNUMBER;i++){
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.pg1[i]),&(_comuni->hu.prm.pg1[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.pg2[i]),&(_comuni->hu.prm.pg2[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.vg1[i]),&(_comuni->hu.prm.vg1[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.tg1[i]),&(_comuni->hu.prm.tg1[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.pcm[i]),&(_comuni->hu.prm.pcm[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.fcm[i]),&(_comuni->hu.prm.fcm[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.arl[i]),&(_comuni->hu.prm.arl[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.arg[i]),&(_comuni->hu.prm.arg[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.arw[i]),&(_comuni->hu.prm.arw[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.hom[i]),&(_comuni->hu.prm.hom[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.saf[i]),&(_comuni->hu.prm.saf[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.esc[i]),&(_comuni->hu.prm.esc[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.tol[i]),&(_comuni->hu.prm.tol[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(p.fvl[i]),&(_comuni->hu.prm.fvl[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_lng((u32)armno,&(p.dmy[i]),&(_comuni->hu.prm.dmy[i]),1)))	return st;
			if( ERR_OK!=(st=pa_cpy_lng((u32)armno,&(p.spa[i]),&(_comuni->hu.prm.spa[i]),1)))	return st;
		}

		if(ERR_OK!=(st=TramParRW(armno,&p,file,'w')) )	return st;
	}else{
		return	ERR_PRM;
	}

	return ERR_OK;
}

/**************************************************************
	���楳��ȥ�����Υѥ��-�� ��-��
		����	armno		������No.
			file		���楳��ȥ�����ѥ�᡼���ե�����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_FILE	���楳��ȥ�����ѥ�᡼���ե����뤬�����ץ�Ǥ��ʤ�
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_PRM		������ϥѥ�᡼�����ѹ��ϤǤ��ʤ�
****************************************************************/
DllExport u32 __stdcall pa_lod_ctl(ARM armno, char *file)
{
	u32	ldmy;
	u32	stat;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&stat,&(_comuni->hu.get.lvl[7]),1);	//��-�����
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if(stat==0){
		ldmy = (u32)	ParamSetEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	5;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	2;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}

		if( ERR_OK!=(st=TramParRW(armno,&_comuni->hd.prm,file,'r'))){
			UnlockMutex((u32)armno);
			return st;
		}
		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			UnlockMutex((u32)armno);
			return st;
		}
		while(pa_fsh_chk(armno));
	}else{
		UnlockMutex((u32)armno);
		return	ERR_PRM;
	}
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/**************************************************************
	��ư����CPU���ѥ�᡼���������
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
****************************************************************/
DllExport u32 __stdcall pa_lod_def(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	SaveParamEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/**************************************************************
	��ư����CPU���ѥ�᡼����-��
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
****************************************************************/
DllExport u32 __stdcall pa_sav_def(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	LoadParamEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

