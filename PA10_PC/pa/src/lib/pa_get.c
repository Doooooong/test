/*


	PAlib arm status get function


*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

#define	RadToDeg(x) ((x)*180.0/M_PI)

/******************************************************************
	�����ӥ�all)���������
		����	armno		������No.
			stat		��Ǽ�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_get_sts(ARM armno, ARMSTATUSP stat)
{
	u32	st;
	st=pa_get_cnt(armno,&stat->count);
	st=pa_get_err(armno,&stat->error);
	st=pa_get_agl(armno,&stat->angle);
	st=pa_get_noa(armno,stat->noap);
	st=pa_get_ypr(armno,stat->ypr);

	pa_map_ctl(armno);
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(stat->remote),&(_comuni->hu.get.lvl[7]),1)))	return(st);
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(stat->max),&(_comuni->hu.ard.max),1)))		return(st);
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(stat->arm),&(_comuni->hu.ard.arm),1)))		return(st);
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(stat->axis),&(_comuni->hu.ard.axs),1)))		return(st);
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(stat->typ),&(_comuni->hu.ard.typ),1)))		return(st);
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(stat->drv),&(_comuni->hu.ard.drv),1)))		return(st);
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(stat->dio),&(_comuni->hu.ard.dio),1)))		return(st);

	return pam_get_err(armno);
}

/******************************************************************
	������Ⱦ�����������
		����	armno		������No.
			cnt			���������

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_cnt(ARM armno, u32* cnt)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,cnt,&(_comuni->hu.err.dmy[14]),1)))	return st;

	return pam_get_err(armno);
}

/******************************************************************
	����-������������
		����	armno		������No.
			er			error

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_get_err(ARM armno, u32* er)
{
	u32 st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	*er=pam_get_err(armno);
	return(*er);
}
/******************************************************************
	�������پ�����������
		����	armno		������No.
			angle		ANGLE�� �إ� �ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_agl(ARM armno, ANGLEP an)
{
	static	float	fdmy;
	u32	axsnum;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1)))	return st;		//	�����༴������

	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.mrk.agl[0]),1)))	return st;
	an->s1=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.mrk.agl[1]),1)))	return st;
	an->s2=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.mrk.agl[2]),1)))	return st;
	if(axsnum==6)		an->s3=0.0f;
	else if(axsnum==7)	an->s3=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.mrk.agl[3]),1)))	return st;
	an->e1=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.mrk.agl[4]),1)))	return st;
	an->e2=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.mrk.agl[5]),1)))	return st;
	an->w1=(float)(fdmy);
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.pos.mrk.agl[6]),1)))	return st;
	an->w2=(float)(fdmy);

	return pam_get_err(armno);
}

/******************************************************************
	xyz������������
		����	armno		������No.
			vec			VECTOR��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_xyz(ARM armno, VECTOR vec)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(vec[0]),&(_comuni->hu.ypr.mrk[0]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(vec[1]),&(_comuni->hu.ypr.mrk[1]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(vec[2]),&(_comuni->hu.ypr.mrk[2]),1)))	return st;

	return pam_get_err(armno);
}
/******************************************************************
	noap������������
		����	armno		������No.
			noa			noap����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_noa(ARM armno, MATRIX noap)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[0][0]),&(_comuni->hu.pos.mrk.noa[0][0]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[0][1]),&(_comuni->hu.pos.mrk.noa[0][1]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[0][2]),&(_comuni->hu.pos.mrk.noa[0][2]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[0][3]),&(_comuni->hu.pos.mrk.xyz[0]),1)))		return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[1][0]),&(_comuni->hu.pos.mrk.noa[1][0]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[1][1]),&(_comuni->hu.pos.mrk.noa[1][1]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[1][2]),&(_comuni->hu.pos.mrk.noa[1][2]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[1][3]),&(_comuni->hu.pos.mrk.xyz[1]),1)))		return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[2][0]),&(_comuni->hu.pos.mrk.noa[2][0]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[2][1]),&(_comuni->hu.pos.mrk.noa[2][1]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[2][2]),&(_comuni->hu.pos.mrk.noa[2][2]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[2][3]),&(_comuni->hu.pos.mrk.xyz[2]),1)))		return st;
	return pam_get_err(armno);
}

/******************************************************************
	ypr������������
		����	armno		������No.
			vec		ypr����

		����	ERR_OK		���ｪλ
			ERR_ARM		��������꤬�ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_ypr(ARM armno, VECTOR vec)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(vec[0]),&(_comuni->hu.ypr.mrk[3]),1)))		return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(vec[1]),&(_comuni->hu.ypr.mrk[4]),1)))		return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(vec[2]),&(_comuni->hu.ypr.mrk[5]),1)))		return st;

	return pam_get_err(armno);
}

/******************************************************************
	parameter������������
		����	armno		������No.
			parm		�ѥ�᡼������

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_PRM		������ϥѥ�᡼�����ѹ��ϤǤ��ʤ�
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_prm(ARM armno, PARAMP parm)
{
	PRM* 	p;
	int	i;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	p=(PRM*)parm->pul;
	for(i=0;i<AXISNUMBER;i++){
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->pul[i]),&(_comuni->hu.prm.pul[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->pdl[i]),&(_comuni->hu.prm.pdl[i]),1)))	return st;
	}
	for(i=0;i<(AXISNUMBER+2);i++){
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->vel[i]),&(_comuni->hu.prm.vel[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->dev[i]),&(_comuni->hu.prm.dev[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->lim[i]),&(_comuni->hu.prm.lim[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->ceh[i]),&(_comuni->hu.prm.ceh[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->cem[i]),&(_comuni->hu.prm.cem[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->cel[i]),&(_comuni->hu.prm.cel[i]),1)))	return st;
	}
	for(i=0;i<AXISNUMBER;i++){
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->pg1[i]),&(_comuni->hu.prm.pg1[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->pg2[i]),&(_comuni->hu.prm.pg2[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->vg1[i]),&(_comuni->hu.prm.vg1[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->tg1[i]),&(_comuni->hu.prm.tg1[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->pcm[i]),&(_comuni->hu.prm.pcm[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->fcm[i]),&(_comuni->hu.prm.fcm[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->arl[i]),&(_comuni->hu.prm.arl[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->arg[i]),&(_comuni->hu.prm.arg[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->arw[i]),&(_comuni->hu.prm.arw[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->hom[i]),&(_comuni->hu.prm.hom[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->saf[i]),&(_comuni->hu.prm.saf[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->esc[i]),&(_comuni->hu.prm.esc[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->tol[i]),&(_comuni->hu.prm.tol[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->fvl[i]),&(_comuni->hu.prm.fvl[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(parm->dmy[i]),&(_comuni->hu.prm.dmy[i]),1)))	return st;
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(parm->spa[i]),&(_comuni->hu.prm.spa[i]),1)))	return st;
	}

	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&(parm->rezl),&(_comuni->hu.ard.rtv[0]),1)))		return st;

	return pam_get_err(armno);
}

/******************************************************************
	�����ȶ�����������������ʥ�˥����ѡ�
		����	armno		������No.
			pntdat		��������-����¤�ΤؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_mon_pnt(ARM armno, PNTDATP pntdat)
{
	u32	i,j;
	u32	axsnum;
	char	cmt[33];
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1)))	return st;	//	�����༴������
	for(i=0;i<7;i++){
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->ply.pnt.agl[i]),&(_comuni->hu.sts.ply.pnt.agl[i]),1)))	return st;
		if(axsnum==6)	pntdat->ply.pnt.agl[2]=0.0f;
	}
	for(i=0;i<2;i++){
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->ply.pnt.vel[i]),&(_comuni->hu.sts.ply.pnt.vel[i]),1)))	return st;
	}
	for(i=0;i<12;i++){
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->ply.pnt.atr[i]),&(_comuni->hu.sts.ply.pnt.atr[i]),1)))	return st;
	}
	for(i=0;i<3;i++){
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->noa.xyz[i]),&(_comuni->hu.sts.noa.xyz[i]),1)))		return st;
		for(j=0;j<3;j++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->noa.noa[i][j]),&(_comuni->hu.sts.noa.noa[i][j]),1)))	return st;
		}
	}
	if(ERR_OK!=(st=pa_mem_blk((u32)armno,cmt,&(_comuni->hu.sts.ply.cmt[0]),(size_t)32,1)))		return st;
	memcpy(pntdat->ply.cmt,cmt,32);

	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.cid),&(_comuni->hu.sts.jmp.cid),1)))	return st;
	for(i=0;i<8;i++){
		for(j=0;j<2;j++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].cnd[j]),&(_comuni->hu.sts.jmp.jdg[i].cnd[j]),1)))	return st;
		}
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].xdi),&(_comuni->hu.sts.jmp.jdg[i].xdi),1)))		return st;
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].tim),&(_comuni->hu.sts.jmp.jdg[i].tim),1)))		return st;
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].key),&(_comuni->hu.sts.jmp.jdg[i].key),1)))		return st;
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].pid),&(_comuni->hu.sts.jmp.jdg[i].pid),1)))		return st;
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].cnt),&(_comuni->hu.sts.jmp.jdg[i].cnt),1)))		return st;
	}

	return pam_get_err(armno);
}

/******************************************************************
	�����ȶ�����������������ʰ������ѡ�
		����	armno		������No.
			pntdat		��������-����¤�ΤؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_pnt(ARM armno, PNTDATP pntdat)
{
	u32	i,j;
	u32	ldmy;
	u32	axsnum;
	char	cmt[33];
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	GetPntAttrEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);

	if( ERR_OK==(st=pam_get_err(armno)) ){
		pa_map_ctl(armno);

		for(i=0;i<7;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->ply.pnt.agl[i]),&(_comuni->hu.ply.pnt.agl[i]),1)))	return st;
			if(axsnum==6)	pntdat->ply.pnt.agl[2]=0.0f;	
		}
		for(i=0;i<2;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->ply.pnt.vel[i]),&(_comuni->hu.ply.pnt.vel[i]),1)))	return st;
		}
		for(i=0;i<12;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->ply.pnt.atr[i]),&(_comuni->hu.ply.pnt.atr[i]),1)))	return st;
		}
		for(i=0;i<3;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->noa.xyz[i]),&(_comuni->hu.noa.xyz[i]),1)))		return st;
			for(j=0;j<3;j++){
				if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->noa.noa[i][j]),&(_comuni->hu.noa.noa[i][j]),1)))	return st;
			}
		}
		if(ERR_OK!=(st=pa_mem_blk((u32)armno,cmt,&(_comuni->hu.sts.ply.cmt[0]),(size_t)32,1)))				return st;
		memcpy(pntdat->ply.cmt,cmt,32);
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.cid),&(_comuni->hu.jmp.cid),1)))				return st;
		for(i=0;i<8;i++){
			for(j=0;j<2;j++){
				if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].cnd[j]),&(_comuni->hu.jmp.jdg[i].cnd[j]),1)))	return st;
			}
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].xdi),&(_comuni->hu.jmp.jdg[i].xdi),1)))	return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].tim),&(_comuni->hu.jmp.jdg[i].tim),1)))	return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].key),&(_comuni->hu.jmp.jdg[i].key),1)))	return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].pid),&(_comuni->hu.jmp.jdg[i].pid),1)))	return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(pntdat->jmp.jdg[i].cnt),&(_comuni->hu.jmp.jdg[i].cnt),1)))	return st;
		}
	}

	return pam_get_err(armno);
}

/******************************************************************
	�����ȶ�����No.���������
		����	armno		������No.
			cur			�ݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_cur(ARM armno, u32* cur)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,cur,&(_comuni->hu.sts.lvl[4]),1)))	return st;

	return pam_get_err(armno);
}

/******************************************************************
	�������������������
		����	armno		������No.
			num		�����������ؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_num(ARM armno, u32* num)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,num,&(_comuni->hu.sts.lvl[3]),1)))	return st;

	return pam_get_err(armno);
}

/******************************************************************
	�ǥХå����ꥢ������������
		����	armno		������No.
			dbg		�ǥХå��ǡ�����¤�ΤؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_dbg(ARM armno, DEBGP dbg)
{
	u32	i;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	for(i=0;i<16;i++){
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(dbg->ldbg[i]),&(_comuni->hu.dbg.lvl[i]),1)))	return st;
	}
	for(i=0;i<32;i++){
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(dbg->fdbg[i]),&(_comuni->hu.dbg.fvl[i]),1)))	return st;
	}
	return pam_get_err(armno);
}

/******************************************************************
	�ܡ��ɤ������ǽ����������������
		����	armno		������No.
			num		�����ǽ�������1or2

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_max(ARM armno, u32* num)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;

	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,num,&(_comuni->hu.ard.max),1)))	return st;

	return pam_get_err(armno);
}

/******************************************************************
	���Ȥ������ܤˤ��뤫���������
		����	armno		������No.
			spt		0or1

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_spt(ARM armno, u32* spt)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;

	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,spt,&(_comuni->hu.ard.arm),1)))	return st;

	return pam_get_err(armno);
}
/******************************************************************

  
******************************************************************/
DllExport u32 	pa_get_lvl(ARM armno, u32 pos,u32 *lvl)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )		return st;
	if( ERR_OK!=(st=pa_cpy_lng(	(u32)armno,lvl,&(_comuni->hu.get.lvl[pos]),1)))		return st;
	return pam_get_err(armno);
}
/******************************************************************

  
******************************************************************/
DllExport u32	pa_get_fvl(ARM armno, u32 pos,float *fvl)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )		return st;
	if( ERR_OK!=(st=pa_cpy_flt(	(u32)armno,fvl,&(_comuni->hu.get.fvl[pos]),1)))	return st;
	return pam_get_err(armno);
}
/******************************************************************

	��������ͤȸ��ߤ�NOAP���������

******************************************************************/
DllExport u32 	pa_get_cnp(ARM armno, u32 *count,MATRIX noap)
{
	u32	st;
	if( ERR_OK!=(st=pa_map_ctl(armno)) )		return st;
	if( ERR_OK!=(st=pa_cpy_lng((u32)armno,count,&(_comuni->hu.dbg.lvl[0]),1)))				return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[0][0]),&(_comuni->hu.dbg.fvl[3]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[0][1]),&(_comuni->hu.dbg.fvl[4]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[0][2]),&(_comuni->hu.dbg.fvl[5]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[0][3]),&(_comuni->hu.dbg.fvl[0]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[1][0]),&(_comuni->hu.dbg.fvl[6]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[1][1]),&(_comuni->hu.dbg.fvl[7]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[1][2]),&(_comuni->hu.dbg.fvl[8]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[1][3]),&(_comuni->hu.dbg.fvl[1]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[2][0]),&(_comuni->hu.dbg.fvl[9]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[2][1]),&(_comuni->hu.dbg.fvl[10]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[2][2]),&(_comuni->hu.dbg.fvl[11]),1)))		return st;
	if( ERR_OK!=(st=pa_cpy_flt((u32)armno,&(noap[2][3]),&(_comuni->hu.dbg.fvl[2]),1)))		return st;
	return pam_get_err(armno);
}














