/*


	PAlib digital I/O function


*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	�ި�������
		����	armno		������No.
			dk		�ܡ��ɤμ���
			ds		������

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall	pa_inp_dio(ARM armno, DIOKIND dk, DIOSTATUSP ds)
{
	u32	st;

	if(ERR_OK!=(st=pa_map_ctl(armno)))	return st;
	if(ERR_OK!=(st=pa_mem_cpy((u32)armno,ds,&(_comuni->hu.dio.i1420[dk]),sizeof(DIOSTATUS),1)))	return(st);
	return pam_get_err(armno);
}

/******************************************************************
	�ǥ������� ����
		����	armno		������No.
			dk		�ܡ��ɤμ���
			ds		������

		����	ERR_OK			���ｪλ
			ERR_ARM			������No.���ְ�äƤ���.
			ERR_INT			�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall	pa_oup_dio(ARM armno, DIOKIND dk, DIOSTATUSP ds)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_SET_DIO;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	st=pa_mem_cpy(	(u32)armno,&(_comuni->hd.dio.o1420[dk]),ds,sizeof(DIOSTATUS),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_sub(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	�ǥ������� ch. ����
		����	armno			������No.
			dk			�ܡ��ɤμ���
			dp			�ǥ�������port
			dc			�ǥ�������channal
			byte			���ϥǡ���

		����	ERR_OK			���ｪλ
			ERR_ARM			������No.���ְ�äƤ���.
			ERR_INT			�����ߤ���������ʤ��ä�.
			ERR_DP			�ݡ����ֹ椬�ְ�äƤ���
			ERR_DC			������ͥ��ֹ椬�ְ�äƤ���
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall	pa_get_dio(ARM armno, DIOKIND dk, DIOPORT dp, DIOCH dc, unsigned char* byte)
{
	unsigned char 	*p,bit;
	DIOSTATUS		ds;
	u32			st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )		return st;
	if( dc<DC_CH1 || dc>DC_CH8 )			return ERR_DC;
	if( dk!=DIO_INTERNAL && dk!=DIO_EXTERNAL )	return ERR_BOARD;

	switch(dp){
	case DP_PORT1:
	case DP_PORT2:
	case DP_PORT3:
	case DP_PORT4:
		if(ERR_OK!=(st=pa_mem_cpy((u32)armno,&ds,
			(void*)&_comuni->hu.dio.i1420[dk],sizeof(DIOSTATUS),1)))	return(st);
		p=(unsigned char*)&ds;
		bit=(unsigned char)(1<<dc);
		bit &= p[dp];
		*byte=bit;
		return pam_get_err(armno);
	case DPO_PORT1:
	case DPO_PORT2:
	case DPO_PORT3:
	case DPO_PORT4:
		if(ERR_OK!=(st=pa_mem_cpy((u32)armno,(void*)&ds,
			(void*)&_comuni->hu.dio.o1420[dk],sizeof(DIOSTATUS),1)))	return(st);
		p=(unsigned char*)&ds;
		bit=(unsigned char)(1<<dc);
		bit &= p[dp-4];
		*byte=bit;
		return pam_get_err(armno);
	case DPX_PORT1:
	case DPX_PORT2:
	case DPX_PORT3:
	case DPX_PORT4:
		if(ERR_OK!=(st=pa_mem_cpy((u32)armno,(void*)&ds,
			(void*)&_comuni->hu.dio.x1420[dk],sizeof(DIOSTATUS),1)))	return(st);
		p=(unsigned char*)&ds;
		bit=(unsigned char)(1<<dc);
		bit &= p[dp-8];
		*byte=bit;
		return pam_get_err(armno);
	default:
		return ERR_DP;
	}
}

/******************************************************************
	�ǥ������� set ����
		����	armno		������No.
			dk		�ܡ��ɤμ���
			dp		�ǥ�������ݡ���
			dc		�����ͥ�

		����	ERR_OK			���ｪλ
			ERR_ARM			������No.���ְ�äƤ���.
			ERR_INT			�����ߤ���������ʤ��ä�.
			ERR_DP			�ݡ����ֹ椬�ְ�äƤ���
			ERR_DC			������ͥ��ֹ椬�ְ�äƤ���
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall	pa_set_dio(ARM armno, DIOKIND dk, DIOPORT dp, DIOCH dc)
{
	u32			ldmy;
	unsigned char 	bit,*p;
	DIOSTATUS		ds;
	u32			st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_SET_DIO;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	st=pa_mem_cpy(	(u32)armno,(void*)&ds,
			(void*)&_comuni->hu.dio.o1420[dk],sizeof(DIOSTATUS),1);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	p=(unsigned char*)&ds;

	if( dc<DC_CH1 || dc>DC_CH8 ){
		UnlockMutex((u32)armno);
		return ERR_DC;
	}
	if( dk!=DIO_INTERNAL && dk!=DIO_EXTERNAL ){
		UnlockMutex((u32)armno);
		return ERR_BOARD;
	}
	
	switch(dp){
	case DP_PORT1:
	case DP_PORT2:
	case DP_PORT3:
	case DP_PORT4:
		bit=(unsigned char)(1<<dc);
		p[dp] |= bit;
		st=pa_mem_cpy((u32)armno,(void*)&_comuni->hd.dio.o1420[dk],
					p,sizeof(DIOSTATUS),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			UnlockMutex((u32)armno);
			return st;
		}
		while( pa_fsh_sub(armno) );
		UnlockMutex((u32)armno);
		return pam_get_err(armno);
	default:
		UnlockMutex((u32)armno);
		return ERR_DP;
	}
}

/******************************************************************
	�ǥ������� reset ����
		����	armno		������No.
			dk		�ܡ��ɤμ���
			dp		�ǥ�������ݡ���
			dc		�ǥ�����������ͥ�

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_DP		�ݡ����ֹ椬�ְ�äƤ���
			ERR_DC		������ͥ��ֹ椬�ְ�äƤ���
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall	pa_rst_dio(ARM armno, DIOKIND dk, DIOPORT dp, DIOCH dc)
{
	u32			ldmy;
	unsigned char 	bit,*p;
	DIOSTATUS		ds;
	u32			st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_SET_DIO;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	st=pa_mem_cpy(	(u32)armno,(void*)&ds,
			(void*)&_comuni->hu.dio.o1420[dk],sizeof(DIOSTATUS),1);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	p=(unsigned char*)&ds;

	if( dc<DC_CH1 || dc>DC_CH8 ){
		UnlockMutex((u32)armno);
		return ERR_DC;
	}
	if( dk!=DIO_INTERNAL && dk!=DIO_EXTERNAL ){
		UnlockMutex((u32)armno);
		return ERR_BOARD;
	}
	switch(dp){
	case DP_PORT1:
	case DP_PORT2:
	case DP_PORT3:
	case DP_PORT4:
		bit=(unsigned char)(1<<dc);
		p[dp] &=~bit;
		st=pa_mem_cpy((u32)armno,
		(void*)&_comuni->hd.dio.o1420[dk],p,sizeof(DIOSTATUS),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			UnlockMutex((u32)armno);
			return st;
		}
		while( pa_fsh_sub(armno) );
		UnlockMutex((u32)armno);
		return pam_get_err(armno);
	default:
		UnlockMutex((u32)armno);
		return ERR_DP;
	}
}

/******************************************************************
	DIO �ޥ�������
		����	armno	������No.
			dio	DO or DI
			kind	�ܡ��ɤμ���
			mask	�ޥ���bit�ʥ����ƥ�ϲ���8bit�Τߡ���ĥ32bit��

		����	ERR_OK			���ｪλ
			ERR_ARM			������No.���ְ�äƤ���.
			ERR_INT			�����ߤ���������ʤ��ä�.
			ERR_BOARD		�ܡ����ֹ椬�ְ�äƤ���
			ERR_DIO			DIO���꤬�ְ�äƤ���
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall	pa_dio_msk(ARM armno, u32 dio, u32 kind, u32 msk)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	if( kind!=DIO_INTERNAL && kind!=DIO_EXTERNAL ){
		UnlockMutex((u32)armno);
		return ERR_BOARD;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	msk;
	switch(dio){
	case DIMSK:
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dio.imask[kind]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
		ldmy = (u32)	EV_DIS_MSK;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
		break;
	case DOMSK:
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dio.omask[kind]),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
		ldmy = (u32)	EV_DOS_MSK;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_DIO;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_sub(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	DIO �ޥ�������
		����	armno	������No.
			dio	DO or DI
			kind	�ܡ��ɤμ���
			mask	�ޥ���bit�ʥ����ƥ�ϲ���8bit�Τߡ���ĥ32bit��

		����	ERR_OK			���ｪλ
			ERR_ARM			������No.���ְ�äƤ���.
			ERR_BOARD		�ܡ����ֹ椬�ְ�äƤ���
			ERR_DIO			DIO���꤬�ְ�äƤ���
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall	pa_get_msk(ARM armno, u32 dio, u32 kind, u32* msk)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )		return st;
	if( kind!=DIO_INTERNAL && kind!=DIO_EXTERNAL )	return ERR_BOARD;

	switch(dio){
	case DIMSK:
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,msk,&(_comuni->hu.dio.imask[kind]),1)))	return(st);
		break;
	case DOMSK:
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,msk,&(_comuni->hu.dio.omask[kind]),1)))	return(st);
		break;
	default:
		return ERR_DIO;
	}
	return pam_get_err(armno);
}



