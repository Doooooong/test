/*


	PAlib depended control function


*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	��ɸ�͸�Ǥ��mode
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_mod_dpd(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	RMRCStartEvent8;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	��ɸ�͸�Ǥ���⡼������
		����	armno		������No.
			mtx			���а�����ɸ����
			an			ANGLE���ؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_odr_dpd(ARM armno, MATRIX mtx, ANGLEP an)
{
	short 	j,k;
	u32	ldmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	ldmy = (u32)	7;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	SendNOAPEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.xyz[0]),&(mtx[0][3]),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.xyz[1]),&(mtx[1][3]),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.xyz[2]),&(mtx[2][3]),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	for(j=0;j<3;j++){
		for(k=0;k<3;k++){
			st=pa_cpy_flt((u32)armno,&(_comuni->hd.mat.noa[j][k]),&(mtx[j][k]),2);
			if(st<0){
				UnlockMutex((u32)armno);
				return(st);
			}
		}
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[0]),&(an->s1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[1]),&(an->s2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	if(axsnum==6)	an->s3=0.0f;
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[2]),&(an->s3),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[3]),&(an->e1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[4]),&(an->e2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[5]),&(an->w1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[6]),&(an->w2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

