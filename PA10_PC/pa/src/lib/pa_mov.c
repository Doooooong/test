/*


	PAlib moving function


*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/*********************************************************************
	�����żӡ��ۥ�����쵥�����
		��armno		������No.

		���ť�	ERR_OK		�����������ۥ�
			ERR_ARM   	������No.���㥨�衼榥ơ��ˡ�����
			ERR_INT		�����ߤ���������ʤ��ä
			�ʾ��¾�����फ��Υ��顼������ޤ���
**********************************************************************/
DllExport u32  pa_sta_arm(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT04;  // Do not change it to others like COM_FMT00... , that operation will get -3000 error.
	st=pa_cpy_lng(	(u32)armno,(void*)&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	PowerOnEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/*********************************************************************
	�����ƥ�����潪λ
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�����फ��Υ��顼������ޤ���
*********************************************************************/
DllExport u32  pa_ext_arm(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT04;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	PowerOffEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
		
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/*********************************************************************
	��ߥ��٥��ȯ��
		����	armno		������No.
			wfp			ư��δ�λ�ޤ��ԤĤ��ɤ��������

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
*********************************************************************/
DllExport u32  pa_stp_arm(ARM armno, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	StopEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/**********************************************************
	������(�����ڥ��)
		����	armno		������No.
			wfp		ư���λ�ޤ��ԤĤ��ɤ��������

		����	ERR_OK		���ｪλ
			ERR_ARM   	������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
***********************************************************/
DllExport u32  pa_sus_arm(ARM armno, u32 wfp)
{
	u32	ldmy;
	u32	st;
	
	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	PouseEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
		
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/**********************************************************
	�Ƶ�ư
		����	armno		������No.
			wfp		ư���λ�ޤ��ԤĤ��ɤ��������

		����	ERR_OK		���ｪλ
			ERR_ARM   	������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
***********************************************************/
DllExport u32  pa_rsm_arm(ARM armno, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	RestartEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
		
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/**************************************************************************
	�Ƽ�ư��
		����	armno		������No.
			axis		ư�����
			angle		ư����ٹ�¤�� [rad]
			wfp		ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
*************************************************************************/
DllExport u32  pa_exe_axs(ARM armno, AXIS axis, ANGLEP angle, u32 wfp)
{
	u32	ldmy;
	u32	axsnum;
	PRM		p;
	u32	i;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	for(i=0;i<7;i++){
		st=pa_cpy_flt(	(u32)armno,&(p.pul[i]),&(_comuni->hu.prm.pul[i]),1);		//	�ѥ�᡼���������Ǿ��ɤ�
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(p.pdl[i]),&(_comuni->hu.prm.pdl[i]),1);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);				//	�����༴��
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EachAngleEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}


	ldmy = (u32)	axis;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(p.pul[0]<angle->s1)		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[0]),&(p.pul[0]),2);
	else if(p.pdl[0]>angle->s1)	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[0]),&(p.pdl[0]),2);
	else						st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[0]),&(angle->s1),2);
        if(st<0){
                UnlockMutex((u32)armno);
                return st;
        }

	if(p.pul[1]<angle->s2)		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[1]),&(p.pul[1]),2);
	else if(p.pdl[1]>angle->s2)	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[1]),&(p.pdl[1]),2);
	else						st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[1]),&(angle->s2),2);
        if(st<0){
                UnlockMutex((u32)armno);
                return st;
        }

	if(axsnum==6)	angle->s3=0.0f;
	if(p.pul[2]<angle->s3)		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[2]),&(p.pul[2]),2);
	else if(p.pdl[2]>angle->s3)	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[2]),&(p.pdl[2]),2);
	else						st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[2]),&(angle->s3),2);
        if(st<0){
                UnlockMutex((u32)armno);
                return st;
        }

	if(p.pul[3]<angle->e1)		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[3]),&(p.pul[3]),2);
	else if(p.pdl[3]>angle->e1)	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[3]),&(p.pdl[3]),2);
	else						st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[3]),&(angle->e1),2);
        if(st<0){
                UnlockMutex((u32)armno);
                return st;
        }

	if(p.pul[4]<angle->e2)		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[4]),&(p.pul[4]),2);
	else if(p.pdl[4]>angle->e2)	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[4]),&(p.pdl[4]),2);
	else						st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[4]),&(angle->e2),2);
        if(st<0){
                UnlockMutex((u32)armno);
                return st;
        }

	if(p.pul[5]<angle->w1)		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[5]),&(p.pul[5]),2);
	else if(p.pdl[5]>angle->w1)	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[5]),&(p.pdl[5]),2);
	else						st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[5]),&(angle->w1),2);
        if(st<0){
                UnlockMutex((u32)armno);
                return st;
        }

	if(p.pul[6]<angle->w2)		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[6]),&(p.pul[6]),2);
	else if(p.pdl[6]>angle->w2)	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[6]),&(p.pdl[6]),2);
	else						st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[6]),&(angle->w2),2);
        if(st<0){
                UnlockMutex((u32)armno);
                return st;
        }

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	ͽ�����ꤵ�줿"����"������ư��롣
		����	armno		������No.
			wfp		ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_exe_hom(ARM armno, u32 wfp)
{
	u32	ldmy;
	u32	st;
	
	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_AXS_HOM;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
		
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	ͽ�����ꤵ�줿"����"������ư��롣
		����	armno		������No.
			wfp		ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_exe_esc(ARM armno, u32 wfp)
{
	u32	ldmy;
	u32	st;
	
	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_AXS_ESC;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	ͽ�����ꤵ�줿"����¾"������ư��롣
		����	armno		������No.
			wfp		ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_exe_saf(ARM armno, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_AXS_SAF;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	RMRC ���ܥåȺ�ɸ �����к�ư��
		����	armno		������No.
			X			���ܥåȺ�ɸ x���� �����к�
			Y			���ܥåȺ�ɸ y���� �����к�
			Z			���ܥåȺ�ɸ z���� �����к�
			wfp			ư���λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mov_XYZ(ARM armno, float X, float Y, float Z, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[0]),&X,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[1]),&Y,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[2]),&Z,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	RMRCStartEvent1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	RMRC ���ܥåȺ�ɸ �����к�ư��
		����	armno		������No.
			X			���ܥåȺ�ɸ x���� �����к�
			Y			���ܥåȺ�ɸ y���� �����к�
			Z			���ܥåȺ�ɸ z���� �����к�
			wfp			ư���λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM   	������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mov_YPR(ARM armno, float yaw, float pitch, float roll, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	RMRCStartEvent9;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[0]),&yaw,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[1]),&pitch,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[2]),&roll,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	RMRC �����ɸ �����к�ư��
		����	armno		������No.
			x			�����ɸ  x���� �����к�
			y			�����ɸ  y���� �����к�
			z			�����ɸ  z���� �����к�
			wfp			ư���λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM   	������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mov_xyz(ARM armno, float x, float y, float z, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	RMRCStartEvent2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[0]),&x,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[1]),&y,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[2]),&z,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}


	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	RMRC �����ɸ �����к�ư��
		����	armno		������No.
			yaw			�����ɸ  x���� �����к�
			pitch		�����ɸ  y���� �����к�
			roll		�����ɸ  z���� �����к�
			wfp			ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM   	������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mov_ypr(ARM armno, float yaw, float pitch, float roll, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	RMRCStartEvent6;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[0]),&yaw,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[1]),&pitch,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[2]),&roll,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}


	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	RMRC �����ɸ �����к�ư��
		����	armno	������No.
			x			�����ɸ  x���� �����к�
			y			�����ɸ  y���� �����к�
			z			�����ɸ  z���� �����к�
			wfp			ư���λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM   	������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mov_XYZ0(ARM armno, float x, float y, float z, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	RMRCStartEvent2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[0]),&x,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[1]),&y,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.xyz[2]),&z,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}


	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	RMRC �����ɸ �����к�ư��
		����	armno		������No.
			yaw			�����ɸ  x���� �����к�
			pitch		�����ɸ  y���� �����к�
			roll		�����ɸ  z���� �����к�
			wfp			ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM   	������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mov_YPR0(ARM armno, float yaw, float pitch, float roll, u32 wfp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	COM_FMT01;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	RMRCStartEvent6;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[0]),&yaw,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[1]),&pitch,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.ypr[2]),&roll,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}


	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	������ɸ����ư��
		����	armno		������No.
			mm		��ɸ���󤬡����֡����������ֻ����λ���
			mtx		��ɸ����
			an		ANGLE���ؤΥݥ���
			wfp		ư��δ�λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ��롣
			ERR_INT		�����ߤ���������ʤ��ä���
			ERR_MM		MOVEMODE���ְ�äƤ��롣
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mov_mat(ARM armno, MOVEMODE mm, MATRIX mtx, ANGLEP an,u32 wfp)
{
	short	j,k;
	u32	ldmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	
	switch(mm){
	case MM_XYZ:
		ldmy = (u32)	RMRCStartEvent11;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case MM_NOA:
		ldmy = (u32)	RMRCStartEvent12;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	case MM_XYZNOA:
		ldmy = (u32)	RMRCStartEvent13;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_MM;
	}
	ldmy = (u32)	COM_FMT06;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].xyz[0]),&(mtx[0][3]),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].xyz[1]),&(mtx[1][3]),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].xyz[2]),&(mtx[2][3]),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	for(j=0;j<3;j++){
		for(k=0;k<3;k++){
			st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].noa[j][k]),&(mtx[j][k]),2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
		}
	}

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].agl[0]),&(an->s1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].agl[1]),&(an->s2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if(axsnum==6)	an->s3=0.0f;
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].agl[2]),&(an->s3),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].agl[3]),&(an->e1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].agl[4]),&(an->e2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].agl[5]),&(an->w1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].agl[6]),&(an->w2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
