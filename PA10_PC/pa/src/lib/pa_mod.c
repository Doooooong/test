#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

//2002/04/26
/******************************************************************
	�����⡼������
		����	armno		������No.
			mod			0�������⡼�ɲ��
						1����
						2����
						3����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_ply_mod(ARM armno, u32 mod)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EV_PLY_MOD;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	mod;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�����⡼�ɼ���
		����	armno		������No.
			*mod		0�������⡼�ɲ��
					1����
					2����
					3����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_get_pmd(ARM armno, u32* mod)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,mod,&(_comuni->hu.sts.lvl[6]),1)))	return st;

	return pam_get_err(armno);
}


/******************************************************************
	TEACH_LOCK������
		����	armno		������No.
			mod		1:�����⡼��ON	
					0:�����⡼��OFF

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall	pa_set_lok(ARM armno, u32 mod)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_TCH_LOK;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	mod;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);

}

/******************************************************************
	TEACH_LOCK�μ���
		����	armno		������No.
			mod		1:�����⡼��ON	
					0:�����⡼��OFF

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall	pa_get_lok(ARM armno, u32* mod)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,mod,&(_comuni->hu.sts.lvl[8]),1)))	return st;

	return pam_get_err(armno);
}

/******************************************************************
	�����ܤ����TEACHMODE����
		����	armno		������No.
			*mod		0��OFF
					1��ON

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_get_smd(ARM armno, u32* mod)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,mod,&(_comuni->hu.sts.lvl[9]),1)))	return st;

	return pam_get_err(armno);
}

