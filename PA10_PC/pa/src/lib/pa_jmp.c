//2002/04/25
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	JUMP�������
		����	armno	������No.
			key		�����ǡ�����Key�ֹ�
			id		������ID�ֹ�
			jmp		JUMP����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_get_jmp(ARM armno, u32 key, u32 id, JUMPP jmp)
{
	u32	ldmy;
	u32	i;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_GET_JMP;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	key;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	id;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);

	if( ERR_OK==(st=pam_get_err(armno)) ){
		pa_map_ctl(armno);
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->cid),&(_comuni->hu.jmp.cid),1)))	return st;
		for(i=0;i<8;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].cnd[0]),&(_comuni->hu.jmp.jdg[i].cnd[0]),1)))	return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].cnd[1]),&(_comuni->hu.jmp.jdg[i].cnd[1]),1)))	return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].xdi),&(_comuni->hu.jmp.jdg[i].xdi),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].tim),&(_comuni->hu.jmp.jdg[i].tim),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].key),&(_comuni->hu.jmp.jdg[i].key),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].pid),&(_comuni->hu.jmp.jdg[i].pid),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].cnt),&(_comuni->hu.jmp.jdg[i].cnt),1)))		return st;
		}
	}

	return pam_get_err(armno);

}

/******************************************************************
	JUMP��������
		����	armno		������No.
			key			�����ǡ�����Key�ֹ�
			id			������ID�ֹ�
			jmp			JUMP����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_set_jmp(ARM armno, u32 key, u32 id, JUMPP jmp)
{
	u32	ldmy;
	u32	i;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_SET_JMP;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	key;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	id;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.jmp.cid),&(jmp->cid),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	for(i=0;i<8;i++){
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.jmp.jdg[i].cnd[0]),&(jmp->jdg[i].cnd[0]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.jmp.jdg[i].cnd[1]),&(jmp->jdg[i].cnd[1]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.jmp.jdg[i].xdi),&(jmp->jdg[i].xdi),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.jmp.jdg[i].tim),&(jmp->jdg[i].tim),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.jmp.jdg[i].key),&(jmp->jdg[i].key),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.jmp.jdg[i].pid),&(jmp->jdg[i].pid),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.jmp.jdg[i].cnt),&(jmp->jdg[i].cnt),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
	
}


/******************************************************************
	�ֹ�����JUMP�������
		����	armno		������No.
			key			�����ǡ�����Key�ֹ�
			num			�ǡ����ֹ�
			jmp			JUMP����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_jmp_set(ARM armno, u32 key, u32 num, JUMPP jmp)
{
	u32	ldmy;
	u32	i;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_JMP_SET;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	key;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	num;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);

	if( ERR_OK==(st=pam_get_err(armno)) ){
		pa_map_ctl(armno);
		if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->cid),&(_comuni->hu.jmp.cid),1)))	return st;
		for(i=0;i<8;i++){
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].cnd[0]),&(_comuni->hu.jmp.jdg[i].cnd[0]),1)))	return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].cnd[1]),&(_comuni->hu.jmp.jdg[i].cnd[1]),1)))	return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].xdi),&(_comuni->hu.jmp.jdg[i].xdi),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].tim),&(_comuni->hu.jmp.jdg[i].tim),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].key),&(_comuni->hu.jmp.jdg[i].key),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].pid),&(_comuni->hu.jmp.jdg[i].pid),1)))		return st;
			if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(jmp->jdg[i].cnt),&(_comuni->hu.jmp.jdg[i].cnt),1)))		return st;
		}
	}
	
	return pam_get_err(armno);
		
}

/******************************************************************
	JUMP����ͭ��/̵������
		����	armno		������No.
			stat		0:̵��
					1:ͭ��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_ena_jmp(ARM armno, u32 stat)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_ENA_JMP;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	stat;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	JUMP����ͭ��/̵������
		����	armno		������No.
			*stat		0:̵��
					1:ͭ��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_get_ena(ARM armno, u32* stat)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,stat,&(_comuni->hu.sts.lvl[7]),1)))	return st;

	return pam_get_err(armno);
}

/******************************************************************
	JUMP������
		����	armno		������No.
			key			Key�ֹ�
			jnm			JUMP�ֹ�

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_del_jmp(ARM armno, u32 key, u32 jnm)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_DEL_JMP;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	key;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	jnm;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
