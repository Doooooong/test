/*


	PAlib direct control function


*/
#include	<stdio.h>
#include	<stdlib.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/******************************************************************
	�������
		����	armno		������No.
			axis		������

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_wet_ded(ARM armno, AXIS axis)
{
	u32	ldmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	if(axsnum==6)	axis=axis-S3;

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	DeadWeightEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	axis;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	��ή���ɤ߹���
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_get_amp(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	ReadCurrentEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/*******************************************************************
	�����쥯�ȥ���-��/�����쥯�ȥ��ȥå�
		����	armno		��-��No.
			dm		

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_DM		DIRECTMODE���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
*********************************************************************/
DllExport u32 __stdcall pa_mod_dir(ARM armno, DIRECTMODE dm)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	switch(dm){
	case DM_START:
		ldmy = (u32)	DirectStartEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
		break;
	case DM_STOP:
		ldmy = (u32)	DirectStopEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return(st);
		}
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_DM;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	���������-��ο����դ�����
		����	armno		������No.
			vec		��-��ο����դ�����
					 ARM_STANDING  ���֤�����
					 ARM_HANGING   ŷ�ߤ����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 __stdcall pa_drt_ded(ARM armno, u32 vec)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	ldmy = (u32)	SetDirectionEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}
	if(vec!=ARM_HANGING)	vec=ARM_STANDING;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&vec,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return(st);
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

