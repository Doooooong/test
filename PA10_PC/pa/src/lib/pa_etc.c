/*

	PAlib other function (pa_etc.c)

*/
#include	<stdio.h>
#include	<stdlib.h>
#include	<fcntl.h>


#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

/**********************************************************
	��-���
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
***********************************************************/
DllExport u32 __stdcall pa_rst_ctl(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);										//S2-W2���ֵ�Υ������
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	ErrorResetEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

#define	BR	break

/******************************************************************
	���顼��å��������������
		����	err			���顼No.
			buf			��å������򥳥ԡ�����Хåե�

		����	ERR_MES 	�������顼��å��������ʤ���
			ERR_OK		OK
******************************************************************/
DllExport u32 __stdcall pa_err_mes(u32 err, char *buf)
{
	char	*str;
//
//	��˥�
//
	switch(err){
	case ERR_OK:		str=" ";BR;

	/*ư��顼(PAlib���Ф���)*/
	case ERR_FILE:		str="-1 ���ꤵ�줿�ե�����Ϥ���ޤ���";BR;
	case ERR_READ:		str="-2 �ե������ɤ߹��߼���";BR;
	case ERR_WRITE:		str="-3 �ե�����񤭹��߼���";BR;
	case ERR_INT:		str="-4 486�س����߼���";BR;
	case ERR_OPEN:		str="-5 pa_opn_arm()����Ƥ��ޤ���";BR;
	case ERR_MALLOC:	str="-6 ������ݤ˼��Ԥ��ޤ���";BR;
	case ERR_PRM:		str="-7	������ϥѥ�᡼�����ѹ��ϤǤ��ޤ���";BR;
	case ERR_PNT:		str="-8 �������ǡ����λ�����٤��ѥ�᡼���ϰϤ�ۤ��Ƥ��ޤ�";BR;

	/*�������顼(PAlib���Ф���)*/
	case ERR_ARM:	str="-20 ���ꤵ�줿������Ϥ���ޤ���";BR;
	case ERR_AXIS:	str="-21 ���ꤵ�줿���Ϥ���ޤ���";BR;
	case ERR_DRV:	str="-22 ���ꤵ�줿�ɥ饤�ФϤ���ޤ���";BR;
	case ERR_PB:	str="-23 �ץ쥤�Х�ư����̤��ְ�äƤ��ޤ�";BR;
	case ERR_PD:	str="-24 ��������������̤��ְ�äƤ��ޤ�";BR;
	case ERR_PA:	str="-25 ������°���ѹ����̤��ְ�äƤ��ޤ�";BR;
	case ERR_PTN:	str="-26 ������®�٥ѥ�-��°���ͤ��ְ�äƤ��ޤ�";BR;
	case ERR_PT:	str="-27 �������ǡ��������׼��̤��ְ�äƤ��ޤ�";BR;
	case ERR_PM:	str="-28 �����������̤��ְ�äƤ��ޤ�";BR;
	case ERR_VT:	str="-29 �ǥե����®���ѹ����̤��ְ�äƤ��ޤ�";BR;
	case ERR_VM:	str="-30 ®������⡼�ɼ��̤��ְ�äƤ��ޤ�";BR;
	case ERR_JM:	str="-31 ��Ĺ������⡼�ɼ��̤��ְ�äƤ��ޤ�";BR;
	case ERR_JT:	str="-32 ��Ĺ�������̤��ְ�äƤ��ޤ�";BR;
	case ERR_MM:	str="-33 ��ɸ��ü��������⡼�ɼ��̤��ְ�äƤ��ޤ�";BR;
	case ERR_DM:	str="-34 �����쥯��������̤��ְ�äƤ��ޤ�";BR;
	case ERR_DP:	str="-35 �ǥ������������ϥݡ��Ȼ��꤬�ְ�äƤ��ޤ�";BR;
	case ERR_DC:	str="-36 �ǥ������������ϥ��ͥ���꤬�ְ�äƤ��ޤ�";BR;
	case ERR_MES:	str="-37 ����-��-�ɤ��������Ƥ��ޤ���";BR;
	case ERR_BOARD:	str="-38 �ǥ������������ϥܡ��ɻ��꤬�ְ�äƤ��ޤ�";BR;
	case ERR_DIO:	str="-39 �ǥ�������������DIorDO���꤬�ְ�äƤ��ޤ�";BR;
	case ERR_PRJ:	str="-40 �ץ��������Ȥ������ɤ���Ƥ��ޤ���";BR;

	/*WinRT���顼(PAlib���Ф���)*/
	case ERR_UNMAPMEMORY:	str="-100 WinRTUnMapMemory�ǥ��顼��ȯ�����ޤ���";BR;
	case ERR_UNMAPMEMORY2:	str="-101 WinRTUnMapMemory2�ǥ��顼��ȯ�����ޤ���";BR;
	case ERR_OPENDEVICE:	str="-200 WinRTOpenNamedDevice�ǥ��顼��ȯ�����ޤ���";BR;
	case ERR_CONFIG:		str="-201 WinRTGetFullConfiguration�ǥ��顼��ȯ�����ޤ���";BR;
	case ERR_MAPMEMORY:		str="-300 WinRTMapMemory�ǥ��顼��ȯ�����ޤ���";BR;
	case ERR_MAPMEMORY2:	str="-301 WinRTMapMemory2�ǥ��顼��ȯ�����ޤ���";BR;

	/*����˥󥰥��顼*/

	case ERR_CANT_CPU:	str="-1000 ���楳��ȥ�����ؤΥ���������������ޤ���";BR;
	case ERR_NON_EVNT:	str="-1001 �ե����ޥåȤȥ��ޥ�ɤ��ޥå����ޤ���";BR; 
	case ERR_CANT_EVNT:	str="-1002 ���ߤΥ⡼�ɤǤ��б��Ǥ��ʤ����ޥ�ɤǤ�";BR;
	case ERR_INVALD_EVNT:str="-1003 ���ޥ�ɤ�̵�� ";BR;
	case ERR_NON_ARM:	str="-1004 �ؼ����줿�������ֹ椬����ޤ���";BR;
	case ERR_NON_ROB:	str="-1005 ROB�ե�����ο�������������ɤ�ɬ��";BR;
	case ERR_NON_TOL:	str="-1006 TOL�ե�����ο�������������ɤ�ɬ��";BR;

	case ERR_S1_VEL	:	str="-1010 �ӣ���®�٥��ݥ� ";BR;
	case ERR_S2_VEL	:	str="-1011 �ӣ���®�٥��ݥ� ";BR;
	case ERR_S3_VEL	:	str="-1012 �ӣ���®�٥��ݥ� ";BR;
	case ERR_E1_VEL	:	str="-1013 �ţ���®�٥��ݥ� ";BR;
	case ERR_E2_VEL	:	str="-1014 �ţ���®�٥��ݥ� ";BR;
	case ERR_W1_VEL	:	str="-1015 �ף���®�٥��ݥ� ";BR;
	case ERR_W2_VEL	:	str="-1016 �ף���®�٥��ݥ� ";BR;
	case ERR_XYZ_VEL:	str="-1018 ��ü����®�٥��ݥ� ";BR;
	case ERR_YPR_VEL:	str="-1019 ��ü����®�٥��ݥ� ";BR;

	case ERR_S1_SAGL:	str="-1020 �ӣ��� ���� ���٥��ݥ�";BR;
	case ERR_S2_SAGL:	str="-1021 �ӣ��� ���� ���٥��ݥ�";BR;
	case ERR_S3_SAGL:	str="-1022 �ӣ��� ���� ���٥��ݥ�";BR;
	case ERR_E1_SAGL:	str="-1023 �ţ��� ���� ���٥��ݥ�";BR;
	case ERR_E2_SAGL:	str="-1024 �ţ��� ���� ���٥��ݥ�";BR;
	case ERR_W1_SAGL:	str="-1025 �ף��� ���� ���٥��ݥ�";BR;
	case ERR_W2_SAGL:	str="-1026 �ף��� ���� ���٥��ݥ�";BR;

	case ERR_S1_TAGL:	str="-1030 �ӣ��� ��ɸ ���٥��ݥ�";BR;
	case ERR_S2_TAGL:	str="-1031 �ӣ��� ��ɸ ���٥��ݥ�";BR;
	case ERR_S3_TAGL:	str="-1032 �ӣ��� ��ɸ ���٥��ݥ�";BR;
	case ERR_E1_TAGL:	str="-1033 �ţ��� ��ɸ ���٥��ݥ�";BR;
	case ERR_E2_TAGL:	str="-1034 �ţ��� ��ɸ ���٥��ݥ�";BR;
	case ERR_W1_TAGL:	str="-1035 �ף��� ��ɸ ���٥��ݥ�";BR;
	case ERR_W2_TAGL:	str="-1036 �ף��� ��ɸ ���٥��ݥ�";BR;

	case ERR_NOA_CLC:	str="-1038 NOA�׻����Ǥ��ޤ���";BR;
	case ERR_LNK_CTL:	str="-1039 ������Ϣ³���ݸ�Τ��������Բ�ǽ";BR;
	case ERR_MEM_FULL:	str="-1040 ����γ��ݤ˼��� ";BR;
	case ERR_MIS_COMD:	str="-1041 ���Υ��ޥ�ɤ�ȯ�Ԥ������ˤϼ�³����ɬ�� ";BR;
	case ERR_PB_CIR:    str="-1042 �ߡ��߸̤λ��꤬���äƤ��ޤ�";BR;
	case ERR_PB_NEXT:   str="-1043 ���Υݥ��󥿤�¸�ߤ��ޤ���";BR;
	case ERR_PB_PRIV:   str="-1044 ���Υݥ��󥿤�¸�ߤ��ޤ���";BR;
	case ERR_PB_END:    str="-1045 �ץ쥤�Хå��ǡݥ��ν�λ";BR;
	case ERR_PB_NULL:   str="-1046 �ץ쥤�Хå��ǡݥ�������ޤ���";BR;
	case ERR_PB_REFER:  str="-1047 �ץ쥤�Хå��ǡݥ��θ����˼��Ԥ��ޤ���";BR;
	case ERR_PB_REPLACE:str="-1048 ��ץ쥤�����ޥ�ɤȲ�ᤷ�ޤ���";BR;
	case ERR_PB_PANIC:  str="-1049 �ݥ��󥿴�������";BR;

	case ERR_NOT_ENUGH: str="-1050 ��ɸ�ͤ������Բ�ǽ�ΰ�Ǥ��ʥ�����Ĺ��­��ޤ����";BR;
	case ERR_MIS_PARAM:	str="-1051 ����ѥ�᡼���ͤ������ϰϤ�ۤ��Ƥ��ޤ�";BR;
	case ERR_NOA_DAT:	str="-1060 ���ꤵ�줿�Σϣ�����Ŭ�ڤǤ�";BR;
	case ERR_PNT_ATR:	str="-1061 CP��-���κǸ��Ƽ�°���ǳ������ޤ���";BR;
	case ERR_PTP_DAT:	str="-1062 RMRC�β�ư�ϰϤ�ۤ��Ƥ��ޤ�";BR;
	case ERR_CP_LOGGING:str="-1063 CP��-��������ǻ��ѤǤ��ޤ���";BR;
	case ERR_FIFO_MAX:	str="-1064 ������ֿ��ۤ��ޤ�����      ";BR;
	case ERR_FIFO_ARC:	str="-1065 �ߡ��߸̤��������Ǥ��ޤ���  ";BR;

	case COVERS1:		str="-1070 �ӣ��� ®������ ���٥��ݥ�";BR;
	case COVERS2:		str="-1071 �ӣ��� ®������ ���٥��ݥ�";BR;
	case COVERS3:		str="-1072 �ӣ��� ®������ ���٥��ݥ�";BR;
	case COVERE1:		str="-1073 �ţ��� ®������ ���٥��ݥ�";BR;
	case COVERE2:		str="-1074 �ţ��� ®������ ���٥��ݥ�";BR;
	case COVERW1:		str="-1075 �ף��� ®������ ���٥��ݥ�";BR;
	case COVERW2:		str="-1076 �ף��� ®������ ���٥��ݥ�";BR;

	case ERR_MIS_VAL:   str="-1080 �����ͤ��礭�����뤫�ޤ��Ͼ��������ޤ�";BR;
	case ERR_PNT_APP:	str="-1081 �Ƽ�����Ǥϥ��ץ������Ǥ��ޤ���";BR;

	case ERR_PLY_FOR:	str="-1098 �����⡼�����Ϣ³��ž�ϤǤ��ޤ���";BR;
	case ERR_PLY_MOD:	str="-1099 �������Ƕ����⡼�ɤ��ڤ��ؤ��ޤ���";BR;
	case ERR_USE_TCH:	str="-1100 �����⡼�ɰʳ��ǥƥ��������å���ON�Ǥ��ޤ���";BR;
	case ERR_ACT_DAT:	str="-1101 ���ꤵ�줿Key�ζ����ǡ���������ޤ���";BR;
	case ERR_CHG_KEY:	str="-1103 �����ǡ�����Key�ѹ��Ǥ��ޤ���Ǥ���";BR;

	case ERR_CUB_NUM:	str="-1200 �����ΰ�����ֹ楨�顼";BR;
	case ERR_CUB_LEN:	str="-1201 ���Υ��塼�־���ˤ���Ĺ���λ��꤬�Ǥ��ޤ��� �̤Υ��塼��°������äƤ��ޤ�";BR;
	case ERR_CUB_MAX:	str="-1202 ���Υ��塼�־���ˤϾ���Ͷ����Ǥ��ޤ��� �̤Υ��塼��°������äƤ��ޤ�";BR;
	case ERR_CUB_MIN:	str="-1203 ���Υ��塼�־���ˤϲ����Ͷ����Ǥ��ޤ��� �̤Υ��塼��°������äƤ��ޤ�";BR;
	case ERR_CUB_CTR:	str="-1205 ���Υ��塼�־���ˤ��濴�Ͷ����Ǥ��ޤ��� �̤Υ��塼��°������äƤ��ޤ�";BR;
	case ERR_CUB_PRM:	str="-1206 ���塼������ѥ�᡼�������Ǥ�";BR;
	case ERR_CUB_SET:	str="-1207 ���Υ��塼�־���ˤ�����Ǥ��ޤ��� �̤Υ��塼��°������äƤ��ޤ�";BR;

	case ERR_PLY_KEY:	str="-1249 Key�������˻��ꤷ���ֹ椬���äƤ��ޤ�";BR;

	case ERR_NON_KEY:	str="-1250 Key�ǻ��ꤵ�줿�����ǡ�����˻��ꤵ�줿ID°���Ϥ���ޤ���";BR;
	case ERR_NON_CID:	str="-1251 ���ꤵ�줿��������JUMP�������äƤ��ޤ���";BR;
	case ERR_JMP_SET:	str="-1252 Key�ǻ��ꤵ�줿�����ǡ����Ϥ����ֹ��JUMP�������äƤ��ޤ���";BR;
	case ERR_NON_IDN:	str="-1253 ID°���ǻؼ����줿��������JUMP�������äƤ��ޤ���";BR;
	case ERR_JMP_NUM:	str="-1254 ������°���˻��ꤵ��Ƥ���JUMP�����ȯ���Ǥ��ޤ���";BR;
	case ERR_JMP_ATR:	str="-1255 JUMP���������������λ���ѥ�᡼���˸��꤬����ޤ�";BR;
	case ERR_KEY_ATR:	str="-1256 JUMP���������������λ���ѥ�᡼���˸��꤬����ޤ�";BR;

	case ERR_SOC_TST:	str="-1300 �����åȤ������˼���";BR;
	case ERR_BND_TST:	str="-1311 �����åȤȥ��ɥ쥹�ΥХ���ɤ˼���";BR;
	case ERR_LSN_TST:	str="-1312 ��å���˼���";BR;
	case ERR_APT_TST:	str="-1313 �������ץȤ˼���";BR;
	case ERR_SOC_SND:	str="-1314 �����å���������";BR;
	case ERR_SOC_BLK:	str="-1315 ̤����";BR;
	case ERR_SOC_CLT:	str="-1316 ��³���饤����Ȥ�¿�����ޤ�";BR;

	case ERR_PRM_DEV:	str="-1350 �ѥ�᡼����ư��®�٤�®�٥�ߥå��ͤ�Ķ���Ƥ��ޤ����ѥ�᡼���ѹ���̵���Ǥ�";BR;

	/*����ž��³��ǽ���顼���ݡݡ䡡�ʥ֥졼����߾��֡�*/

	case ERR_OVER900:	str="-2017 ư����˥�-��Ĺ��RMRCư��³�Ĺ�򥪡��С����ޤ���";BR;

	case ERR_S1_AGL:	str="-2020 �ӣ������٥��ݥ�";BR;
	case ERR_S2_AGL:	str="-2021 �ӣ������٥��ݥ�";BR;
	case ERR_S3_AGL:	str="-2022 �ӣ������٥��ݥ�";BR;
	case ERR_E1_AGL:	str="-2023 �ţ������٥��ݥ�";BR;
	case ERR_E2_AGL:	str="-2024 �ţ������٥��ݥ�";BR;
	case ERR_W1_AGL:	str="-2025 �ף������٥��ݥ�";BR;
	case ERR_W2_AGL:	str="-2026 �ף������٥��ݥ�";BR;

	case DOVERS1:		str="-2030 �ӣ��� �����쥯������ ���٥��ݥ�";BR;
	case DOVERS2:		str="-2031 �ӣ��� �����쥯������ ���٥��ݥ�";BR;
	case DOVERS3:		str="-2032 �ӣ��� �����쥯������ ���٥��ݥ�";BR;
	case DOVERE1:		str="-2033 �ţ��� �����쥯������ ���٥��ݥ�";BR;
	case DOVERE2:		str="-2034 �ţ��� �����쥯������ ���٥��ݥ�";BR;
	case DOVERW1:		str="-2035 �ף��� �����쥯������ ���٥��ݥ�";BR;
	case DOVERW2:		str="-2036 �ף��� �����쥯������ ���٥��ݥ�";BR;

	case ERR_CANT_MOVE:	str="-2051 ���߰��֤Ǥ�RMRC���������ޤ���";BR;

	case ERR_S1_REZ:	str="-2060 �ӣ��쥾����к��۾�";BR;
	case ERR_S2_REZ:	str="-2061 �ӣ��쥾����к��۾�";BR;
	case ERR_S3_REZ:	str="-2062 �ӣ��쥾����к��۾�";BR;
	case ERR_E1_REZ:	str="-2063 �ţ��쥾����к��۾�";BR;
	case ERR_E2_REZ:	str="-2064 �ţ��쥾����к��۾�";BR;
	case ERR_W1_REZ:	str="-2065 �ף��쥾����к��۾�";BR;
	case ERR_W2_REZ:	str="-2066 �ף��쥾����к��۾�";BR;
/*
	������쥾����к��۾�Ȥϡ��������Ϥ����쥾����ͤȺ������Ϥ����ͤ�����
����	�������ϰϰʾ�ˤʤä���硣���ɤߤ��ܤ��ε��������
*/
	case ERR_TIMEOUT:	str="-2070 �ƻ���֥�-�ФǼ�ư��ߤ��ޤ���";BR;
	case ERR_SYNCOUT:	str="-2071 ��ɸ�ͤޤ���ã���ޤ���Ǥ���";BR;

	case ERR_SYNC_S1:	str="-2080 �Ƽ�����ˤ�����ӣ���Ʊ���۾�";BR;
	case ERR_SYNC_S2:	str="-2081 �Ƽ�����ˤ�����ӣ���Ʊ���۾�";BR;
	case ERR_SYNC_S3:	str="-2082 �Ƽ�����ˤ�����ӣ���Ʊ���۾�";BR;
	case ERR_SYNC_E1:	str="-2083 �Ƽ�����ˤ�����ţ���Ʊ���۾�";BR;
	case ERR_SYNC_E2:	str="-2084 �Ƽ�����ˤ�����ţ���Ʊ���۾�";BR;
	case ERR_SYNC_W1:	str="-2085 �Ƽ�����ˤ�����ף���Ʊ���۾�";BR;
	case ERR_SYNC_W2:	str="-2086 �Ƽ�����ˤ�����ף���Ʊ���۾�";BR;
	case ERR_RMRC_X:	str="-2087 �ңͣң�����ˤ�����ؼ�Ʊ���۾�";BR;
	case ERR_RMRC_Y:	str="-2088 �ңͣң�����ˤ�����ټ�Ʊ���۾�";BR;
	case ERR_RMRC_Z:	str="-2089 �ңͣң�����ˤ�����ڼ�Ʊ���۾�";BR;
	case ERR_VELOCITY:	str="-2090 ®���к��۾�";BR;
	case ERR_RMRC_YPR:	str="-2091 �ңͣң�����ˤ������������к��۾�";BR;

	case ERR_CUB_INN:	str="-2100 ���塼�֤˴��Ĥ��ޤ���";BR;

	case ERR_ARM_ERR0:	str="-2200 �������ð�����ư������Ϸ�³�Ǥ��ޤ���";BR;
	case ERR_ARM_ERR1:	str="-2201 �������ð�����ư������Ϸ�³�Ǥ��ޤ���";BR;
	case ERR_ARM_ERR2:	str="-2202 �������ð�����ư������Ϸ�³�Ǥ��ޤ���";BR;

/*
	������Ʊ���۾�Ȥϡ���ɸ�ͤȸ����ͤ��к��������ϰϰʾ�ˤʤä���硣
	�������ʥ����बư���Ƥ��ʤ������Ϥ��ʤ��٤�Ƥ����
*/

/*����̿Ū���顼���ݡݡ䡡��������߾��֡�*/

	case ERR_POWER_ON:	str="-3000 ���椬���Ϥ���Ƥ��ޤ���";BR;
/*
	��������̿Ū���顼�������ä����,���泫�ϥ��ޥ�ɤ�ȯ�Ԥ�����,¾��
		  ���楳�ޥ�ɤ�ȯ�Ԥ���Ȥ��Υ��顼��ȯ�����롣
*/
	case ERR_EM_CTL:	str="-3001 �����ߤ�������Ƥ��ޤ�";BR;
	case ERR_ARC_SEND:	str="-3002 �������ͥå��̿��۾�";BR;
	case ERR_DRV_TYP:	str="-3005 �����ܥɥ饤�ФΥ����פ��ѥ�᡼������ȰۤʤäƤ��ޤ�";BR;
	case ERR_FORCE_ON:	str="-3010 ��������ǤϤ���ޤ���";BR;

	case ERR_DDD_STA:	str="-3070 �̿����祵���ܡʥޥ����˥��ơ������۾�";BR;
	case ERR_D11_STA:	str="-3071 �����ܥɥ饤�Сʣӣ��˥��ơ������۾�";BR;
	case ERR_D12_STA:	str="-3072 �����ܥɥ饤�Сʣӣ��˥��ơ������۾�";BR;
	case ERR_D21_STA:	str="-3073 �����ܥɥ饤�Сʣӣ��˥��ơ������۾�";BR;
	case ERR_D22_STA:	str="-3074 �����ܥɥ饤�Сʣţ��˥��ơ������۾�";BR;
	case ERR_D31_STA:	str="-3075 �����ܥɥ饤�Сʣţ��˥��ơ������۾�";BR;
	case ERR_D32_STA:	str="-3076 �����ܥɥ饤�Сʣף��˥��ơ������۾�";BR;
	case ERR_D41_STA:	str="-3077 �����ܥɥ饤�Сʣף��˥��ơ������۾�";BR;

	case ERR_S_SUSPD:	str="-3091 ������̿��˳��ϥ��ޥ���̿��۾�";BR;
	case ERR_E_SUSPD:	str="-3092 ������̿��˽�λ���ޥ���̿��۾�";BR;
	case ERR_I_SUSPD:	str="-3093 ��������ޥ���̿��۾�";BR;

	case ERR_MOD_CTL:	str="-4000 �⡼�ɴ����۾�";BR;
/*
	���������楳�ޥ��ȯ�Ի��۾�Ȥϡ������ܥɥ饤�Ф��Ф����ޥ�ɤ�ȯ�Ը�
	�������������ֿ��ˤ�����������̵���ä���硣
	�������ʥ����ܥɥ饤�Ф��۾�Ǥ��롣��
*/
/*�����ƥ२�顼*/
	case DRV_MEM_ERR:	str="1 ��ͭ����۾�";BR;
	case EEP_ROM_ERR:	str="2 EEPROM�۾�";BR;
	case CPU_XXX_ERR:	str="3 CPU�۾�";BR;
	case ARC_NET_ERR:	str="4 �̿������۾�";BR;
	case VEL_SPN_ERR:	str="5 ®���к��۾�";BR;
	case REZ_SPN_ERR:	str="6 �쥾����к��۾�";BR;
	case VEL_LIM_ERR:	str="7 ���֥�ߥåȰ۾�";BR;
	case MTR_TRQ_ERR:	str="8 �⡼���ȥ륯�۾�";BR;
	case IPM_XXX_ERR:	str="9 IPM�۾�";BR;
	case BRK_XXX_ERR:	str="10 �֥졼������";BR;
	case REZ_001_ERR:	str="11 �쥾��Сʥ���������";BR;
	case REZ_002_ERR:	str="12 �쥾��Сʥ⡼��������";BR;
	case OVR_TRQ_ERR:	str="13 ����ή";BR;
	case OVR_VEL_ERR:	str="14 ��®��";BR;
	case DMS_XXX_ERR:	str="15 �ǥåɥޥ�SW�۾�";BR;
	case CPU_NON_ERR:	str="16 ¾CPU�۾�";BR;

/* LAN�ط��Υ��顼 */
	case -5001:			str="�����åȺ������顼";		BR;
	case -5002:			str="�����å���³���顼";		BR;
	case -5003:			str="gethostbyname()���顼";	BR;
	case -5004:			str="�����å��������顼";		BR;
	case -5005:			str="�����å�Wait Failed";		BR;
	case -5006:			str="�����å�Wait timeout";		BR;
	case -5007:			str="�����å�error event";		BR;
	case -5008:			str="�����å�����";				BR;
	case -5009:			str="�����åȼ������顼";		BR;
	case -5010:			str="�����åȥǡ����۾�";		BR;
	case -5011:			str="�����åȥǡ���Ϣ�֥��顼";	BR;
    case -5012:			str="�����å�̤��³";			BR;


	default:
		str=NULL;
		sprintf(buf,"%ld ����Ͽ����Ƥ��ޤ���.",err);
	}
	if( str ){
		strcpy(buf,str);
		return ERR_OK;
	}else
		return ERR_MES;
}
