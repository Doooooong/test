/*

	PAlib support function (pa_ext.c)

*/
#include	<stdio.h>
#include	<stdlib.h>
#include	<fcntl.h>

#include	<pa.h>
#include	<pacmd.h>
#include	<pactl.h>
#include	<pammc.h>
#include	<paerr.h>

#ifndef M_PI
#define M_PI        3.14159265358979323846
#endif
#define	RadToDeg(x) ((x)*180.0/M_PI)

/*********************************************************************
	���Ҽ���-�����ߡ��ۥ�����쵥�����
		��armno		������No.

		���ť�	ERR_OK		�����������ۥ�
			ERR_ARM   	������No.���㥨�衼榥ơ��ˡ�����
			ERR_INT		�����ߤ���������ʤ��ä
			�ʾ��¾�����फ��Υ��顼������ޤ���
**********************************************************************/
DllExport u32  pa_sta_sim(ARM armno)
{
	u32	st;
	static	u32	ldmy;

	LockMutex((u32)armno);										//S2-W2���ֵ�Υ������
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	4;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	SimOnEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}						
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/*********************************************************************
	���ߥ��-���������潪λ
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�����फ��Υ��顼������ޤ���
*********************************************************************/
DllExport u32  pa_ext_sim(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);										//S2-W2���ֵ�Υ������
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	4;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	SimOffEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}					
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/*********************************************************************
	���������ڤӥ���-�ӥ󥰵�ƻ������Ԥ�.

	����	armno		������No.
		trans		���������䥦��-�ӥ󥰵�ƻID
				(MODE_WAVEH|MODE_WAVER|MODE_SENSH|MODE_SENSR)
				����(��ƻ)��-��

	����	ERR_OK		���ｪλ
		ERR_ARM		������No.���ְ�äƤ���.
		ERR_INT		�����ߤ���������ʤ��ä�.
		�ʾ��¾�����फ��Υ��顼������ޤ���
**********************************************************************/
DllExport u32  pa_odr_xyz(ARM armno,TRANSMATP trans)
{
	int	i;
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);										//S2-W2���ֵ�Υ������
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	8;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.trn.Enable),&(trans->Enable),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	for(i=0;i<3;i++){
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.trn._xyz[i]),	&(trans->_xyz[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.trn.Ixyz[i]),	&(trans->Ixyz[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.trn._XYZ[i]),	&(trans->_XYZ[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.trn.IXYZ[i]),	&(trans->IXYZ[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.trn._wave[i]),&(trans->_wave[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.trn.Iwave[i]),&(trans->Iwave[i]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}
	ldmy = (u32)	SendOffsetEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_sub(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_sub(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�������Σģϥǡݥ���ͭ����̵�������ꤷ�ޤ�.

	����	armno		������No.
		sw			ͭ����̵���ѥ��ݥ�

	����	ERR_OK		���ｪλ
		ERR_ARM		������No.���ְ�äƤ���.
		ERR_INT		�����ߤ���������ʤ��ä�.
		�ʾ��¾�����फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_swt_dio(ARM armno,u32 sw)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	8;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if(sw){	
		ldmy = (u32)	DioOnEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}else{
		ldmy = (u32)	DioOffEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
	}

	if( ERR_OK!=(st=pa_req_sub(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_sub(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	�������ʣУԣСˤΣģϥǡݥ�°�������ꤷ�ޤ�.

	����	armno		������No.
		dp			DO��-����°��

	����	ERR_OK		���ｪλ
		ERR_ARM		������No.���ְ�äƤ���.
		ERR_INT		�����ߤ���������ʤ��ä�.
		�ʾ��¾�����फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_chg_dio(ARM armno, DIOSTATUSP dp)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = ((u32)dp->io1|(u32)dp->io2<<8)|((u32)dp->io3<<16)|((u32)dp->io4<<24);
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	SetDIOEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	��������°�������ꤹ�롣
		����	armno		������No.
			pa			�ѹ�°�������
			dat			�ѹ�°����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			ERR_PA		PntSetID���ְ㤤��
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_set_idn(ARM armno, PNTID pa, u32 dat)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	switch(pa){
	case PA_SETID:
		ldmy = (u32)	SetIDEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		break;
	default:
		UnlockMutex((u32)armno);
		return ERR_PA;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&dat,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	��ɸ�Ѵ����������
		����	armno		������No.
			mtx0		����
			mtx1		����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_set_mat(ARM armno, MATRIX mtx0, MATRIX mtx1)
{
	short	j,k;
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	SendMatrixEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	6;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	for(j=0;j<3;j++){
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[0].xyz[j]),&(mtx0[j][3]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.wrk[1].xyz[j]),&(mtx1[j][3]),2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		for(k=0;k<3;k++){
			st=pa_cpy_flt((u32)armno,&(_comuni->hd.wrk[0].noa[j][k]),&(mtx0[j][k]),2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
			st=pa_cpy_flt((u32)armno,&(_comuni->hd.wrk[1].noa[j][k]),&(mtx1[j][k]),2);
			if(st<0){
				UnlockMutex((u32)armno);
				return st;
			}
		}
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	RMRC �ӣ������г��ٻ���ˤ���Ĺ������ư��
		����	armno		������No.
			s3			�ӣ����������
			wfp			ư���λ�ޤ��ԤĤ��ɤ����λ���

		����	ERR_OK		���ｪλ
			ERR_ARM   	������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mov_jou(ARM armno, float s3, u32 wfp)
{
	u32	ldmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if(axsnum==6){
		UnlockMutex((u32)armno);
		return ERR_OK;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0x04;							
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.agl[2]),&s3,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	RMRCStartEvent14;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	pam_exe_wfp(armno,wfp);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	�Ƽ������͸�Ǥ����ݥ�
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_mod_axs(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0x7f;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	EachAngleEvent2;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�Ƽ������͸�Ǥ���⡼������
		����	armno		������No.
			an		ANGLE���ؤΥݥ���

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_odr_axs(ARM armno, ANGLEP an)
{
	u32	ldmy;
	u32	axsnum;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(axsnum),&(_comuni->hu.ard.axs),1);			//	�����༴������
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	SendANGLEEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	7;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[0]),&(an->s1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[1]),&(an->s2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if(axsnum==6)	an->s3=0.0f;
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[2]),&(an->s3),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[3]),&(an->e1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[4]),&(an->e2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[5]),&(an->w1),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.mat.agl[6]),&(an->w2),2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�����쥯�����桤(�Ƽ�®��������)��Ʊ������
		����	armno		������No.

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_chk_cnt(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	ResetTimeOutEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	Ʊ�������Υ����ॢ���Ȼ��֤�����
		����	armno		������No.
			tim		Ʊ�������Υ����ॢ���Ȼ���[*10msec]

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_set_tim(ARM armno, u32 tim)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&tim,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	SetTimeOutEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	���ե��å�­�����ߤΥ�ߥå��к�
		����	armno		������No.
			data		���ե��å�­�����ߤΥ�ߥå��к�[mm]

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_lmt_xyz(ARM armno, float data)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	SetOffsetVelEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.fvl),&data,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�ץ쥤�Хå���DO����ܥåȤ�ư���Ʊ�����Ƽ�ưŪ����ߤ��뤫�ݤ�������
		����	armno		��-��No.
			data		0 : ��ư��ߤ�ͭ��
					1 :           ̵��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_set_dlc(ARM armno, u32 data)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.lvl),&data,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	SetDIOLockEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	���ߥ�졼�������Ψ����
		����	armno		��-��No.
			tim			���ߥ�졼�������Ψ

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_set_sim(ARM armno, u32 tim)
{
	u32	ldmy;
	float	fdmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	SetSimTimeEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	fdmy=(float)tim;
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.fvl),&fdmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	�ꥢ�륿����®������
		����	armno		��-��No.
			inc			�ꥢ�륿����®��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_set_inc(ARM armno, float inc)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	1;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	SetDefIncEvent;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_flt(	(u32)armno,&(_comuni->hd.dwn.fvl),&inc,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_sub(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_sub(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	���ߥ�졼�������Ψ����
		����	armno		��-��No.
			*sim		���ߥ�졼�������Ψ

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_sim(ARM armno, u32* sim)
{
	float	fdmy;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,&fdmy,&(_comuni->hu.get.fvl[1]),1)))	return st;
	*sim=(u32)fdmy;

	return pam_get_err(armno);
}
/******************************************************************
	�ꥢ�륿����®�ټ���
		����	armno		��-��No.
			*inc		�ꥢ�륿����®��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_inc(ARM armno, float* inc)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_flt((u32)armno,inc,&(_comuni->hu.get.fvl[3]),1)))	return st;
	return pam_get_err(armno);
}
/******************************************************************
	���ܥåȥ�ǥ�ե����������
		����	armno		��-��No.
			*file		���ܥåȥ�ǥ�ե�����̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_lod_rob(ARM armno,char *file)
{
	int		fd;
	u32	i,j,flag=1,ldmy;
	u32	all,cnt=0,blk=0,num=0;
	char*	cdat;
	BSE		p;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	cdat=(char*)malloc(sizeof(char)*10000+1);
	if(cdat==NULL){
		UnlockMutex((u32)armno);
		return ERR_MALLOC;
	}

	fd=open(file,O_RDONLY,0666);
	num=read(fd,cdat,10000);
	cdat[num+1]=(char)'\0';
	close(fd);
	all=num/256+1;

	while(flag){
		ldmy=(u32)5;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy=(u32)EV_LOD_ROB;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy=(u32)MOD_ROBFILE;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy=(u32)all;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy=(u32)++cnt;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		if(num>256) blk=256;
		else blk=num;
		ldmy=(u32)blk;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[3]),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		num-=256;

		for(i=(cnt-1)*256,j=0; i<(cnt*256) && cdat[i]!='\0'; i++,j++){
			p.pkt[j]=cdat[i];
		}
		st=pa_mem_blk((u32)armno,&(_comuni->hd.bse.pkt[0]),&(p.pkt[0]),(size_t)j,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}

		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		while( pa_fsh_chk(armno) );
		if(all==cnt) break;
	}

	free(cdat);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	�ġ����ǥ�ե����������
		����	armno		��-��No.
			*file		�ġ����ǥ�ե�����̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_lod_tol(ARM armno,char *file)
{
	int		fd;
	u32	i,j,flag=1,ldmy;
	u32	all,cnt=0,blk=0,num=0;
	char*	cdat;
	BSE		p;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	cdat=(char*)malloc(sizeof(char)*10000+1);
	if(cdat==NULL){
		UnlockMutex((u32)armno);
		return ERR_MALLOC;
	}

	fd=open(file,O_RDONLY,0666);
	num=read(fd,cdat,10000);
	cdat[num+1]=(char)'\0';
	close(fd);
	all=num/256+1;

	while(flag){
		ldmy=(u32)5;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy=(u32)EV_LOD_ROB;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy=(u32)MOD_TOLFILE;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy=(u32)all;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[1]),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy=(u32)++cnt;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[2]),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		if(num>256) blk=256;
		else blk=num;
		ldmy=(u32)blk;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[3]),&ldmy,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		num-=256;

		for(i=(cnt-1)*256,j=0; i<(cnt*256) && cdat[i]!='\0'; i++,j++){
			p.pkt[j]=cdat[i];
		}
		st=pa_mem_blk((u32)armno,&(_comuni->hd.bse.pkt[0]),&(p.pkt[0]),(size_t)i,2);
		if(st<0){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}

		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			free(cdat);
			UnlockMutex((u32)armno);
			return st;
		}
		while( pa_fsh_chk(armno) );
		if(all==cnt) break;
	}

	free(cdat);
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	���ܥåȥ�ǥ�ե����륻����
		����	armno		��-��No.
			*file		���ܥåȥ�ǥ�ե�����̾

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_sav_rob(ARM armno)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy=(u32)5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy=(u32)EV_SAV_ROB;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );

	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}
/******************************************************************
	RETRAC�黻����
		����	armno		��-��No.
			sw		0:T����黻
					1:RETRAC�黻

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32  pa_ena_nom(ARM armno,u32 sw)
{
	u32	ldmy;
	u32	st;


	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy=(u32)0;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy=(u32)EV_ENA_NOM;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy=(u32)sw;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );

	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	T����黻��RETRAC�黻�Τɤ����ԤäƤ��뤫���������
		����	armno		������No.
			nom		0:T����黻��  
						1:RETRAC�黻��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_nom(ARM armno, u32* nom)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,nom,&(_comuni->hu.get.lvl[5]),1)))	return st;
	return pam_get_err(armno);
}

/**************************************************************************
	�Ƽ�������ON/OFF���ּ���
		����	armno		������No.
			sav		���ƥ�����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
*************************************************************************/
DllExport u32  pa_get_sav(ARM armno, u32* sav)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,sav,&(_comuni->hu.rio.odt.sav),1)))	return st;
	return pam_get_err(armno);
}

/**************************************************************************
	�Ƽ������ܥ��ƥ���������
		����	armno		������No.
			sts		���ƥ�����

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
*************************************************************************/
DllExport u32  pa_sav_sts(ARM armno, u32* sts)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;

	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(sts[0]),&(_comuni->hu.err.drv[0]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(sts[1]),&(_comuni->hu.err.drv[1]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(sts[2]),&(_comuni->hu.err.drv[2]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(sts[3]),&(_comuni->hu.err.drv[3]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(sts[4]),&(_comuni->hu.err.drv[4]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(sts[5]),&(_comuni->hu.err.drv[5]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(sts[6]),&(_comuni->hu.err.drv[6]),1)))	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&(sts[7]),&(_comuni->hu.err.drv[7]),1)))	return st;

	return pam_get_err(armno);
}

/**************************************************************************
	RETRAC�ѥ�᡼�������⡼��ON/OFF����
		����	armno		������No.
			sw			0:OFF
							1:ON

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_INT		�����ߤ���������ʤ��ä�.
			�ʾ��¾�������फ��Υ��顼������ޤ���
*************************************************************************/
DllExport u32  pa_tst_nom(ARM armno, u32 sw)
{
	u32	ldmy;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_TST_NOM;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	0;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	sw;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.dwn.axs),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}


	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/**************************************************************************
	RETRAC�ѥ�᡼�������⡼��ON/OFF����
		����	armno		������No.
			*sw		0:OFF
					1:ON

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
*************************************************************************/
DllExport u32  pa_get_rmd(ARM armno, u32* sw)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,sw,&(_comuni->hu.get.lvl[9]),1)))		return st;
	return pam_get_err(armno);
}

/******************************************************************
	RETRAC�黻����ǽ���ɤ������������
		����	armno		������No.
			nom		0:�Բ�ǽ 
						1:��ǽ

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_tkn_nom(ARM armno, u32* nom)
{
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,nom,&(_comuni->hu.get.lvl[6]),1)))	return st;
	return pam_get_err(armno);
}

/******************************************************************
		����	armno		������No.
			snd			 
			rcv						

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			ERR_PRM		������ϥѥ�᡼�����ѹ��ϤǤ��ʤ�
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_ini_drv(ARM armno, char* snd, char* rcv)
{
	u32	ldmy;
	u32	stat;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}

	st=pa_cpy_lng(	(u32)armno,&stat,&(_comuni->hu.get.lvl[7]),1);	//��-�����
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if(stat==0){
		ldmy = (u32)	InitDSPEvent;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		ldmy = (u32)	4;
		st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}
		st=pa_mem_blk( (u32)armno,&(_comuni->hd.bse.pkt[0]),snd,256,2);
		if(st<0){
			UnlockMutex((u32)armno);
			return st;
		}

		if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
			UnlockMutex((u32)armno);
			return st;
		}

		while( pa_fsh_chk(armno) );
		UnlockMutex((u32)armno);

		if( ERR_OK==(st=pam_get_err(armno)) ){
			pa_map_ctl(armno);

			if(ERR_OK!=(st=pa_mem_blk( (u32)armno,rcv,&(_comuni->hu.bse.pkt[0]),256,1)))	return st;
		}
	}else{
		UnlockMutex((u32)armno);
		return	ERR_PRM;
	}

	return pam_get_err(armno);
}

/******************************************************************
	�ǥåɥޥ󥹥��å���ͭ��/̵��
		����	armno		������No.
			type		�����å��μ���	 
			val		1:ͭ��	
					0:̵��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_set_ddm(ARM armno, u32 type, u32 val)
{
	u32	ldmy;
	u32	bit;
	u32	st;

	LockMutex((u32)armno);
	if( ERR_OK!=(st=pa_map_ctl(armno)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	st=pa_cpy_lng(	(u32)armno,&bit,&(_comuni->hu.get.lvl[10]),1);	//�����å��ξ���
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	ldmy = (u32)	EV_SET_DDM;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.evt),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	ldmy = (u32)	5;
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.com),&ldmy,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}
	if(val==1){
		bit|=(1<<type);
	}else{
		bit&=~(1<<type);
	}
	st=pa_cpy_lng(	(u32)armno,&(_comuni->hd.hed.dmy[0]),&bit,2);
	if(st<0){
		UnlockMutex((u32)armno);
		return st;
	}

	if( ERR_OK!=(st=pa_req_ctl(armno,RETRY_COUNT)) ){
		UnlockMutex((u32)armno);
		return st;
	}
	while( pa_fsh_chk(armno) );
	UnlockMutex((u32)armno);
	return pam_get_err(armno);
}

/******************************************************************
	�ǥåɥޥ󥹥��å���ͭ��/̵�����ּ���
		����	armno		������No.
			type		�����å��μ���	 
			val		1:ͭ��	
					0:̵��

		����	ERR_OK		���ｪλ
			ERR_ARM		������No.���ְ�äƤ���.
			�ʾ��¾�������फ��Υ��顼������ޤ���
******************************************************************/
DllExport u32 	pa_get_ddm(ARM armno, u32 type, u32* val)
{
	u32	bit;
	u32	st;

	if( ERR_OK!=(st=pa_map_ctl(armno)) )	return st;
	if(ERR_OK!=(st=pa_cpy_lng((u32)armno,&bit,&(_comuni->hu.get.lvl[10]),1)))	return st;	//�����å��ξ���
	*val=((unsigned char)bit>>type);

	return pam_get_err(armno);
}
