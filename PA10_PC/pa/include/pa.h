/*
 *	pa.h -- Definitions for Parsonal Arm PA10
 *
 *	Copyright (c) Mitsubisgi Heavy Industries, 1993
 *
 */

#ifndef _PA_H_
#define _PA_H_

#define	_PA_	10

#include <stdint.h>
/* 
 * 	�ü�ʰ�̣����ĥǡ��������� 
 */
typedef float 	MATRIX[3][4];	/* ����					*/
typedef float 	NOAMAT[3][3];	/* noa����				*/
typedef float 	VECTOR[3];	/* �٥��ȥ�				*/
typedef uint32_t u32;
/*
 * �ǡ��������ե����ޥå��ֹ�
 */
#define	COM_FMT00	0
#define	COM_FMT01	1
#define	COM_FMT02	2
#define	COM_FMT03	3
#define	COM_FMT04	4
#define	COM_FMT05	5
#define	COM_FMT06	6
#define	COM_FMT07	7
#define	COM_FMT08	8
#define	COM_FMT09	9
#define	COM_FMT10	10
#define	COM_FMT11	11

/* 
 *	���������
 */
typedef u32	ARM;
#define	ARM0	(ARM)0
#define	ARM1	(ARM)1
#define	ARM2	(ARM)2
#define	ARM3	(ARM)3
#define	ARM4	(ARM)4
#define	ARM5	(ARM)5
#define	ARM6	(ARM)6
#define	ARM7	(ARM)7
#define	ARM8	(ARM)8
#define	ARM9	(ARM)9
#define	ARM10	(ARM)10
#define	ARM11	(ARM)11
#define	ARM12	(ARM)12
#define	ARM13	(ARM)13
#define	ARM14	(ARM)14
#define	ARM15	(ARM)15

/* 
 *	�ؿ��Υǡ���������
 */
#define	WM_WAIT		0
#define	WM_NOWAIT	1

/* 
 * �Ƽ��ǡ�����¤�� 
 */
typedef struct {
	float s1;
	float s2;
	float s3;
	float e1;
	float e2;
	float w1;
	float w2;
}ANGLE, *ANGLEP;


/* 
 * �����ǡ�����¤�� 
 */
typedef struct {
	float	agl[7];
	float	vel[2];
	u32	atr[12];
} PLYPNT, *PLYPNTP;

typedef struct {
	PLYPNT	pnt;
	char	cmt[32];
} PLAY, *PLAYP;

/*

*/
typedef struct {
	float	xyz[3];
	float	noa[3][3];
} NOAP, *NOAPP;

/*
	JUMP����¤��
*/
typedef struct {
	u32	cnd[2];
	u32	xdi;
	u32	tim;
	u32	key;
	u32	pid;
	u32	cnt;
}JUDGE, *JUDGEP;

typedef struct {
	u32	cid;
	JUDGE	jdg[8];
}JUMP, *JUMPP;

typedef struct {
	PLAY	ply;
	NOAP	noa;
	JUMP	jmp;
}PNTDAT, *PNTDATP;

/* 
 * ������Υ��ơ�������¤�� 
 */
typedef struct {
    	u32		max;		//'�ܡ��ɤ������ǽ�������   1or2
    	u32		arm;		//'���Ȥ������ܤˤ��뤫    0or1
	u32		axis;		//'�����༴��
    	u32	    typ;		//'�����ॿ����
    	u32		drv;		//'�����ܥɥ饤�м���
    	u32		dio;		//'��ĥDIO�ܡ���̵ͭ
	u32		remote;		//'���⡼�ɡ�ͭ����̵����
	u32		count;		//'���楫������
	u32		error;		//'���顼������
	ANGLE		angle;		//'���ߤμ���
	MATRIX		noap;		//'���ߤ���ü��������
	float		ypr[3];		//'���ߤλ���
}ARMSTATUS, *ARMSTATUSP;

/* 
 * �ǥ������롡�ɡ��Ϥι�¤��
 */
typedef struct {
	unsigned char		io1;	/*DIO 1ch*/
	unsigned char		io2;	/*DIO 2ch*/
	unsigned char		io3;	/*DIO 3ch*/
	unsigned char		io4;	/*DIO 4ch*/
}DIOSTATUS, *DIOSTATUSP;

/* 
 * �ѥ�᡼����¤��
 */
typedef struct {
	float	rezl;			/* �쥾���ʬ��ǽ		*/
	float	pul[7];			/* ���֥�ߥå��ʡܡ�		*/
	float	pdl[7];			/* ���֥�ߥå��ʡݡ�		*/
	float	vel[7 + 2];		/* ®�٥�ߥå�			*/
	float	dev[7 + 2];		/* �ǥե����®��		*/
	float	lim[7 + 2];		/* 				*/
	float	ceh[7 + 2];		/* 				*/
	float	cem[7 + 2];		/* 				*/
	float	cel[7 + 2];		/* 				*/
	float	pg1[7];			/* �������楲����		*/
	float	pg2[7];			/* �������楲����		*/
	float	vg1[7];			/* ®�����楲����		*/
	float	tg1[7];			/* �����楲����			*/
	float	pcm[7];			/* ���������������		*/
	float	fcm[7];			/* �������������  		*/
	float	arl[7];			/* ���ݥ�Ĺ			*/
	float	arg[7];			/* �Ƽ��ſ�����			*/
	float	arw[7];			/* �Ƽ�����			*/
	float	hom[7];			/* ���ܻ���������ɸ��		*/
	float	saf[7];			/* ����¾��������ɸ��		*/
	float	esc[7];			/* ������������ɸ��		*/
	float	tol[7];			/* ����ѥ��ݥ�		*/
	float	fvl[7];			/* 				*/
	u32	dmy[7];			/* �������ӥ󥰥ѥ�᡼��	*/
	u32	spa[7];			/* ͽ��				*/
} PARAM, *PARAMP;

/* 
 *	������
 */
typedef u32	AXIS;
#define	S1	(AXIS)0x01
#define	S2	(AXIS)0x02
#define	S3	(AXIS)0x04
#define	E1	(AXIS)0x08
#define	E2	(AXIS)0x10
#define	W1	(AXIS)0x20
#define	W2	(AXIS)0x40

#define	AXISALL		(S1|S2|S3|E1|E2|W1|W2)
#define	ALLAXIS		(S1|S2|S3|E1|E2|W1|W2)
#define	LOCKAXIS_S1	(S2|S3|E1|E2|W1|W2)
#define	LOCKAXIS_S3	(S1|S2|E1|E2|W1|W2)

/*
 *	�����ܥɥ饤�м���
 */
typedef u32	DRIVER;
#define	DRV1	(DRIVER)0
#define	DRV2	(DRIVER)1
#define	DRV3	(DRIVER)2
#define	DRV4	(DRIVER)3

/*	
 *	�ץ쥤�Хå��⡼��
 */
typedef u32	PLAYBACK;
#define	PB_FORES	(PLAYBACK)0
#define	PB_BACKS	(PLAYBACK)1
#define	PB_FORE		(PLAYBACK)2
#define	PB_BACK		(PLAYBACK)3

/*
 *	������������
 */
typedef u32	PNTDEL;
#define	PD_CUR		(PNTDEL)0x7500	/*���ݥ���Ȥζ��������	*/
#define	PD_FORE		(PNTDEL)0x7501	/*���ݥ���Ȥ����ζ��������	*/
#define	PD_ALL		(PNTDEL)0x7502	/*���ƤΥ����ƥ��ֶ������κ��	*/
#define	PD_ALLDATA	(PNTDEL)0	/*���Ƥζ������κ��		*/

/*
 *	������°������
 */
typedef u32	PNTATTR;
#define	PA_CHGVEL	(PNTATTR)0x7300
#define	PA_CHGWAIT	(PNTATTR)0x7301
#define	PA_VELPTN	(PNTATTR)0x7302	/* �����ǡ���®�٥ѥ�����	*/
#define	PA_ROTVEL	(PNTATTR)0x7303	/* �����ǡ�����ž®��		*/
#define	PA_AXSACC	(PNTATTR)0x7304	/* �Ƽ�����			*/
#define	PA_RMRCACC	(PNTATTR)0x7305	/* ľ������			*/
#define	PA_JUMPID	(PNTATTR)0x7306	/* JUMP����ֹ�			*/

/*
 *	������������
 */
typedef u32	PNTTYPE;
#define	PT_CP		(PNTTYPE)0x7100
#define	PT_PTP		(PNTTYPE)0x7101
#define	PT_BCP		(PNTTYPE)0x7102
#define	PT_BPTP		(PNTTYPE)0x7103
#define	PT_ARC1		(PNTTYPE)0x7104
#define	PT_ARC2		(PNTTYPE)0x7105
#define	PT_ARC3		(PNTTYPE)0x7106
#define	PT_CIR1		(PNTTYPE)0x7107
#define	PT_CIR2		(PNTTYPE)0x7108
#define	PT_CIR3		(PNTTYPE)0x7109
#define	PT_AXS		(PNTTYPE)0x710a
#define	PT_BAXS		(PNTTYPE)0x710b
#define	PT_POS		(PNTTYPE)0x710c
#define	PT_BPOS		(PNTTYPE)0x710d
#define	PT_ARC4		(PNTTYPE)0x710e
#define	PT_ARC5		(PNTTYPE)0x710f
#define	PT_ARC6		(PNTTYPE)0x7110
#define	PT_CIR4		(PNTTYPE)0x7111
#define	PT_CIR5		(PNTTYPE)0x7112
#define	PT_CIR6		(PNTTYPE)0x7113

/*
 *	�������Υݥ������
 */
typedef u32	PNTMOVE;
#define	PM_TOP	(PNTMOVE)0x7100	/* ��Ƭ��				*/
#define	PM_NEXT	(PNTMOVE)0x7101	/* ����					*/
#define	PM_PRIV	(PNTMOVE)0x7102	/* ����					*/
#define	PM_BTM	(PNTMOVE)0x7103	/* �Ǹ�����				*/
#define	PM_JMP	(PNTMOVE)0x7104	/* ���ꥢ�ɥ쥹��			*/
#define	PM_CIR	(PNTMOVE)0x7105	/* �߶������򸡺������ǽ��ȯ�������������� */
#define	PM_ARC	(PNTMOVE)0x7106	/* �߸̡�������"			    */

/*
 *	�ǥե����®���ѹ�����
 */
typedef u32	VELTYPE;
#define	VT_ONEVEL	(VELTYPE)0	/* �Ƽ��ǥե����®��		*/
#define	VT_XYZVEL	(VELTYPE)1	/* ��ü���֥ǥե����®���ѹ�	*/
#define	VT_YPRVEL	(VELTYPE)2	/* ��ü�����ǥե����®���ѹ�	*/

/*
 *	®������
 */
typedef u32	VELMODE;
#define	VM_XYZ		(VELMODE)0x200
#define	VM_YPR		(VELMODE)0x201
#define	VM_xyz		(VELMODE)0x202
#define	VM_ypr		(VELMODE)0x203
#define	VM_ONE		(VELMODE)0x204
#define	VM_XYZYPR	(VELMODE)0x205
#define	VM_xyzypr	(VELMODE)0x206

/*
 *	��Ĺ������⡼��
 */
typedef u32	JOUMODE;
#define	JM_SET		(JOUMODE)0X345
#define	JM_RESET	(JOUMODE)0X346
#define	JM_VSET		(JOUMODE)0X347
#define	JM_ON		(JOUMODE)0X348
#define	JM_OFF		(JOUMODE)0X349
#define	JM_S3ON		(JOUMODE)0X34a
#define	JM_S3DIV	(JOUMODE)0X34b
#define	JM_S3HOLD	(JOUMODE)0X34c

/*
 *	��Ĺ������⡼��
 */
typedef u32	JOUTYPE;
#define	JT_RIGHT	(JOUTYPE)1
#define	JT_HOLD		(JOUTYPE)0
#define	JT_LEFT		(JOUTYPE)-1

/*
 *	
 */
typedef u32	MOVEMODE;
#define	MM_XYZ		(MOVEMODE)0x5680
#define	MM_NOA		(MOVEMODE)0x5681
#define	MM_XYZNOA	(MOVEMODE)0x5682

/*
 *	
 */
typedef u32	DIRECTMODE;
#define	DM_STOP		(DIRECTMODE)0
#define	DM_START	(DIRECTMODE)1

#define	ARM_STANDING	 1	/* ���֤����� */
#define	ARM_HANGING	-1	/* ŷ�ߤ���� */

/*
 *	DIO �ݡ����ֹ�
 */
typedef u32	DIOPORT;
#define	DP_PORT1	(DIOPORT)0
#define	DP_PORT2	(DIOPORT)1
#define	DP_PORT3	(DIOPORT)2
#define	DP_PORT4	(DIOPORT)3
#define	DPO_PORT1	(DIOPORT)4
#define	DPO_PORT2	(DIOPORT)5
#define	DPO_PORT3	(DIOPORT)6
#define	DPO_PORT4	(DIOPORT)7
#define	DPX_PORT1	(DIOPORT)8
#define	DPX_PORT2	(DIOPORT)9
#define	DPX_PORT3	(DIOPORT)10
#define	DPX_PORT4	(DIOPORT)11

/*
 *	DIO channal�ֹ�
 */
typedef u32	DIOCH;
#define	DC_CH1	(DIOCH)0
#define	DC_CH2	(DIOCH)1
#define	DC_CH3	(DIOCH)2
#define	DC_CH4	(DIOCH)3
#define	DC_CH5	(DIOCH)4
#define	DC_CH6	(DIOCH)5
#define	DC_CH7	(DIOCH)6
#define	DC_CH8	(DIOCH)7

/*
 *	�����ݥӥ󥰤ȥ��������κ�ɸ�ϼ���
 */
typedef u32		TRANSMODE;
#define	MODE_xyz	(TRANSMODE)0x01
#define	MODEIxyz	(TRANSMODE)0x02
#define	MODE_XYZ	(TRANSMODE)0x04
#define	MODEIXYZ	(TRANSMODE)0x08
#define	MODE_wave	(TRANSMODE)0x10
#define	MODEIwave	(TRANSMODE)0x20

/*
 *	������°������
 */
typedef u32	PNTID;
#define	PA_SETID	(PNTID)0x7304

/*
 *	�ߡ��߸̤ζ������Σ����
 */
typedef u32	PNTNO;
#define	PN_1 	(PNTNO)1
#define	PN_2 	(PNTNO)2
#define	PN_3 	(PNTNO)3

/*
 *      JUMP����ͭ��/̵���ʶ����ǡ����ˤ����ơ�
 */
typedef u32	JUMPONOFF;
#define JMP_ON     (JUMPONOFF)1		//'/* ON��������"    */
#define JMP_OFF    (JUMPONOFF)0		//'/* OFF ������"    */

/*
 *	JUMP����ͭ��/̵����JUMP����ˤ����ơ�
 */
typedef u32	JUMPENABLEDISABLE;
#define	JMPENABLE 	(JUMPENABLEDISABLE)0x01000000
#define	JMPDISABLE 	(JUMPENABLEDISABLE)0x00000000

/*
 *	JUMP̿��
 */
typedef u32	JUMPORDER;
#define	NO_JUMP		(JUMPORDER)0x00010000
#define	DI_JUMP 	(JUMPORDER)0x00020000
#define	DI_WAITJUMP 	(JUMPORDER)0x00030000
#define	DI_WAIT 	(JUMPORDER)0x00040000

/*
 *	JUMP�������
 */
typedef u32	JUMPDILOGIC;
#define	LEVEL_ON 	(JUMPDILOGIC)0x00000100
#define	LEVEL_OFF	(JUMPDILOGIC)0x00000200
#define	EDGE_ON 	(JUMPDILOGIC)0x00000400
#define	EDGE_OFF	(JUMPDILOGIC)0x00000800

/*
 *	JUMP�����å��о�DI
 */
typedef u32	DIOKIND;
#define	DIO_INTERNAL	(DIOKIND)0x00000000
#define	DIO_EXTERNAL	(DIOKIND)0x00000001

/*
 *  CUBE����������������
 */
typedef u32	CUBEPNT;
#define MAXPNT    (CUBEPNT)1
#define MINPNT    (CUBEPNT)2
#define CENTERPNT (CUBEPNT)3

/*
 *	mask����
 */
typedef u32	DIOMASK;
#define	DIMSK	 	(DIOMASK)0
#define	DOMSK		(DIOMASK)1

/*
 *  RETRACON/OFF
 */
typedef u32	RETRAC;
#define RETRACOFF    (RETRAC)0
#define RETRACON     (RETRAC)1

/*
 *  CUBE����
 */
typedef u32	CUBEINFO;
#define NOCUBE       (CUBEINFO)0x00000000
#define CUBEON       (CUBEINFO)0x00000001
#define CUBEMAX      (CUBEINFO)0x00000002
#define CUBEMIN      (CUBEINFO)0x00000004
#define CUBECENTER   (CUBEINFO)0x00000008
#define CUBESIDE     (CUBEINFO)0x00000010


/*
 *	TEACHMODE
 */
typedef u32	TEACHMODE;
#define	TEACH_OFF 	(TEACHMODE)0
#define	TEACH_LOW	(TEACHMODE)1
#define	TEACH_MID	(TEACHMODE)2
#define	TEACH_HIGH	(TEACHMODE)3

/*
 *  TEACHLOCK
 */
typedef u32	TEACHLOCK;
#define LOCK_OFF    (TEACHLOCK)0
#define LOCK_ON     (TEACHLOCK)1

/*
 *  �����ܥɥ饤�ФȤ��̿�����
 */
typedef u32	COMSTATUS;
#define STP_STATUS    (COMSTATUS)0
#define MOV_STATUS    (COMSTATUS)1
#define SIM_STATUS    (COMSTATUS)2

/*
 *
 */
#define	MOD_ROBFILE	1
#define	MOD_TOLFILE	2

/*
 *	�����ݥӥ󥰤ȥ��������ǡݥ���¤��
 */
typedef struct {
	u32	Enable;		/* ����ӥå� 				*/
	float	_xyz[3];	/* �����ɸ����ʬ̵�����ե��å� 	*/
	float	Ixyz[3];	/* �����ɸ����ʬͭ�ꥪ�ե��å� 	*/
	float	_XYZ[3];	/* ���ܥåȺ�ɸ����ʬ̵�����ե��å� 	*/
	float	IXYZ[3];	/* ���ܥåȺ�ɸ����ʬͭ�ꥪ�ե��å� 	*/
	float	_wave[3];	/* ��ƻ��ɸ����ʬ̵�����ե��å� 	*/
	float	Iwave[3];	/* ��ƻ��ɸ����ʬͭ�ꥪ�ե��å� 	*/
} TRANSMAT, *TRANSMATP;

/* 
 * ���������ɸ�ͤι�¤�� 
 */
typedef struct {
	ANGLE		angle;
	MATRIX		noap;
	float		ypr[3];
} ARMTARGET, *ARMTARGETP;

typedef struct {
	u32	sig;
	u32	trq;
	u32	vel;
} O8DRIVE;
typedef struct {
	u32	sts;
	u32	agl;
	u32	vel;
	u32	trq;
} I8DRIVE;

/*
	�ǥåɥޥ󥹥��å���
*/
#define SET_DDM	3


/*
	CUBE����¤��
*/
typedef struct{
	u32	ena;
	u32	mod;
	float	max[3];
	float	min[3];
	char	cmt[32];
} CUBE, *CUBEP;


/* 
 * �ǥХå���¤��
 */
typedef struct {
	u32	ldbg[16];
	float	fdbg[32];
} DEBG, *DEBGP;

/*===========================================================================
	�ؿ��Υץ��ȥ��������
===========================================================================*/
#define	DllExport	
#define	__stdcall	

#ifdef __cplusplus
extern  "C" {
#endif


/*
 *	������ؿ�
 */
DllExport u32 __stdcall pa_ini_sys(void);/* PA �饤�֥��ν����	*/
DllExport u32 __stdcall pa_ter_sys(void);/* PA �饤�֥��ν�λ	*/

/*
 *	������ư��ؿ�
 */

DllExport u32 __stdcall pa_opn_arm(ARM);	/* ������Υ����ץ�	*/
DllExport u32 __stdcall pa_con_arm(ARM,char*);	/* ������Υ����ץ�	*/
DllExport u32 __stdcall pa_cls_arm(ARM);	/* ������Υ�������	*/

DllExport u32 __stdcall pa_sta_arm(ARM);	/* ��������		*/
DllExport u32 __stdcall pa_ext_arm(ARM);	/* ��λ			*/

DllExport u32 __stdcall pa_stp_arm(ARM,u32);	/* ���			*/
DllExport u32 __stdcall pa_sus_arm(ARM,u32);	/* ������		*/
DllExport u32 __stdcall pa_rsm_arm(ARM,u32);	/* �Ƴ�			*/

DllExport u32 __stdcall pa_exe_axs(ARM,AXIS,ANGLEP,u32);/* �Ƽ�ư��	*/
DllExport u32 __stdcall pa_exe_edr(ARM,AXIS,ANGLEP,u32);/* �ѵ�ư��	*/

DllExport u32 __stdcall pa_exe_hom(ARM,u32);	/* ���ܻ���ư��		*/
DllExport u32 __stdcall pa_exe_esc(ARM,u32);	/* �������ư��		*/
DllExport u32 __stdcall pa_exe_saf(ARM,u32);	/* ��������ư��		*/

DllExport u32 __stdcall pa_mov_XYZ(ARM,float,float,float,u32);
DllExport u32 __stdcall pa_mov_YPR(ARM,float,float,float,u32);
DllExport u32 __stdcall pa_mov_xyz(ARM,float,float,float,u32);
DllExport u32 __stdcall pa_mov_ypr(ARM,float,float,float,u32);

DllExport u32 __stdcall pa_mov_mat(ARM,MOVEMODE,MATRIX,ANGLEP,u32);

/*
 *	���������˴ؤ���ؿ�/�ץ쥤�Хå��˴ؤ���ؿ�
 */
DllExport u32 __stdcall pa_chg_pnt(ARM,PNTMOVE,u32);
DllExport u32 __stdcall pa_add_pnt(ARM,PNTTYPE);
DllExport u32 __stdcall pa_del_pnt(ARM,PNTDEL);
DllExport u32 __stdcall pa_rpl_pnt(ARM);
/*������°���ѹ�	PCI�ǲ�¤	*/
DllExport u32 __stdcall pa_set_pnt(ARM,PNTATTR,u32*,float);

DllExport u32 __stdcall pa_mov_pnt(ARM,u32);
/*�ץ쥤�Хå�		PCI�ǲ�¤	*/
DllExport u32 __stdcall pa_ply_pnt(ARM,PLAYBACK,u32,u32);	
/*�����ǡ���������	PCI�ǲ�¤	*/
DllExport u32 __stdcall pa_lod_pnt(ARM,u32,char *);
DllExport u32 __stdcall pa_sav_pnt(ARM,char *);

DllExport u32 __stdcall pa_vel_pnt(ARM,float);	/* �ץ쥤�Хå�®�ٷ���	*/

/*
 *	®������⡼�ɤ˴ؤ���ؿ�
 */
DllExport u32 __stdcall pa_mod_vel(ARM,VELMODE,AXIS);	/* Ver.1.3�ǲ�¤ */
DllExport u32 __stdcall pa_odr_vel(ARM,float[]);	/* Ver.1.3�ǲ�¤ */

/*
 *	��Ĺ��
 */
DllExport u32 __stdcall pa_mod_jou(ARM,JOUMODE);
DllExport u32 __stdcall pa_odr_jou(ARM,JOUTYPE);

/*
 *	��ü���а��֡���������ؿ�
 */
DllExport u32 __stdcall pa_mod_dpd(ARM);
DllExport u32 __stdcall pa_odr_dpd(ARM,MATRIX,ANGLEP);

/*
 *	�����쥯������ؿ�
 */
DllExport u32 __stdcall pa_wet_ded(ARM,AXIS);
DllExport u32 __stdcall pa_get_amp(ARM);
DllExport u32 __stdcall pa_mod_dir(ARM,DIRECTMODE);

/*
 *	����˴ؤ���ؿ�
 */
DllExport u32 __stdcall pa_set_hom(ARM,ANGLEP);
DllExport u32 __stdcall pa_set_esc(ARM,ANGLEP);
DllExport u32 __stdcall pa_set_saf(ARM,ANGLEP);

DllExport u32 __stdcall pa_def_hom(ARM);
DllExport u32 __stdcall pa_def_esc(ARM);
DllExport u32 __stdcall pa_def_saf(ARM);

/*
 *	��ɸ�Ѵ�����˴ؤ���ؿ�
 */
DllExport u32 __stdcall pa_set_mtx(ARM,MATRIX);

/*
 *	������Υ��ơ�����������ɤ߹���ؿ�
 */
DllExport u32 __stdcall pa_get_sts(ARM,ARMSTATUSP);
DllExport u32 __stdcall pa_get_cnt(ARM,u32*);
DllExport u32 __stdcall pa_get_err(ARM,u32*);
DllExport u32 __stdcall pa_get_agl(ARM,ANGLEP);
DllExport u32 __stdcall pa_get_xyz(ARM,VECTOR);
DllExport u32 __stdcall pa_get_noa(ARM,MATRIX);
DllExport u32 __stdcall pa_get_ypr(ARM,VECTOR);
DllExport u32 __stdcall pa_get_prm(ARM,PARAMP);
DllExport u32 __stdcall pa_get_pnt(ARM,PNTDATP);
DllExport u32 __stdcall pa_get_cur(ARM,u32*);
DllExport u32 __stdcall pa_get_num(ARM,u32*);
DllExport u32 __stdcall pa_get_lvl(ARM,u32,u32*);
DllExport u32 __stdcall pa_get_fvl(ARM,u32,float*);

/*
 *	�ǥ������������Ϥ˴ؤ���ؿ�	PCI�ǲ�¤
 */
DllExport u32 __stdcall pa_inp_dio(ARM,DIOKIND,DIOSTATUSP);
DllExport u32 __stdcall pa_oup_dio(ARM,DIOKIND,DIOSTATUSP);
DllExport u32 __stdcall pa_get_dio(ARM,DIOKIND,DIOPORT,DIOCH,unsigned char*);
DllExport u32 __stdcall pa_set_dio(ARM,DIOKIND,DIOPORT,DIOCH);
DllExport u32 __stdcall pa_rst_dio(ARM,DIOKIND,DIOPORT,DIOCH);

/*
 *	�ѥ�᡼���˴ؤ���ؿ�
 */
/* ���������ѹ�����	*/
DllExport u32 __stdcall pa_set_tol(ARM,float,float,float,float);
/* �ǥե����®�٤��ѹ����� 	*/
DllExport u32 __stdcall pa_set_vel(ARM,VELTYPE,float*);

/* ��ư����CPU�إѥ�᡼���Υ����� 	*/
DllExport u32 __stdcall pa_lod_ctl(ARM,char *);
/* ��ư����CPU�Υѥ�᡼����-�� 	*/
DllExport u32 __stdcall pa_sav_ctl(ARM,char *);		

DllExport u32 __stdcall pa_lod_def(ARM);/* ��ư����CPU���ѥ�᡼���������*/
DllExport u32 __stdcall pa_sav_def(ARM);/* ��ư����CPU���ѥ�᡼����-�� */

/*
 *	����¾�δؿ�
 */
DllExport u32 __stdcall pa_map_ctl(ARM);				/* ���楳��ȥ�����ζ�ͭ�ΰ�ޥåԥ�	*/
DllExport short __stdcall pa_fsh_chk(ARM);				/* ���楳�ޥ�ɤν�λ��λ�Ԥ�	*/
DllExport u32 __stdcall pa_req_ctl(ARM,u32);			/* ���楳��ȥ�����˥��ޥ�����������*/
DllExport u32 __stdcall pa_rst_ctl(ARM);				/* ������Υ��顼�ꥻ�å�			*/

DllExport u32 __stdcall	pa_err_mes(u32,char*);		/* ���顼��å����������		*/

/*======== ���Ver. 1.3 �ɲåǡݥ������ס�� ========*/
/*
 *	������ؿ�
 */
DllExport u32 __stdcall pa_sta_sim(ARM);				/* ���ߥ��-����󥹥�����			*/
DllExport u32 __stdcall pa_ext_sim(ARM);				/* ���ߥ��-�����λ			*/

/*
 *	�ǥ������������Ϥ˴ؤ���ؿ�
 */
DllExport u32 __stdcall pa_swt_dio(ARM,u32);
DllExport u32 __stdcall pa_chg_dio(ARM,DIOSTATUSP);	/* �������Σģϥ�-��°������ؿ� */
/*
 *	�������ץ쥤�Хå��˴ؤ���ؿ�
 */
DllExport u32 __stdcall pa_set_idn(ARM,PNTID,u32);	/* �������ɣ�����ؿ�	*/
DllExport u32 __stdcall pa_get_idn(ARM,u32*);			/* �������ɣĻ��ȴؿ�	*/
DllExport u32 __stdcall pa_axs_pnt(ARM,u32);			/* �Ƽ�����ǥץ쥤�Хó������ذ�ư*/
DllExport u32 __stdcall pa_set_dlc(ARM,u32);			/* �ץ쥤�Хå�DO�μ�ư��ߤ����� 	*/
DllExport u32 __stdcall pa_get_dlc(ARM,u32*);			/* �ץ쥤�Хå�DO�μ�ư��ߤμ��� 	*/

/*
 *	�����ݥӥ����������������˴ؤ���ؿ�
 */
DllExport u32 __stdcall   pa_odr_xyz(ARM,TRANSMATP);	/* ���������˴ؤ���ؿ�  */
DllExport u32 __stdcall   pa_lmt_xyz(ARM,float);		/* ���ե��å�­�����ߤΥ�ߥå��к� */
DllExport u32 __stdcall   pa_get_lmt(ARM,float*);		/* ��ü���ե��å��ͤΥ�ߥå��ͤ����*/
DllExport short __stdcall pa_fsh_sub(ARM);				/* ���楳�ޥ�ɤν�λ��λ�Ԥ�*/
DllExport u32 __stdcall   pa_req_sub(ARM,u32);		/* ���楳��ȥ�����˥��ޥ�����������*/

/*
 *	��ɸ�Ѵ�����˴ؤ���ؿ�
 */
DllExport u32 __stdcall pa_set_mat(ARM,MATRIX,MATRIX);

/*
 *	�����쥯������ؿ�
 */
DllExport u32 __stdcall pa_drt_ded(ARM,u32);			/* ��-��ο����դ���������	*/
DllExport u32 __stdcall pa_chk_cnt(ARM);				/* �Ƽ������-�ɤ�Ʊ������ 	*/
DllExport u32 __stdcall pa_set_tim(ARM,u32);			/* �����ॢ���Ȼ�������  		*/

/*
 *	������Υ��ơ�����������ɤ߹���ؿ�
 */
DllExport u32 __stdcall pa_get_mod(ARM,u32*);			/* ��ư����Υ���-����  	*/
DllExport u32 __stdcall pa_get_jou(ARM,u32*);			/* RMRC����Ĺ����-�ɼ��� */
DllExport u32 __stdcall pa_get_ver(ARM,float*);		/* ��ư�����Ver.   	*/
DllExport u32 __stdcall pa_get_pvl(ARM,float*);		/* �ץ쥤�Хå�����   	*/
DllExport u32 __stdcall pa_get_pdo(ARM,u32*);			/* �ץ쥤�Хå���DIO   	*/
DllExport u32 __stdcall pa_get_cpt(ARM,PNTNO,PNTDATP);	/* �ߡ��߸̥�-������ 	*/
DllExport u32 __stdcall pa_get_mat(ARM,MATRIX,MATRIX);	/* ��ɸ�Ѵ����� 	*/
DllExport u32 __stdcall pa_get_sns(ARM,TRANSMATP);		/* ���󥵾���		*/
DllExport u32 __stdcall pa_get_tar(ARM,ARMTARGETP);	/* ��ɸ�;���		*/
DllExport u32 __stdcall pa_get_com(ARM,u32*);			/* �̿����֤μ���   	*/
DllExport u32 __stdcall pa_get_tim(ARM,u32*);			/* �����ॢ���Ȼ��֤μ��� 	*/
DllExport u32 __stdcall pa_get_drt(ARM,u32*);			/* �ޥ����־��֤μ���	*/

/*
 *	�ӣ������г��ٻ���ˤ���Ĺ��ư������ؿ�
 */
DllExport u32 __stdcall pa_mov_jou(ARM,float,u32);			/* ��Ĺ���������� 	*/
/*
 *	�Ƽ�����ꥢ���ݥɤ˴ؤ�������ؿ�
 */
DllExport u32 __stdcall pa_mod_axs(ARM);						/* �Ƽ�����ꥢ���-������ 	*/
DllExport u32 __stdcall pa_odr_axs(ARM,ANGLEP);				/* �Ƽ�����ꥢ���-������ 	*/



/*======== ���PCI��-���� �ɲåǡݥ�������	��� ========*/
DllExport u32 __stdcall pa_ply_set(ARM,u32,u32*);			/* �ֹ椷�Ƥˤ�궵���ǡ���Key���� 	*/
DllExport u32 __stdcall pa_act_pnt(ARM,u32);					/* �����ƥ��֤ʶ����ǡ������ڴ��� 	*/
DllExport u32 __stdcall pa_jmp_set(ARM,u32,u32,JUMPP);		/* �ֹ����ˤ��JUMP������� 	*/
DllExport u32 __stdcall pa_get_jmp(ARM,u32,u32,JUMPP);		/* Key/ID����ˤ��JUMP������� 	*/
DllExport u32 __stdcall pa_set_jmp(ARM,u32,u32,JUMPP);		/* JUMP�������� 	*/
DllExport u32 __stdcall pa_ena_jmp(ARM,u32);					/* JUMP���ͭ��/̵������ 	*/
DllExport u32 __stdcall pa_ply_mod(ARM,u32);					/* �����⡼������ 	*/
DllExport u32 __stdcall pa_set_cub(ARM,u32,float[],float[]);	/* CUBE�λ��� 	*/
DllExport u32 __stdcall pa_get_cub(ARM,u32,u32);				/* CUBE�ζ������� 	*/
DllExport u32 __stdcall pa_cub_len(ARM,u32,float[]);			/* CUBE����Ĺ������ 	*/
DllExport u32 __stdcall pa_cub_cmt(ARM,u32,char*);			/* CUBE��̾����Ĥ��� 	*/
DllExport u32 __stdcall pa_del_cub(ARM,u32);					/* CUBE�κ�� 	*/
DllExport u32 __stdcall pa_ena_cub(ARM,u32,u32);				/* CUBE��ͭ��/̵�� 	*/
DllExport u32 __stdcall pa_inf_cub(ARM,u32,CUBEP);			/* CUBE�ξ��󻲾� 	*/
DllExport u32 __stdcall pa_get_prj(ARM,char*);					/* �ץ���������̾���� 	*/
DllExport u32 __stdcall pa_set_prj(ARM,char*);					/* �ץ���������̾���� 	*/
DllExport u32 __stdcall pa_sav_prj(ARM,char*);					/* �ץ��������ȤΥ����� 	*/
DllExport u32 __stdcall pa_lod_prj(ARM,char*);					/* �ץ��������ȤΥ����� 	*/
DllExport u32 __stdcall pa_get_sav(ARM,u32*);					/* �����ܥ��ƥ��������� 	*/
DllExport u32 __stdcall pa_chg_key(ARM,u32);					/* ���ߥ����ƥ��֤ʶ����ǡ�����Key�ѹ� 	*/
DllExport u32 __stdcall pa_get_key(ARM,u32*);					/* ���ߥ����ƥ��֤ʶ����ǡ�����Key���� 	*/
DllExport u32 __stdcall pa_mon_pnt(ARM,PNTDATP);				/* �����ǡ����ξ��֤��˥������뤿��˼��� 	*/
DllExport u32 __stdcall pa_sav_sts(ARM,u32[]);				/* �����ܥ��ƥ��������� */
DllExport u32 __stdcall pa_set_cmt(ARM,char*);					/* ���������� */
DllExport u32 __stdcall pa_tst_nom(ARM,u32);					/* RETRAC����ON/OFF���� */
DllExport u32 __stdcall pa_get_rmd(ARM,u32*);					/* RETRAC����ON/OFF���� */
DllExport u32 __stdcall pa_dio_msk(ARM,u32,u32,u32);		/* DIO mask���� */
DllExport u32 __stdcall pa_get_msk(ARM,u32,u32,u32*);		/* DIO mask���� */
DllExport u32 __stdcall pa_jmp_cmt(ARM,u32,char*);			/* �����Ȥˤ�ꥫ���ȥݥ���Ȱ�ư */
DllExport u32 __stdcall pa_get_ena(ARM,u32*);					/* JUMP���ͭ��/̵������    */
DllExport u32 __stdcall pa_get_pmd(ARM,u32*);					/* �����⡼�ɼ���     */	
DllExport u32 __stdcall pa_sav_ptj(ARM,char*);					/* �����ǡ�����JUMP����򥻡��֤���   */
DllExport u32 __stdcall pa_lod_ptj(ARM,char*);					/* �����ǡ�����JUMP���������ɤ���   */
DllExport u32 __stdcall pa_get_dbg(ARM,DEBGP);					/* �ǥХå�������� */
DllExport u32 __stdcall pa_set_ddm(ARM,u32,u32);				/* �ǥåɥޥ�SWͭ��/̵�� */
DllExport u32 __stdcall pa_get_ddm(ARM,u32,u32*);			/* �ǥåɥޥ�SWͭ��/̵�����ּ��� */
DllExport u32 __stdcall pa_del_jmp(ARM,u32,u32);				/* JUMP������ */
DllExport u32 __stdcall pa_set_lok(ARM,u32);					/* TEACHLOCK���� */
DllExport u32 __stdcall pa_get_lok(ARM,u32*);					/* TEACHLOCK���� */
DllExport u32 __stdcall pa_tct_tim(ARM,u32*);					/* �����ȥ�����ʥץ쥤�Хå����֡˼��� */
DllExport u32 __stdcall pa_get_smd(ARM,u32*);					/* �����ܤ����TEACHMODE���� */
DllExport u32 __stdcall pa_get_max(ARM,u32*);					/* �ܡ��ɤ������ǽ���������������� */
DllExport u32 __stdcall pa_get_spt(ARM,u32*);					/* ���Ȥ������ܤˤ��뤫��������� */




/*======== ���486��-���� �ɲåǡݥ�������	H8.4.12��� ========*/
/*======== ���ISA��-�����б��Ѥ��ѹ�		H9.7.18��� ========*/
/*
 *	����¾�δؿ� pa_mem.c
 */
DllExport u32 __stdcall pa_mem_cpy(u32,void*,void*,size_t,int);/* 1�Х���ñ�̤Υ��ꥳ��-	*/
DllExport u32 __stdcall pa_mem_blk(u32,void*,void*,size_t,int);/* 1�Х���ñ�̤Υ��ꥳ��-	*/
DllExport u32 __stdcall pa_cpy_flt(u32,void*,void*,int);	/* float��1�Х���ñ�̤Υ��ꥳ��-	*/
DllExport u32 __stdcall pa_cpy_lng(u32,void*,void*,int);	/* u32��1�Х���ñ�̤Υ��ꥳ��-	*/
DllExport u32 __stdcall pa_cpy_uby(u32,void*,void*,int);	/* unsigned char��1�Х���ñ�̤Υ��ꥳ��-*/
DllExport u32 __stdcall pa_ret_lng(u32,void*);				/* u32��1�Х���ñ�̤Υ��ꥳ��-	*/
/* pa_egt.c */
DllExport u32 __stdcall	pa_get_lgh(ARM,float*);			/* S2-W2���ֵ�Υ�����ͼ���	*/

DllExport u32 __stdcall WinLANinit(u32,char*);
DllExport u32 __stdcall WinLANabort(u32);

DllExport u32 __stdcall WinRTinit(u32);
DllExport u32 __stdcall WinRTabort(u32);
DllExport u32 __stdcall MakeMutex(u32);
DllExport u32 __stdcall DestroyMutex(u32);
DllExport u32 __stdcall LockMutex(u32);
DllExport u32 __stdcall UnlockMutex(u32);

DllExport u32 __stdcall pa_set_sim(ARM, u32);
DllExport u32 __stdcall pa_set_inc(ARM, float);
DllExport u32 __stdcall pa_get_sim(ARM, u32*);
DllExport u32 __stdcall pa_get_inc(ARM, float*);
DllExport u32 __stdcall pa_lod_rob(ARM armno,char *file);
DllExport u32 __stdcall pa_lod_tol(ARM armno,char *file);
DllExport u32 __stdcall pa_sav_rob(ARM armno);
DllExport u32 __stdcall pa_ena_nom(ARM armno,u32 sw);
DllExport u32 __stdcall pa_ini_axs(ARM,AXIS,ANGLEP,u32);/* ��������*/
DllExport u32 __stdcall pa_get_nom(ARM armno,u32 *rpm);


DllExport u32 __stdcall pa_get_lvl(ARM armno,u32 pos,u32 *lvl);
DllExport u32 __stdcall pa_get_fvl(ARM armno,u32 pos,float *fvl);
DllExport u32 __stdcall pa_get_cnp(ARM armno,u32 *count,MATRIX noap);
DllExport u32 __stdcall pa_ini_drv(ARM armno, char* snd, char* rcv);

DllExport u32 __stdcall pa_sav_rom(ARM armno);
DllExport u32 __stdcall pa_lod_rom(ARM armno);

#ifdef __cplusplus
}
#endif


#endif	/* _PA_H_	*/



