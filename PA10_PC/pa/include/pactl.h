#ifndef _PACTL_H
#define	_PACTL_H

#define	EPDATA			16
#define	AXISNUMBER		7
#define	VECTNUMBER		6
#define	COORDNUMBER		3
#define	DATABLOCKNUMBER	5
#define	ARCNETPACET		256

#include <stdint.h>

typedef uint32_t u32;

/*
	Communication Header Discription
*/
typedef struct {
	u32	com;			/* �̿���ݥ�	*/
	u32	evt;			/* ���٥��		*/
	u32	sts;			/* ����			*/
	u32	mod[4];			/* ��ư�����ݥ�*/
	u32	dmy[8];			/* ͽ��			*/
} HED;
/*
	High Lank Controller Communication structure (UP) 
*/
typedef struct {
	u32	axs;					/* ���������ѿ�*/
	float	agl[AXISNUMBER];		/* ��ɸ����		*/
	float	xyz[COORDNUMBER];		/* ��ɸ����		*/
	float	ypr[COORDNUMBER];		/* ��ɸ����		*/
	u32	lvl;					/* �ץ쥤�Хå�®��*/
	float	fvl;
} DWN;
/*
	Robot OutPut Data structure
*/
typedef struct {
	u32	brk;					/* �֥�ݥ� ON/OFF*/
	u32	sav;					/* ���ݥ� ON/OFF*/
	u32	vts;					/* ®�١��ȥ륯����*/
	u32	trq[AXISNUMBER];		/* �ȥ륯����*/
	u32	vel[AXISNUMBER];		/* ®�ٻ���*/
	u32	dmy[AXISNUMBER];		/* ͽ��*/
} ODT;
/*
	Robot InPut Data structure
*/
typedef struct {
	u32	rez[AXISNUMBER];	/* �쥾�����*/
	u32	trq[AXISNUMBER];	/* �ȥ륯��*/
	u32	sts[AXISNUMBER*2];	/* ���ݥܥ��ơݥ���*/
	u32	dmy[AXISNUMBER];	/* ͽ��*/
} IDT;
/*
	Target [N,O,A,P] structure
*/
typedef struct {
	float	agl[AXISNUMBER];		/* �Ƽ�(���١�®��)��ɸ��*/
	float	xyz[COORDNUMBER];		/* ������ɸ��*/
	float	noa[COORDNUMBER][COORDNUMBER];	/* ������ɸ�� */
	float	fce[VECTNUMBER];		/* ����ɸ��*/
} TGT;
/*
	Mark [N,O,A,P] structure
*/
typedef struct {
	float	agl[AXISNUMBER];		/* �Ƽ�(���١�®��)������*/
	float	xyz[COORDNUMBER];		/* ���ָ�����*/
	float	noa[COORDNUMBER][COORDNUMBER];	/* ����������*/
	float	fce[VECTNUMBER];
} MRK;
/*
	Error note structure
*/
typedef struct {
/*
	u32	wrn[AXISNUMBER];
	u32	err[AXISNUMBER];
*/
	u32	dmy[16];
	u32	drv[8];
} ERR;
/*
	Parameter structure
*/
typedef struct {
	float	pul[AXISNUMBER];	/* ���֥�ߥå��ʡܡ�*/
	float	pdl[AXISNUMBER];	/* ���֥�ߥå��ʡݡ�*/
	float	vel[AXISNUMBER + 2];/* ®�٥�ߥå�*/
	float	dev[AXISNUMBER + 2];/* �ǥե����®��*/
	float	lim[AXISNUMBER + 2];/* */
	float	ceh[AXISNUMBER + 2];/* */
	float	cem[AXISNUMBER + 2];/* */
	float	cel[AXISNUMBER + 2];/* */
	float	pg1[AXISNUMBER];	/* �������楲����*/
	float	pg2[AXISNUMBER];	/* �������楲����*/
	float	vg1[AXISNUMBER];	/* ®�����楲����*/
	float	tg1[AXISNUMBER];	/* �����楲����*/
	float	pcm[AXISNUMBER];	/* ��������������� 	!!*/
	float	fcm[AXISNUMBER];	/* �������������   	!!*/
	float	arl[AXISNUMBER];	/* ���ݥ�Ĺ*/
	float	arg[AXISNUMBER];	/* �Ƽ��ſ�����*/
	float	arw[AXISNUMBER];	/* �Ƽ�����*/
	float	hom[AXISNUMBER];	/* ���ܻ���������ɸ�� */
	float	saf[AXISNUMBER];	/* ����¾��������ɸ��*/
	float	esc[AXISNUMBER];	/* ������������ɸ��*/
	float	tol[AXISNUMBER];	/* ����ѥ��ݥ�*/
	float	fvl[AXISNUMBER];	/* */
	u32	dmy[AXISNUMBER];	/* �������ӥ󥰥ѥ�᡼��*/
	u32	spa[AXISNUMBER];	/* ͽ��			!!*/
} PRM;
/*
	Arcnet transform structure
*/
typedef struct {
	unsigned char pkt[256];	/* �����ܥɥ饤���̿��ѥ��å�*/
} BSE;

typedef struct {
	u32	Enable;			/* ����ӥå� */
	float	_xyz[3];		/* �����ɸ����ʬ̵�����ե��å� */
	float	Ixyz[3];		/* �����ɸ����ʬͭ�ꥪ�ե��å� */
	float	_XYZ[3];		/* ���ܥåȺ�ɸ����ʬ̵�����ե��å� */
	float	IXYZ[3];		/* ���ܥåȺ�ɸ����ʬͭ�ꥪ�ե��å� */
	float	_wave[3];		/* ��ƻ��ɸ����ʬ̵�����ե��å� */
	float	Iwave[3];		/* ��ƻ��ɸ����ʬͭ�ꥪ�ե��å� */
} TRN;

/*
	standard & extend digital I/O
	[0]:standard digital I/O
	[1]:extend digital I/O
*/
typedef struct {
	u32	imask[2];
	u32	omask[2];	
	u32	i1420[2];	/* �ǥ�����������*/
	u32	o1420[2];	/* �ǥ����������*/
	u32	x1420[2];	/* �ǥ���������� Ver.1.3*/
} DIO;
/*
	debug monitor
*/
//typedef struct {
//	u32	L[14];			/* ���󥰥ǡ����ǥХå����ꥢ*/
//	float	F[14];
//} DBG;
/*
	386 - 486 communication flag
*/
typedef struct {
	u32	flag[7];		/* �̿��ѥե饰*/
} FLG;
/*
	x, y, z, y, p, r
*/
typedef struct {
	float	max[7];	/* �Ƽ����� ������[rad] */
	float	tax[7];	/* �Ƽ����� ��ɸ��[rad] */
	float	mrk[6]; /* ���ܥåȺ�ɸ�� ����&���� ������*/
	float	tgt[6];	/* ���ܥåȺ�ɸ�� ����&���� ��ɸ��*/
	float	len[7];	/* S2������W2���ޤǤμ��ֵ�Υ[0] */
} YPR;
/*
	Teaching data structure Parts
*/
/*
typedef struct {
	u32	BlockNum;
	u32	BlockCount;
//	u32	TeachData[DATABLOCKNUMBER][EPDATA];
	float	TeachData[DATABLOCKNUMBER][EPDATA];
} TEACH;
*/
/*
	arm constant data
*/
typedef struct {
	float	agl[7];
	float	vel[2];
	u32	atr[12];
} PNT;
typedef struct {
	PNT		pnt;
	char	cmt[32];
} PLY;
/*

*/
typedef struct {
	float	xyz[3];
	float	noa[3][3];
} NOA;
/*

*/
typedef struct {
	u32	cnd[2];
	u32	xdi;
	u32	tim;
	u32	key;
	u32	pid;
	u32	cnt;
} JDG;
typedef struct {
	u32	cid;
	JDG		jdg[8];
} JMP;
/*
	CUBE������
*/
typedef struct{
	u32	ena;
	u32	mod;
	float	max[3];
	float	min[3];
	char	cmt[32];
} CUB;
/*

*/
typedef struct{
	u32	max;
	u32	arm;
	u32	axs;
	u32	typ;
	u32	drv;
	u32	dio;
	float	ver;		/* version	*/
	float	rtv[7];		/* �쥾����Ѵ����� 			*/
	float	len[8];		/* [0]-[6]��-��Ĺ [7]RMRC����³�Ĺ */
/*	float	spa[7];	*/	/* [0]����Ʊ�� [1]-[6]ͽ�� 	*/
}ARD;
/*


  */
typedef struct {
	u32	lvl[16];
	float	fvl[16];
}GET;

typedef struct{
	u32	lvl[16];
	PLY		ply;
	NOA		noa;
	JMP		jmp;
}STS;

typedef struct {
	u32	lvl[16];
	float	fvl[32];
}DBG;

/*-----------------------------------------------------------------------------
		 ������ѥ�᡼��
-----------------------------------------------------------------------------*/
typedef struct {
	float	SNS[6];
	float	TOG[6];
	float	XCE[6][6];
	float	XCI[6][6];
	float	VEC[3][3];
	float	MU1[6];
	float	MU2[6];
	float	MD1[6];
	float	MD2[6];
	float	FGR[6];
	float	FGH[6];
	float	V2N[6];
	float	DEV[6];
	float	SP1[6];
	float	SP2[6];
	float	DMY[6];
} D_FPARAMETER;

/*---------------------------------------------------------------------------
	��������� 
----------------------------------------------------------------------------*/
typedef struct {
	float	PMX[6];
	float	FMX[6];
} D_PFMAT;

typedef struct {
	u32	Coord;
	float	Force[6];
	u32	Fdata;
} D_FDOWN;

typedef struct {
	float	ORGforce[6];
	float	OFFforce[6];
	float	XCEforce[6];
	float	TGTforce[6];
	float	XCIforce[6];
} D_FORCE;

typedef struct {
	u32	state[256];
} M_STATE;

typedef struct {
	u32	Switch[2];
	u32	DeadMain[2];
	u32	DoLine[2];
	u32	InterLock[2];
} M_TERM;

typedef struct {
	u32	control;
	u32	vcltrol;
	u32	cbltrol;
	u32	ack;
	u32	err;
} M_ASPECT;
typedef struct {
	short	sig;
	short	trq;
	short	vel;
} O8_DRIVER;
typedef struct {
	short	sts;
	char	agl[4];
	short	vel;
	short	trq;
} I8_DRIVER;
typedef struct {
	u32	no;
	u32	ev;
	u32	en7;
	u32	en8;
	O8DRIVE	o7;
	O8DRIVE	o8;
	u32	sdo;

	I8DRIVE	i7;
	I8DRIVE	i8;
	u32	sdi;		/* 8ch */
	u32	sai[4];		/* 10bit data */
	M_ASPECT	aspect;	
} GTE;

/*-----------------------------------------------------------------------------
		For 386 Communication format
-----------------------------------------------------------------------------*/
/*
	command area
*/
typedef struct {
	HED			hed;
	DWN			dwn;			/* 386����ǡ���			(1)*/
	PLY			ply;			/* �����ǡ���				(3)*/
	NOA			noa;
	JMP			jmp;
	BSE			bse;		/* �����ܥɥ饤���ѥǡ���		(4)*/
	PRM			prm;		/* �ѥ�᡼���ơ��֥�			(5)*/
	MRK			wrk[2];		/* ��ɸ�Ѵ�����			(6)*/
	MRK			mat;			/* J/Sprm 	(7)	//noap	(8)*/
	TRN			trn;		/* �������ӥ󥰡����󥵡�etc�ǡ���				*/
	DIO			dio;
	GTE			gte;
} HDN;

typedef struct {
	D_FPARAMETER	fprm;		/* ������ѥ�᡼��							*/
	D_FDOWN			fdown;		/* ��������� 							*/
	D_PFMAT			pfmat;		/* ���֤��Ϥ��������					*/
} FDN;
typedef struct {
	D_FPARAMETER	fprm;		/* ������ѥ�᡼��							*/
	D_FORCE			force;		/* �ϥ������ϥ�-�� 	*/
} FUP;
/*-----------------------------------------------------------------------------
		For 386 Communication format
-----------------------------------------------------------------------------*/
/*
	answer area
*/
typedef struct {			/* ROBOT�����ϥǡݥ�*/
	IDT		idt;
	ODT		odt;
} RIO;
typedef struct {			/* �����͡��ڤӡ���ɸ��*/
	MRK		mrk;
	TGT		tgt;
} POS;
typedef struct {
	HED			hed;
	RIO			rio;		/* ���ܥå��̿��ǡ���*/
	POS			pos;		/* ������&��ɸ��*/
	MRK			mat[2];
	TRN			trn;		/* �������ӥ󥰡����󥵡�etc���û��ǡ���			*/
	PLY			ply;
	NOA			noa;
	JMP			jmp;
	BSE			bse;		/* �����ܥɥ饤�Хǡ���*/
	PRM			prm;		/* �ѥ�᡼���ơ��֥�			(5)*/
	ERR			err;		/* ���顼��å��������ꥢ*/
	DIO			dio;		/* ɸ��������*/
	FLG			flg;		/* 386�ѥե饰*/
	YPR			ypr;		/* ��-�฽����&��ɸ��(�ޥ�ޥ��󥳥�ȥ�-�黲�Ȥ���) */
	CUB			cub;		/* CUBE������ */
	ARD			ard;
	GET			get;
	STS			sts;
	DBG			dbg;
} HUP;

/*---------------------------------------------------------------------------
	COMINUCATION MAP.(���楳��ȥ�-��ȥޥ�ޥ��󥳥�ȥ�-��ζ�ͭ���ꥢ)
----------------------------------------------------------------------------*/
typedef struct {
	HDN				hd;			/* �ޥ�ޥ��󥳥�ȥ�-�� -> ���楳��ȥ�-�� �̿����ꥢ 	*/
	FDN				fd;
	HUP				hu;			/* �ޥ�ޥ��󥳥�ȥ�-�� <- ���楳��ȥ�-�� �̿����ꥢ 	*/
	FUP				fu;
}COMUNI, *COMUNIptr;

/*



*/
typedef struct {
	HDN				hd;			/* �ޥ�ޥ��󥳥�ȥ�-�� -> ���楳��ȥ�-�� �̿����ꥢ 	*/
	FDN				fd;
	HUP				hu;			/* �ޥ�ޥ��󥳥�ȥ�-�� <- ���楳��ȥ�-�� �̿����ꥢ 	*/
	FUP				fu;
}COMCOMX;

#endif



