#ifndef _PAERR_H
#define _PAERR_H

	/*・マンマシンコントローラエラー*/
#define ERR_OK			 0	/* エラ−無し */

	/*動作エラー*/
#define ERR_FILE		-1	/* 指定されたファイルはありません*/
#define	ERR_READ		-2 	/* ファイル読み込み失敗*/
#define	ERR_WRITE		-3 	/* ファイル書き込み失敗*/
#define ERR_INT			-4	/* 486へ割り込み失敗*/
#define ERR_OPEN		-5	/* pa_opn_arm()されていません*/
#define ERR_MALLOC		-6	/* メモリ確保に失敗しました*/
#define ERR_PRM			-7	/* 制御中はパラメータの変更はできません*/
#define ERR_PNT			-8	/* 教示点データの指定角度がパラメータ範囲を越えています*/

	/*引数エラー*/
#define ERR_ARM			-20	/* 指定されたアームはありません*/
#define ERR_AXIS		-21	/* 指定された軸はありません*/
#define ERR_DRV			-22	/* 指定されたドライバはありません*/
#define ERR_PB			-23	/* プレイバク動作種別が間違っています*/
#define ERR_PD			-24	/* 教示点削除操作種別が間違っています*/
#define ERR_PA			-25	/* 教示点属性変更種別が間違っています*/
#define ERR_PTN			-26	/* 教示点速度パタ-ン属性値が間違っています*/
#define ERR_PT			-27	/* 教示点データタイプ種別が間違っています*/
#define ERR_PM			-28	/* 教示点操作種別が間違っています*/
#define ERR_VT			-29	/* デフォルト速度変更種別が間違っています*/
#define ERR_VM			-30	/* 速度制御モード種別が間違っています*/
#define ERR_JM			-31	/* 冗長軸制御モード種別が間違っています*/
#define ERR_JT			-32	/* 冗長軸操作種別が間違っています*/
#define ERR_MM			-33	/* 目標先端行列制御モード種別が間違っています*/
#define ERR_DM			-34	/* ダイレクト制御種別が間違っています*/
#define ERR_DP			-35	/* ディジタル入出力ポート指定が間違っています*/
#define ERR_DC			-36	/* ディジタル入出力チャネル指定が間違っています*/
#define ERR_MES			-37	/* エラ-コ-ドは定義されていなっています*/
#define ERR_BOARD		-38	/* ディジタル入出力ボード指定が間違っています*/
#define ERR_DIO			-39	/* ディジタル入出力DIorDO指定が間違っています*/
#define ERR_PRJ			-40	/* プロジェクトがロードされていません*/

	/*WinRTエラー*/
#define ERR_UNMAPMEMORY	-100	/* WinRTUnMapMemoryでエラーが発生しました*/
#define ERR_UNMAPMEMORY2 -101	/* WinRTUnMapMemory2でエラーが発生しました*/
#define ERR_OPENDEVICE	-200	/* WinRTOpenNamedDeviceでエラーが発生しました*/
#define ERR_CONFIG		-201	/* WinRTGetFullConfigurationでエラーが発生しました*/
#define ERR_MAPMEMORY	-300	/* WinRTMapMemoryでエラーが発生しました*/
#define ERR_MAPMEMORY2	-301	/* WinRTMapMemory2でエラーが発生しました*/

	/*・ワーニングエラー*/

#define	ERR_CANT_CPU	-1000	  /*制御コントローラへのアクセス権がありません*/
#define	ERR_NON_EVNT	-1001	  /*フォーマットとコマンドがマッチしません*/ 
#define	ERR_CANT_EVNT	-1002	  /*現在のモードでは対応できないコマンドです*/
#define	ERR_INVALD_EVNT	-1003	  /*コマンドが無効 */
							/*=== Ver.PCI ====*/
#define	ERR_NON_ARM		-1004	  /*指定されたアーム番号がありません */
#define	ERR_NON_ROB		-1005	  /*ROBファイルの新規ダウンロードが必要 */
#define	ERR_NON_TOL		-1006	  /*TOLファイルの新規ダウンロードが必要 */
							/*===============*/

#define	ERR_S1_VEL		-1010	  /*Ｓ１軸速度オ−バ */
#define	ERR_S2_VEL		-1011	  /*Ｓ２軸速度オ−バ */
#define	ERR_S3_VEL		-1012	  /*Ｓ３軸速度オ−バ */
#define	ERR_E1_VEL		-1013	  /*Ｅ１軸速度オ−バ */
#define	ERR_E2_VEL		-1014	  /*Ｅ２軸速度オ−バ */
#define	ERR_W1_VEL		-1015	  /*Ｗ１軸速度オ−バ */
#define	ERR_W2_VEL		-1016	  /*Ｗ２軸速度オ−バ */

#define	ERR_XYZ_VEL		-1018	  /*先端位置速度オ−バ */
#define	ERR_YPR_VEL		-1019	  /*先端姿勢速度オ−バ */

#define	ERR_S1_SAGL		-1020	  /*Ｓ１軸 安全 角度オ−バ*/
#define	ERR_S2_SAGL		-1021	  /*Ｓ２軸 安全 角度オ−バ*/
#define	ERR_S3_SAGL		-1022	  /*Ｓ３軸 安全 角度オ−バ*/
#define	ERR_E1_SAGL		-1023	  /*Ｅ１軸 安全 角度オ−バ*/
#define	ERR_E2_SAGL		-1024	  /*Ｅ２軸 安全 角度オ−バ*/
#define	ERR_W1_SAGL		-1025 	  /*Ｗ１軸 安全 角度オ−バ*/
#define	ERR_W2_SAGL		-1026	  /*Ｗ２軸 安全 角度オ−バ*/
							/*=== Ver.1.3 ====*/
#define	ERR_S1_TAGL		-1030	  /*Ｓ１軸 目標 角度オ−バ*/
#define	ERR_S2_TAGL		-1031	  /*Ｓ２軸 目標 角度オ−バ*/
#define	ERR_S3_TAGL		-1032	  /*Ｓ３軸 目標 角度オ−バ*/
#define	ERR_E1_TAGL		-1033	  /*Ｅ１軸 目標 角度オ−バ*/
#define	ERR_E2_TAGL		-1034	  /*Ｅ２軸 目標 角度オ−バ*/
#define	ERR_W1_TAGL		-1035	  /*Ｗ１軸 目標 角度オ−バ*/
#define	ERR_W2_TAGL		-1036	  /*Ｗ２軸 目標 角度オ−バ*/
							/*===============*/

#define	ERR_NOA_CLC		-1038	  /*NOA計算ができません Ver.PCI*/
#define	ERR_LNK_CTL		-1039	  /*教示点連続性保護のため生成不可能 Ver.1.3*/
#define	ERR_MEM_FULL	-1040	  /*メモリの確保に失敗 */
#define	ERR_MIS_COMD	-1041	  /*このコマンドを発行する前には手続きが必要 */
#define	ERR_PB_CIR		-1042	  /*円、円弧の指定が誤っています */
#define	ERR_PB_NEXT		-1043	  /*次のポインタは存在しません */
#define	ERR_PB_PRIV		-1044	  /*前のポインタは存在しません*/
#define	ERR_PB_END		-1045	  /*プレイバックデ−タの終了*/
#define	ERR_PB_NULL		-1046	  /*プレイバックデ−タがありません*/
#define	ERR_PB_REFER	-1047	  /*プレイバックデ−タの検索に失敗しました*/
#define	ERR_PB_REPLACE	-1048	  /*リプレイスコマンドと解釈しました*/
#define	ERR_PB_PANIC	-1049	  /*ポインタ管理事故*/

#define	ERR_NOT_ENUGH	-1050	  /*目標値は制御不可能領域です。（アーム長が足りません）*/
#define	ERR_MIS_PARAM	-1051	  /*指定パラメータ値が設定範囲を越えています*/
#define	ERR_NOA_DAT		-1060	  /*指定されたＮＯＡは不適切です Ver.1.3*/
#define	ERR_PNT_ATR		-1061	  /*CPデ-タの最後を各軸属性で獲得しました Ver.1.3*/
#define	ERR_PTP_DAT		-1062	  /*RMRCの可動範囲を越えています Ver.1.3*/
#define	ERR_CP_LOGGING	-1063	  /*CPデ-タ獲得中で使用できません Ver.1.3*/
#define	ERR_FIFO_MAX	-1064	  /*最大補間数越えました　Ver.1.3*/
#define	ERR_FIFO_ARC	-1065	  /*円，円弧の生成ができません　Ver.1.3*/
							/*=== Ver.1.3 ====*/
#define	COVERS1			-1070	  /*Ｓ１軸 速度制御 角度オ−バ*/
#define	COVERS2    		-1071	  /*Ｓ２軸 速度制御 角度オ−バ*/
#define	COVERS3    		-1072	  /*Ｓ３軸 速度制御 角度オ−バ*/
#define	COVERE1    		-1073	  /*Ｅ１軸 速度制御 角度オ−バ*/
#define	COVERE2    		-1074	  /*Ｅ２軸 速度制御 角度オ−バ*/
#define	COVERW1    		-1075	  /*Ｗ１軸 速度制御 角度オ−バ*/
#define	COVERW2     	-1076	  /*Ｗ２軸 速度制御 角度オ−バ*/
							/*===============*/

							/*=== Ver.PCI ====*/
#define	ERR_MIS_VAL    	-1080	  /*設定値が大きすぎるかまたは小さすぎます*/
#define	ERR_PNT_APP    	-1081	  /*各軸制御ではアプローチできません*/

#define	ERR_PLY_FOR    	-1098	  /*教示モード中に連続運転はできません*/
#define	ERR_PLY_MOD    	-1099	  /*外部操作で教示モードに切り替わりました*/
#define	ERR_USE_TCH    	-1100	  /*教示モード以外でティーチロックをONできません*/
#define	ERR_ACT_DAT    	-1101	  /*指定されたKeyの教示データがありません*/
#define	ERR_CHG_KEY    	-1103	  /*教示データのKey検索できませんでした*/

#define	ERR_CUB_NUM    	-1200	  /*干渉領域指定番号エラー*/
#define	ERR_CUB_LEN    	-1201	  /*このキューブ情報には辺長さの指定ができません。別のキューブ属性を持っています。*/
#define	ERR_CUB_MAX    	-1202	  /*このキューブ情報には上限値教示できません。別のキューブ属性を持っています。*/
#define	ERR_CUB_MIN    	-1203	  /*このキューブ情報には下限値教示できません。別のキューブ属性を持っています。*/
#define	ERR_CUB_CTR    	-1205	  /*このキューブ情報には中心値教示できません。別のキューブ属性を持っています。*/
#define	ERR_CUB_PRM    	-1206	  /*キューブ設定パラメータ不明です。*/
#define	ERR_CUB_SET    	-1207	  /*このキューブ情報には設定できません。別のキューブ属性を持っています。*/

#define	ERR_PLY_KEY    	-1249	  /*Key獲得時に指定した番号が誤っています*/

#define	ERR_NON_KEY    	-1250	  /*Keyで指定された教示データ内に指定されたID属性はありません*/
#define	ERR_NON_CID    	-1251	  /*指定された教示点はJUMP情報を持っていません*/
#define	ERR_JMP_SET    	-1252	  /*Keyで指定された教示データはその番号のJUMP情報を持っていません*/
#define	ERR_NON_IDN    	-1253	  /*ID属性で指示された教示点はJUMP情報を持っていません*/
#define	ERR_JMP_NUM    	-1254	  /*教示点属性に指定されているJUMP情報を発見できません*/
#define	ERR_JMP_ATR    	-1255	  /*JUMP情報獲得・設定時の指定パラメータに誤りがあります*/
#define	ERR_KEY_ATR    	-1256	  /*JUMP情報獲得・設定時の指定パラメータに誤りがあります*/

#define	ERR_SOC_TST    	-1300	  /*ソケットの生成に失敗*/
#define	ERR_BND_TST    	-1311	  /*ソケットとアドレスのバインドに失敗*/
#define	ERR_LSN_TST    	-1312	  /*リッスンに失敗*/
#define	ERR_APT_TST    	-1313	  /*アクセプトに失敗*/
#define	ERR_SOC_SND    	-1314	  /*ソケット送信失敗*/
#define ERR_SOC_BLK		-1315	  /*未使用*/
#define ERR_SOC_CLT		-1316	  /*接続クライアントが多すぎます*/
							/*===============*/
#define ERR_PRM_DEV		-1350	  /*パラメータの動作速度が速度リミット値を超えています。パラメータ変更は無効です。*/


/*	・運転継続不能エラー　−−＞　（ブレーキ停止状態）*/

#define	ERR_OVER900		-2017	/*動作中にア−ム長がRMRC動作限界長をオ−バしました*/

#define	ERR_S1_AGL		-2020	  /*Ｓ１軸角度オ−バ*/
#define	ERR_S2_AGL		-2021	  /*Ｓ２軸角度オ−バ*/
#define	ERR_S3_AGL		-2022	  /*Ｓ３軸角度オ−バ*/
#define	ERR_E1_AGL		-2023	  /*Ｅ１軸角度オ−バ*/
#define	ERR_E2_AGL		-2024	  /*Ｅ２軸角度オ−バ*/
#define	ERR_W1_AGL		-2025	  /*Ｗ１軸角度オ−バ*/
#define	ERR_W2_AGL		-2026	  /*Ｗ２軸角度オ−バ*/
							/*=== Ver.1.3 ====*/
#define	DOVERS1			-2030	  /*Ｓ１軸 ダイレクト制御 角度オ−バ*/
#define	DOVERS2    		-2031	  /*Ｓ２軸 ダイレクト制御 角度オ−バ*/
#define	DOVERS3    		-2032	  /*Ｓ３軸 ダイレクト制御 角度オ−バ*/
#define	DOVERE1    		-2033	  /*Ｅ１軸 ダイレクト制御 角度オ−バ*/
#define	DOVERE2    		-2034	  /*Ｅ２軸 ダイレクト制御 角度オ−バ*/
#define	DOVERW1    		-2035	  /*Ｗ１軸 ダイレクト制御 角度オ−バ*/
#define	DOVERW2     	-2036	  /*Ｗ２軸 ダイレクト制御 角度オ−バ*/
							/*===============*/
#define	ERR_CANT_MOVE	-2051	/*現在位置ではＲＭＲＣ制御に入れません*/

#define	ERR_S1_REZ		-2060	  /*Ｓ１レゾルバ偏差異常*/
#define	ERR_S2_REZ		-2061	  /*Ｓ２レゾルバ偏差異常*/
#define	ERR_S3_REZ		-2062	  /*Ｓ３レゾルバ偏差異常*/
#define	ERR_E1_REZ		-2063	  /*Ｅ１レゾルバ偏差異常*/
#define	ERR_E2_REZ		-2064	  /*Ｅ２レゾルバ偏差異常*/
#define	ERR_W1_REZ		-2065	  /*Ｗ１レゾルバ偏差異常*/
#define	ERR_W2_REZ		-2066	  /*Ｗ２レゾルバ偏差異常*/
/*
	＜注＞レゾルバ偏差異常とは，前回入力したレゾルバ値と今回入力した値が許容
　　	　　　範囲以上になった場合。（読みこぼしの疑いあり）
*/
#define	ERR_TIMEOUT		-2070	  /*監視時間オ-バで自動停止しました Ver.1.3*/
#define	ERR_SYNCOUT		-2071	  /*目標値まで到達しませんでした 　Ver.1.3*/

#define	ERR_SYNC_S1		-2080	  /*各軸制御におけるＳ１軸同期異常*/
#define	ERR_SYNC_S2		-2081	  /*各軸制御におけるＳ２軸同期異常*/
#define	ERR_SYNC_S3		-2082	  /*各軸制御におけるＳ３軸同期異常*/
#define	ERR_SYNC_E1		-2083	  /*各軸制御におけるＥ１軸同期異常*/
#define	ERR_SYNC_E2		-2084	  /*各軸制御におけるＥ２軸同期異常*/
#define	ERR_SYNC_W1		-2085	  /*各軸制御におけるＷ１軸同期異常*/
#define	ERR_SYNC_W2		-2086	  /*各軸制御におけるＷ２軸同期異常*/
#define	ERR_RMRC_X		-2087	  /*ＲＭＲＣ制御におけるＸ軸同期異常*/
#define	ERR_RMRC_Y		-2088	  /*ＲＭＲＣ制御におけるＹ軸同期異常*/
#define	ERR_RMRC_Z		-2089	  /*ＲＭＲＣ制御におけるＺ軸同期異常*/
#define	ERR_VELOCITY	-2090	  /*速度偏差異常*/
#define	ERR_RMRC_YPR	-2091	  /*ＲＭＲＣ制御における手先姿勢偏差異常*/

#define	ERR_CUB_INN		-2100	  /*キューブに干渉しましたVer.PCI*/

#define	ERR_ARM_ERR0	-2200	  /*アーム特異点で動作開始又は継続できませんVer.PCI*/
#define	ERR_ARM_ERR1	-2201	  /*アーム特異点で動作開始又は継続できませんVer.PCI*/
#define	ERR_ARM_ERR2	-2202	  /*アーム特異点で動作開始又は継続できませんVer.PCI*/
/*
	＜注＞同期異常とは，目標値と現在値の偏差が許容範囲以上になった場合。
	　　　（アームが動いていない，又はかなり遅れている）
*/

/*	・致命的エラー　−−＞　（制御停止状態）*/
	
#define	ERR_POWER_ON	-3000	/*制御が開始されていません*/
/*
	＜注＞致命的エラーが起こった後に，制御開始コマンドを発行せずに，他の
	　　　制御コマンドを発行するとこのエラーが発生する。
*/

#define ERR_EM_CTL		-3001	  /*非常停止が押されています。*/
#define	ERR_ARC_SEND	-3002	  /*アークネット通信異常*/
#define	ERR_S1X_LIM		-3003	  /*S1軸のリミットスイッチがONです*/
#define	ERR_DRV_TYP		-3005	  /*サーボドライバのタイプがパラメータ指定と異なっているVre.PCI*/
#define	ERR_FORCE_ON	-3010	  /*力制御中でない	Ver.1.3 */

#define	ERR_DDD_STA		-3070	  /*通信統括サーボ（マスタ）ステータス異常*/
#define	ERR_D11_STA		-3071	  /*サーボドライバ（Ｓ１）ステータス異常*/
#define	ERR_D12_STA		-3072	  /*サーボドライバ（Ｓ２）ステータス異常*/
#define	ERR_D21_STA		-3073	  /*サーボドライバ（Ｓ３）ステータス異常*/
#define	ERR_D22_STA		-3074	  /*サーボドライバ（Ｅ１）ステータス異常*/
#define	ERR_D31_STA		-3075	  /*サーボドライバ（Ｅ２）ステータス異常*/
#define	ERR_D32_STA		-3076	  /*サーボドライバ（Ｗ１）ステータス異常*/
#define	ERR_D41_STA		-3077	  /*サーボドライバ（Ｗ２）ステータス異常*/

#define	ERR_S_SUSPD		-3091	  /*制御（通信）開始コマンド発行時異常*/
#define	ERR_E_SUSPD		-3092	  /*制御（通信）終了コマンド発行時異常*/
#define	ERR_I_SUSPD		-3093	  /*初期化コマンド発行時異常*/

#define	ERR_MOD_CTL		-4000	  /*モード管理異常*/
/*
	＜注＞制御コマンド発行時異常とは，サーボドライバに対しコマンドを発行後
	　　　応答（返信）がある時間内に無かった場合。
	　　　（サーボドライバが異常である。）
*/


/*サーボドライバエラー番号*/

#define	DRV_MEM_ERR	 1	  /*共有メモリ異常*/
#define	EEP_ROM_ERR	 2	  /*EEPROM異常*/
#define	CPU_XXX_ERR	 3	  /*CPU異常*/
#define	ARC_NET_ERR	 4	  /*通信周期異常*/
#define	VEL_SPN_ERR	 5	  /*速度偏差異常*/
#define	REZ_SPN_ERR	 6	  /*レゾルバ偏差異常*/
#define	VEL_LIM_ERR	 7	  /*位置リミット異常*/
#define	MTR_TRQ_ERR	 8	  /*モータトルク異常*/
#define	IPM_XXX_ERR	 9	  /*IPM異常*/
#define	BRK_XXX_ERR	 10	  /*ブレーキ断線*/
#define	REZ_001_ERR	 11	  /*レゾルバ（ギア）断線*/
#define	REZ_002_ERR	 12	  /*レゾルバ（モータ）断線*/
#define	OVR_TRQ_ERR	 13	  /*過電流*/
#define	OVR_VEL_ERR	 14	  /*過速度*/
#define	DMS_XXX_ERR	 15	  /*デッドマンSW異常*/
#define	CPU_NON_ERR	 16	  /*他CPU異常*/

#endif


