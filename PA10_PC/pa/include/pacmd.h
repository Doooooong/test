#ifndef	_PACMD_H
#define	_PACMD_H

/**
 **	HOST EVENT number description.
 **/
#define	PowerOnEvent		1	/** DSP通信開始 **/
#define	PowerOffEvent		2	/** DSP通信終了 **/
#define	StopEvent		3	/** ブレ−キ停止 **/
#define	AutoStopEvent		4	/** 自動終了 **/
#define	EV_LOD_ROB		5
#define EV_SAV_ROB		6
#define	EV_GET_ROB		7
#if 0
#define	SelfCheckEvent		5	/** < 未使用 > **/
#define	InputCheckEvent		6	/** < 未使用 > **/
#define	OutputCheckEvent	7	/** < 未使用 > **/
#endif
#define	ParamSetEvent		8	/** < 未使用 > **/

#define	EachAngleEvent		9	/** 各軸角度制御 **/
#define	EachVelocityEvent	10	/** 各軸速度制御 **/
#define	CurrentEvent		11	/** 電流値制御モ−ド **/
#define	DeadWeightEvent		12	/** 自重補償制御モ−ド **/
#define	RMRCStartEvent1		131	/** ロボット座標位置偏差制御 **/ 
#define	RMRCStartEvent2		132	/** 手先座標位置偏差制御 **/
#define	RMRCStartEvent3		133	/** ロボット座標速度制御 **/
#define	RMRCStartEvent4		134	/** 手先座標速度制御 **/
#define	RMRCStartEvent5		135	/** 手先座標姿勢速度制御 **/ 
#define	RMRCStartEvent6		136	/** 手先座標姿勢偏差制御 **/
#define	RMRCStartEvent7		137	/** 冗長軸操作制御 **/
#define	RMRCStartEvent8		138	/** 目標値御任せ制御 **/
#define	RMRCStartEvent9		139	/** ロボット座標姿勢偏差制御 **/

#define	RMRCStartEvent10	140	/** ロボット座標姿勢速度制御 **/
#define	RMRCStartEvent11	141	/** 絶対位置指定制御 **/
#define	RMRCStartEvent12	142	/** 絶対姿勢指定制御 **/
#define	RMRCStartEvent13	143	/** 絶対位置、姿勢指定制御 **/
#define	RMRCStartEvent14	144	/** 冗長軸角度制御 Ver.1.3 **/
#define	RMRCStartEvent15	145	/** 冗長軸速度制御 Ver.1.3 **/
#define	RMRCStartEvent16	146	/** ロボット位置姿勢速度 Ver.1.3 **/
#define	RMRCStartEvent17	147	/** 手先位置姿勢速度 Ver.1.3 **/
#define	RMRCStartEvent18	148	/** 予備 **/

#define	PBTOPStartEvent		14	/** プレイバック開始(順方向連続) **/
#define	PlayBackStartEvent	14	/** プレイバック開始(順方向連続)**/
#define	ProgramCPEvent		15	/** <内部発生イベント> **/
#define	MakeCircleEvent		16	/** <　　　〃　　　　> **/
#define	MakeArcEvent		17	/** <　　　〃　　　　> **/
#define	MakeLineEvent		18	/** <　　　〃　　　　> **/

#define	OverDataEvent		19	/** プレイバックデ−タ終了 **/
#define	TopPointerEvent		20	/** ポインタ移動トップ **/
#define MakeAxisEvent       	21      /** < 未使用 > **/
#define	ForeStepEvent		22	/** プレイバック開始(順方向STEP)**/
#define	BackStepEvent		23	/** プレイバック開始(逆方向STEP)**/
#define	ChVelocityEvent		24	/** プレイバック速度変更 **/
#define	PouseEvent		25	/** 一時停止 **/
#define	RestartEvent		26	/** 一時停止解除 **/
#define	DirectStartEvent	27	/** 電流値読込モ−ド **/
#define	ReadCPEvent		28	/** ＣＰ読込 **/
#define	ReadPTPEvent		29	/** ＰＴＰ読込 **/
#define	ErrorLevel1Event	30	/** 致命的でないエラ−発生 **/
#define	DirectStopEvent		31	/** 自重補償より復帰 **/
#define	ErrorLevel2Event	32	/** 致命的エラ−の発生 **/
#define	PlayBackReadyEvent	33	/** ポインタとロボットの軸値合わせ **/
#define	PlayBackReadyEvent2	44	/** ポインタとロボットの軸値合わせ **/
#define	CancelEvent		45
#define	ChVptnEvent		46	/** 教示データ速度パターン変更 **/
#define	ChRotateEvent		47	/** 教示データ回転速度変更 **/
#define	EACHVelocityEvent	48	/** 各軸デフォルト速度変更 **/
#define	RMRCVelocityEvent	49	/** rmrcデフォルト速度変更 **/

#define	PBENDStartEvent		34	/** プレイバック開始(逆方向連続) **/
#define	EndPointerEvent		35	/** ポインタ移動ボトム **/
#define	DeletePointerEvent	36	/** 現在の次のポインタ消去 **/
#define	DeleteAllPointerEvent	37	/** 全ポインタの消去 **/
#define	DataLoad		40	/** パソコンからデ−タのロ−ド **/
#define	DataSave		41	/** パソコンへデ−タをセ−ブ **/
#define	GetPntAttrEvent		42	/** カレント教示点デ-タ獲得**/

#define	InitDSPEvent		50	/** DSPデ−タ専用コ−ド **/
#define	ChWaitTimeEvent		51	/** 待ち時間の変更 **/
#define	ReadCurrentEvent	52	/** 電流値読込 **/
#define	JouCtrlEvent		53	/** 冗長軸制御のON/OFF **/
#define	JouParamResetEvent	54	/** 冗長軸パラメータのリセット **/
#define	RMRCRotateEvent		55	/** rmrcデフォルト回転速度変更 **/
#define	SetToolEvent		56	/** ツ-ルパラメ-タ ノ ヘンコウ **/
#define	EV_DEF_HOM		57	/** 基本姿勢設定 **/
#define	EV_DEF_ESC		58	/** 退避点設定 **/
#define	EV_DEF_SAF		59	/** その他点設定 **/

#define	EV_AXS_HOM		60	/** 基本姿勢復帰 **/
#define	EV_AXS_ESC		61	/** 退避点復帰 **/
#define	EV_AXS_SAF		62	/** その他点復帰 **/
#define	EV_GET_HOM		601	/** 基本姿勢デ−タ読込 **/
#define	EV_GET_ESC		611	/** 退避点デタ読込 **/
#define	EV_GET_SAF		621	/** その他点デ−タ読込 **/
#define	ReadReplaceEvent	63	/** ポインタデ−タリプレイス **/
#define	ReadPTPEvent2		64	/** PTPデ−タ挿入 **/
#define	ReadCPEvent2		65	/** PTPデ−タ挿入 **/
#define	SForeStepEvent		66	/** ポインタ順送り **/
#define	SBackStepEvent		67	/** ポインタ逆送り **/
#define	JumpPointerEvent	68	/** 指定ポインタへのジャンプ **/
#define	DeleteCPointerEvent	69	/** 現在のポインタの消去 **/

#define	ArcSerchEvent		70	/** （円弧）軌道ポインタを捜す **/
#define	ReadArc1Event		701	/** （円孤）第1番ポインタを教示 **/
#define	ReadArc2Event		702	/** （円弧）第2番ポインタを教示 **/
#define	ReadArc3Event		703	/** （円弧）第3番ポインタを教示 **/
#define	ReadArc1Event2		704	/** （円孤）第1番ポインタを教示 **/
#define	ReadArc2Event2		705	/** （円弧）第2番ポインタを教示 **/
#define	ReadArc3Event2		706	/** （円弧）第3番ポインタを教示 **/
#define	CircleSerchEvent	71	/** （円）　軌道ポインタを捜す **/
#define	ReadCircle1Event	711	/** （円）　第1番ポインタを教示 **/
#define	ReadCircle2Event	712	/** （円）　第2番ポインタを教示 **/
#define	ReadCircle3Event	713	/** （円）　第3番ポインタを教示 **/
#define	ReadCircle1Event2	714	/** （円）　第1番ポインタを教示 **/
#define	ReadCircle2Event2	715	/** （円）　第2番ポインタを教示 **/
#define	ReadCircle3Event2	716	/** （円）　第3番ポインタを教示 **/
#define	PBVelocityEvent		72	/** プレイバック速度係数変更 **/

#define	SendMatrixEvent		80	/** 工具座標変換マトリックスのロード **/
#define	SendJSEvent		81	/** 速度指令値のロード **/
#define	SendNOAPEvent		82	/** 目標ＮＯＡＰのロード **/
#define	SendFCEvent		83	/** 目標ＮＯＡＰのロード **/
#define	SendANGLEEvent		84	/** 目標各軸のロード **/

#define	EachAngleEvent7		99
#define	ErrorResetEvent		100

#define	EV_ENA_NOM		200	/* RETRAC計算切り替え */

///////////////////////////////////
//	追加PCI
#define	EV_ACT_PNT	210	/** アクティブな教示データの切換え **/
#define	EV_CHG_KEY	211	/** 現在アクティブな教示データKeyを変更 **/
#define	EV_SET_CMT	74	/** カレント教示点のコメント変更 **/
#define	EV_SET_PRJ	214	/** 現在のプロジェクト名設定 **/
#define	EV_GET_PRJ	215	/** 現在のプロジェクト名取得 **/
#define	EV_GET_JMP	216	/** 教示データKey＋IDからJUMP情報取得 **/
#define	EV_DIS_MSK	96	/** DI MASK設定 運動制御が2台のアームに対応しているため必要 **/
#define	EV_DOS_MSK	95	/** DO MASK設定 運動制御が2台のアームに対応しているため必要 **/
#define	EV_SET_DIO	94	/** DOの設定 **/
#define	EV_SET_CUB	230	/** CUBEの指定 **/
#define	EV_GET_CUB	231	/** CUBEの教示指定 **/
#define	EV_CUB_LEN	232	/** CUBEの辺長さ指定 **/
#define	EV_CUB_CMT	233	/** CUBEに名前をつける **/
#define	EV_DEL_CUB	234	/** CUBEの削除 **/
#define	EV_ENA_CUB	235	/** CUBEの有効/無効 **/
#define	EV_INF_CUB	236	/** CUBE情報参照 **/
#define	EV_PLY_SET	240	/** 番号指定で教示データのKeyを獲得 **/
#define	EV_JMP_SET	241	/** 番号指定でJMP情報獲得 **/
#define	EV_ENA_JMP	242	/** JMP情報有効/無効 **/
#define	EV_SET_JMP	243	/** JMP情報設定 **/
#define	EV_PLY_MOD	244	/** 教示モード設定 **/
#define	EV_AXS_ACC	311	/** 各軸属性の精度変更 **/
#define	EV_RMC_ACC	310	/** RMRC属性の精度変更 **/
#define	EV_PNT_JMP	312	/** JUMP条件番号変更 **/
#define	EV_TST_NOM	201	/** RETRACパラメータ生成モードON/OFF **/
#define	EV_JMP_CMT	212	/** コメント指定によるカレントポイント変更 **/
#define	EV_SET_DDM	202	/** デッドマンスイッチの有効/無効 **/
#define	EV_DEL_JMP	313	/** JUMP情報の削除 **/
#define EV_TCH_LOK	245	/** TEACH_LOCKの設定 **/

///////////////////////////////////

#define	iStopEvent	203	/** <内部発生イベント> **/
#define	iAutoStopEvent	204	/** 自動終了 **/
#define	iPouseEvent	225 	/** <　　　〃　　　　> **/
#define	iOverDataEvent	219 	/** <　　　〃　　　　> **/

#define	LoadParamEvent		993	/** デフォルトパラメ-タ ロ-ド **/
#define	SaveParamEvent		994	/** デフォルトパラメ-タ セ-ブ **/
#define	SetDateEvent		995	/** 年月日の設定 **/
#define	SetTimeEvent		996	/** 時間の設定 **/
#define	ResetErrLogEvent	997	/** エラーログのリセット **/
#define	GetErrLogEvent		998	/** エラーログの内容を４ポートへ **/
#define	RunningTestEvent	999

/*======== ＜＜Ver. 1.3 追加イベント＞＞ =============================*/
	/* 各軸プレイバック制御，その他プレイバック関係追加に伴うイベントの宣言	*/
#define ReadPTPEvent3        43 /** 各軸属性教示点の生成 **/
#define ReadPTPEvent4        73 /** 各軸属性の教示点デ−タ挿入 **/
#define JouCtrlEvent2        74 /** 冗長軸制御のON/OFF(未使用) **/
#define PlayBackReadyEvent3  75 /** 各軸プレイバック移動 **/
#define GetPntAttrEvent2     76 /** 円，円弧教示デ−タ取得 **/
#define	ReadPTPEvent5        78
#define	ReadPTPEvent6        79

#define	EachAngleEvent2	     77	/** 各軸角度制御リアルモ-ド **/
#define PBMOVEmtxEvent       90 /**  **/
#define SetDWDIOEvent        91 /** 教示点(CP)のDOデ-タ属性を設定 **/
#define SetDirectionEvent    92 /** 自重補償ア-ムの据え付け姿勢 **/
#define SetDIOLockEvent	     93 /** プレイバックのDIOの自動停止有効/無効 **/

	/* シミュレ-ション制御追加に伴うイベントの宣言	*/
#define SimOnEvent          301 /** シミュレーション開始 **/
#define SimOffEvent         302 /** シミュレーション終了 **/

	/* ダイレクト制御追加に伴うイベントの宣言 	*/
#define DIOEnableEvent      400
#define DIODisableEvent     401
#define ResetTimeOutEvent   102 /** 486との同期処理 **/
#define SetTimeOutEvent	    103 /** タイムアウト時間設定  **/

	/* 位置オフセット，その他イベントの宣言	*/
#define DWParamSetEvent     410 /** 先端ウィ-ビングパラメ-タの設定 **/
#define DioOnEvent          500 /** 教示点DIOデータ有効 **/
#define DioOffEvent         501 /** 教示点DIOデータ無効 **/
#define SetDIOEvent         503 /** 教示点DIO属性変更 **/
#define SetIDEvent          504 /** 教示点ID属性変更 **/
#define SendwaveEvent       510 /**  **/
#define SendsensorEvent     511 /**  **/
#define SendSENSOREvent     512 /**  **/
#define SendWAVEEvent       513 /**  **/
#define SendOffsetEvent     514 /** 先端位置オフセットの設定 **/
#define ResetOffsetEvent    515 /** 先端位置オフセットのリセット **/
#define SetOffsetVelEvent   516 /** 先端位置オフセットのリミット速度の設定 **/

#define	SetSimTimeEvent		105	/** シミュレーション倍率設定	**/
#define	SetDefIncEvent		106	/** リアルタイム速度変更		**/

#define	Axis8OnEvent		1003
#define	Axis8OffEvent		1004
#define	Axis8SimOnEvent		1005
#define	Axis8SimOffEvent	1006
/**
 **	HOST STATUS number description.
 **/
#define	NoStatus		1	/** 未使用 **/
#define	Initial			2	/** 未使用 **/
#define	BrakeStop		3	/** ブレ−キ停止状態 **/
#define	SelfCheck		4	/** 未使用 **/
#define	InputCheck		5	/** 未使用 **/
#define	OutputCheck		6	/** 未使用 **/
#define	ParamSet		7	/** 未使用 **/
#define	EachAngle		8	/** 各軸角度制御状態 **/
#define	EachVelocity		9	/** 各軸速度制御状態 **/
#define	Direct			10	/** サ−ボロック状態 **/
#define	Current			11	/** 簡易自重補償状態 **/
#define	DeadWeight		12	/** 自重補償状態 **/
#define	RMRC			13	/** RMRC制御状態 **/
#define	RMRCMatching		14	/** RMRC冗長軸補正状態 **/
#define	PouseA			15	/** 各軸制御サ−ボロック状態 **/
#define	Program			16	/** CPデ−タプレイバック状態 **/
#define	ProgramMatching		17	/** 各軸軸位置補正状態 **/
#define	MakeCircle		18	/** 円補間 **/
#define	MakeLine		19	/** 直線補間 **/
#define	MakeArc			20	/** 円弧補間 **/
#define	PouseB			21	/** RMRC制御サ−ボロック状態 **/
#define	Ready			22	/** プレイバック開始待状態 **/
/**		       			(23)	Reserved 		*/
/**		       			(24) 	Reserved		*/
#define	Ready2			25	/** プレイバック開始待状態 **/
#define	ToReady			26	/** プレイバック開始待状態 **/
#define	RMRCsub			27	/** 冗長軸動作可能状態 **/

/*======== ＜＜Ver. 1.3 追加状態＞＞ ==================================*/
#define	RMRCreal		28	/** 冗長軸含むリアルモード **/
#define	MakeAxis		29	/** プレイバック各軸補間角度合わせ **/
#define	RMRCmtx			30	/** 座標変換された位置姿勢へ移動する中間状態 **/
#define	RMRCsub2		31	/** 冗長軸動作可能状態（Ｓ３軸補間） **/
#define	AXISreal		32	/** 各軸リアルモード **/
#define	RMRCPlyMove		33	/** 教示データ間移動（RMRC） **/
#define	AXISPlyMove		34	/** 教示データ間移動（各軸） **/
#endif
