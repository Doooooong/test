/*****************************************************************************/
/*                                                                           */
/*  FILE        :papci.h	                                             */
/*  DESCRIPTION :PA-10(6C/7C) motion control board  Linux Driver header file */
/*  history:                                                                 */
/*     v1.0.0:  2002.11.30 by m.togawa 		                             */
/*        original issue                                                     */
/*****************************************************************************/

//ioctl code
#define PA_WRITE_PCI		0x4001
#define PA_READ_PCI		0x4002
#define PA_INTR_PCI		0x4003

typedef struct {
	u32	arm;
	u32	addr;
	u32	size;
	unsigned char	data[256];
} PA_ARG;

#define NODEVICE 0

typedef struct _PCI_CONFIG_DATA {
	unsigned short	VendorID;
	unsigned short	DeviceID;
	unsigned char	BusNumber;
	unsigned char	SlotNumber;
	unsigned char	FuncNumber;
	unsigned char	BoardNo;
	u32				PortAdr;
	unsigned char	*mem;
	unsigned char	*mem_top;
	unsigned char	*intr; 	
	unsigned char	*intr_top; 	
} PCI_CONFIG_DATA, *PPCI_CONFIG_DATA;
