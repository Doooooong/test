This folder and its files is only for handle comile error.

1. pa library is designed to use with your program, not for installing to system wide.
2. I want install pa library to system wide for simpling RTC_PA10 project.
3. In system wide, for easy finding include files of pa library, I install them to /usr/local/include/pa10 folder.
4. But include file [pammc.h], it has [#include <pactl.h>], and it can't compile when you use it in RTC_PA10, so I change it to [#include <pa10/pactl.h>]
5. But, this change make another problem: Got include error when compile pa library.
6. Handle the problem above, I create this folder and COPY pactl.h to here.
