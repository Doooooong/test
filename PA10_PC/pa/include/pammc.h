#ifndef	_PAMMC_H
#define	_PAMMC_H

#include	<pa10/pactl.h>

/*******************************************
 *	Servo Driver Parameter input structure
********************************************/

typedef struct {			/*�ᥫ��������¬*/
	unsigned char	stat;			/*set,def,reset*/
	long			angle0;		/*1���ܳ���*/
	long			angle1;		/*2���ܳ���*/
}MechaZero, *MechaZeroPtr;

typedef struct {
	MechaZero	dsp0;
	MechaZero	dsp1;
	MechaZero	dsp2;
	MechaZero	dsp3;
}MechaZeroDSP, *MechaZeroDSPPtr;

typedef struct {			/*®��PIGain*/
	short		pGain;
	short		iGain;
}PIGain, *PIGainPtr;

typedef struct {
	PIGain		axis0;
	PIGain		axis1;
}SpeedPIGain, *SpeedPIGainPtr;

typedef struct {
	SpeedPIGain	dsp0;
	SpeedPIGain	dsp1;
	SpeedPIGain	dsp2;
	SpeedPIGain	dsp3;
}SpeedPIGainDSP, *SpeedPIGainDSPPtr;

typedef struct {
	long		plus;
	long		minus;
}PosLmt, *PosLmtPtr;

typedef struct {	/*�⡼����¦��ߥå�*/
	PosLmt		angle0;
	PosLmt		angle1;
}MotorPosLmt, *MotorPosLmtPtr;

typedef struct {
	MotorPosLmt	dsp0;
	MotorPosLmt	dsp1;
	MotorPosLmt	dsp2;
	MotorPosLmt	dsp3;
}MotorPosLmtDSP, *MotorPosLmtDSPPtr;


typedef struct {	/*�쥾��Х��ե��å�*/
	short	rezoff;
}RezoOffset, *RezoOffsetPtr;

typedef struct {	/*�쥾��Х��ե��å� 2��ʬ*/
	RezoOffset	angle0;
	RezoOffset	angle1;
}RezoOffsets, *RezoOffsetsPtr;

typedef struct {
	RezoOffsets	dsp0;
	RezoOffsets	dsp1;
	RezoOffsets	dsp2;
	RezoOffsets	dsp3;
}RezoOffsetDSP, *RezoOffsetDSPPtr;


typedef struct {	/*DSP�ѥ�᡼��ͽ��*/
	long		reserve[7];
}Reserve, *ReservePtr;

typedef unsigned long	FileID;		/* �ե����뼱��ID */
#define	_DRV_PRM	(FileID)0x0729
#define	_CTL_PRM	(FileID)0x0512
#define	_PNT_DAT	(FileID)0x1965

/* �ѥ�᡼�����쥳���ɿ���*/
#define COMMENTMAX 38
#define MAXPATH1 12

typedef struct {
	unsigned short	id;
	char path[MAXPATH1+1];		/*filename*/
	char comment[COMMENTMAX+1];	/*comment*/
}File, *FilePtr;

/*
	�ɥ饤�Хѥ��-��(�ѥ�᡼���ե�����쥳���ɤȤ��Ƥ����)
*/
typedef struct {
	MechaZeroDSP	mecha_zero_dsp;		/*�ᥫ����		*/
	SpeedPIGainDSP	speed_PI_gain_dsp;	/*®��PI������		*/
	MotorPosLmtDSP	motor_pos_lmt_dsp;	/*�⡼����¦Lmt		*/
	RezoOffsetDSP	rezo_offset_dsp;	/*�쥾��Х��ե��å�	*/
	Reserve		reserv;			/*DSP parameter ͽ�� 	*/
}DspPara, *DspParaPtr;

//typedef PNTDAT Record;
//typedef PNTDATP RecordPtr;
typedef PLAY Record;
//typedef PLYP RecordPtr;
typedef NOAP	Record2;


#define	RETRY_COUNT	10			/*	�����ߥ�ȥ饤���*/
#define	MAX_ARM	16				/*	486�������դ����*/

extern COMUNIptr _comuni;		/* comunication aera pointer */
extern long _d14;				/* ��������¸�� */

static long pam_get_err(ARM armno)
{	
	long tmp = 0xCCCCCCCC;
#ifdef PAlan
	extern	long	_SOCKerr_[];
	if(_SOCKerr_[armno]<0)	return(_SOCKerr_[armno]);
#endif
	LockMutex((long)armno);
	pa_map_ctl(armno);
	tmp=pa_ret_lng( (long)armno,&(_comuni->hu.err.dmy[15]));
	UnlockMutex((long)armno);
	return(tmp);
}
#define	pam_exe_wfp(id,wfp)				\
	if(wfp==WM_WAIT){				\
		while(1){				\
			LockMutex(id);			\
			pa_map_ctl(id);			\
			if(!pa_fsh_chk(id)){		\
				UnlockMutex(id);	\
				break;			\
			}				\
			UnlockMutex(id);		\
		}					\
	}
/*
	 Unique I/O Add. to the XVME-688
*/
#define	CNT_REG1_PORT	0x30
#define	VME_HI_ADD_PORT	0x34
#define	CNT_REG2_PORT	0x36


#define	VME_WIN_ADD	0xe000		/*	VMEbus ref. Window ADD.	*/

#ifdef MHI5600
#define INT_OFFSET	0xfffc	/*��̽񤭹��ߴ�λ������offset���ɥ쥹*/
#else 
#define INT_OFFSET	0xfff8	/*��̽񤭹��ߴ�λ������offset���ɥ쥹*/
#endif

#endif
