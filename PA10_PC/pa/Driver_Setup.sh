#!/bin/bash
set -x
driver_root=$(cd $(dirname $0); pwd)

# Enter to papci driver directory.
cd $driver_root/src/lib/pci/driver
make clean
make

# Make storge directory for startup scripts and papci driver module.
mkdir ~/.startup_scripts/
sudo chattr -i ~/.startup_scripts/*
rm ~/.startup_scripts/*

# Copy files
cp ./install.sh ~/.startup_scripts/pa10_driver.sh
cp ./papci.ko ~/.startup_scripts/

# Add uneditable and undeletable attribute. And add executable property for start up script file.
sudo chattr +i ~/.startup_scripts/papci.ko
sudo chattr +i ~/.startup_scripts/pa10_driver.sh
sudo chmod +x ~/.startup_scripts/pa10_driver.sh

# Use crontab for startup script execution. Add startup_scripts.sh to crontab config file.
grep 'reboot root /home/pa10/.startup_scripts/pa10_driver.sh' /etc/crontab || sudo bash -c "echo \"@reboot root /home/pa10/.startup_scripts/pa10_driver.sh\" >> /etc/crontab"

# Make and install pa library to system.
cd $driver_root/src/lib/pci
make -f Makefile.so clean
make -f Makefile.so 
sudo make -f Makefile.so install_to_sys
# include/pammc.h #include <pactl.h>  change to   #include <pa10/pactl.h>

set +x
