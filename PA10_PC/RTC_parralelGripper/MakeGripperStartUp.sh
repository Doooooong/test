#!/bin/bash

sudo -S mv -i ./bash_aliases /etc/
sudo -S bash -c "echo -e \"if [ -f /etc/bash_aliases ]; then\n\t . /etc/bash_aliases \nfi\">>/etc/bash.bashrc"

sudo -S cp ./parraleGripper_* /etc/init.d/
sudo -S cp ./namingServer /etc/init.d/
cd /etc/init.d/
sudo chmod +x parraleGripper_*
sudo chmod +x namingServer

# Change "omniorb4-nameserver"'s name to let naming server not start at system start.
sudo mv /etc/rc5.d/S02omniorb4-nameserver /etc/rc5.d/K02omniorb4-nameserver  
sudo update-rc.d omniorb4-nameserver defaults  # Setup the /etc/init.d folder to apply the setting.

# Add the service into the PC's startup scripts sequence.
sudo update-rc.d parraleGripper_* defaults 90
sudo update-rc.d namingServer defaults 80

