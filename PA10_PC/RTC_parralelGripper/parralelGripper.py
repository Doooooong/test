#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
# -*- Python -*-

"""
 @file parralelGripper.py
 @brief ModuleDescription
 @date $Date$


"""
import sys
import time
sys.path.append(".")

# Import RTM module
import RTC
import OpenRTM_aist

import serial

class Gripper(object):
 
	def __init__(self, port, baudrate, timeout=1):
		
		self.myserial = serial.Serial()
		self.open_port(port, baudrate, timeout)
 
		self.serial = serial
 
	def open_port(self, port='COM1', baudrate=115200, timeout=1):
		print (port)
		self.myserial.port = port
		self.myserial.baudrate = baudrate
		self.myserial.timeout = timeout
		self.myserial.parity = serial.PARITY_NONE
		try:
			self.myserial.open()
		except IOError:
			print('Failed to open port, check the device and port number')
			exit
		else:
			print('Succeede to open port: ' + port)
 
	def close_port(self):
		
		self.myserial.close()
 
	def set_port(self, baudrate=115200, timeout=0x01):
		
		self.myserial.baudrate = baudrate
		self.myserial.timeout = timeout
		self.myserial._reconfigurePort()
		print('Succeede to set baudrate:%d, timeout:%d' % (baudrate, timeout))
 
	def get_position(self):
		
		send = [0xFA,
			0xAF,
			0x04,
			0xAA,
			0xFF]
 
		self._write_command(send)
 		# print (self.read_data())
		return self.read_data()
 
	def set_measure(self, mode=0):
 
		send = [0xFA,
			0xAF,
			0x06,
			0xAA,
			0xFF]
 
		if mode == 1:
			send[2] = 0x05
 
		self._write_command(send)
		   
	def read_data(self):
 
		while 1:
			header = ord(self.myserial.read(1))
			if header == 0xFA:
			
				header = ord(self.myserial.read(1))
				if header == 0xAF:
 
					receive = map(ord, self.myserial.read(4))
 
					if receive[2] == 0xAA and receive[3] == 0xFF:
 
						resistor = (receive[1] << 8) & 0x0000FF00 | (receive[0] & 0x000000FF)
 
						return resistor
 
	def open_gripper(self, pwm_period):
		self._check_range(pwm_period, 0, 255, "pwm_period")
		# power range 0~255
		
		send = [0xFA,
			0xAF,
			0x00,
			pwm_period,
			0xAA,
			0xFF]
		print("open Gripper")
		self._write_command(send)
 
	def close_gripper(self, pwm_period):
		self._check_range(pwm_period, 0, 255, "pwm_period")
		# power range 0~255
		
		send = [0xFA,
			0xAF,
			0x01,
			pwm_period,
			0xAA,
			0xFF]
		
		self._write_command(send)
		
	def free_gripper(self):
		
		send = [0xFA,
			0xAF,
			0x02,
			0xAA,
			0xFF]
		
		self._write_command(send)
                
	def servo_gripper(self, target_position, kp, kd, ki):
                
		self._check_range(target_position, 0, 32767, "target_position")
		self._check_range(kp, 0, 255, "kp")
		self._check_range(kd, 0, 255, "kd")
		self._check_range(ki, 0, 255, "ki")
		
		send = [0xFA,
			0xAF,
			0x03,
			target_position & 0x00FF,
			(target_position & 0xFF00) >> 8,
			kp,
			kd,
			ki,
			0xAA,
			0xFF]
		
		self._write_command(send)
	
	def _check_range(self, value, lower_range, upper_range, name='value'):
		if value < lower_range or value > upper_range:
			raise ValueError(name + ' must be set in the range from '
					 + str(lower_range) + ' to ' + str(upper_range))
 
	def _write_command(self, send):
		self.myserial.flushOutput()
		self.myserial.flushInput()
		try:
			port.write(bytes(bytearray(send)))
		except IOError:
			print("Error!!")           
 
 

# Import Service implementation class
# <rtc-template block="service_impl">

# </rtc-template>

# Import Service stub modules
# <rtc-template block="consumer_import">
# </rtc-template>


# This module's spesification
# <rtc-template block="module_spec">
parralelgripper_spec = ["implementation_id", "parralelGripper",
		 "type_name", "parralelGripper",
		 "description", "ModuleDescription",
		 "version", "1.0.0",
		 "vendor", "Keisuke Nagano",
		 "category", "Category",
		 "activity_type", "STATIC",
		 "max_instance", "1",
		 "language", "Python",
		 "lang_type", "SCRIPT",
		 ""]
# </rtc-template>

# #
# @class parralelGripper
# @brief ModuleDescription
# 
# 
class parralelGripper(OpenRTM_aist.DataFlowComponentBase):
	
	# #
	# @brief constructor
	# @param manager Maneger Object
	# 
	def __init__(self, manager):
		OpenRTM_aist.DataFlowComponentBase.__init__(self, manager)

# 		command_in_arg = [None] * ((len(RTC._d_TimedStringSeq) - 4) / 2)
# 		self._d_command_in = RTC.TimedStringSeq(*command_in_arg)
# 		"""
# 		"""
# 		self._command_inIn = OpenRTM_aist.InPort("command_in", self._d_command_in)
# 		
# 		AUX_in_arg = [None] * ((len(RTC._d_TimedStringSeq) - 4) / 2)
# 		self._d_AUX_in = RTC.TimedStringSeq(*AUX_in_arg)
# 		"""
# 		"""
# 		self._AUX_inIn = OpenRTM_aist.InPort("AUX_in", self._d_AUX_in)
# 		
# 		data_out_arg = [None] * ((len(RTC._d_TimedDoubleSeq) - 4) / 2)
# 		self._d_data_out = RTC.TimedDoubleSeq(*data_out_arg)
# 		"""
# 		"""
# 		self._data_outOut = OpenRTM_aist.OutPort("data_out", self._d_data_out)
# 		
# 		AUX_out_arg = [None] * ((len(RTC._d_TimedStringSeq) - 4) / 2)
# 		self._d_AUX_out = RTC.TimedStringSeq(*AUX_out_arg)
# 		"""
# 		"""
# 		self._AUX_outOut = OpenRTM_aist.OutPort("AUX_out", self._d_AUX_out)

		self._d_command_in = RTC.TimedStringSeq(RTC.Time(0, 0), [])
		self._command_inIn = OpenRTM_aist.InPort("command_in", self._d_command_in)
 
		self._d_AUX_in = RTC.TimedStringSeq(RTC.Time(0, 0), [])
		self._AUX_inIn = OpenRTM_aist.InPort("AUX_in", self._d_AUX_in)
 
		self._d_data_out = RTC.TimedDoubleSeq(RTC.Time(0, 0), [])
		self._data_outOut = OpenRTM_aist.OutPort("data_out", self._d_data_out)
		
		self._d_AUX_out = RTC.TimedStringSeq(RTC.Time(0, 0), [])
		self._AUX_outOut = OpenRTM_aist.OutPort("AUX_out", self._d_AUX_out)
		
		serialNumber_L = "101000000000000000000002F7F1A3F1"
		serialNumber_R = "101000000000000000000002F7F1AF19"
		
		if len(sys.argv) > 1:
			if sys.argv[1] == "-l":
				str_comPort = "/dev/serial/by-id/usb-mbed_Microcontroller_" + serialNumber_L + "-if01"  # str_comPort = "/dev/gripL"
			if sys.argv[1] == "-r":
				str_comPort = "/dev/serial/by-id/usb-mbed_Microcontroller_" + serialNumber_R + "-if01"  # str_comPort = "/dev/gripR"
		
		# left:COM3, right:COM4
		self.gripper = Gripper(str_comPort, 115200, 1)
		


		# initialize of configuration-data.
		# <rtc-template block="init_conf_param">
		
		# </rtc-template>


		 
	# #
	#
	# The initialize action (on CREATED->ALIVE transition)
	# formaer rtc_init_entry() 
	# 
	# @return RTC::ReturnCode_t
	# 
	#
	def onInitialize(self):
		# Bind variables and configuration variable
		
		# Set InPort buffers
		self.addInPort("command_in", self._command_inIn)
		self.addInPort("AUX_in", self._AUX_inIn)
		
		# Set OutPort buffers
		self.addOutPort("data_out", self._data_outOut)
		self.addOutPort("AUX_out", self._AUX_outOut)
		
		# Set service provider to Ports
		
		# Set service consumers to Ports
		
		# Set CORBA Service Ports
		self.gripper.close_gripper (120)
		time.sleep(1.0)
		self.gripper.open_gripper(120)
		time.sleep(1.0)
		self.gripper.free_gripper()
		print("Init")


		return RTC.RTC_OK
	
	# 	##
	# 	# 
	# 	# The finalize action (on ALIVE->END transition)
	# 	# formaer rtc_exiting_entry()
	# 	# 
	# 	# @return RTC::ReturnCode_t
	#
	# 	# 
	# def onFinalize(self):
	#
	# 	return RTC.RTC_OK
	
	# 	##
	# 	#
	# 	# The startup action when ExecutionContext startup
	# 	# former rtc_starting_entry()
	# 	# 
	# 	# @param ec_id target ExecutionContext Id
	# 	#
	# 	# @return RTC::ReturnCode_t
	# 	#
	# 	#
	# def onStartup(self, ec_id):
	#
	# 	return RTC.RTC_OK
	
	# 	##
	# 	#
	# 	# The shutdown action when ExecutionContext stop
	# 	# former rtc_stopping_entry()
	# 	#
	# 	# @param ec_id target ExecutionContext Id
	# 	#
	# 	# @return RTC::ReturnCode_t
	# 	#
	# 	#
	# def onShutdown(self, ec_id):
	#
	# 	return RTC.RTC_OK
	
	# 	##
	# 	#
	# 	# The activated action (Active state entry action)
	# 	# former rtc_active_entry()
	# 	#
	# 	# @param ec_id target ExecutionContext Id
	# 	# 
	# 	# @return RTC::ReturnCode_t
	# 	#
	# 	#
	# def onActivated(self, ec_id):
	#
	# 	return RTC.RTC_OK
	
	# 	##
	# 	#
	# 	# The deactivated action (Active state exit action)
	# 	# former rtc_active_exit()
	# 	#
	# 	# @param ec_id target ExecutionContext Id
	# 	#
	# 	# @return RTC::ReturnCode_t
	# 	#
	# 	#
	# def onDeactivated(self, ec_id):
	#
	# 	return RTC.RTC_OK
	
		# #
		#
		# The execution action that is invoked periodically
		# former rtc_active_do()
		#
		# @param ec_id target ExecutionContext Id
		#
		# @return RTC::ReturnCode_t
		#
		#
	def onExecute(self, ec_id):
		
		if self._command_inIn.isNew():
			print ("Gripper get new command!")
			for command in self._command_inIn.read().data:
				print ("command:\"", command, "\"")
				word = command.split(" ")
 				
			if word[0] == "open_gripper":
				self.gripper.open_gripper(int(word[1]))
				print ("open")
 
			if word[0] == "close_gripper":
				self.gripper.close_gripper(int(word[1]))
				print ("close")
                                        
			if word[0] == "free_gripper" :
				self.gripper.free_gripper()
				print ("free")
 
			if word[0] == "get_position" :
				print ("exe get position function ")
				ret = self.gripper.get_position()
#  				
				OpenRTM_aist.setTimestamp(self._d_data_out)
				self._d_data_out.data = [ret]
				print ("data:", self._d_data_out.data)
				
				self._data_outOut.write()
 
			if word[0] == "servo_gripper" :
				target = int(word[1])
				kp = int(word[2])
				kd = int(word[3])
				ki = int(word[4])
				self.gripper.servo_gripper(target, kp, kd, ki)
				print ("servo ", target, " ", kp, " ", kd, " ", ki )
				self._d_data_out.data = [0]
				self._data_outOut.write()

		return RTC.RTC_OK
	
	# 	##
	# 	#
	# 	# The aborting action when main logic error occurred.
	# 	# former rtc_aborting_entry()
	# 	#
	# 	# @param ec_id target ExecutionContext Id
	# 	#
	# 	# @return RTC::ReturnCode_t
	# 	#
	# 	#
	# def onAborting(self, ec_id):
	#
	# 	return RTC.RTC_OK
	
	# 	##
	# 	#
	# 	# The error action in ERROR state
	# 	# former rtc_error_do()
	# 	#
	# 	# @param ec_id target ExecutionContext Id
	# 	#
	# 	# @return RTC::ReturnCode_t
	# 	#
	# 	#
	# def onError(self, ec_id):
	#
	# 	return RTC.RTC_OK
	
	# 	##
	# 	#
	# 	# The reset action that is invoked resetting
	# 	# This is same but different the former rtc_init_entry()
	# 	#
	# 	# @param ec_id target ExecutionContext Id
	# 	#
	# 	# @return RTC::ReturnCode_t
	# 	#
	# 	#
	# def onReset(self, ec_id):
	#
	# 	return RTC.RTC_OK
	
	# 	##
	# 	#
	# 	# The state update action that is invoked after onExecute() action
	# 	# no corresponding operation exists in OpenRTm-aist-0.2.0
	# 	#
	# 	# @param ec_id target ExecutionContext Id
	# 	#
	# 	# @return RTC::ReturnCode_t
	# 	#

	# 	#
	# def onStateUpdate(self, ec_id):
	#
	# 	return RTC.RTC_OK
	
	# 	##
	# 	#
	# 	# The action that is invoked when execution context's rate is changed
	# 	# no corresponding operation exists in OpenRTm-aist-0.2.0
	# 	#
	# 	# @param ec_id target ExecutionContext Id
	# 	#
	# 	# @return RTC::ReturnCode_t
	# 	#
	# 	#
	# def onRateChanged(self, ec_id):
	#
	# 	return RTC.RTC_OK
	



def parralelGripperInit(manager):
    profile = OpenRTM_aist.Properties(defaults_str=parralelgripper_spec)
    manager.registerFactory(profile,
                            parralelGripper,
                            OpenRTM_aist.Delete)

def MyModuleInit(manager):
    parralelGripperInit(manager)

    # Create a component
    comp = manager.createComponent("parralelGripper")

def main():
	if(len(sys.argv) == 1):
		print ("Please excute this program with \"-l\" or \"-r\" parameter for initialize Gripper_L or Gripper_R.")
	if(sys.argv[1] == "-l"):
		initArgv=[sys.argv[0], '-f', 'rtc_l.conf']
	elif(sys.argv[1] == "-r"):
		initArgv=[sys.argv[0], '-f', 'rtc_r.conf']
	elif(sys.argv[1] == "-h" or sys.argv[1] == "--help"):
		print ("Please excute this program with \"-l\" or \"-r\" parameter for initialize Gripper_L or Gripper_R.")
	else:
		print ("Please excute this program with \"-l\" or \"-r\" parameter for initialize Gripper_L or Gripper_R.")
		print ("Error parameter!! Exit!!")
		exit()
	mgr = OpenRTM_aist.Manager.init(initArgv)
	mgr.setModuleInitProc(MyModuleInit)
	mgr.activateManager()
	mgr.runManager()

if __name__ == "__main__":
	main()

