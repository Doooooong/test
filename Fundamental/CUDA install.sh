#!/bin/bash

wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-ubuntu1804.pin
sudo mv cuda-ubuntu1804.pin /etc/apt/preferences.d/cuda-repository-pin-600

if [ ! -e ./cuda-repo-ubuntu1804-10-1-local-10.1.243-418.87.00_1.0-1_amd64.deb ]; then
	wget http://developer.download.nvidia.com/compute/cuda/10.1/Prod/local_installers/cuda-repo-ubuntu1804-10-1-local-10.1.243-418.87.00_1.0-1_amd64.deb
fi

sudo dpkg -i cuda-repo-ubuntu1804-10-1-local-10.1.243-418.87.00_1.0-1_amd64.deb
sudo apt-key add /var/cuda-repo-10-1-local-10.1.243-418.87.00/7fa2af80.pub
sudo apt-get update
sudo apt-get -y install cuda

sudo apt install smbclient
if [ ! -e ./libcudnn7_7.6.5.32-1+cuda10.1_amd64.deb ]; then
	smbget -U=admin smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/Fundamental/libcudnn7_7.6.5.32-1+cuda10.1_amd64.deb
fi

if [ ! -e ./libcudnn7-dev_7.6.5.32-1+cuda10.1_amd64.deb ]; then
	smbget -U=admin smb://filenic/hdd2/各研究グループ/PA10/Env_Setup_deb/Fundamental/libcudnn7-dev_7.6.5.32-1+cuda10.1_amd64.deb
fi

sudo dpkg -i libcudnn7_7.6.5.32-1+cuda10.1_amd64.deb
sudo dpkg -i libcudnn7-dev_7.6.5.32-1+cuda10.1_amd64.deb

