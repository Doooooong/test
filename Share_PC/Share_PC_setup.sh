#!/bin/bash
InstallScriptsPath="`pwd`/../InstallScripts/"
source ../InstallScripts/InstallFunctions.sh

if [ $1 == "-h" ]; then
{
	echo -e "\
	-h --help: Print this.
	-l: Setup PA 10 Left PC.
	-r: Setup PA 10 Right PC.
	"
	exit
}
elif [ $1 == "--help" ]; then
{
	echo -e "\
	-h --help: Print this.
	-l: Setup PA 10 Left PC.
	-r: Setup PA 10 Right PC.
	"
	exit
}
elif [ $1 == "-gripper" ]; then
{
	echo "Setup Share PC with gripper."
	read -p "Enter to continue."
}
elif [ -z $1 ]; then
{
	read -p "Enter to continue setup Share PC."
}
else
{
	echo "Wrong parameters. Input \"-l\" or \"-r\" to specify PCs you want to setup"
	exit
}
fi

download_deb_from_filenic

setup_convenient_utilities
setup_dev_env_packages
Net_setup -share
install_timeshift
timeshift_backup "New System"
install_input_method

install_python37_pip_ubuntu1804

install_librealsense2
install_PCL_base_packages
install_OpenCV_base_packages
#install_PCL_noCUDA
install_PCL_CUDA
#install_OpenCV_noCUDA
install_OpenCV_CUDA

install_openrtm_all

install_visual_code
install_pycharm

install_turboVNC

timeshift_backup "Env setup success"

setup_development_env

timeshift_backup "All setup success"

