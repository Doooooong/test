#!/bin/bash

# arg1: User name
# arg2: User password. Default is set to zutt0issy0
function make_user()
{
	if [[ $2 == '' ]]; then
	{
		echo "== No pass word set."
		password="zutt0issy0"
	}
	else
	{
		password=$2
	}
	fi

	sudo bash -c "echo -e '$password\n$password\n$1\n\n\n\n\n\n' | adduser $1"
	sudo usermod -aG sudo,adm $1

	sudo gpasswd --add `users` dialout
	read -p "You need to restart or re-login your PC before you use serial port."
}

function make_user_dev_env()
{
	echo $1 $2
}

make_user $1 $2
