#!/bin/bash
# /home/dong/Workspace/share/tools/method/Cook-Team.ttt
script_path=`cd \`dirname $0\`; pwd` 

if [ ! -d /usr/bin/vrep ]; then
	echo "vrep NOT exist, make symbollink!"
	sudo ln -s $script_path"/vrep.sh" /usr/bin/vrep
else
	echo "vrep is exist, do nothing!"
fi

if [ ! -d /usr/local/lib/python3.7/dist-packages/vreplib ]; then
	echo "Directory vreplib NOT exist, make symbollink!"
	sudo ln -s $script_path"/programming/remoteApiBindings/python/python" /usr/local/lib/python3.7/dist-packages/vreplib
else
	echo "Directory vreplib is exist, do nothing!"
fi

if [[ ! -e /usr/local/lib/python3.7/dist-packages/vreplib_path.pth ]]; then
	echo "vreplib_path.pth NOT exist, make it, and write path info to this file."
	sudo touch /usr/local/lib/python3.7/dist-packages/vreplib_path.pth
	sudo bash -c "echo /usr/local/lib/python3.7/dist-packages/vreplib >> /usr/local/lib/python3.7/dist-packages/vreplib_path.pth"
else
	echo "vreplib_path.pth is exist, do nothing!"
fi

if [[ ! -e /usr/local/lib/python3.7/dist-packages/vreplib/remoteApi.so ]]; then
	echo "remoteApi.so NOT exist, make it, make symbollink."
	sudo ln -s $script_path"/programming/remoteApiBindings/lib/lib/Linux/64Bit/remoteApi.so" /usr/local/lib/python3.7/dist-packages/vreplib/remoteApi.so
else
	echo "remoteApi.so, do nothing!"
fi

echo "Use python to check the installation, if success, output nothing:"
python3.7 -c "import vrep"
echo "Output finished."

read 

